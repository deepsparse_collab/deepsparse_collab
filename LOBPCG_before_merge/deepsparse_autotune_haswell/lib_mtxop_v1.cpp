#include <iostream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include <algorithm>
#include <sstream>
#include <cstring>
#include <fstream>

using namespace std;

#include <mkl.h>
#include <omp.h>

#include "lib_mtxop_v1.h"
#include "lib_variable_v1.h"


void _XTY(double *X, double *Y, double *result ,int M, int N, int P, int blocksize)
{
    /*
  	Input: X[M * N], Y[M * P]
  	Output: result[N * P]
  	*/

  	int i, j, k, blksz, tid, nthreads;
  	double sum,tstart,tend;
  	#pragma omp parallel shared(nthreads)
  	nthreads = omp_get_num_threads();

  	double *buf = (double *) malloc(nthreads * N * P * sizeof(double)); 
  	
    #pragma omp parallel for default(shared)
  	for(i = 0 ; i < nthreads * N * P; i++)
  	{
   		buf[i] = 0.0;
    }

    #pragma omp parallel for default(shared) private(blksz)
    for(k = 0 ; k < M ; k = k + blocksize)
    {
        blksz = blocksize;
        if(k + blksz>M)
    		blksz = M - k;
        
        tid = omp_get_thread_num();
        cblas_dgemm(CblasRowMajor,CblasTrans,CblasNoTrans,N,P,blksz,1.0,X+(k * N),N,Y+(k * P),P,1.0,buf+(tid * N * P),P);
    }

    #pragma omp parallel for default(shared) private(k, sum, j) 
    for(i = 0 ; i < N ; i++)
    {
        for(k = 0 ; k < P ; k++)
        {
        	sum = 0.0;
            for(j = 0 ; j < nthreads; j++) //for each thread access corresponding N * N matrix
            {
            	sum += buf[j * N * P + i * P + k];
          	}
          	result[i * P + k] = sum;
      	}
    }

  	free(buf);
}

void transpose(double *src, double *dst, const int N, const int M)
{
    int i, j;
    //#pragma omp parallel for private(j) default(shared)
    for(i=0; i<M; i++)
    {
      	for(j=0; j<N; j++)
      	{
        	dst[j*M+i] = src[i*N+j];
    	}
	}
}

void inverse(double *arr, int m, int n)
{
    /*
    input: arr[m*n] in row major format.
    */

	int lda_t = m;
	int lda = n;
	int info;
	int lwork = -1;
	double* work = NULL;
	double work_query;
	int *ipiv = new int[n+1]();

	double *arr_t = new double[m*n]();

	transpose(arr, arr_t, n, m);
	
  dgetrf_( &n, &m, arr_t, &lda_t, ipiv, &info );
	
  if(info < 0)
	{
		cout << "dgetrf_: Transpose error!!" << endl;
	}

    /* Query optimal working array(s) size */
	dgetri_( &m, arr_t, &lda_t, ipiv, &work_query, &lwork, &info );
	
    if(info < 0)
	{
		cout << "dgetri_ 1: Transpose error!!" << endl;
  }
	
    lwork = (int)work_query;
	work = new double[lwork]();
	
  dgetri_( &m, arr_t, &lda_t, ipiv, work, &lwork, &info );
	
    if(info < 0)
	{
		cout << "dgetri_ 2: Transpose error!!" << endl;
	}
  
	transpose(arr_t, arr, m, n);

	delete []arr_t;
	delete []ipiv;
}



void print_mat(double *arr, const int row, const int col) 
{
  cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
    int i, j;
	for(i = 0 ; i < row ; i++)
	{
    //cout << i << " : " ;
		for(j = 0 ; j < col ; j++)
		{
			cout << arr[i * col + j] << " ";
		}
		cout << endl;
	}
}

void make_identity_mat(double *arr, const int row, const int col)
{
	int i, j;
    #pragma omp parallel for private(j) default(shared)
	for(i = 0 ; i < row ; i++)
	{
		for(j = 0 ; j < col ; j++)
		{
			if(i == j)
				arr[i * row + j] = 1.00;
			else
				arr[i * row + j] = 0.00;
		}
	}
}

void diag(double *src, double *dst, const int size)
{
	int i, j;

  	#pragma omp parallel for private(j) default(shared)
	for(i = 0; i < size ; i++)
	{
		for(j = 0 ; j < size ; j++)
		{
			if(i == j)
			{
				dst[i * size + j] = src[i];
			}
			else
				dst[i * size + j] = 0.0;
		}
	}
}

void mat_sub(double *src1, double *src2, double *dst, const int row, const int col)
{
	int i, j;

  	#pragma omp parallel for private(j) default(shared)
	for(i = 0; i < row; i++)
	{
		for(j = 0 ; j < col ; j++)
		{
			dst[i * col + j] = src1[i * col + j] - src2[i * col + j];
		}
	}
}

void mat_addition(double *src1, double *src2, double *dst, const int row, const int col)
{
	int i, j;
  #pragma omp parallel for private(j) default(shared)
	for(i = 0 ; i < row ; i++)
	{
		for(j = 0 ; j < col ; j++)
		{
			dst[i * col + j] = src1[i * col + j] + src2[i * col + j];
		}
	}
}

void mat_mult(double *src1, double *src2, double *dst, const int row, const int col)
{
	int i, j;

  	#pragma omp parallel for private(j) default(shared)
	for(i = 0 ; i < row ; i++)
	{
		for(j = 0 ; j < col ; j++)
		{
			dst[i * col + j] = src1[i * col + j] * src2[i * col + j];
		}
	}
}

void sum_sqrt(double *src, double *dst, const int row, const int col)
{
	int i, j;
  	for(i = 0 ; i < col ; i++) //i->col
  	{
	    #pragma omp parallel for default(shared)
	    for(j = 0 ; j < row ; j++) //j->row
	    {
	    	dst[i] += src[j * col + i];
	    }
	}

  	#pragma omp parallel for default(shared)
  	for(i = 0 ; i < col ; i++) //i->col
  	{
  		dst[i] = sqrt(dst[i]);
  	}
}

void update_activeMask(int *activeMask, double *residualNorms, double residualTolerance, int blocksize)
{
	int i;
  	#pragma omp parallel for
	for(i = 0 ; i < blocksize ; i++)
	{
		if((residualNorms[i] > residualTolerance) && activeMask[i] == 1)
			activeMask[i] = 1;
		else
			activeMask[i] = 0;
	}
}

void getActiveBlockVector(double *activeBlockVectorR, int *activeMask, double *blockVectorR, int M, int blocksize, int currentBlockSize)
{
    int i, j, k=0;
    #pragma omp parallel for firstprivate(k) private(j) default(shared)
    for(i = 0 ; i < M ; i++)
    {
        k = 0;
        for(j = 0 ; j < blocksize ; j++)
        {
           	if(activeMask[j] == 1)
           	{
            	activeBlockVectorR[i * currentBlockSize + k] = blockVectorR[i * blocksize + j];
            	k++;
        	}
    	}
	}
}

void updateBlockVector(double *activeBlockVectorR, int *activeMask, double *blockVectorR, int M, int blocksize, int currentBlockSize)
{

    int i, j, k=0;
    #pragma omp parallel for firstprivate(k) private(j) default(shared)
    for(i = 0 ; i < M ; i++)
    {
        k = 0;
        for(j = 0 ; j < blocksize ; j++)
        {
           	if(activeMask[j] == 1)
           	{
            	blockVectorR[i * blocksize + j]= activeBlockVectorR[i * currentBlockSize + k];
            	k++;
        	}
    	}
	}
}

void mat_copy(double *src,  int row, int col, double *dst, int start_row, int start_col, int ld_dst)
{
    int i, j;
    #pragma omp parallel for private(j) default(shared)
    for(i = 0 ; i < row ; i++)
    {
        for(j = 0 ; j < col ; j++)
        {
            dst[(start_row + i) * ld_dst + (start_col + j)] = src[i * col + j];
        }

    }
}

void custom_dlacpy(double *src, double *dst, int m, int n)
{
    //src[m*n] and dst[m*n]
    int i, j;
    #pragma omp parallel for private(j) default(shared)
    for(i = 0 ; i < m ; i++) //each row
    {
        for(j = 0 ; j < n ; j++) //each column
        {
            dst[i * n + j] = src[i * n + j];
        }
    }
}

void print_eigenvalues( MKL_INT n, double* wr, double* wi ) 
{
    MKL_INT j;

    for( j = 0; j < n; j++ ) 
    {
      	if( wi[j] == (double)0.0 ) 
      	{
       		printf( " %.3f", wr[j] );
   		} 
	   	else 
	   	{
	   		printf( " (%.3f,%6.2f)", wr[j], wi[j] );
	   	}
	}
	printf( "\n" );
}

