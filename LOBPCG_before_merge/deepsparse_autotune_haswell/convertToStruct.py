
import os
import platform
import sys
import glob



def ConvertToStruct(filename):
	index = filename.find('.')
	mianName = filename[:index]; 
	newName = mianName + "_taskinfo" + filename[index:];
	partName = mianName + "_partinfo" + filename[index:];
	#print(newName);

	runBuildStructCommand = "./buildTaskInfoStruct_priority.x " + filename;
	print(runBuildStructCommand);
	#os.system(runBuildStructCommand)
		
	renameCommand = "mv taskinfo.txt " + newName;
	print(renameCommand);  
	#os.system(renameCommand)

	renamePartFile = "mv partinfo.txt " + partName;

	#os.system(renamePartFile)
	print(renamePartFile);

def main():
	if(sys.argv[1] == "non"):
		fileList = glob.glob("msdoor*_nonloop.txt");
	elif(sys.argv[1] == "first"):
		fileList = glob.glob("msdoor*_firstloop.txt");
	elif(sys.argv[1] == "second"):
		fileList = glob.glob("msdoor*_secondloop.txt");

	for i in range(len(fileList)):
		#print(str( "\""+ fileList[i]) + "\",");
		ConvertToStruct(fileList[i]);
	

if __name__ == '__main__':
	main()
