import os
import platform
import sys
import glob

small_block = [8192, 8192, 8192, 8192, 8192, 8192, 8192]
small_block_mimic = ["8k", "8k", "8k", "8k", "8k", "8k", "8k"]
blocksize = 8

def runPartitioner(matName, matpath, smallblock, bigblock, partdir, loop, part, big_block, big_block_mimic):
	#./lobpcg_gen_graph_v29.x a.dot 4096 32 32768 ../Matrices/HV15R.cus 4096
	indx = small_block.index(smallblock)
	big_indx = big_block.index(bigblock)

	if(loop == "nonloop"):
		runPartitionerCommand = "./lobpcg_gen_graph_v29.x a.dot " + str(part) + " " + str(blocksize) + " " + str(bigblock) + " " + str(matpath) + " " +str(smallblock) + " " +str(matName)
	elif(loop == "first"):
		runPartitionerCommand = "./lobpcg_gen_graph_v30.x a.dot " + str(part) + " " + str(blocksize) + " " + str(bigblock) + " " + str(matpath) + " " +str(smallblock) + " " +str(matName)
	elif(loop == "second"):
		runPartitionerCommand = "./lobpcg_gen_graph_v28.x a.dot " + str(part) + " " + str(blocksize) + " " + str(bigblock) + " " + str(matpath) + " " +str(smallblock) + " " +str(matName)
	
	print(runPartitionerCommand);
	os.system(runPartitionerCommand)
	
		
	if(loop == "nonloop"):
		fileName = str(matName) + "_" + str(big_block_mimic[big_indx]) + "_" + str(small_block_mimic[indx]) + "_part_" + str(part) + "_40mb_nonloop.txt"
	elif(loop == "first"):
		fileName = str(matName) + "_" + str(big_block_mimic[big_indx]) + "_" + str(small_block_mimic[indx]) + "_part_" + str(part) + "_40mb_firstloop.txt"
	elif(loop == "second"):
		fileName = str(matName) + "_" + str(big_block_mimic[big_indx]) + "_" + str(small_block_mimic[indx]) + "_part_" + str(part) + "_40mb_secondloop.txt"
	
	
	renameCommand = "mv refined_graph_partition.txt " + fileName;
	print(renameCommand);  
	os.system(renameCommand)

	moveCommand = "mv " + fileName + " " + partdir
	print(moveCommand)
	os.system(moveCommand)
	

def main():
	matpath = ""
	matName = ""

	if(sys.argv[1] == "msdoor"):
		partitionDIR = "/global/cscratch1/sd/afibuzza/deepsparse_autotune_haswell/bigblock_8k/msdoor/"
		matpath = "/global/cscratch1/sd/afibuzza/deepsparse_autotune/msdoor.cus"
		matName = "msdoor"
		part = 4096
		big_block = [16384, 32768, 65536, 131072]
		big_block_mimic = ["16k", "32k", "64k", "128k"]

	elif(sys.argv[1] == "inline1"):
		partitionDIR = "/global/cscratch1/sd/afibuzza/deepsparse_autotune_haswell/bigblock_8k/inline1/"
		matpath = "/global/cscratch1/sd/afibuzza/deepsparse_autotune/inline_1.cus"
		matName = "inline1"
		part = 4096
		big_block = [16384, 32768, 65536, 131072]
		big_block_mimic = ["16k", "32k", "64k", "128k"]

	elif(sys.argv[1] == "HV15R"):
		partitionDIR = "/global/cscratch1/sd/afibuzza/deepsparse_autotune_haswell/bigblock_8k/HV15R/"
		matpath = "/global/cscratch1/sd/afibuzza/deepsparse_autotune/HV15R.cus"
		matName = "HV15R"
		part = 4096
		big_block = [16384, 32768, 65536, 131072]
		big_block_mimic = ["16k", "32k", "64k", "128k"]

	elif(sys.argv[1] == "Nm7"):
		partitionDIR = "/global/cscratch1/sd/afibuzza/deepsparse_autotune_haswell/bigblock_8k/Nm7/"
		matpath = "/global/cscratch1/sd/afibuzza/deepsparse_autotune/Nm7-original.cus"
		matName = "Nm7"
		part = 2048
		big_block = [ 32768, 65536, 131072]
		big_block_mimic = [ "32k", "64k", "128k"]
	
		
	loop = ["non", "first", "second"]
	#loop = ["first", "second"]
	
	for i in range(0, len(big_block)):
		print(str(small_block[i]));
		for j in range(len(loop)):
			if(loop[j] == "non"):
				runPartitioner(matName, matpath, small_block[i], big_block[i], partitionDIR, "nonloop", part, big_block, big_block_mimic)
			elif(loop[j] == "first"):
				runPartitioner(matName, matpath, small_block[i], big_block[i], partitionDIR, "first", part, big_block, big_block_mimic)
			elif(loop[j] == "second"):
				runPartitioner(matName, matpath, small_block[i], big_block[i], partitionDIR, "second", part, big_block, big_block_mimic)
			os.system("rm *.txt")
		

if __name__ == '__main__':
	main()
