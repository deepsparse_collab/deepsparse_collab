#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dgraph.h"
#include "utils.h"
#include <limits.h>
#include <math.h>
#include "bmmio.h"
#include "rMLGP.h"
//#include "../../lobpcg_gen_graph_v28.h"
//#include "../../lobpcg_gen_graph_v29.h"
//#include "../../lib_dag.h"

#define KEYVAL(i) (G->inEnd[i]-G->inStart[i] - G->outEnd[i] + G->outStart[i] )

 char** task_output_name;
 double* task_output_amount;

int calcLiveSizeMixMulti(dgraph *G, int rep){
    int liveset = INT_MAX;
    int i,l;
    for (i=0; i < rep; i++){
        l = calcLiveSizeMix(G);
        liveset = liveset < l ? liveset : l;
    }

    return liveset;
}

int calcLiveSizeMix(dgraph *G){
    idxType* DFStop = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 2));
    DFStopsort(G, DFStop);
    int DFSlive = calcLiveSize(G, DFStop);

    idxType* randDFStop = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 2));
    randDFStopsort(G, randDFStop);
    int randDFSlive = calcLiveSize(G, randDFStop);

    idxType* BFStop = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 2));
    BFStopsort(G, BFStop);
    int BFSlive = calcLiveSize(G, BFStop);

    idxType* randBFStop = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 2));
    randBFStopsort(G, randBFStop);
    int randBFSlive = calcLiveSize(G, randBFStop);

    idxType* mixtop = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 2));
    mixtopsort(G, mixtop, 50, 1);
    int mixlive = calcLiveSize(G, mixtop);

    int liveset = INT_MAX;
    liveset = liveset < DFSlive ? liveset : DFSlive;
    liveset = liveset < randDFSlive ? liveset : randDFSlive;
    liveset = liveset < BFSlive ? liveset : BFSlive;
    liveset = liveset < randBFSlive ? liveset : randBFSlive;
    liveset = liveset < mixlive ? liveset : mixlive;
    free(DFStop);
    free(randDFStop);
    free(BFStop);
    free(randBFStop);
    free(mixtop);

    return liveset;
}

int calcLiveSizeMix_with_part_Multi(dgraph *G, idxType* part, int nbpart, idxType* toporder, int rep){
    int liveset = INT_MAX;
    int i,l;
    for (i=0; i < rep; i++){
        l = calcLiveSizeMix_with_part(G, part, nbpart, toporder);
        liveset = liveset < l ? liveset : l;
    }

    return liveset;
}

int calcLiveSizeMix_with_part(dgraph *G, idxType* part, int nbpart, idxType* toporder){
    idxType i;

    idxType* DFStop = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 2));
    DFStopsort_with_part(G, part, nbpart, DFStop);
    int DFSlive = calcLiveSize(G, DFStop);

    idxType* randDFStop = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 2));
    randDFStopsort_with_part(G, part, nbpart, randDFStop);
    int randDFSlive = calcLiveSize(G, randDFStop);

    idxType* BFStop = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 2));
    BFStopsort_with_part(G, part, nbpart, BFStop);
    int BFSlive = calcLiveSize(G, BFStop);

    idxType* randBFStop = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 2));
    randBFStopsort_with_part(G, part, nbpart, randBFStop);
    int randBFSlive = calcLiveSize(G, randBFStop);

    int liveset = INT_MAX;
    if (DFSlive < liveset){
        liveset = DFSlive;
        for (i=1; i <= G->nVrtx; i++)
            toporder[i] = DFStop[i];
    }
    if (randDFSlive < liveset){
        liveset = randDFSlive;
        for (i=1; i <= G->nVrtx; i++)
            toporder[i] = randDFStop[i];
    }
    if (BFSlive < liveset){
        liveset = BFSlive;
        for (i=1; i <= G->nVrtx; i++)
            toporder[i] = BFStop[i];
    }
    if (randBFSlive < liveset){
        liveset = randBFSlive;
        for (i=1; i <= G->nVrtx; i++)
            toporder[i] = randBFStop[i];
    }
    free(DFStop);
    free(randDFStop);
    free(BFStop);
    free(randBFStop);

    return liveset;
}


int calcLiveSize(dgraph *g,idxType* toporder){
    idxType* incoming = (idxType*) calloc(sizeof(idxType),(g->nVrtx + 2));
    idxType* outgoing = (idxType*) calloc(sizeof(idxType),(g->nVrtx + 2));
    int i=0,j=0;
    int r=0,rend=0;
    int looper=0,lend=0;
    int maxLiveSize=0;
    int liveSize=0;

  /*  printf("inEdges\n");
    for(i=0;i<g->nEdge;++i)
        printf("%d ",g->in[i]);
    printf("\n");
    printf("outEdges\n");
    for(i=0;i<g->nEdge;++i)
        printf("%d ",g->out[i]);
    
    printf("\ninEdgeRanges\n");
    for(i=1;i<=g->nVrtx;++i)
        printf("%d:: %d to %d\n",i,g->inStart[i],g->inEnd[i]);
    printf("\noutEdgeRanges\n");
    for(i=1;i<=g->nVrtx;++i)
        printf("%d:: %d to %d\n",i,g->outStart[i],g->outEnd[i]);
    printf("toporder\n");
    for(i=1;i<=g->nVrtx;++i){
        printf("%d, ",toporder[i] );
    }
    printf("\n");
*/
    //printf("start\n");
    j=1;
    int no = 1;
    while(j<=g->nVrtx){
        i = toporder[j];
        if(g->hollow[i]){
            //printf("%d is hollow \n",i);
            no=1;
            if(g->inEnd[i]<g->inStart[i]) // no in edge
                no=0; // don't skip
            /*
            for (k=g->inStart[i];k<=g->inEnd[i];++k){
                if(g->hollow[g->in[k]]==0){ // out node, skip
                    no=1;
                    break;
                }
            }
            */
            if(no){ // skip
                //printf("i %d no\n",i);
                ++j;
                continue;
            }
        }
        ++liveSize;
        //printf("%d is alive now\n", i);
        if(liveSize>maxLiveSize){
            maxLiveSize=liveSize;
            //printf("maxLiveSize updated to %d\n", maxLiveSize);
        }
        r = g->inStart[i];
        rend = g->inEnd[i];
        while(r <= rend){
            ++outgoing[g->in[r]];
            //printf("increasing out of %d %d\n",g->in[r], outgoing[g->in[r]]);
            if(outgoing[g->in[r]]>=g->outEnd[g->in[r]]-g->outStart[g->in[r]]+1){
                //printf("%d dies\n", g->in[r] );
                --liveSize;
            }
            else{
                //printf(" %d still alive %d --  %d\n", g->in[r],outgoing[g->in[r]],g->outEnd[g->in[r]]-g->outStart[g->in[r]]+1);
            }
            ++r;
        }
        looper = g->outStart[i];
        lend = g->outEnd[i];
        if (lend<looper){
            //printf("no child, %d dies, too\n",i);
            --liveSize;
        }
        while(looper<=lend){
            ++incoming[g->out[looper]];
            ++looper;
        }
        ++j;
    }

    //printf("end\n");
    free(incoming);
    free(outgoing);
    return maxLiveSize;
}

int calcLiveSizePartMixMulti(dgraph *G, idxType* part, idxType part_idx, int rep){
    int liveset = INT_MAX;
    int i,l;
    for (i=0; i < rep; i++){
        l = calcLiveSizePartMix(G, part, part_idx);
        liveset = liveset < l ? liveset : l;
    }

    return liveset;
}

int calcLiveSizePartMix(dgraph *G, idxType* part, idxType part_idx){
    idxType* DFStop = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 2));
    DFStopsort(G, DFStop);
    int DFSlive = calcLiveSizePart(G, DFStop, part, part_idx);

    idxType* randDFStop = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 2));
    randDFStopsort(G, randDFStop);
    int randDFSlive = calcLiveSizePart(G, randDFStop, part, part_idx);

    idxType* BFStop = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 2));
    BFStopsort(G, BFStop);
    int BFSlive = calcLiveSizePart(G, BFStop, part, part_idx);

    idxType* randBFStop = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 2));
    randBFStopsort(G, randBFStop);
    int randBFSlive = calcLiveSizePart(G, randBFStop, part, part_idx);

    idxType* mixtop = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 2));
    mixtopsort(G, mixtop, 50, 1);
    int mixlive = calcLiveSizePart(G, mixtop, part, part_idx);

    int liveset = INT_MAX;
    liveset = liveset < DFSlive ? liveset : DFSlive;
    liveset = liveset < randDFSlive ? liveset : randDFSlive;
    liveset = liveset < BFSlive ? liveset : BFSlive;
    liveset = liveset < randBFSlive ? liveset : randBFSlive;
    liveset = liveset < mixlive ? liveset : mixlive;
    free(DFStop);
    free(randDFStop);
    free(BFStop);
    free(randBFStop);
    free(mixtop);

    return liveset;
}

int calcLiveSizePart(dgraph *g, idxType* toporder, idxType* part, idxType part_idx){
    idxType* incoming = (idxType*) calloc(sizeof(idxType),(g->nVrtx + 2));
    idxType* outgoing = (idxType*) calloc(sizeof(idxType),(g->nVrtx + 2));
    int i=0,j=0;
    int r=0,rend=0;
    int looper=0,lend=0;
    int maxLiveSize=0;
    int liveSize=0;
    /*
    printf("inEdges\n");
    for(i=0;i<g->nEdge;++i)
        printf("%d ",g->in[i]);
    printf("\n");
    printf("outEdges\n");
    for(i=0;i<g->nEdge;++i)
        printf("%d ",g->out[i]);

    printf("\ninEdgeRanges\n");
    for(i=1;i<=g->nVrtx;++i)
        printf("%d:: %d to %d\n",i,g->inStart[i],g->inEnd[i]);
    printf("\noutEdgeRanges\n");
    for(i=1;i<=g->nVrtx;++i)
        printf("%d:: %d to %d\n",i,g->outStart[i],g->outEnd[i]);
    printf("toporder\n");
    for(i=1;i<=g->nVrtx;++i){
        printf("%d, ",toporder[i] );
    }
    printf("\n");
    */
    //printf("start\n");
    j=1;
    int no = 1;
    while(j<=g->nVrtx){
        i = toporder[j];
        //printf("Node %d\n", (int) i);
        if(g->hollow[i]){
            //printf("%d is hollow \n",i);
            no=1;
            if(g->inEnd[i]<g->inStart[i]) // no in edge
                no=0; // don't skip
            /*
            for (k=g->inStart[i];k<=g->inEnd[i];++k){
                if(g->hollow[g->in[k]]==0){ // out node, skip
                    no=1;
                    break;
                }
            }
            */
            if(no){ // skip
                //printf("i %d no\n",i);
                ++j;
                continue;
            }
        }
        int no_in_part = 1;
        for (r = g->inStart[i]; r <= g->inEnd[i]; r++)
            if (part[g->in[r]] == part_idx)
                no_in_part = 0;
        int no_out_part = 1;
        for (r = g->outStart[i]; r <= g->outEnd[i]; r++)
            if (part[g->out[r]] == part_idx)
                no_out_part = 0;
        if ((part[i] != part_idx)&&no_out_part&&no_in_part) {
            ++j;
            continue;
        }
        ++liveSize;
        //printf("%d is alive now\n", i);
        if(liveSize>maxLiveSize){
            maxLiveSize=liveSize;
            //printf("maxLiveSize updated to %d\n", maxLiveSize);
        }
        r = g->inStart[i];
        rend = g->inEnd[i];
        while(r <= rend){
            ++outgoing[g->in[r]];
            //printf("increasing out of %d %d\n",g->in[r], outgoing[g->in[r]]);
            if(outgoing[g->in[r]]>=g->outEnd[g->in[r]]-g->outStart[g->in[r]]+1){
                //printf("%d dies\n", g->in[r] );
                --liveSize;
            }
            else{
                //printf(" %d still alive %d --  %d\n", g->in[r],outgoing[g->in[r]],g->outEnd[g->in[r]]-g->outStart[g->in[r]]+1);
            }
            ++r;
        }
        looper = g->outStart[i];
        lend = g->outEnd[i];
        if (lend<looper){
            //printf("no child, %d dies, too\n", (int) i);
            --liveSize;
        }
        while(looper<=lend){
            ++incoming[g->out[looper]];
            ++looper;
            //printf("looper %d\n", (int) looper);

        }
        ++j;
    }

    //printf("end\n");
    free(incoming);
    free(outgoing);
    return maxLiveSize;
}


void maxHeapInsert(dgraph *G, idxType *heapIds, idxType *sz, idxType i)
{  
    idxType j, kv = KEYVAL(i);
    *sz = *sz+1;
    heapIds[*sz] = i;
    j = *sz;
    
    while(j > 1 && KEYVAL(heapIds[j/2]) < kv)
    {
        heapIds[j] = heapIds[j/2] ;
        j = j/2;
    }
    heapIds[j] = i;    
}


void maxHeapify(dgraph *G, idxType *heapIds, idxType sz, idxType i)
{
    idxType largest, j, l,r, tmp;
    
    largest = j = i;
    while(j<=sz/2)
    {
        l = 2*j;
        r = 2*j + 1;
        
        if(l<=sz && KEYVAL(heapIds[l])>KEYVAL(heapIds[j]))
            largest = l;
        else
            largest = j;
        
        if(r<=sz && KEYVAL(heapIds[r])>KEYVAL(heapIds[largest]))
            largest = r;
        
        if(largest != j)
        {
            tmp = heapIds[largest];
            heapIds[largest] = heapIds[j];
            heapIds[j] = tmp;
            j = largest;
        }
        else
            break;
    }
}

idxType maxHeapExtract(dgraph *G, idxType *heapIds, idxType *sz)
{
    idxType maxind ;
    if( *sz < 1)
        u_errexit("heap underflow\n");
    
    maxind = heapIds[1];
    heapIds[1] = heapIds[*sz];
    *sz = *sz - 1;
    maxHeapify(G, heapIds, *sz, 1);
    return maxind;
    
}

void maxBuildHeap(dgraph *G, idxType *heapIds, idxType sz)
{
    idxType i;
    for (i=sz/2; i>=1; i--)
        maxHeapify(G, heapIds, sz, i);
}


void allocateDGraphData(dgraph *G, idxType nVrtx, idxType nEdge, int frmt)
{
    /*assumes *G is allocated*/
    G->frmt = frmt;
    G->nVrtx = nVrtx;
    G->nEdge = nEdge;

    G->ecIn = G->ecOut = NULL;
    G->vw = NULL;

    if (nEdge <= 0)
        u_errexit("allocateDGraphData: empty edge set\n");
    
    if (nVrtx <=-1)
        u_errexit("allocateDGraphData: empty vertex set\n");
    
    G->inStart  = (idxType * ) malloc(sizeof(idxType) * (nVrtx + 2));
    G->inEnd= (idxType * ) malloc(sizeof(idxType) * (nVrtx + 2));
    G->in = (idxType * ) malloc(sizeof(idxType) * nEdge);
    G->hollow = (idxType *) malloc(sizeof(idxType) * (nVrtx+2));
    G->outStart  = (idxType * ) malloc(sizeof(idxType) * (nVrtx + 2));
    G->outEnd  = (idxType * ) malloc(sizeof(idxType) * (nVrtx + 2));
    G->out = (idxType * ) malloc(sizeof(idxType) * nEdge);

    
    if (frmt == 1 || frmt == 3)
        G->vw = (idxType *) malloc(sizeof(idxType) * (nVrtx+1));

    if (frmt == 2 || frmt == 3)
    {
        G->ecIn = (ecType *) malloc(sizeof(ecType) * nEdge);
        G->ecOut = (ecType *) malloc(sizeof(ecType) * nEdge);
    }
    G->maxVW = G->maxEC = -1;
}
/*******************************************************************************/
void freeDGraphData(dgraph *G)
{
    free(G->inStart);
    free(G->inEnd);
    free(G->in);
    free(G->outStart);
    free(G->outEnd);
    free(G->out);
    free(G->hollow);
    
    if (G->frmt == 1 || G->frmt == 3)
    {
        if(!G->vw)
            u_errexit("free a graph with frmt %d, but vw is not allocated", G->frmt);
        else
            free(G->vw);
    }
    
    if (G->frmt == 2 || G->frmt == 3)
    {
        if(!G->ecIn){
            u_errexit("free a graph with frmt %d, but ecIn is not allocated", G->frmt);
        }
        else
            free(G->ecIn);
        
        if(!G->ecOut)
            u_errexit("free a graph with frmt %d, but ecOut is not allocated", G->frmt);
        else
            free(G->ecOut);
    }
    
    G->frmt = -1;
    G->nVrtx = 0;
    G->nEdge = 0;
    G->maxVW = G->maxEC = -1;
    
    G->out = G->outEnd = G->outStart = G->in = G->inEnd = G->inStart = NULL;
    
    G->ecIn = G->ecOut = NULL;
    G->vw = NULL;
    //free(G);
}
/*******************************************************************************/
void dgraph_info(dgraph* G, int* maxindegree, int* minindegree, double* aveindegree, int* maxoutdegree, int* minoutdegree, double* aveoutdegree)
{
    idxType i;
    int indegree, outdegree;
    *aveindegree = 0.0;
    *aveoutdegree = 0.0;
    *maxindegree = 0;
    *minindegree = 0;
    *maxoutdegree = 0;
    *minoutdegree = 0;
    for (i=1; i<=G->nVrtx; i++){
	indegree = G->inEnd[i]-G->inStart[i]+1;
	outdegree = G->outEnd[i]-G->outStart[i]+1;
	if (indegree > *maxindegree)
	    *maxindegree = indegree;
	if (indegree < *minindegree)
	    *minindegree = indegree;
	*aveindegree += indegree;
	if (outdegree > *maxoutdegree)
	    *maxoutdegree = outdegree;
	if (outdegree < *minoutdegree)
	    *minoutdegree = outdegree;
	*aveoutdegree += outdegree;
    }
    *aveindegree = *aveindegree / G->nVrtx;
    *aveoutdegree = *aveoutdegree / G->nVrtx;
}

void set_dgraph_info(dgraph* G)
{
    idxType i,j;
    
    double totvw = 0.0;
    double totec = 0.0;
    idxType maxindegree = 0;
    idxType maxoutdegree = 0;
    vwType maxVW = 0;
    ecType maxEC = 0;

    
      for (i=1; i<=G->nVrtx; i++) {
          if(maxindegree < G->inEnd[i] - G->inStart[i]+1)
              maxindegree = G->inEnd[i] - G->inStart[i]+1;

          totvw += G->vw[i];
          if (maxVW < G->vw[i]) maxVW = G->vw[i];
              
          for (j=G->inStart[i]; j<=G->inEnd[i]; j++) {
              totec += G->ecIn[j];
              if(maxEC < G->ecIn[j]) maxEC = G->ecIn[j];
              
          }
          if(maxoutdegree < G->outEnd[i] - G->outStart[i]+1)
              maxoutdegree = G->outEnd[i] - G->outStart[i]+1;
          
      }
    G->totvw = totvw;
    G->totec = totec;
    G->maxindegree = maxindegree;
    G->maxoutdegree = maxoutdegree;
    G->maxVW = maxVW;
    G->maxEC = maxEC;
}
typedef struct{
    idxType ngh;
    ecType cst;
}sortData;

int dgqsortfnct(const void *i1, const void *i2)
{
    idxType ii1=((sortData*)i1)->ngh, ii2 = ((sortData*)i2)->ngh;
    if( ii1 < ii2) return -1;
    else if(ii1 > ii2) return 1;
    else return 0;
}

/*******************************************************************************/
void sortNeighborLists(dgraph *G)
{
    /*sorts in and out in the increasing order of neighbor indices*/
    idxType i, j, at, nVrtx = G->nVrtx;
    idxType *out=G->out, *outStart = G->outStart, *outEnd = G->outEnd;
    idxType *in=G->in, *inStart = G->inStart, *inEnd = G->inEnd;
    ecType *ecIn=G->ecIn, *ecOut = G->ecOut;
    
    sortData *sd = (sortData*) malloc(nVrtx * sizeof(sortData));
    
    /*sort in*/
    for (j=1; j <= nVrtx; j++)
    {
        at = 0;
        for (i = inStart[j]; i <= inEnd[j]; i++)
        {
            sd[at].ngh = in[i];
            if(G->frmt == 2 || G->frmt == 3)
                sd[at].cst = ecIn[i];
            at ++;
        }
        qsort(sd, inEnd[j]-inStart[j]+1, sizeof(sortData), dgqsortfnct);
        at = 0;
        for (i = inStart[j]; i <= inEnd[j]; i++)
        {
            in[i] =  sd[at].ngh ;
            if(G->frmt == 2 || G->frmt == 3)
                ecIn[i] = sd[at].cst ;
            at ++;
        }
    }
    
    /*sort out*/
    for (j=1; j <= nVrtx; j++)
    {
        at = 0;
        for (i = outStart[j]; i <= outEnd[j]; i++)
        {
            sd[at].ngh = out[i];
            if(G->frmt == 2 || G->frmt == 3)
                sd[at].cst = ecOut[i];
            at ++;
        }
        qsort(sd, outEnd[j]-outStart[j]+1, sizeof(sortData), dgqsortfnct);
        at = 0;
        for (i = outStart[j]; i <= outEnd[j]; i++)
        {
            out[i] =  sd[at].ngh ;
            if(G->frmt == 2 || G->frmt == 3)
                ecOut[i] = sd[at].cst ;
            at ++;
        }
    }
    
    free(sd);
}
/*******************************************************************************/
void fillOutFromIn(dgraph *G)
{
    idxType i, at, j, nVrtx = G->nVrtx;
    idxType *out=G->out, *outStart = G->outStart, *outEnd=G->outEnd;
    idxType *in=G->in, *inStart = G->inStart, *inEnd = G->inEnd;
    ecType *ecIn=G->ecIn, *ecOut = G->ecOut;
    
    //printf("in filloutformin vertex = %d edges = %d\n",nVrtx,G->nEdge);




    for (i = 0; i <= G->nVrtx+1; i++)
        outStart[i] = 0;
    
    /*count*/
    for (j = 1; j<= nVrtx; j++)
    {
        for (i = inStart[j]; i <= inEnd[j]; i++)
	    {
            if( i<0 || i>=G->nEdge){
                //printf("buggg %d's in edge index %d\n",j, i);
            }
		idxType tmp = G->in[i];
		//printf("tmp = %d\n", (int) tmp);
		outStart[tmp]++;
	    }
    }


    /*prefix sum*/
    for (j = 1; j<= nVrtx+1; j++){
        outStart[j] += outStart[j-1];
    }
    
 //   for (i = 0; i <= G->nVrtx+1; i++)
  //      printf("outstart[%d] = %d\n",i,outStart[i]);
 //   for (i = 0; i <= G->nVrtx+1; i++)
   //         printf("instart[%d] = %d\n",i,inStart[i]);

    /*write*/
    for (j = 1; j<= nVrtx; j++)
    {
        for (i = inStart[j]; i <= inEnd[j]; i++)
        {
        	//printf("j = %d outstart[in[%d]]  = %d\n",j,i,outStart[in[i]]);
            at =  --outStart[in[i]];
            out[ at ] = j;

   //         printf("out[%d] = %d\n",at,out[at]);
            if(G->frmt==2 || G->frmt == 3){
                ecOut[at] = ecIn[i];
   //             printf("ecout[%d] = ecin[%d] = %lf\n",at,i,ecOut[at]);
            }
        }
    }
    G->maxoutdegree = 0;
    for (j = 1; j<= nVrtx; j++){
        outEnd[j] = outStart[j+1]-1;
	    idxType degree = outEnd[j] - outStart[j] + 1;
	    G->maxoutdegree = G->maxoutdegree < degree ? degree : G->maxoutdegree;
    }
    
    if(inStart[1] != 0)
        u_errexit("fillOutFromIn: the first index not right");
    
    if(inStart[nVrtx+1] != G->nEdge)
        u_errexit("fillOutFromIn: the last index not right");
}


/*******************************************************************************/
void loadFromCSC(dgraph *G, idxType nVrtx, idxType nEdge, int frmt, idxType *col_ptrs, idxType *row_inds, ecType *vals)
{
    /* assumes G's data allocated and G->nEdge > nEdge.
     
     * at the end we set G->nEdge to the exact number of edges, where the lower traingular, including the diagonal, entries are discarded from the CSC structure.
     
     * the edgeCosts (in vals) are mode positive, if any.
     
     * (i,j) is an edge from i to j. Therefore the col_ptrs[i] and row_inds[colptrs[i] to colptrs[i+1]-1] define incoming edges of i.
     */
    idxType i, at, j;
        printf ("\n---->%d %d %d %d\n", G->nVrtx, nVrtx, G->nEdge, nEdge);
    
    if(G->nVrtx < nVrtx || G->nEdge < nEdge){
        printf ("\n---->%d %d %d %d\n", G->nVrtx, nVrtx, G->nEdge, nEdge);
        u_errexit("loadFromCSC: potentially not enough space in G");
    }
    
    G->nVrtx = nVrtx;
    G->inStart[0] = 0;
    G->inStart[1] = at = 0;
    long upctr=0,lowctr=0;
    /*inStart[i]  is already set*/
    for (j = 1; j<=nVrtx; j++)
    {
        for (i = col_ptrs[j]; i < col_ptrs[j+1]; i++)
        {
            if(row_inds[i] < j)
            {
                ++upctr;
            }
            if(row_inds[i]>j){
                ++lowctr;
            }
        }
    }
    if (upctr>=lowctr){
        /*inStart[i]  is already set*/
        for (j = 1; j<=nVrtx; j++) {
            for (i = col_ptrs[j]; i < col_ptrs[j+1]; i++) {
                if(row_inds[i] < j) {
                    G->in[at++] = row_inds[i];
                    if ( (G->frmt == 2 || G->frmt == 3) && (vals))
                        G->ecIn[at-1] = vals[i] >=0 ? vals[i]: -vals[i];
                    else
                        G->ecIn[at-1] = 1;
                }
            }
            G->inEnd[j] = at-1;
            G->inStart[j+1] = at;
        }
    }
    else{
        /*inStart[i]  is already set*/
        for (j = 1; j<=nVrtx; j++) {
            for (i = col_ptrs[j]; i < col_ptrs[j+1]; i++) {
                if(row_inds[i] > j) {
                    G->in[at++] = row_inds[i];
                    if ( (G->frmt == 2 || G->frmt == 3) && (vals))
                        G->ecIn[at-1] = vals[i] >=0 ? vals[i]: -vals[i];
                    else
                        G->ecIn[at-1] = 1;
                }
            }
            G->inEnd[j] = at-1;
            G->inStart[j+1] = at;
        }
    }
    G->nEdge = at;
    fillOutFromIn(G);
    sortNeighborLists(G);
    
}
/*******************************************************************************/
void setVertexWeights(dgraph *G, vwType *vw)
{
    /*assuming n weights in vw*/
    idxType i, nVrtx = G->nVrtx;
    G->totvw = 0.0;
    G->maxVW = -1;
    
    if (G->frmt == 2 || G->frmt == 0)
    {
        if(G->vw == NULL)
            G->vw = (vwType * ) malloc(sizeof(vwType) * (nVrtx+1));
        	//G->vw = (ecType * ) malloc(sizeof(ecType) * (nVrtx+1));
        G->frmt +=1;/*if ec, then ec+vw, if no w no cost, then vw*/
    }
    for (i = 1; i <= nVrtx; i++)
    {
        G->vw[i] = vw[i-1];
        G->totvw +=vw[i-1];
        G->maxVW = G->maxVW < vw[i-1] ? vw[i-1] : G->maxVW;
    }
    
}
/*******************************************************************************/
void checkTopsort(dgraph *G, idxType *toporder)
{
    idxType i, j, neigh, nVrtx = G->nVrtx;
    //idxType  *inEnd = G->inEnd,  *inStart = G->inStart;
    idxType *outEnd = G->outEnd, *outStart = G->outStart, *out = G->out;
    idxType *topindex = (idxType *) malloc(sizeof(idxType)* (nVrtx+1));
    
    for (i = 1; i <= nVrtx; i++)
        topindex[toporder[i]] = i;
    
    for (i = 1; i <= nVrtx; i ++)
    {
        for ( j = outStart[i]; j<= outEnd[i]; j++)
        {
            neigh = out[j];
            if(topindex[i] >= topindex[neigh])
                u_errexit("The topological order is not correct %.0f %.0f\n", 1.0*topindex[i], 1.0*topindex[neigh]);
        }
    }
    free(topindex);
}

void topSortOnParts(dgraph *G, idxType *part, idxType *toporder, idxType nbpart){
    /*Fill toporder assuming it's already allocated*/
    idxType i, j, k;
    idxType** outpart = (idxType**) malloc(sizeof(idxType*)*nbpart);
    idxType* nbout = (idxType*) malloc(sizeof(idxType)*nbpart);
    idxType** inpart = (idxType**) malloc(sizeof(idxType*)*nbpart);
    idxType* nbin = (idxType*) malloc(sizeof(idxType)*nbpart);
    for (i=0; i<nbpart; i++){
	    outpart[i] = (idxType*) malloc(sizeof(idxType)*nbpart);
	    nbout[i] = 0;
	    inpart[i] = (idxType*) malloc(sizeof(idxType)*nbpart);
	    nbin[i] = 0;
    }

    // for (i = 1 ; i <= G->nVrtx ; i++){
    //     printf("%s %d\n",G->vertices[i-1],part[i]);
    // }
    for (i=1; i<=G->nVrtx; i++){
	    for (j=G->outStart[i]; j<=G->outEnd[i]; j++){


	        idxType outnode = G->out[j];
            //printf("%s(%d) --> %s(%d)\n", G->vertices[i-1],part[i],G->vertices[outnode-1],part[outnode]);
	        if (part[i] == part[outnode])
		        continue;
	        int is_new = 1;
	        for (k=0; k<nbout[part[i]]; k++)
		        if (outpart[part[i]][k] == part[outnode]){

		            is_new = 0;
		            break;
		        }
	        if (is_new == 1){
		        outpart[part[i]][nbout[part[i]]] = part[outnode];
		        nbout[part[i]]++;
            }
	        is_new = 1;
	        //printf("i = %d, nVrtx = %d, nbpart = %d, outnode = %d, part[outnode] = %d\n", i, G->nVrtx, nbpart, outnode, part[outnode]);
	        //printf("nbin[%d] = %d\n", part[outnode], nbin[part[outnode]]);
	        for (k=0; k<nbin[part[outnode]]; k++)
		        if (inpart[part[outnode]][k] == part[i]){
		            is_new = 0;
		            break;
		        }
	        if (is_new == 1){
		        inpart[part[outnode]][nbin[part[outnode]]] = part[i];
		        nbin[part[outnode]]++;
	        }
	    }
    }
    idxType* ready = (idxType*) malloc(sizeof(idxType)*(nbpart));
    idxType* nbinleft = (idxType*) malloc(sizeof(idxType)*(nbpart));
    int nbready = 0;
    for (i=0; i<nbpart; i++) {
        nbinleft[i] = nbin[i];
        if (nbin[i] == 0)
            ready[nbready++] = i;
    }
    
    int to = 0;
        printf("In topsortpart, nbready = %d\n", nbready);
    while (nbready > 0) {
        idxType part = ready[nbready-1];
        nbready--;
        toporder[to++] = part;
	//printf("For topsortpart, nbready = %d, to = %d, part = %d\n", nbready, to, part);
        for (i = 0; i < nbout[part]; i++) {
	        idxType succ = outpart[part][i];
            nbinleft[succ]--;
            if (nbinleft[succ] == 0)
                ready[nbready++] = succ;
        }
    }

    //print topsort of parts

   //  //FILE* topsort_part  = fopen("topsort_of_parts.txt","w");
   // // FILE* part_in = fopen("partin.txt","w");
   // // FILE* part_out = fopen("partout.txt","w");
   //  for(i=0;i<nbpart;i++){
   //      //printf( "%d\n", toporder[i]);
   //      //fprintf(topsort_part, "%d\n", toporder[i]);
   //  }
   //  for(i = 0; i<nbpart ; i++){
   //      //fprintf(part_in, "%d <--- ", i);

   //      for(j = 0 ; j < nbin[i] ; j++){
   //          //fprintf(part_in, "%d ", inpart[i][j]);
   //      }
   //      //fprintf(part_in, "\n" );
   //  }



   //  fclose(topsort_part);
   //  fclose(part_in);
   //  fclose(part_out);


    if (to != nbpart){
        u_errexit("In topsortPart, not all sort: to = %d, nbpart = %d\n", to, nbpart);
    }
    for (i=0; i<nbpart; i++){
	    free(outpart[i]);
	    free(inpart[i]);
    }
    free(ready);
    free(nbinleft);
    free(nbin);
    free(nbout);
    free(inpart);
    free(outpart);
}

void randTopSortOnParts(dgraph *G, idxType *part, idxType *toporder, idxType nbpart){
    /*Fill toporder assuming it's already allocated*/
    idxType i, j, k, ip;
    idxType** outpart = (idxType**) malloc(sizeof(idxType*)*nbpart);
    idxType* nbout = (idxType*) malloc(sizeof(idxType)*nbpart);
    idxType** inpart = (idxType**) malloc(sizeof(idxType*)*nbpart);
    idxType* nbin = (idxType*) malloc(sizeof(idxType)*nbpart);
    for (i=0; i<nbpart; i++){
        outpart[i] = (idxType*) malloc(sizeof(idxType)*nbpart);
        nbout[i] = 0;
        inpart[i] = (idxType*) malloc(sizeof(idxType)*nbpart);
        nbin[i] = 0;
    }
    for (i=1; i<=G->nVrtx; i++){
        for (j=G->outStart[i]; j<=G->outEnd[i]; j++){
            idxType outnode = G->out[j];
            if (part[i] == part[outnode])
                continue;
            int is_new = 1;
            for (k=0; k<nbout[part[i]]; k++)
                if (outpart[part[i]][k] == part[outnode]){
                    is_new = 0;
                    break;
                }
            if (is_new == 1){
                outpart[part[i]][nbout[part[i]]] = part[outnode];
                nbout[part[i]]++;
            }
            is_new = 1;
            //printf("i = %d, nVrtx = %d, nbpart = %d, outnode = %d, part[outnode] = %d\n", i, G->nVrtx, nbpart, outnode, part[outnode]);
            //printf("nbin[part[outnode]] = %d\n", nbin[part[outnode]]);
            for (k=0; k<nbin[part[outnode]]; k++)
                if (inpart[part[outnode]][k] == part[i]){
                    is_new = 0;
                    break;
            }
            if (is_new == 1){
                inpart[part[outnode]][nbin[part[outnode]]] = part[i];
                nbin[part[outnode]]++;
            }
        }
    }
    
    idxType* ready = (idxType*) malloc(sizeof(idxType)*(nbpart));
    idxType* nbinleft = (idxType*) malloc(sizeof(idxType)*(nbpart));
    int nbready = 0;
    for (i=0; i<nbpart; i++) {
        nbinleft[i] = nbin[i];
        if (nbin[i] == 0)
            ready[nbready++] = i;
    }

    
    int to = 0;
    idxType* shuffle = (idxType *) malloc(nbpart * sizeof(idxType));
    //    printf("In topsortpart, nbready = %d\n", nbready);
    while (nbready > 0) {
        idxType part = ready[nbready-1];
        nbready--;
        toporder[to++] = part;
	    //printf("For topsortpart, nbready = %d, to = %d, part = %d\n", nbready, to, part);
	    shuffleTab(0, nbout[part]-1, shuffle);
        for (ip = 0; ip < nbout[part]; ip++) {
	        i = shuffle[ip];
	        idxType succ = outpart[part][i];
            nbinleft[succ]--;
            if (nbinleft[succ] == 0){
                ready[nbready++] = succ;
            }
        }
    }
    for (i=0; i<nbpart; i++){
        free(outpart[i]);
        free(inpart[i]);
    }
    free(nbinleft);
    free(shuffle);
    free(ready);
    free(nbin);
    free(nbout);
    free(inpart);
    free(outpart);
    if (to != nbpart){
        u_errexit("In topsortPart, not every nodes are sorted: to = %d, nbpart = %d\n", to, nbpart);
    }
}

int checkAcyclicity(dgraph *G, idxType *part, idxType nbpart){
    idxType i, j, k, ip;
    idxType** outpart = (idxType**) malloc(sizeof(idxType*)*nbpart);
    idxType* nbout = (idxType*) malloc(sizeof(idxType)*nbpart);
    idxType** inpart = (idxType**) malloc(sizeof(idxType*)*nbpart);
    idxType* nbin = (idxType*) malloc(sizeof(idxType)*nbpart);
    for (i=0; i<nbpart; i++){
        outpart[i] = (idxType*) malloc(sizeof(idxType)*nbpart);
        nbout[i] = 0;
        inpart[i] = (idxType*) malloc(sizeof(idxType)*nbpart);
        nbin[i] = 0;
    }
    for (i=1; i<=G->nVrtx; i++){
        for (j=G->outStart[i]; j<=G->outEnd[i]; j++){
            idxType outnode = G->out[j];
            if (part[i] == part[outnode])
                continue;
            int is_new = 1;
            for (k=0; k<nbout[part[i]]; k++)
                if (outpart[part[i]][k] == part[outnode]){
                    is_new = 0;
                    break;
                }
            if (is_new == 1){
                outpart[part[i]][nbout[part[i]]] = part[outnode];
                nbout[part[i]]++;
            }
            is_new = 1;
            //printf("i = %d, nVrtx = %d, nbpart = %d, outnode = %d, part[outnode] = %d\n", i, G->nVrtx, nbpart, outnode, part[outnode]);
            //printf("nbin[part[outnode]] = %d\n", nbin[part[outnode]]);
            for (k=0; k<nbin[part[outnode]]; k++)
                if (inpart[part[outnode]][k] == part[i]){
                    is_new = 0;
                    break;
            }
            if (is_new == 1){
                inpart[part[outnode]][nbin[part[outnode]]] = part[i];
                nbin[part[outnode]]++;
            }
        }
    }
    /*
    for(i=0;i<nbpart;++i){
        printf("PART = %d part\nIN: ", i);
        for(j=0;j<nbin[i];++j)
            printf("%d ", inpart[i][j]);
        printf("\nOUT: ");
        for(j=0;j<nbout[i];++j)
            printf("%d ", outpart[i][j]);
        printf("\n");
    }
    */
    idxType* ready = (idxType*) malloc(sizeof(idxType)*(nbpart));
    idxType* nbinleft = (idxType*) malloc(sizeof(idxType)*(nbpart));
    int nbready = 0;
    for (i=0; i<nbpart; i++) {
        nbinleft[i] = nbin[i];
        if (nbin[i] == 0)
            ready[nbready++] = i;
    }

    
    int to = 0;
    idxType* shuffle = (idxType *) malloc(nbpart * sizeof(idxType));
    //    printf("In topsortpart, nbready = %d\n", nbready);
    while (nbready > 0) {
        idxType part = ready[nbready-1];
        nbready--;
        to++;
        shuffleTab(0, nbout[part]-1, shuffle);
        for (ip = 0; ip < nbout[part]; ip++) {
            i = shuffle[ip];
            idxType succ = outpart[part][i];
            nbinleft[succ]--;
            if (nbinleft[succ] == 0){
                ready[nbready++] = succ;
            }
        }
    }
    for (i=0; i<nbpart; i++){
        free(outpart[i]);
        free(inpart[i]);
    }
    free(nbinleft);
    free(shuffle);
    free(ready);
    free(nbin);
    free(nbout);
    free(inpart);
    free(outpart);
    if (to != nbpart){
        printf("checkAcyclicity Failed! Expect errors in the following steps! #partsFound = %d, #partsExpected = %d\n", to, nbpart);
        return 1;
    }
    return 0;
}

void topsortwithTies(dgraph *G, idxType *toporder)
{
    /*topologically sorts the vertices of the dag G, when there are ties,
     chooses the one with the max (indegree -outdegree).
     
     Assumes toporder is allocated (of size nVrtx + 1).
     */
    
    idxType i, j, neigh, nVrtx = G->nVrtx;
    idxType  *inEnd = G->inEnd,  *inStart = G->inStart;
    idxType *outEnd = G->outEnd, *outStart = G->outStart, *out = G->out;
    
    //idxType keyval, maxKey;
    
    idxType *inDegrees = (idxType*) malloc(sizeof(idxType)*2*(G->nVrtx+1)); /*get working storage*/
    idxType heapsz = 0; /*will be from 1 to heapsz*/
    idxType outtop = 1;
    idxType maxind;
    
    idxType *heapIds = inDegrees + nVrtx + 1; /*get working storage*/
    
    
    for (i = 1; i <= nVrtx; i++)
    {
        inDegrees[i] = inEnd[i]- inStart[i] + 1;
        if(inDegrees[i] == 0)
            heapIds[++heapsz] = i;
    }
    
    maxBuildHeap(G, heapIds, heapsz);
    /*choose from indzelst the one with the largest key. toporder it. reduce the indeg of its neighbors and add them to indzerolst when necessary*/
    for (outtop = 1; outtop <= nVrtx; outtop ++)
    {
        maxind = maxHeapExtract(G, heapIds, &heapsz);
        toporder[outtop] = maxind;
        for ( j = outStart[maxind]; j<= outEnd[maxind]; j++)
        {
            neigh = out[j];
            inDegrees[neigh] --;
            if(inDegrees[neigh] == 0)
                maxHeapInsert(G, heapIds, &heapsz, neigh);
            else if(inDegrees[neigh] == -1)
                u_errexit("wrong indeg\n");
        }
    }
    free(inDegrees);
}


/*******************************************************************************/
idxType sourcesList(dgraph* G, idxType* sources)
{
    /*Assume that sources is already allocated and fill the tab sources with the sources nodes of G
      Return the number of sources in G*/
    idxType i, ind = 0;
    for (i=1; i<=G->nVrtx; i++){
        if (G->inStart[i] > G->inEnd[i]){
            sources[ind++] = i;
        //    printf("srclist added = %d\n",i);
        }

    }
    return ind;
}

idxType sourcesListPart(dgraph* G, idxType* sources, idxType *part, int part_idx)
{
    /*Assume that sources is already allocated and fill the tab sources with the sources nodes of part_idx
      Return the number of sources in G*/
    idxType i, ind = 0, is_source, j;
    for (i=1; i<=G->nVrtx; i++){
        if (part[i] != part_idx)
            continue;
        is_source = 1;
        for (j=G->inStart[i]; j <= G->inEnd[i]; j++)
            if (part[G->in[j]] == part_idx)
                is_source = 0;
        if (is_source == 1)
            sources[ind++] = i;
    }
    return ind;
}

idxType outputList(dgraph* G, idxType* output)
{
    /*Assume that output is already allocated and fill the tab output with the output nodes of G
      Return the number of output in G*/
    idxType i, ind = 0;
    for (i=1; i<=G->nVrtx; i++)
        if (G->outStart[i] > G->outEnd[i])
            output[ind++] = i;
    return ind;
}

void shuffleTab(idxType deb, idxType end, idxType* shuffle)
{
    /*Suppose shuffle is allocated
      Return shuffle filled with a shuffle list deb...end*/
    idxType i, j, tmp;
    for (i=deb; i<=end; i++)
        shuffle[i-deb] = i;
    for (i=deb; i<=end; i++){
        j = rand()%(end-deb+1);
        tmp = shuffle[i-deb];
        shuffle[i-deb] = shuffle[j];
        shuffle[j] = tmp;
    }
}

void DFStopsort_with_part(dgraph* G, idxType *part, int nbpart, idxType *toporder)
{
    /*Assume that toporder is already allocated
     Fill toporder to have a topological order of nodes in G
    with DFS respecting the convexity of the partitioning
    (every nodes of one part, then another part...)*/
    idxType i,j;
    idxType* toporderpart = (idxType*) malloc (sizeof(idxType) * nbpart);
    topSortOnParts(G, part, toporderpart, nbpart);
    idxType* topsortpart = (idxType*) malloc (sizeof(idxType) * nbpart);

    for (i = 0; i<nbpart; i++){
	    topsortpart[toporderpart[i]] = i;
    }

    idxType to = 1;
    int current_part = 0;
    idxType* ready;
    ready = (idxType*) malloc(sizeof(idxType) * G->nVrtx);
    idxType nbready = sourcesList(G, ready);

    idxType* nbinleft;
    nbinleft = (idxType*) malloc(sizeof(idxType) * (G->nVrtx+1));
    for (i = 1; i <= G->nVrtx; i++)
        nbinleft[i] = G->inEnd[i] - G->inStart[i] + 1;
    while (nbready > 0)
    {
	    idxType node = -1;
	    for (i = 1; i<=nbready; i++){
	        if (part[ready[nbready-i]] == toporderpart[current_part]){
		        node = ready[nbready-i];
		        break;
	        }
	    }
	    if (node == -1){
	        current_part++;
	        continue;
	    }
	    for (j = nbready-i; j<nbready-1; j++){
	        ready[j] = ready[j+1]; /*BU2JH: a regarder*/
	    }

        nbready--;
        toporder[to++] = node;

	    //printf("Loop nbready = %d, node = %d, to = %d, part = %d\n", (int) nbready, (int) node, (int) to, part[node]);
	    for (i = G->outStart[node]; i <= G->outEnd[node]; i++) {
	        idxType succ = G->out[i];
            nbinleft[succ]--;
            if (nbinleft[succ] == 0)
                ready[nbready++] = succ;
        }
    }
    if (to < G->nVrtx)
        u_errexit("DFStopsort_with_part: not every node concerned! to = %d, nVrtx = %d\n", to, G->nVrtx);
    free(nbinleft);
    free(toporderpart);
    free(topsortpart);
    free(ready);
}


void DFStopsortPart(dgraph* G, idxType *part, idxType partsize, int part_idx, idxType *toporder)
{
    /*Assume that toporder is already allocated
     Fill toporder to have a topological order of nodes of G in part_idx
    with DFS*/
    idxType to = 1;
    idxType* ready = (idxType*) malloc(sizeof(idxType) * G->nVrtx);
    idxType i;
    idxType nbready = sourcesListPart(G, ready, part, part_idx);
    idxType* nbinleft = (idxType*) malloc(sizeof(idxType) * (G->nVrtx+1));
    for (i = 1; i <= G->nVrtx; i++)
        nbinleft[i] = G->inEnd[i] - G->inStart[i] + 1;
    while (nbready > 0){
        if (to > partsize)
            printf("Proof to = %d\n", to);
        idxType node = ready[nbready-1];
        nbready--;
        toporder[to++] = node;
        for (i = G->outStart[node]; i <= G->outEnd[node]; i++){
            idxType succ = G->out[i];
            nbinleft[succ]--;
            if (part[succ] == part_idx)
                continue;
            if (nbinleft[succ] == 0)
                ready[nbready++] = succ;
        }
    }
    if (to != partsize+1)
        u_errexit("DFStopsortPart : Not every node concerned!\n");
    free(nbinleft);
    free(ready);
}


void randDFStopsort_with_part(dgraph* G, idxType *part, int nbpart, idxType *toporder)
{
    /*Assume that toporder is already allocated
     Fill toporder to have a topological order of nodes in G
    with DFS respecting the convexity of the partitioning
    (every nodes of one part, then another part...)*/
    idxType i,j, itmp;
    idxType* toporderpart = (idxType*) malloc (sizeof(idxType) * nbpart);
    topSortOnParts(G, part, toporderpart, nbpart);
    idxType* topsortpart = (idxType*) malloc (sizeof(idxType) * nbpart);

    for (i = 0; i<nbpart; i++){
        topsortpart[toporderpart[i]] = i;
    }

    idxType to = 1;
    int current_part = 0;
    idxType* ready;
    ready = (idxType*) malloc(sizeof(idxType) * G->nVrtx);
    idxType nbready = sourcesList(G, ready);
    idxType* shuffle = (idxType*) malloc(sizeof(idxType)*(G->maxoutdegree));

    idxType* nbinleft;
    nbinleft = (idxType*) malloc(sizeof(idxType) * (G->nVrtx+1));
    for (i = 1; i <= G->nVrtx; i++)
        nbinleft[i] = G->inEnd[i] - G->inStart[i] + 1;
    while (nbready > 0)
    {
        idxType node = -1;
        for (i = 1; i<=nbready; i++){
            if (part[ready[nbready-i]] == toporderpart[current_part]){
                node = ready[nbready-i];
                break;
            }
        }
        if (node == -1){
            current_part++;
            continue;
        }
        for (j = nbready-i; j<nbready-1; j++){
            ready[j] = ready[j+1]; /*BU2JH: a regarder*/
        }

        nbready--;
        toporder[to++] = node;
        shuffleTab(G->outStart[node], G->outEnd[node], shuffle);

        //printf("Loop nbready = %d, node = %d, to = %d, part = %d\n", (int) nbready, (int) node, (int) to, part[node]);
        for (itmp = 0; itmp <= G->outEnd[node]-G->outStart[node]; itmp++)
        {
            i = shuffle[itmp];
            idxType succ = G->out[i];
            nbinleft[succ]--;
            if (nbinleft[succ] == 0)
                ready[nbready++] = succ;
        }
    }
    if (to < G->nVrtx)
        u_errexit("RandDFStopsort_with_part, not every node concerned! to = %d, nVrtx = %d\n", to, G->nVrtx);
    free(shuffle);
    free(nbinleft);
    free(toporderpart);
    free(topsortpart);
    free(ready);
}

void BFStopsort_with_part(dgraph* G, idxType *part, int nbpart, idxType *toporder)
{
    /*Assume that toporder is already allocated
     Fill toporder to have a topological order of nodes in G
    with DFS respecting the convexity of the partitioning
    (every nodes of one part, then another part...)*/
    idxType i,j;
    idxType* toporderpart = (idxType*) malloc (sizeof(idxType) * nbpart);
    topSortOnParts(G, part, toporderpart, nbpart);
    idxType* topsortpart = (idxType*) malloc (sizeof(idxType) * nbpart);

    for (i = 0; i<nbpart; i++){
        topsortpart[toporderpart[i]] = i;
    }

    idxType to = 1;
    int current_part = 0;
    idxType* ready;
    ready = (idxType*) malloc(sizeof(idxType) * G->nVrtx);
    idxType nbready = sourcesList(G, ready);

    idxType* nbinleft;
    nbinleft = (idxType*) malloc(sizeof(idxType) * (G->nVrtx+1));
    for (i = 1; i <= G->nVrtx; i++)
        nbinleft[i] = G->inEnd[i] - G->inStart[i] + 1;;
    while (nbready > 0)
    {
        idxType node = -1;
        for (i = 1; i<=nbready; i++){
            if (part[ready[nbready-i]] == toporderpart[current_part]){
                node = ready[nbready-i];
                break;
            }
        }
        if (node == -1){
            current_part++;
            continue;
        }
        for (j = nbready-i; j<nbready-1; j++){
            ready[j] = ready[j+1];  /*BU2JH: a regarder*/
        }

        nbready--;
        toporder[to++] = node;

        //printf("Loop nbready = %d, node = %d, to = %d, part = %d\n", (int) nbready, (int) node, (int) to, part[node]);
        for (i = G->outStart[node]; i <= G->outEnd[node]; i++) {
            idxType succ = G->out[i];
            nbinleft[succ]--;
            if (nbinleft[succ] == 0){
                //		printf("succ = %d is ready\n",succ);
                for (j = nbready-1; j>=0; j--)
                    ready[j+1] = ready[j];  /*BU2JH: a regarder*/
                ready[0] = succ;
                nbready++;
            }
        }
    }
    if (to < G->nVrtx)
        u_errexit("BFStopsort_with_part, not every node concerned! to = %d, nVrtx = %d\n", to, G->nVrtx);
    free(nbinleft);
    free(toporderpart);
    free(topsortpart);
    free(ready);
}

void randBFStopsort_with_part(dgraph* G, idxType *part, int nbpart, idxType *toporder)
{
    /*Assume that toporder is already allocated
     Fill toporder to have a topological order of nodes in G
    with DFS respecting the convexity of the partitioning
    (every nodes of one part, then another part...)*/
    idxType i,j,itmp;
    idxType* toporderpart = (idxType*) malloc (sizeof(idxType) * nbpart);
    topSortOnParts(G, part, toporderpart, nbpart);
    idxType* topsortpart = (idxType*) malloc (sizeof(idxType) * nbpart);

    for (i = 0; i<nbpart; i++){
        topsortpart[toporderpart[i]] = i;
    }

    idxType to = 1;
    int current_part = 0;
    idxType* ready;
    ready = (idxType*) malloc(sizeof(idxType) * G->nVrtx);
    idxType nbready = sourcesList(G, ready);

    idxType* nbinleft;
    nbinleft = (idxType*) malloc(sizeof(idxType) * (G->nVrtx+1));
    idxType* shuffle = (idxType*) malloc(sizeof(idxType)*(G->maxoutdegree));
    for (i = 1; i <= G->nVrtx; i++)
        nbinleft[i] = G->inEnd[i] - G->inStart[i] + 1;
    while (nbready > 0)
    {
        idxType node = -1;
        for (i = 1; i<=nbready; i++){
            if (part[ready[nbready-i]] == toporderpart[current_part]){
                node = ready[nbready-i];
                break;
            }
        }
        if (node == -1){
            current_part++;
            continue;
        }
        for (j = nbready-i; j<nbready-1; j++){
            ready[j] = ready[j+1];  /*BU2JH: a regarder*/
        }

        nbready--;
        toporder[to++] = node;
        shuffleTab(G->outStart[node], G->outEnd[node], shuffle);

        //printf("Loop nbready = %d, node = %d, to = %d, part = %d\n", (int) nbready, (int) node, (int) to, part[node]);
        for (itmp = 0; itmp <= G->outEnd[node]-G->outStart[node]; itmp++) {
            i = shuffle[itmp];
            idxType succ = G->out[i];
            nbinleft[succ]--;
            if (nbinleft[succ] == 0){
                //		printf("succ = %d is ready\n",succ);
                for (j = nbready-1; j>=0; j--)
                    ready[j+1] = ready[j];  /*BU2JH: a regarder*/
                ready[0] = succ;
                nbready++;
            }
        }
    }
    if (to < G->nVrtx)
        u_errexit("RandBFStopsort_with_part, not every node concerned! to = %d, nVrtx = %d\n", to, G->nVrtx);
    free(nbinleft);
    free(toporderpart);
    free(topsortpart);
    free(shuffle);
    free(ready);
}


void DFStopsort(dgraph* G, idxType *toporder)
{
    /*Assume that toporder is already allocated
     Fill toporder to have a topological order of nodes in G
    with DFS*/
	//printf("\n\nDFSTopsort called\n\n");
    idxType to = 1;
    idxType* ready = (idxType*) malloc(sizeof(idxType) * (G->nVrtx+1));
    idxType i;
    idxType nbready = sourcesList(G, ready);
    //printf("DFStopsort nbready value = %d\n",nbready);
    idxType* nbinleft = (idxType *) calloc(G->nVrtx+1, sizeof(idxType));
    for (i = 1; i <= G->nVrtx; i++)
        nbinleft[i] = G->inEnd[i] - G->inStart[i] + 1;
    while (nbready > 0){
        if (to > G->nVrtx)
	        printf("Proof to = %d\n", to);
        idxType node = ready[nbready-1];
        //printf("DFStopsort node = %d\n",node);
        nbready--;
        toporder[to++] = node;
        for (i = G->outStart[node]; i <= G->outEnd[node]; i++){
	        idxType succ = G->out[i];
	   //     printf("seccessor = %d\n",succ);
            nbinleft[succ]--;
            if (nbinleft[succ] == 0){
                ready[nbready++] = succ;
        //fDFS
             //   printf("ready[%d] = %d\n",nbready-1,ready[nbready-1]);
            }
            else if (nbinleft[succ]<0)
                u_errexit("DFStopsort: negative indegree\n");
        }
    }
    if (to != G->nVrtx+1)
        u_errexit("DFStopsort : Not every node concerned! to = %d, nVrtx = %d\n", to, G->nVrtx);
    free(nbinleft);
    free(ready);
    //printf("DFStopsort done\n");
}



void DFSsort(dgraph* G, idxType *toporder)
{
    /*Assume that toporder is already allocated
     Fill toporder to have a topological order of nodes in G
    with DFS*/
    idxType to = 1;
    idxType* ready = (idxType*) malloc(sizeof(idxType) * G->nVrtx);
    idxType i;
    idxType nbready = sourcesList(G, ready);
    //printf("sources = %d\n",nbready);
    for(i = 0;i<nbready;i++){
    //	printf("ready[%d] = %d\n",i,ready[i]);
    }
    idxType* done = (idxType *) calloc(G->nVrtx+1, sizeof(idxType));
    while (nbready > 0)
    {
	    if (to > G->nVrtx)
	        printf("Proof to = %d\n", to);
        idxType node = ready[nbready-1];
      //  printf("traversed node = %d\n",node);
        nbready--;
        toporder[to++] = node;
        for (i = G->outStart[node]; i <= G->outEnd[node]; i++)
        {
	        idxType succ = G->out[i];
	      //  printf("successor node = %d\n",succ);
	        if (done[succ] == 1)
		        continue;
	        ready[nbready++] = succ;
	        done[succ] = 1;
        }
    }
    if (to != G->nVrtx+1)
        u_errexit("DFSsort : Not every node concerned! to = %d, nVrtx = %d\n", to, G->nVrtx);
    free(done);
    free(ready);
}


void randDFStopsort(dgraph *G, idxType *toporder)
{
    /*Assume that toporder is already allocated
      Fill toporder to have a random topological order of nodes in G*/
    idxType to = 1;
    idxType* ready;
    ready = (idxType*) malloc(sizeof(idxType) * G->nVrtx);
    idxType i,itmp;
    idxType nbready = sourcesList(G, ready);
    idxType* nbinleft = (idxType *) calloc(G->nVrtx+1, sizeof(idxType));
    for (i = 1; i <= G->nVrtx; i++)
        nbinleft[i] = G->inEnd[i] - G->inStart[i] + 1;
    idxType* shuffle = (idxType*) malloc(sizeof(idxType)*(G->maxoutdegree));
    while (nbready > 0)
    {
        idxType node = ready[nbready-1];
        nbready--;
        toporder[to++] = node;
    	shuffleTab(G->outStart[node], G->outEnd[node], shuffle);
        for (itmp = 0; itmp <= G->outEnd[node]-G->outStart[node]; itmp++) {
	        i = shuffle[itmp];
	        idxType succ = G->out[i];
            nbinleft[succ]--;
            if (nbinleft[succ] == 0)
                ready[nbready++] = succ;
        }
    }
    free(shuffle);
    free(nbinleft);
    free(ready);
}

void randDFStopsortPart(dgraph* G, idxType *part, idxType partsize, int part_idx, idxType *toporder)
{
    /*Assume that toporder is already allocated
      Fill toporder to have a random topological order of nodes in part_idx*/
    idxType to = 1;
    idxType* ready;
    ready = (idxType*) malloc(sizeof(idxType) * G->nVrtx);
    idxType i,itmp;
    idxType nbready = sourcesListPart(G, ready, part, part_idx);;
    idxType* nbinleft = (idxType *) calloc(G->nVrtx+1, sizeof(idxType));
    for (i = 1; i <= G->nVrtx; i++)
        nbinleft[i] = G->inEnd[i] - G->inStart[i] + 1;
    idxType* shuffle = (idxType*) malloc(sizeof(idxType)*(G->maxoutdegree));
    while (nbready > 0)
    {
        if (to > partsize)
            printf("Proof to = %d\n", to);
        idxType node = ready[nbready-1];
        nbready--;
        toporder[to++] = node;
        shuffleTab(G->outStart[node], G->outEnd[node], shuffle);
        for (itmp = 0; itmp <= G->outEnd[node]-G->outStart[node]; itmp++) {
            i = shuffle[itmp];
            idxType succ = G->out[i];
            nbinleft[succ]--;
            if (part[succ] != part_idx)
                continue;
            if (nbinleft[succ] == 0)
                ready[nbready++] = succ;
        }
    }
    if (to != partsize+1)
        u_errexit("randDFStopsortPart : Not every node concerned! to = %d, nVrtx = %d\n", to, G->nVrtx);
    free(shuffle);
    free(nbinleft);
    free(ready);
}

void randDFSsort(dgraph *G, idxType *toporder)
{
    /*Assume that toporder is already allocated
      Fill toporder to have a random topological order of nodes in G*/
    idxType to = 1;
    idxType* ready;
    ready = (idxType*) malloc(sizeof(idxType) * G->nVrtx);
    idxType i,itmp;
    idxType nbready = sourcesList(G, ready);
    idxType* done = (idxType *) calloc(G->nVrtx+1, sizeof(idxType));
    idxType* shuffle = (idxType*) malloc(sizeof(idxType)*(G->maxoutdegree));
    while (nbready > 0) {
        idxType node = ready[nbready-1];
        nbready--;
        toporder[to++] = node;
	    shuffleTab(G->outStart[node], G->outEnd[node], shuffle);
        for (itmp = 0; itmp <= G->outEnd[node]-G->outStart[node]; itmp++) {
	        i = shuffle[itmp];
	        idxType succ = G->out[i];
	        if (done[succ] == 1)
		        continue;
	        ready[nbready++] = succ;
	        done[succ] = 1;
        }
    }
    free(shuffle);
    free(done);
    free(ready);
}

int gcd ( int a, int b )
{
  int c;
  while ( a != 0 ) {
     c = a; a = b%a;  b = c;
  }
  return b;
}

void mixtopsort(dgraph* G,idxType* toporder,int priority,int first){
    //first==0 --> bfs first
    //first==1 --> dfs first
    idxType* readyb = (idxType*)malloc(sizeof(idxType)*(G->nVrtx+1));
    idxType* readyd = (idxType*)malloc(sizeof(idxType)*(G->nVrtx+1));
    idxType* mark = (idxType *) calloc(G->nVrtx+1, sizeof(idxType));
    idxType* nbinleft = (idxType *) calloc(G->nVrtx+1, sizeof(idxType));
    idxType bcount = sourcesList(G,readyb);
    idxType bfrac=0,dfrac=0;
    idxType biter=0,diter=0;
    idxType node,dcount=bcount;
    //idxType fromb,fromd;
    idxType i,j,to=1;
    for (i = 1; i <= G->nVrtx; i++)
        nbinleft[i] = G->inEnd[i] - G->inStart[i] + 1;
    
    //copy reverse of readyb to readyd
    for(i=0;i<bcount;++i)
        readyd[bcount-i-1]=readyb[i];
    //for(i=0;i<bcount;++i)
    //    printf("%d %d\n",readyb[i],readyd[i] );
    //the gcd stuff here for biter, diter
    i=gcd(priority,100-priority);
    if(i==0){
        bfrac=priority/100;
        dfrac=(100-priority)/100;
    }
    else{
        bfrac=priority/i;
        dfrac=(100-priority)/i;
    }
    if(first)
        diter=dfrac;
    else
        biter=bfrac;
    /*
    printf("%s first\n",first==0? "bfs":"dfs");
    printf("biter %d\tditer %d\n",biter,diter );
    printf("bfrac %d\tdfrac %d\n",bfrac,dfrac );
    */
    if(bfrac==0)
        bcount=0;
    if(dfrac==0)
        dcount=0;
    //printf("b:%d %d\n",biter,bfrac );
    //printf("d:%d %d\n",diter,dfrac );
    while(bcount>0||dcount>0){
        if (to > G->nVrtx+1){
            printf("Proof to = %d\n", to);
            break;
        }

        //pick from bfs priority
        if(biter>=0){
            do{
                if(bcount<1){
                    biter=-1;
                    break;
                }
                node = readyb[bcount-1];
                biter--;
                if(biter<0){
                    node=-1;
                    break;
                }
                bcount--;
                //printf("node %d mark = %d\n",node ,mark[node]);
            }while(mark[node]);
            if(biter<0){
                diter=dfrac;
                //printf("continue 1\n");
                continue;
            }
        }
        else{ //pick from dfs priority
            do{
                if(dcount<1){
                    diter=-1;
                    break;
                }
                node = readyd[dcount-1];
                diter--;
                if(diter<0){
                    node=-1;
                    break;
                }
                dcount--;
                //printf("node %d mark = %d\n",node ,mark[node]);
                //printf("node %d dcount-1 = %d\n",node ,dcount);
            }while(mark[node]);
            if(diter<0){
                biter=bfrac;
                //printf("continue 1\n");
                continue;
            }
        }
        if(node==-1){
            //printf("continue 2\n");
            continue;
        }
        //printf("processing %d\n", node);
        mark[node]=1;
        toporder[to++] = node;
        for (i = G->outStart[node]; i <= G->outEnd[node]; i++) {
            idxType succ = G->out[i];
            //printf("node %d succ %d\n",node,succ );
            nbinleft[succ]--;
            if (nbinleft[succ] == 0){
                readyd[dcount] = succ;
                for (j = bcount-1; j>=0; j--)
                    readyb[j+1] = readyb[j];  /*BU2JH: a regarder*/
                readyb[0] = succ;
                if(bfrac!=0)
                    bcount++;
                if(dfrac!=0)
                    dcount++;
            }
        }
        /*
        printf("BQ\n");
        for (i=0;i<bcount;++i){
            printf("%d,",readyb[i] );
        }
        printf("\n");
        printf("DQ\n");
        for (i=0;i<dcount;++i){
            printf("%d,",readyd[i] );
        }
        printf("\n");
        */
        //printf("\nready end\n");

    }
    if (to != G->nVrtx+1)
        u_errexit("Mixtopsort : Not every node concerned!  to = %d, nVrtx = %d\n", to, G->nVrtx);
    /*
    for(i=1;i<=G->nVrtx;++i)
        printf("%d, ", toporder[i]);
    printf("\n");
    */
    free(mark);
    free(readyb);
    free(readyd);
}


void BFStopsort(dgraph* G, idxType *toporder)
{
    /*Assume that toporder is already allocated
     Fill toporder to have a topological order of nodes in G
    with DFS*/
    idxType to = 1;
    idxType* ready;
    ready = (idxType*) malloc(sizeof(idxType) * G->nVrtx);
    idxType i,j;
    idxType endready = sourcesList(G, ready) - 1;
    idxType beginready = 0;
    idxType* nbinleft = (idxType *) calloc(G->nVrtx+1, sizeof(idxType));





    for (i = 1; i <= G->nVrtx; i++)
        nbinleft[i] = G->inEnd[i] - G->inStart[i] + 1;
    printf("BFSTOPSORT number of sources = %d\n",endready+1);
    while (endready >= beginready) {
	    if (to > G->nVrtx)
	        printf("Proof to = %d\n", to);
        idxType node = ready[beginready];
        printf("BFSTOPSORT : %s\n", G->vertices[node-1]);
        beginready++;
        toporder[to++] = node;
        for (i = G->outStart[node]; i <= G->outEnd[node]; i++) {

	        idxType succ = G->out[i];
            printf("%s ---> %s\n", G->vertices[node-1], G->vertices[succ-1]);
            nbinleft[succ]--;
            if (nbinleft[succ] == 0){
        		ready[++endready] = succ;
            }
        }
    }
    /*
    printf("TOPORDER:\n");
    for (i=1;i<=G->nVrtx;++i)
        printf("%d, ", toporder[i]);
    printf("TOPORDER END\n");
    */
    if (to != G->nVrtx+1)
        u_errexit("BFStopsort : Not every node concerned! to = %d, nVrtx = %d\n", to, G->nVrtx);
    free(nbinleft);
    free(ready);
}

void BFStopsortPart(dgraph* G, idxType *part, idxType partsize, int part_idx, idxType *toporder)
{
    /*Assume that toporder is already allocated
     Fill toporder to have a topological order of nodes in part_idx
    with BFS*/
    idxType to = 1;
    idxType* ready;
    ready = (idxType*) malloc(sizeof(idxType) * G->nVrtx);
    idxType i;
    idxType endready = sourcesListPart(G, ready, part, part_idx) - 1;
    idxType beginready = 0;
    idxType* nbinleft = (idxType *) calloc(G->nVrtx+1, sizeof(idxType));
    for (i = 1; i <= G->nVrtx; i++)
        nbinleft[i] = G->inEnd[i] - G->inStart[i] + 1;

    while (endready >= beginready) {
        if (to > partsize)
            printf("Proof to = %d\n", to);
        idxType node = ready[beginready];
        beginready++;
        toporder[to++] = node;
        for (i = G->outStart[node]; i <= G->outEnd[node]; i++) {
            idxType succ = G->out[i];
            nbinleft[succ]--;
            if (part[succ] != part_idx)
                continue;
            if (nbinleft[succ] == 0){
                //		printf("succ = %d is ready\n",succ);
                ready[++endready] = succ;
            }
        }
    }
    /*
    printf("TOPORDER:\n");
    for (i=1;i<=G->nVrtx;++i)
        printf("%d, ", toporder[i]);
    printf("TOPORDER END\n");
    */
    if (to != partsize+1)
        u_errexit("BFStopsortPart : Not every node concerned! to = %d, nVrtx = %d\n", to, G->nVrtx);
    free(nbinleft);
    free(ready);
}

void BFSsort(dgraph* G, idxType *toporder)
{
    /*Assume that toporder is already allocated
     Fill toporder to have a topological order of nodes in G
     with DFS*/
    idxType to = 1;
    idxType* ready;
    ready = (idxType*) malloc(sizeof(idxType) * G->nVrtx);
    idxType i;
    idxType endready = sourcesList(G, ready) - 1;
    idxType beginready = 0;
    idxType* done = (idxType *) calloc(G->nVrtx+1, sizeof(idxType));
    while (endready >= beginready)
    {
        if (to > G->nVrtx)
            printf("Proof to = %d\n", to);
        idxType node = ready[beginready];
        beginready++;
        toporder[to++] = node;
        for (i = G->outStart[node]; i <= G->outEnd[node]; i++) {
            idxType succ = G->out[i];
            if (done[succ] == 1)
                continue;
            ready[++endready] = succ;
            done[succ] = 1;
        }
    }
    if (to != G->nVrtx+1)
        u_errexit("BFSsort : Not every node concerned! to = %d, nVrtx = %d\n", to, G->nVrtx);
    free(done);
    free(ready);
}


void randBFStopsort(dgraph* G, idxType *toporder)
{
    /*Assume that toporder is already allocated
     Fill toporder to have a topological order of nodes in G
    with DFS*/
    idxType to = 1;
    idxType* ready;
    ready = (idxType*) malloc(sizeof(idxType) * G->nVrtx);
    idxType i,itmp;
    idxType endready = sourcesList(G, ready) - 1;
    idxType beginready = 0;
    idxType* nbinleft = (idxType *) calloc(G->nVrtx+1, sizeof(idxType));
    for (i = 1; i <= G->nVrtx; i++)
        nbinleft[i] = G->inEnd[i] - G->inStart[i] + 1;
    idxType* shuffle = (idxType*) malloc(sizeof(idxType)*(G->maxoutdegree));
    while (endready >= beginready) {
	if (to > G->nVrtx)
	    printf("Proof to = %d\n", to);
        idxType node = ready[beginready];
        beginready++;
        toporder[to++] = node;
	    shuffleTab(G->outStart[node], G->outEnd[node], shuffle);
        for (itmp = 0; itmp <= G->outEnd[node]-G->outStart[node]; itmp++) {
	        i = shuffle[itmp];
	        idxType succ = G->out[i];
            nbinleft[succ]--;
            if (nbinleft[succ] == 0){
		        ready[++endready] = succ;
	        }
        }
    }
    if (to != G->nVrtx+1)
        u_errexit("randBFStopsort : Not every node concerned! to = %d, nVrtx = %d\n", to, G->nVrtx);
    free(nbinleft);
    free(shuffle);
    free(ready);
}

void randBFStopsortPart(dgraph* G, idxType *part, idxType partsize, int part_idx, idxType *toporder)
{
    /*Assume that toporder is already allocated
     Fill toporder to have a topological order of nodes in part_idx
    with randBFS*/
    idxType to = 1;
    idxType* ready;
    ready = (idxType*) malloc(sizeof(idxType) * G->nVrtx);
    idxType i,itmp;
    idxType endready = sourcesListPart(G, ready, part, part_idx) - 1;
    idxType beginready = 0;
    idxType* nbinleft = (idxType *) calloc(G->nVrtx+1, sizeof(idxType));
    for (i = 1; i <= G->nVrtx; i++)
        nbinleft[i] = G->inEnd[i] - G->inStart[i] + 1;
    idxType* shuffle = (idxType*) malloc(sizeof(idxType)*(G->maxoutdegree));
    while (endready >= beginready)
    {
        if (to > partsize)
            printf("Proof to = %d\n", to);
        idxType node = ready[beginready];
        beginready++;
        toporder[to++] = node;
        shuffleTab(G->outStart[node], G->outEnd[node], shuffle);
        for (itmp = 0; itmp <= G->outEnd[node]-G->outStart[node]; itmp++) {
            i = shuffle[itmp];
            idxType succ = G->out[i];
            nbinleft[succ]--;
            if (part[succ] != part_idx)
                continue;
            if (nbinleft[succ] == 0){
                ready[++endready] = succ;
            }
        }
    }
    if (to != partsize+1)
        u_errexit("randBFStopsort : Not every node concerned! to = %d, nVrtx = %d\n", to, G->nVrtx);
    free(nbinleft);
    free(shuffle);
    free(ready);
}



void randBFSsort(dgraph* G, idxType *toporder)
{
    /*Assume that toporder is already allocated
     Fill toporder to have a topological order of nodes in G
    with DFS*/
    idxType to = 1;
    idxType* ready;
    ready = (idxType*) malloc(sizeof(idxType) * G->nVrtx);
    idxType i,itmp;
    idxType endready = sourcesList(G, ready) - 1;
    idxType beginready = 0;
    idxType* done = (idxType *) calloc(G->nVrtx+1, sizeof(idxType));
    idxType* shuffle = (idxType*) malloc(sizeof(idxType)*(G->maxoutdegree));
    while (endready >= beginready)
    {
	if (to > G->nVrtx)
	    printf("Proof to = %d\n", to);
        idxType node = ready[beginready];
        beginready++;
        toporder[to++] = node;
	    shuffleTab(G->outStart[node], G->outEnd[node], shuffle);
        for (itmp = 0; itmp <= G->outEnd[node]-G->outStart[node]; itmp++) {
	        i = shuffle[itmp];
	        idxType succ = G->out[i];
	        if (done[succ] == 1)
		        continue;
	        ready[++endready] = succ;
	        done[succ] = 1;
	}
    }
    if (to != G->nVrtx+1)
        u_errexit("randBFSsort : Not every node concerned! to = %d, nVrtx = %d\n", to, G->nVrtx);
    free(done);
    free(shuffle);
    free(ready);
}


void randTopsort(dgraph* G, idxType *toporder)
{
    /*Assume that toporder is already allocated
     Fill toporder to have a topological order of nodes in G
    with DFS*/
    idxType to = 1;
    idxType* ready;
    ready = (idxType*) malloc(sizeof(idxType) * G->nVrtx);
    idxType i,j;
    idxType nbready = sourcesList(G, ready);
    idxType* nbinleft = (idxType *) calloc(G->nVrtx+1, sizeof(idxType));
    for (i = 1; i <= G->nVrtx; i++)
        nbinleft[i] = G->inEnd[i] - G->inStart[i] + 1;
    while (nbready > 0) {
	    if (to >= G->nVrtx)
	        printf("Proof to = %d\n", to);
        idxType node = ready[nbready-1];
        nbready--;
        toporder[to++] = node;
        for (i = G->outStart[node]; i <= G->outEnd[node]; i++) {
	        idxType succ = G->out[i];
            nbinleft[succ]--;
            if (nbinleft[succ] == 0){
		        idxType randidx = rand()%(nbready+1);
		        for (j = nbready-1; j>=randidx; j--)
		            ready[j+1] = ready[j];  /*BU2JH: a regarder*/
                ready[randidx] = succ;
		        nbready++;
	        }
        }
    }
    if (to != G->nVrtx)
        u_errexit("randTopsort : Not every node concerned! to = %d, nVrtx = %d\n", to, G->nVrtx);
    free(nbinleft);
    free(ready);
}

void oneDegreeFirst(dgraph* G, idxType* order)
{
    idxType i,j;
    for (i=1; i<=G->nVrtx; i++){
	idxType node = order[i];
	int nboutnodes = G->outEnd[node] - G->outStart[node] + 1;
	int nbinnodes = G->inEnd[node] - G->inStart[node] + 1;
	if ((nboutnodes == 1)&&(nbinnodes == 1)){
	    for (j=i; j>=2; j--)
		order[j] = order[j-1];
	    order[1] = node;  
	}
    }
}


void computeToplevels(dgraph* G, int* toplevels)
{
    /*We assume that toplevels is already allocated*/
    int i,j;
    idxType* toporder = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    //printf("Now DFStopsort\n");
    DFStopsort(G, toporder);
    //printf("Done DFStopsort\n");
    for (i=1; i<=G->nVrtx; i++)
    {
        idxType node = toporder[i];
	int tl = 0;
	for (j=G->inStart[node]; j<= G->inEnd[node]; j++)
	    tl = tl > toplevels[G->in[j]]+1 ? tl : toplevels[G->in[j]]+1;
	toplevels[node] = tl;
    }
    free(toporder);
}

void computeToplevelsWithTopOrder(dgraph* G, int* toplevels, idxType* toporder)
{
    /*We assume that toplevels is already and toprder computed allocated*/
    idxType i,j;
    for (i=1; i<=G->nVrtx; i++)
    {
        idxType node = toporder[i];
        int tl = 0;
        for (j=G->inStart[node]; j<= G->inEnd[node]; j++)
            tl = tl > toplevels[G->in[j]]+1 ? tl : toplevels[G->in[j]]+1;
        toplevels[node] = tl;
    }
}
double computeLatency(dgraph* G, idxType* part, double l1, double l2)
{
    int i,j;
    double max = 0.0;
    double* latencies = (double*) malloc(sizeof(double)*(G->nVrtx + 1));
    idxType* toporder = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    DFStopsort(G, toporder);
    for (i=1; i<=G->nVrtx; i++)
    {
        idxType node = toporder[i];
	double lat = 1.0;
	for (j=G->inStart[node]; j<= G->inEnd[node]; j++) {
	  double currentLat = part[node] == part[G->in[j]] ? l1 : l2;
	  lat = lat > latencies[G->in[j]]+currentLat+1.0 ? lat : latencies[G->in[j]]+currentLat+1.0;
	}
	latencies[node] = lat;
	max = max > lat ? max : lat;
    }
    free(latencies);
    free(toporder);
    return max;
}

void copyDGraph(dgraph *G)
{

}

void generateDGraphFromFile(dgraph *G, char* file_name)
{
    FILE* file;
    file = fopen(file_name, "r");
    int nVrtx, nEdge, i, tmp;
    fscanf(file, "%d", &nVrtx);
    fscanf(file, "%d", &nEdge);

    G->frmt = 3;
    G->nVrtx = nVrtx;
    G->nEdge = nEdge;
    G->inStart  = (idxType * ) malloc(sizeof(idxType) * (nVrtx + 2));
    G->hollow  = (int * ) calloc((nVrtx + 2),sizeof(int));
    G->inEnd= (idxType * ) malloc(sizeof(idxType) * (nVrtx + 2));
    G->in = (idxType * ) malloc(sizeof(idxType) * nEdge);
    G->totvw = nVrtx;
    G->maxindegree = 0;

    G->inStart[0] = 0;
    for (i=1; i<=nVrtx+1; i++){
        fscanf(file, "%d", &tmp);
	G->inStart[i] = tmp;
    }

    G->inEnd[0] = -1;
    for (i=1; i<=nVrtx; i++){
        G->inEnd[i] = G->inStart[i+1] -1;
	int degree = G->inEnd[i] - G->inStart[i] + 1;
	G->maxindegree = G->maxindegree < degree ? degree : G->maxindegree;
    }
    G->inEnd[nVrtx+1] = nEdge;
    for (i=0; i<nEdge; i++){
        fscanf(file, "%d", &tmp);
	G->in[i] = tmp;
    }

    G->ecIn = G->ecOut = NULL;
    G->vw = NULL;

    if (nEdge <= 0)
        u_errexit("allocateDGraphData: empty edge set\n");
    
    if (nVrtx <=-1)
        u_errexit("allocateDGraphData: empty vertex set\n");

    G->outStart  = (idxType * ) malloc(sizeof(idxType) * (nVrtx + 2));
    G->outEnd  = (idxType * ) malloc(sizeof(idxType) * (nVrtx + 2));
    G->out = (idxType * ) malloc(sizeof(idxType) * nEdge);
    
    /*
    if (G->frmt == 1 || G->frmt == 3){
        G->vw = (vwType *) malloc(sizeof(vwType) * (nVrtx+1));
	for (i=1; i<=nVrtx; i++)
	    G->vw[i] = 1;
    }

    if (G->frmt == 2 || G->frmt == 3)
    {
        G->ecIn = (ecType *) malloc(sizeof(ecType) * nEdge);
        G->ecOut = (ecType *) malloc(sizeof(ecType) * nEdge);
	for (i=0; i< nEdge; i++){
	    G->ecIn[i] = 1;
	    G->ecOut[i] = 1;
	}
    }
    */

    G->vw = (vwType *) malloc(sizeof(vwType) * (nVrtx+1));
    //G->vw = (ecType * ) malloc(sizeof(ecType) * (nVrtx+1));
    for (i=1; i<=nVrtx; i++)
    	G->vw[i] = 1.0;
    G->ecIn = (ecType *) malloc(sizeof(ecType) * nEdge);
    G->ecOut = (ecType *) malloc(sizeof(ecType) * nEdge);
    for (i=0; i< nEdge; i++){
	G->ecIn[i] = 1;
	G->ecOut[i] = 1;
    }

    fillOutFromIn(G);
    
    G->maxVW = G->maxEC = -1;
    fclose(file);
}
void generateDGraphFromMtx(dgraph *G, char* file_name, int use_binary_input){

    char fnbin[100];
    idxType i;
    
    strcpy(fnbin,file_name);
    strcat(fnbin,".bin");

    if(use_binary_input){
        FILE *fp = fopen(fnbin,"rb");
        if(fp!=NULL){
            fclose(fp);
            // printf("Yes-ish\n");
            generateDGraphFromBinaryDot(G,fnbin);
            return;
        }
        else if (strstr(file_name,".bin")){
            generateDGraphFromBinaryDot(G,file_name);
            return;
        }
        // printf("No bin file found for input!\n");
    }

    idxType M, N, nnz, *col_ptrs, *row_inds;
    M=N=nnz=-1;
    mm_read_into_csc_pattern(file_name, &M, &N, &nnz, &col_ptrs, &row_inds);
    allocateDGraphData(G,M,nnz,3);
    /*the vertex weights are not set, while 3 is used*/
    /*let us set them*/
    
    if(G->vw == NULL)
    {
        G->vw = (vwType*) malloc(sizeof(vwType)*(M+1));
    	//G->vw = (ecType * ) malloc(sizeof(ecType) * (M+1));
    }
    for (i=0; i <= M; i++)
          G->vw[i] = 1;
    
    loadFromCSC(G,M,nnz,3,col_ptrs,row_inds,NULL);
    
    
    if(use_binary_input){
        FILE *fp = fopen(fnbin,"wb");

        if(fp == NULL){
           u_errexit("Cannot open file");
        }

        fwrite(&G->frmt,sizeof(G->frmt),1,fp);
        fwrite(&G->nVrtx,sizeof(G->nVrtx),1,fp);
        fwrite(&G->nEdge,sizeof(G->nEdge),1,fp);
        fwrite(&G->totvw,sizeof(G->totvw),1,fp);
        fwrite(&G->maxindegree,sizeof(G->maxindegree),1,fp);
        fwrite(&G->maxoutdegree,sizeof(G->maxoutdegree),1,fp);
        fwrite(G->inStart,sizeof(G->inStart[0]),(G->nVrtx+2),fp);
        fwrite(G->inEnd,sizeof(G->inEnd[0]),(G->nVrtx+2),fp);
        fwrite(G->in,sizeof(G->in[0]),G->nEdge,fp);
        fwrite(G->vw,sizeof(G->vw[0]),(G->nVrtx+1),fp);
        fwrite(G->outStart,sizeof(G->outStart[0]),(G->nVrtx+2),fp);
        fwrite(G->outEnd,sizeof(G->outEnd[0]),(G->nVrtx+2),fp);
        fwrite(G->out,sizeof(G->out[0]),G->nEdge,fp);
        fwrite(G->ecIn,sizeof(ecType),G->nEdge,fp);
        fwrite(G->ecOut,sizeof(ecType),G->nEdge,fp);
        
        fclose(fp);
    }

    return;
        
}
void generateDGraphFromBinaryDot(dgraph *G, char* file_name){

    FILE *fp = fopen(file_name,"rb");
    if(fp == NULL){
       u_errexit("Cannot open file");
    }

    fread(&G->frmt,sizeof(G->frmt),1,fp);
    fread(&G->nVrtx,sizeof(G->nVrtx),1,fp);
    fread(&G->nEdge,sizeof(G->nEdge),1,fp);
    fread(&G->totvw,sizeof(G->totvw),1,fp);
    fread(&G->maxindegree,sizeof(G->maxindegree),1,fp);
    fread(&G->maxoutdegree,sizeof(G->maxoutdegree),1,fp);

    idxType nVrtx = G->nVrtx;
    idxType nEdge = G->nEdge;

    if (nEdge <= 0)
        u_errexit("allocateDGraphData: empty edge set\n");
    
    if (nVrtx <=-1)
        u_errexit("allocateDGraphData: empty vertex set\n");
    
    G->hollow  = (int * ) calloc(nVrtx + 2, sizeof(int));
    G->inStart  = (idxType * ) calloc(nVrtx + 2, sizeof(idxType));
    G->inEnd= (idxType * ) malloc(sizeof(idxType) * (nVrtx + 2));
    G->in = (idxType * ) malloc(sizeof(idxType) * nEdge);

    G->outStart  = (idxType * ) malloc(sizeof(idxType) * (nVrtx + 2));
    G->outEnd  = (idxType * ) malloc(sizeof(idxType) * (nVrtx + 2));
    G->out = (idxType * ) malloc(sizeof(idxType) * nEdge);
    
    G->vw = (vwType *) malloc(sizeof(vwType) * (nVrtx+1));
    //G->vw = (ecType * ) malloc(sizeof(ecType) * (nVrtx+1));
    
    G->ecIn = (ecType *) malloc(sizeof(ecType) * nEdge);
    G->ecOut = (ecType *) malloc(sizeof(ecType) * nEdge);
    
    fread(G->inStart,sizeof(G->inStart[0]),(nVrtx+2),fp);
    fread(G->inEnd,sizeof(G->inEnd[0]),(nVrtx+2),fp);
    fread(G->in,sizeof(G->in[0]),nEdge,fp);
    fread(G->vw,sizeof(G->vw[0]),(nVrtx+1),fp);
    fread(G->outStart,sizeof(G->outStart[0]),(nVrtx+2),fp);
    fread(G->outEnd,sizeof(G->outEnd[0]),(nVrtx+2),fp);
    fread(G->out,sizeof(G->out[0]),nEdge,fp);
    fread(G->ecIn,sizeof(ecType),nEdge,fp);
    fread(G->ecOut,sizeof(ecType),nEdge,fp);

    fclose(fp);
    fillOutFromIn(G);
    
}


////Anik
void generateDGraphFromDot(dgraph *G, char* file_name,int use_binary_input)
{
    printf("\n\n\n\ngenerateDgraph called\n\n\n\n");

	FILE* file;
    file = fopen(file_name, "r");

    FILE* my_file;
//    my_file = fopen("mtx/pagerank_sixteen_blk8_mod.dot","r");
//    my_file = fopen("mtx/pagerank_sixteen_blk8.dot","r");
//    my_file = fopen("mtx/pagerank_sixteen_blk8_vweight.dot","r");
//    my_file = fopen("mtx/pagerank_sixteen_blk8_vweight_1.dot","r");

//    my_file = file;
    my_file = fopen("mtx/1024_wblk_512.dot","r");
//    my_file = fopen("mtx/lanczos_1024_512blk.dot","r");


    char fnbin[100];
    strcpy(fnbin,file_name);
    strcat(fnbin,".bin");

    if(use_binary_input){
        FILE *fp = fopen(fnbin,"rb");
        if(fp!=NULL){
            fclose(fp);
            // printf("Yes-ish\n");
            generateDGraphFromBinaryDot(G,fnbin);
            return;
        }
        else if (strstr(file_name,".bin")){
            generateDGraphFromBinaryDot(G,file_name);
            return;
        }
        // printf("No bin file found for input!\n");
    }

    char line[1000];
    char token[100];
    idxType nVrtx = 0, nEdge = 0;
    idxType i, j;
    idxType* nbneighbors;
    idxType** inneighbors;


    idxType my_nVrtx = 0 , my_nEdge = 0 ;
    idxType* my_nbneighbors;
    idxType** my_inneighbors;
 //   ecType** my_inedgeweights;

//    ecType my_inedgeweights[1000][100];
    char vertices[1000][100];
 //   ecType edge_weights[1000];
 //   char* ew[10];
    double vweights[1000];

    double my_inedgeweights[1000][100];
    fgets(line,100,my_file);

    while(!feof(my_file)){
  //  	printf("\n\n\ninside my file while loop\n\n");
    	int my_v1,my_v2;
    	double edge_w;
    	fgets(line,100,my_file);
//    	printf("%s\n",line);
//	    char *pos = strstr(line, "{");
//	    if (pos != NULL) continue;
//	    pos = strstr(line, "}");
//	    if (pos != NULL) continue;
    	if(line[0] == '}')continue;



    	char *pos = strstr(line,"{");
    	if(pos != NULL){
    		////////// comma input testing/////////////
    		char ver1[20],ver2[20],ew[20];
    		i=1;
    		j=0;
    		while(line[i]!=')'){
//    			printf(" line[%d] = %c\n",i,line[i]);
    			ver1[j] = line[i];
    			i++;
    			j++;


    		}
    		ver1[j] = line[i];
    		ver1[++j] = '\0';
    		i++;
    		i++;
    		j=0;

    		while(line[i]!=')'){
 //   			printf(" line[%d] = %c\n",i,line[i]);
    			ver2[j] = line[i];
    			i++;
    			j++;
    		}
 //   		printf("after everything i = %d j = %d\n",i,j);
    		ver2[j] = line[i];
       		ver2[++j] = '\0';
   // 		printf("v1 = %s \n",ver1);
   // 		printf("v2 = %s \n",ver2);

       		j =0 ;
       		i += 2;
 //      		printf("line[%d] = %c\n",i,line[i]);
       		while(line[i] != '}'){
       			ew[j] = line[i];
//       			printf("ew[%d] = %c  line[%d] = %c\n",j,ew[j],i,line[i]);
       			i++;
       			j++;
       		}
 //      		ew[j] = line[i];
 //      		printf("j = %d\n",j);
       		ew[j] = '\0';
//       		printf("edge weight = %s len = %d %c %c\n",ew,strlen(ew),ew[0],ew[1]);
       		edge_w = atoi(ew);
//       		printf("edge weight int = %d\n",edge_w);



  //     		edge_weights[my_nEdge] = edge_w;

    		my_nEdge++;



	        if (my_nEdge == 1){
		        my_nbneighbors = (idxType*) calloc(my_nVrtx+1, sizeof(idxType));
		        my_inneighbors = (idxType**) malloc((my_nVrtx+1) * sizeof(idxType*));
	//	        my_inedgeweights = (ecType**) malloc((my_nVrtx+1) * sizeof(ecType*));
		        for (i = 0; i <= my_nVrtx; i++)
		            my_inneighbors[i] = (idxType*) malloc(100 * sizeof(idxType));
	//	        	my_inedgeweights[i] = (ecType*) malloc(100 * sizeof(ecType));
	        }


    		char* tok = strtok(line,"->");
//    		while(tok!=NULL){
//    			printf("%s\n",tok);
//    			tok = strtok(NULL,"->");
//    		}
    		for(i=0;i<my_nVrtx;i++){
    			if(!strcmp(ver1,vertices[i])){
    				my_v2 = i;
    				break;
    			}
    		}
    		tok = strtok(NULL,"->");
    		for(i=0;i<my_nVrtx;i++){
    			if(!strcmp(ver2,vertices[i])){
    				my_v1 = i;
    				break;
    			}
    		}


 //   		printf("v2 = %d v1 = %d cost = %lf\n",my_v2,my_v1,edge_w);


	        if ((my_nbneighbors[my_v1+1] + 1) % 100 == 0){
		        my_inneighbors[my_v1+1] = (idxType*) realloc (my_inneighbors[my_v1+1], (my_nbneighbors[my_v1+1] + 101) * sizeof(idxType));
		   //     my_inedgeweights[my_v1+1] = (ecType*) realloc (my_inedgeweights[my_v1+1], (my_nbneighbors[my_v1+1] + 101) * sizeof(ecType));

	        }
//	        printf("\n\ninneighbors[%d][%d] = %d inedgeweight[%d][%d] = %lf\n\n",my_v1+1,my_nbneighbors[my_v1+1],my_v2+1,my_v1+1,my_nbneighbors[my_v1+1],edge_w);

	        my_inneighbors[my_v1+1][my_nbneighbors[my_v1+1]] = my_v2+1;
	        my_inedgeweights[my_v1+1][my_nbneighbors[my_v1+1]] = edge_w;
	        my_nbneighbors[my_v1+1]++;
//	        printf("allocation done\n");



    	}
    	else{
    		my_nVrtx++;
    		double weight;
    		line[strlen(line)-2] = '\0';
    		printf("%s\n",line);
    		char *x;
    		char* v = strtok(line,"=");


    		strcpy(vertices[my_nVrtx-1],v);


    //		printf("i = %d str = %s\n",my_nVrtx-1,vertices[my_nVrtx-1]);
    		v = strtok(NULL,"=");
    		weight = atof(v);
    		vweights[my_nVrtx-1] = weight;
   // 		printf("i = %d str = %s weight = %lf\n",my_nVrtx-1,vertices[my_nVrtx-1],vweights[my_nVrtx-1]);

    	}


    }
    printf("my vertex count = %d\n",my_nVrtx);



    fclose(file);
    fclose(my_file);



    G->frmt = 2;
    G->nVrtx = my_nVrtx;
    G->nEdge = my_nEdge;
//    G->totvw = my_nVrtx;
    G->maxindegree = 0;
    G->hollow  = (int * ) calloc(my_nVrtx + 2, sizeof(int));
    G->inStart  = (idxType * ) calloc(my_nVrtx + 2, sizeof(idxType));
    G->inEnd= (idxType * ) malloc(sizeof(idxType) * (my_nVrtx + 2));
    G->in = (idxType * ) malloc(sizeof(idxType) * my_nEdge);

    G->ecIn = (ecType *) malloc(sizeof(ecType) * my_nEdge);
    G->ecOut = (ecType *) malloc(sizeof(ecType) * my_nEdge);

    //anik add
    G->vertices = (char**)malloc(sizeof(char*)*(my_nVrtx+1));
    for(i = 0;i<my_nVrtx;++i){
    	G->vertices[i] = (char*)malloc(50*sizeof(char));
    }

    idxType idx = 0, degree;

 //   printf("edge weight graph %lf\n",my_inedgeweights[2][0]);

    for (i=1; i<=my_nVrtx; i++){
	    G->inStart[i] = idx;
	    G->inEnd[i-1] = idx-1;
	    if (i>1){
	        degree = G->inEnd[i-1] - G->inStart[i-1] + 1;
	        G->maxindegree = G->maxindegree < degree ? degree : G->maxindegree;
	    }
	    for (j=0; j< my_nbneighbors[i]; j++){
	//    	printf("i = %d j = %d idx = %d myedgeweight = %lf\n",i,j,idx,my_inedgeweights[i][j]);
	        G->in[idx] = my_inneighbors[i][j];
	        G->ecIn[idx] = my_inedgeweights[i][j];
	        idx++;
	    }
    }
    G->inStart[0] = 0;
    G->inEnd[0] = -1;
    G->inEnd[my_nVrtx] = idx-1;
    G->inEnd[my_nVrtx+1] = my_nEdge;
    G->inStart[my_nVrtx+1] = my_nEdge;
    degree = G->inEnd[my_nVrtx] - G->inStart[my_nVrtx] + 1;
    G->maxindegree = G->maxindegree < degree ? degree : G->maxindegree;
    if (my_nEdge <= 0)
        u_errexit("allocateDGraphData: empty edge set\n");
    
    if (my_nVrtx <=-1)
        u_errexit("allocateDGraphData: empty vertex set\n");

    G->outStart  = (idxType * ) malloc(sizeof(idxType) * (my_nVrtx + 2));
    G->outEnd  = (idxType * ) malloc(sizeof(idxType) * (my_nVrtx + 2));
    G->out = (idxType * ) malloc(sizeof(idxType) * my_nEdge);
    
    G->vw = (vwType *) malloc(sizeof(vwType) * (my_nVrtx+1));
    G->totvw = 0;
    for (i=1; i<=my_nVrtx; i++){
	    G->vw[i] = vweights[i-1];
	    G->totvw += G->vw[i];
//	    printf("G.vw[%d] = %lf totvw = %lf\n",i,G->vw[i],G->totvw);
    }

    for (i=0; i< my_nEdge; i++){
	    G->ecIn[i] = 1.0;
	    G->ecOut[i] = 1.0;
//    	G->ecIn[i] = edge_weights[i];
//    	G->ecOut[i] = edge_weights[i];
    }

 //   for(i=1;i<=my_nVrtx;++i){
 //   	for(j=G->ecIn[])
//    }

    for(i=0;i<my_nVrtx;++i){
 //   	printf("vertices %d = %s\n",i,vertices[i]);
    	strcpy(G->vertices[i],vertices[i]);
    }

    for(i = 0 ; i < G->nEdge ; i++){
 //   	printf("ecIn[%d] = %lf\n",i,G->ecIn[i]);
    }

 //   printf("before fillout for min\n");
//    for(i = 0 ; i < G->nEdge ; i++){
//    	printf("out[%d] = %d\n",i,G->out[i]);
//    }


    fillOutFromIn(G);
    
    if(use_binary_input){
        FILE *fp = fopen(fnbin,"wb");

        if(fp == NULL){
           u_errexit("Cannot open file");
        }

        fwrite(&G->frmt,sizeof(G->frmt),1,fp);
        fwrite(&G->nVrtx,sizeof(G->nVrtx),1,fp);
        fwrite(&G->nEdge,sizeof(G->nEdge),1,fp);
        fwrite(&G->totvw,sizeof(G->totvw),1,fp);
        fwrite(&G->maxindegree,sizeof(G->maxindegree),1,fp);
        fwrite(&G->maxoutdegree,sizeof(G->maxoutdegree),1,fp);
        fwrite(G->inStart,sizeof(G->inStart[0]),(nVrtx+2),fp);
        fwrite(G->inEnd,sizeof(G->inEnd[0]),(nVrtx+2),fp);
        fwrite(G->in,sizeof(G->in[0]),nEdge,fp);
        fwrite(G->vw,sizeof(G->vw[0]),(nVrtx+1),fp);
        fwrite(G->outStart,sizeof(G->outStart[0]),(nVrtx+2),fp);
        fwrite(G->outEnd,sizeof(G->outEnd[0]),(nVrtx+2),fp);
        fwrite(G->out,sizeof(G->out[0]),nEdge,fp);
        fwrite(G->ecIn,sizeof(ecType),nEdge,fp);
        fwrite(G->ecOut,sizeof(ecType),nEdge,fp);
        
        //anik add
        fwrite(G->vertices,sizeof(G->vertices[0]),(nVrtx+1),fp);

        fclose(fp);
    }
/*
    for(i=0;i<my_nVrtx+2;++i){
 //   	printf("G1->inStart[%d] = %d\n",i,G->inStart[i]);
    }
    for(i=0;i<my_nVrtx+2;++i){
//    	printf("G1->inEnd[%d] = %d\n",i,G->inEnd[i]);
    }
    for(i=0;i<my_nEdge;++i){
 //   	printf("G1->in[%d] = %d\n",i,G->in[i]);
    }
    for(i=0;i<my_nVrtx+2;++i){
 //   	printf("G1->inStart[%d] = %d\n",i,G->outStart[i]);
    }
    for(i=0;i<my_nVrtx+2;++i){
 //   	printf("G1->inEnd[%d] = %d\n",i,G->outEnd[i]);
    }
    for(i=0;i<my_nEdge;++i){
 //   	printf("G1->in[%d] = %d\n",i,G->out[i]);
    }
*/


    for (i = 1; i <= nVrtx; i++){
        free(inneighbors[i]);

    }
    free(nbneighbors);
    free(inneighbors);

}


///////////////////////////////////////my graph generator anik////////////


void my_generate_graph(dgraph *G, char* file_name,int use_binary_input)
	{
	    printf("\n\n\n\ngenerateDgraph called\n\n\n\n");

		FILE* file;
	    file = fopen(file_name, "r");

	    FILE* my_file;
	//    my_file = fopen("mtx/pagerank_sixteen_blk8_mod.dot","r");
	//    my_file = fopen("mtx/pagerank_sixteen_blk8.dot","r");
	//    my_file = fopen("mtx/pagerank_sixteen_blk8_vweight.dot","r");
	//    my_file = fopen("mtx/pagerank_sixteen_blk8_vweight_1.dot","r");

	//    my_file = file;
	//    my_file = fopen("mtx/1024_wblk_512.dot","r");
	//    my_file = fopen("mtx/lanczos_1024_512blk.dot","r");
	//    my_file = fopen("mtx/graph_fazlay.dot","r");
	//    my_file = fopen("mtx/graph_blk50.txt","r");
	//      my_file = fopen("mtx/graph_blk25.txt","r");
	//      my_file = fopen("mtx/graph_csb_blk50.txt","r");


	    //large graphs
//	    my_file = fopen("mtx/graph_1M1M_16_blk4k.txt","r");
//	    my_file = fopen("mtx/graph_20k20k_8_blksz_1k.txt","r");
	    my_file = fopen("mtx/graph_1M1M_16_blk32k.txt","r");


	    char fnbin[100];
	    strcpy(fnbin,file_name);
	    strcat(fnbin,".bin");

	    if(use_binary_input){
	        FILE *fp = fopen(fnbin,"rb");
	        if(fp!=NULL){
	            fclose(fp);
	            // printf("Yes-ish\n");
	            generateDGraphFromBinaryDot(G,fnbin);
	            return;
	        }
	        else if (strstr(file_name,".bin")){
	            generateDGraphFromBinaryDot(G,file_name);
	            return;
	        }
	        // printf("No bin file found for input!\n");
	    }

	    char line[1000];
	    char token[200];
	    idxType nVrtx = 0, nEdge = 0;
	    idxType i, j;
	    idxType* nbneighbors;
	    idxType** inneighbors;


	    idxType my_nVrtx = 0 , my_nEdge = 0 ;
	    idxType* my_nbneighbors;
	    idxType** my_inneighbors;
	    char vertices[12000][100];
	//    ecType edge_weights[1000];
	 //   char* ew[10];
	    double vweights[12000];
	    ecType** my_inedgeweights;
	//    ecType my_inedgeweights[12000][10];



	    fgets(line,300,my_file);

	    while(!feof(my_file)){
	  //  	printf("\n\n\ninside my file while loop\n\n");
	    	int my_v1,my_v2;
	    	double edge_w;
	    	fgets(line,300,my_file);
	//    	printf("%s\n",line);
	//	    char *pos = strstr(line, "{");
	//	    if (pos != NULL) continue;
	//	    pos = strstr(line, "}");
	//	    if (pos != NULL) continue;
	    	if(line[0] == '}')continue;



	    	char *pos = strstr(line,"{");
	    	if(pos != NULL){
	    		////////// comma input testing/////////////
	    		char ver1[100],ver2[200],ew[100];
	    		i=1;
	    		j=0;





	    		my_nEdge++;

	    		char* nodes = strtok(line,",");
	    		for(i=1;i<strlen(nodes);i++){
	    			ver1[i-1] = nodes[i];
	    		}
	    		ver1[i-1] = '\0';
	//    		printf("ver1 = %s\n ",ver1);

	    		nodes = strtok(NULL,",");
	    		strcpy(ver2,nodes);
	//    		printf("ver2 = %s\n",nodes);
	    		nodes = strtok(NULL,",");

	    		//nodes[strlen(ew)-2] = '\0';
	    		strcpy(ew,nodes);
	    		ew[strlen(ew)-3] = '\0';
	//    		printf("ew = %s\n",ew);

	    		edge_w = atof(ew);
//	    		edge_weights[my_nEdge] = edge_w;


/*

		        if (my_nEdge == 1){
			        my_nbneighbors = (idxType*) calloc(my_nVrtx+1, sizeof(idxType));
			        my_inneighbors = (idxType**) malloc((my_nVrtx+1) * sizeof(idxType*));
			        for (i = 1; i <= my_nVrtx; i++)
			            my_inneighbors[i] = (idxType*) malloc(100 * sizeof(idxType));
		        }


	    		char* tok = strtok(line,"->");
	//    		while(tok!=NULL){
	//    			printf("%s\n",tok);
	//    			tok = strtok(NULL,"->");
	//    		}
	    		for(i=0;i<my_nVrtx;i++){
	    			if(!strcmp(ver1,vertices[i])){
	    				my_v2 = i;
	    				break;
	    			}
	    		}
	    		tok = strtok(NULL,"->");
	    		for(i=0;i<my_nVrtx;i++){
	    			if(!strcmp(ver2,vertices[i])){
	    				my_v1 = i;
	    				break;
	    			}
	    		}

	    		printf("v2 = %d v1 = %d cost = %lf\n",my_v2,my_v1,edge_w);



		        if ((my_nbneighbors[my_v1+1] + 1) % 100 == 0)
			        my_inneighbors[my_v1+1] = (idxType*) realloc (my_inneighbors[my_v1+1], (my_nbneighbors[my_v1+1] + 101) * sizeof(idxType));
	//	        printf("\n\ninneighbors[%d][%d] = %d\n\n",v1+1,nbneighbors[v1+1],v2+1);
		        my_inneighbors[my_v1+1][my_nbneighbors[my_v1+1]++] = my_v2+1;

*/
		        if (my_nEdge == 1){
			        my_nbneighbors = (idxType*) calloc(my_nVrtx+1, sizeof(idxType));
			        my_inneighbors = (idxType**) malloc((my_nVrtx+1) * sizeof(idxType*));
			        my_inedgeweights = (ecType**) malloc((my_nVrtx+1) * sizeof(ecType*));
			        for (i = 0; i <= my_nVrtx; i++){
			            my_inneighbors[i] = (idxType*) malloc(100 * sizeof(idxType));
			        	my_inedgeweights[i] = (ecType*) malloc(100 * sizeof(ecType));
			        }
		        }


	    		char* tok = strtok(line,"->");
	//    		while(tok!=NULL){
	//    			printf("%s\n",tok);
	//    			tok = strtok(NULL,"->");
	//    		}
	    		for(i=0;i<my_nVrtx;i++){
	    			if(!strcmp(ver1,vertices[i])){
	    				my_v2 = i;
	    				break;
	    			}
	    		}
	    		tok = strtok(NULL,"->");
	    		for(i=0;i<my_nVrtx;i++){
	    			if(!strcmp(ver2,vertices[i])){
	    				my_v1 = i;
	    				break;
	    			}
	    		}


	 //   		printf("v2 = %d v1 = %d cost = %lf\n",my_v2,my_v1,edge_w);


		        if ((my_nbneighbors[my_v1+1] + 1) % 100 == 0){
			        my_inneighbors[my_v1+1] = (idxType*) realloc (my_inneighbors[my_v1+1], (my_nbneighbors[my_v1+1] + 101) * sizeof(idxType));
			        my_inedgeweights[my_v1+1] = (ecType*) realloc (my_inedgeweights[my_v1+1], (my_nbneighbors[my_v1+1] + 101) * sizeof(ecType));

		        }
	//	        printf("\n\ninneighbors[%d][%d] = %d inedgeweight[%d][%d] = %lf\n\n",my_v1+1,my_nbneighbors[my_v1+1],my_v2+1,my_v1+1,my_nbneighbors[my_v1+1],edge_w);

		        my_inneighbors[my_v1+1][my_nbneighbors[my_v1+1]] = my_v2+1;
		        my_inedgeweights[my_v1+1][my_nbneighbors[my_v1+1]] = edge_w;
		        my_nbneighbors[my_v1+1]++;

	    	}


	    	else{
	    		my_nVrtx++;
	    		double weight;
	    		line[strlen(line)-2] = '\0';
	   // 		printf("%s\n",line);
	    		char *x;
	    		char v[100];
	    		strcpy(v,line);


	    		strcpy(vertices[my_nVrtx-1],v);


//	    		printf("i = %d str = %s\n",my_nVrtx-1,vertices[my_nVrtx-1]);
//	    		v = strtok(NULL,"=");
//	    		weight = atof(v);
	    		weight = 1.0;
	    		vweights[my_nVrtx-1] = weight;
//	    		printf("i = %d str = %s weight = %lf\n",my_nVrtx-1,vertices[my_nVrtx-1],vweights[my_nVrtx-1]);

	    	}


	    }
	    printf("my vertex count = %d\n",my_nVrtx);



	    fclose(file);
	    fclose(my_file);

	    for(i = 1; i <= my_nVrtx ; i++){
	    //	printf("nbneighbors[%d] = %d\n",i,my_nbneighbors[i]);
	    }



	    G->frmt = 2;
	    G->nVrtx = my_nVrtx;
	    G->nEdge = my_nEdge;
	//    G->totvw = my_nVrtx;
	    G->maxindegree = 0;
	    G->hollow  = (int * ) calloc(my_nVrtx + 2, sizeof(int));
	    G->inStart  = (idxType * ) calloc(my_nVrtx + 2, sizeof(idxType));
	    G->inEnd= (idxType * ) malloc(sizeof(idxType) * (my_nVrtx + 2));
	    G->in = (idxType * ) malloc(sizeof(idxType) * my_nEdge);

	    //anik add
	    G->vertices = (char**)malloc(sizeof(char*)*(my_nVrtx+1));
	    for(i = 0;i<my_nVrtx;++i){
	    	G->vertices[i] = (char*)malloc(50*sizeof(char));
	    }
	    G->ecIn = (ecType *) malloc(sizeof(ecType) * my_nEdge);
	    G->ecOut = (ecType *) malloc(sizeof(ecType) * my_nEdge);

	    idxType idx = 0, degree;

	    for (i=1; i<=my_nVrtx; i++){
		    G->inStart[i] = idx;
		    G->inEnd[i-1] = idx-1;
		    if (i>1){
		        degree = G->inEnd[i-1] - G->inStart[i-1] + 1;
		        G->maxindegree = G->maxindegree < degree ? degree : G->maxindegree;
		    }
		    for (j=0; j< my_nbneighbors[i]; j++){
		        G->in[idx] = my_inneighbors[i][j];
		        G->ecIn[idx] = my_inedgeweights[i][j];
		    //    printf("node = %d inneighbor[%d][%d] = %d inedgeweight[%d][%d] = %lf\n",i,i,j, my_inneighbors[i][j],i,j,my_inedgeweights[i][j]);
		        idx++;
		    }

	    }
	    printf("maxindegree = %d\n",G->maxindegree);
	    G->inStart[0] = 0;
	    G->inEnd[0] = -1;
	    G->inEnd[my_nVrtx] = idx-1;
	    G->inEnd[my_nVrtx+1] = my_nEdge;
	    G->inStart[my_nVrtx+1] = my_nEdge;
	    degree = G->inEnd[my_nVrtx] - G->inStart[my_nVrtx] + 1;
	    G->maxindegree = G->maxindegree < degree ? degree : G->maxindegree;
	    if (my_nEdge <= 0)
	        u_errexit("allocateDGraphData: empty edge set\n");

	    if (my_nVrtx <=-1)
	        u_errexit("allocateDGraphData: empty vertex set\n");

	    G->outStart  = (idxType * ) malloc(sizeof(idxType) * (my_nVrtx + 2));
	    G->outEnd  = (idxType * ) malloc(sizeof(idxType) * (my_nVrtx + 2));
	    G->out = (idxType * ) malloc(sizeof(idxType) * my_nEdge);

	    G->vw = (vwType *) malloc(sizeof(vwType) * (my_nVrtx+1));
	    G->totvw = 0;
	    for (i=1; i<=my_nVrtx; i++){
		    G->vw[i] = vweights[i-1];
		    G->totvw += G->vw[i];
	//	    printf("G.vw[%d] = %lf totvw = %lf\n",i,G->vw[i],G->totvw);
	    }

	    for (i=0; i< my_nEdge; i++){
	//	    G->ecIn[i] = 1;
	//	    G->ecOut[i] = 1;
	//    	G->ecIn[i] = edge_weights[i];
	//    	G->ecOut[i] = edge_weights[i];
	    }

	 //   for(i=1;i<=my_nVrtx;++i){
	 //   	for(j=G->ecIn[])
	//    }

	    for(i=0;i<my_nVrtx;++i){
	 //   	printf("vertices %d = %s\n",i,vertices[i]);
	    	strcpy(G->vertices[i],vertices[i]);
	    }

//	    for(i = 0 ; i < G->nEdge ; i++){
//	    	printf("ecIn[%d] = %lf\n",i,G->ecIn[i]);
//	    }

	 //   printf("before fillout for min\n");
	//    for(i = 0 ; i < G->nEdge ; i++){
	//    	printf("out[%d] = %d\n",i,G->out[i]);
	//    }



	    fillOutFromIn(G);

	    if(use_binary_input){
	        FILE *fp = fopen(fnbin,"wb");

	        if(fp == NULL){
	           u_errexit("Cannot open file");
	        }

	        fwrite(&G->frmt,sizeof(G->frmt),1,fp);
	        fwrite(&G->nVrtx,sizeof(G->nVrtx),1,fp);
	        fwrite(&G->nEdge,sizeof(G->nEdge),1,fp);
	        fwrite(&G->totvw,sizeof(G->totvw),1,fp);
	        fwrite(&G->maxindegree,sizeof(G->maxindegree),1,fp);
	        fwrite(&G->maxoutdegree,sizeof(G->maxoutdegree),1,fp);
	        fwrite(G->inStart,sizeof(G->inStart[0]),(nVrtx+2),fp);
	        fwrite(G->inEnd,sizeof(G->inEnd[0]),(nVrtx+2),fp);
	        fwrite(G->in,sizeof(G->in[0]),nEdge,fp);
	        fwrite(G->vw,sizeof(G->vw[0]),(nVrtx+1),fp);
	        fwrite(G->outStart,sizeof(G->outStart[0]),(nVrtx+2),fp);
	        fwrite(G->outEnd,sizeof(G->outEnd[0]),(nVrtx+2),fp);
	        fwrite(G->out,sizeof(G->out[0]),nEdge,fp);
	        fwrite(G->ecIn,sizeof(ecType),nEdge,fp);
	        fwrite(G->ecOut,sizeof(ecType),nEdge,fp);

	        //anik add
	        fwrite(G->vertices,sizeof(G->vertices[0]),(nVrtx+1),fp);

	        fclose(fp);
	    }

//	    for(i=0;i<my_nVrtx+2;++i){
//	    	printf("G1->inStart[%d] = %d\n",i,G->inStart[i]);
//	    }
//	    for(i=0;i<my_nVrtx+2;++i){
//	    	printf("G1->inEnd[%d] = %d\n",i,G->inEnd[i]);
//	    }
//	    for(i=0;i<my_nEdge;++i){
//	    	printf("G1->in[%d] = %d\n",i,G->in[i]);
//	    }
//	    for(i=0;i<my_nVrtx+2;++i){
//	    	printf("G1->inStart[%d] = %d\n",i,G->outStart[i]);
//	    }
//	    for(i=0;i<my_nVrtx+2;++i){
//	    	printf("G1->inEnd[%d] = %d\n",i,G->outEnd[i]);
//	    }
//	    for(i=0;i<my_nEdge;++i){
//	    	printf("G1->in[%d] = %d\n",i,G->out[i]);
//	    }
//




	    for (i = 1; i <= nVrtx; i++)
	        free(inneighbors[i]);
	    free(nbneighbors);
	    free(inneighbors);


}



//////////////////////////////////////////////////

void generateDGraphFromDotWithPart(dgraph *G, char* file_name, idxType* part)
{
    FILE* file;
    file = fopen(file_name, "r");
    char line[200];
    idxType nVrtx = 0, nEdge = 0;
    idxType i, j;
    idxType* nbneighbors;
    idxType** inneighbors;

    while(!feof(file)){
        fgets(line, 200, file);
        char *pos = strstr(line, "{");
        if (pos != NULL) continue;
        pos = strstr(line, "}");
        if (pos != NULL) continue;
        pos = strstr(line, "->");
        if (pos != NULL){
            nEdge++;
            if (nEdge == 1){
                nbneighbors = (idxType*) calloc(nVrtx+1, sizeof(idxType));
                inneighbors = (idxType**) malloc((nVrtx+1) * sizeof(idxType*));
                for (i = 1; i <= nVrtx; i++)
                    inneighbors[i] = (idxType*) malloc(100 * sizeof(idxType));
            }
            char* token = strtok(line, "->");
            int v1 = atoi(token);
            token = strtok(NULL, "->");
            int v2 = atoi(token);
            //printf("%d -> %d\n", v2, v1);
            if ((nbneighbors[v1+1] + 1) % 100 == 0)
                inneighbors[v1+1] = (idxType*) realloc (inneighbors[v1+1], (nbneighbors[v1+1] + 101) * sizeof(idxType));
            inneighbors[v1+1][nbneighbors[v1+1]++] = v2+1;
        }
        else
            nVrtx++;
    }
    fclose(file);

    G->frmt = 3;
    G->nVrtx = nVrtx;
    G->nEdge = nEdge;
    G->totvw = nVrtx;
    G->maxindegree = 0;
    G->inStart  = (idxType * ) calloc(nVrtx + 2, sizeof(idxType));
    G->inEnd= (idxType * ) malloc(sizeof(idxType) * (nVrtx + 2));
    G->in = (idxType * ) malloc(sizeof(idxType) * nEdge);
    idxType idx = 0, degree;

    for (i=1; i<=nVrtx; i++){
        G->inStart[i] = idx;
        G->inEnd[i-1] = idx-1;
        if (i>1){
            degree = G->inEnd[i-1] - G->inStart[i-1] + 1;
            G->maxindegree = G->maxindegree < degree ? degree : G->maxindegree;
        }
        for (j=0; j< nbneighbors[i]; j++){
            G->in[idx++] = inneighbors[i][j];
        }
    }

    degree = G->inEnd[nVrtx] - G->inStart[nVrtx] + 1;
    G->maxindegree = G->maxindegree < degree ? degree : G->maxindegree;
    G->inStart[0] = 0;
    G->inEnd[0] = -1;
    G->inEnd[nVrtx] = idx-1;
    G->inEnd[nVrtx+1] = nEdge;
    G->inStart[nVrtx+1] = nEdge;

    if (nEdge <= 0)
        u_errexit("allocateDGraphData: empty edge set\n");

    if (nVrtx <=-1)
        u_errexit("allocateDGraphData: empty vertex set\n");

    G->outStart  = (idxType * ) malloc(sizeof(idxType) * (nVrtx + 2));
    G->outEnd  = (idxType * ) malloc(sizeof(idxType) * (nVrtx + 2));
    G->out = (idxType * ) malloc(sizeof(idxType) * nEdge);

    G->vw = (vwType *) malloc(sizeof(vwType) * (nVrtx+1));
    //G->vw = (ecType * ) malloc(sizeof(ecType) * (nVrtx+1));
    for (i=1; i<=nVrtx; i++)
        G->vw[i] = 1;
    G->ecIn = (ecType *) malloc(sizeof(ecType) * nEdge);
    G->ecOut = (ecType *) malloc(sizeof(ecType) * nEdge);
    for (i=0; i< nEdge; i++){
        G->ecIn[i] = 1;
        G->ecOut[i] = 1;
    }


    fillOutFromIn(G);
    for (i = 1; i <= nVrtx; i++)
        free(inneighbors[i]);
    free(nbneighbors);
    free(inneighbors);
}

void reverseDgraph(dgraph *G){

}


void sortOutNeighboursWeight(dgraph* G, idxType node, idxType* outnodes){
    idxType i,j, idx=0, tmp;
    for (i=G->outStart[node]; i<=G->outEnd[node]; i++)
	outnodes[idx++] = i;
    for (i=1; i<idx; i++){
	for (j=i; j>=1; j--){
	    if (G->ecOut[outnodes[j]] > G->ecOut[outnodes[j-1]]){
		tmp = outnodes[j];
		outnodes[j] = outnodes[j-1];
		outnodes[j-1] = tmp;
	    }
	    else
		break;
	}
    }
    for (i=0; i<idx; i++){
	outnodes[i] = G->out[outnodes[i]];
    }
}


void my_sortNeighbourWithWeight(dgraph* G, idxType node, idxType* sorted_outnodes){
    idxType i,j, idx=0, tmp;
    for (i=G->outStart[node]; i<=G->outEnd[node]; i++)
    sorted_outnodes[idx++] = i;
    for (i=1; i<idx; i++){
    for (j=i; j>=1; j--){
        if (G->ecOut[sorted_outnodes[j]] > G->ecOut[sorted_outnodes[j-1]]){
        tmp = sorted_outnodes[j];
        sorted_outnodes[j] = sorted_outnodes[j-1];
        sorted_outnodes[j-1] = tmp;
        }
        else
        break;
    }
    }
    for (i=0; i<idx; i++){
        sorted_outnodes[i] = G->out[sorted_outnodes[i]];
    }
}

void sortOutNeighboursRatio(dgraph* G, idxType node, idxType* outnodes){
    idxType i,j, idx=0, tmp;
    for (i=G->outStart[node]; i<=G->outEnd[node]; i++)
	    outnodes[idx++] = i;
    for (i=1; i<idx; i++){
	    for (j=i; j>=1; j--){
	        if (G->ecOut[outnodes[j]]/G->vw[G->out[outnodes[j]]] > G->ecOut[outnodes[j-1]]/G->vw[G->out[outnodes[j-1]]]){
		        tmp = outnodes[j];
		        outnodes[j] = outnodes[j-1];
		        outnodes[j-1] = tmp;
	        }
	        else
		        break;
	    }
    }
    for (i=0; i<idx; i++){
	    outnodes[i] = G->out[outnodes[i]];
    }
}




///// added by anik

void my_DFS(dgraph* G, idxType *visited){
	idxType* active_nodes = (idxType*)malloc(sizeof(idxType) * G->nVrtx);
	int i;
	int active_nodes_count;
	int out_vertex;
	int* already_visited = (int*)calloc(G->nVrtx+1,sizeof(int));


	active_nodes_count = sourcesList(G,active_nodes);

	int visited_head = 1;

	while(active_nodes_count > 0){
		if(visited_head>G->nVrtx){
			printf("head > vertex count, something wrong\n");
			break;
		}

		int curr = active_nodes[active_nodes_count-1];
		active_nodes_count--;
		visited[visited_head++] = curr;
//		printf("visited[%d] = %d\n",visited_head-1,visited[visited_head-1]);
		for(i = G->outStart[curr] ; i<=G->outEnd[curr] ; ++i){
			out_vertex = G->out[i];
			if(already_visited[out_vertex] == 1)
				continue;
			active_nodes[active_nodes_count++] = out_vertex;
			already_visited[out_vertex] = 1;

		}

	}
	if(visited_head != (G->nVrtx+1))
		printf("graph is disconnected\n");

	free(active_nodes);
	free(already_visited);



}

// my_generate_graph fazlay

void my_generate_graph_fazlay(dgraph *G, char* file_name, int use_binary_input, int *edge_u, int *edge_v, double *edge_weight, int edgeCount, int vertexCount,const char** vertexName,double* vertexWeight)
{
        printf("\n\n\n\ngenerateDgraph called\n\n\n\n");

        //printf("\n\n%s\n\n",vertexName[0]);

        char fnbin[100];
        strcpy(fnbin,file_name);
        strcat(fnbin,".bin");

        if(use_binary_input){
            FILE *fp = fopen(fnbin,"rb");
            if(fp!=NULL){
                fclose(fp);
                // printf("Yes-ish\n");
                generateDGraphFromBinaryDot(G,fnbin);
                return;
            }
            else if (strstr(file_name,".bin")){
                generateDGraphFromBinaryDot(G,file_name);
                return;
            }
            // printf("No bin file found for input!\n");
        }

        char line[1000];
        char token[200];
        idxType nVrtx = 0, nEdge = 0;
        idxType i, j, k = 0 ;
        idxType* nbneighbors; //how many neighbours of each vertex
        idxType** inneighbors; //adjacency list (in-neighbours) , parent tracking


        idxType my_nVrtx = vertexCount , my_nEdge = 0 ;
        idxType* my_nbneighbors;
        idxType** my_inneighbors;
        //char vertices[12000][100]; //vertex name
        //double vweights[12000]; 
       // ecType my_inedgeweights[12000][10]; //ecType double incoming edge weights
        ecType** my_inedgeweights;

        my_nbneighbors = (idxType*) calloc((vertexCount+1), sizeof(idxType));
//        my_nbneighbors = (idxType*) malloc((vertexCount+1) * sizeof(idxType));
        my_inneighbors = (idxType**) malloc((vertexCount+1) * sizeof(idxType*));
        my_inedgeweights = (ecType**) malloc((my_nVrtx+1) * sizeof(ecType*));
        
        for (i = 0; i <= my_nVrtx; i++)
        {
            my_inneighbors[i] = (idxType*) malloc(100 * sizeof(idxType)); //say ever vertex has max 100 parents
        	my_inedgeweights[i] = (ecType*) malloc(100 * sizeof(ecType));
        }

        while(k < edgeCount)
        {
      
            int my_v1, my_v2; //(my_v1, my_v2) edge
            double edge_w;
           
            
            my_v2 = edge_u[k];
            my_v1 = edge_v[k];
            edge_w = edge_weight[k];

            my_nEdge++;

            if ((my_nbneighbors[my_v1+1] + 1) % 100 == 0)
            {
                my_inneighbors[my_v1+1] = (idxType*) realloc (my_inneighbors[my_v1+1], (my_nbneighbors[my_v1+1] + 101) * sizeof(idxType));
		        my_inedgeweights[my_v1+1] = (ecType*) realloc (my_inedgeweights[my_v1+1], (my_nbneighbors[my_v1+1] + 101) * sizeof(ecType));
            }
    

            my_inneighbors[my_v1+1][my_nbneighbors[my_v1+1]] = my_v2+1;
            my_inedgeweights[my_v1+1][my_nbneighbors[my_v1+1]] = edge_w;
            my_nbneighbors[my_v1+1]++;
            
            k++;
        }
        printf("my vertex count = %d my edge count= %d\n", vertexCount, my_nEdge);

        //exit(1);

        G->frmt = 2;
        G->nVrtx = my_nVrtx;
        G->nEdge = my_nEdge;
        G->totvw = my_nVrtx;
        G->maxindegree = 0;
        G->hollow  = (int * ) calloc(my_nVrtx + 2, sizeof(int));
        G->inStart  = (idxType * ) calloc(my_nVrtx + 2, sizeof(idxType));
        G->inEnd= (idxType * ) malloc(sizeof(idxType) * (my_nVrtx + 2));
        G->in = (idxType * ) malloc(sizeof(idxType) * my_nEdge);

        //anik add
        G->vertices = (char**)malloc(sizeof(char*)*(my_nVrtx+1));
        for(i = 0;i<my_nVrtx;++i){
            G->vertices[i] = (char*)malloc(100*sizeof(char));
        }
        G->ecIn = (ecType *) malloc(sizeof(ecType) * my_nEdge);
        G->ecOut = (ecType *) malloc(sizeof(ecType) * my_nEdge);

        idxType idx = 0, degree;

        for (i=1; i<=my_nVrtx; i++){
            G->inStart[i] = idx;
            G->inEnd[i-1] = idx-1;
            if (i>1){
                degree = G->inEnd[i-1] - G->inStart[i-1] + 1;
                G->maxindegree = G->maxindegree < degree ? degree : G->maxindegree;
            }
            for (j=0; j< my_nbneighbors[i]; j++){
                G->in[idx] = my_inneighbors[i][j];
                G->ecIn[idx] = my_inedgeweights[i][j];
                idx++;
            }

        }
        G->inStart[0] = 0;
        G->inEnd[0] = -1;
        G->inEnd[my_nVrtx] = idx-1;
        G->inEnd[my_nVrtx+1] = my_nEdge;
        G->inStart[my_nVrtx+1] = my_nEdge;
        degree = G->inEnd[my_nVrtx] - G->inStart[my_nVrtx] + 1;
        G->maxindegree = G->maxindegree < degree ? degree : G->maxindegree;
        if (my_nEdge <= 0)
            u_errexit("allocateDGraphData: empty edge set\n");

        if (my_nVrtx <=-1)
            u_errexit("allocateDGraphData: empty vertex set\n");

        G->outStart  = (idxType * ) malloc(sizeof(idxType) * (my_nVrtx + 2));
        G->outEnd  = (idxType * ) malloc(sizeof(idxType) * (my_nVrtx + 2));
        G->out = (idxType * ) malloc(sizeof(idxType) * my_nEdge);

        G->vw = (vwType *) malloc(sizeof(vwType) * (my_nVrtx+1));
        G->vWeight = (ecType*)malloc(sizeof(ecType)*(my_nVrtx+1));
    //    G->totvw = 0;
        for (i=1; i<=my_nVrtx; i++){
      //      G->vw[i] = vweights[i-1];
            G->vw[i] = 1.0;
     //       G->totvw += G->vw[i];
    //      printf("G.vw[%d] = %lf totvw = %lf\n",i,G->vw[i],G->totvw);
        }


        ////// incoming edge weight needed for partitioning
        G->incoming_edge_weight = (ecType*)calloc((my_nVrtx+1),sizeof(ecType));

        for (i=0; i< my_nEdge; i++){
    //      G->ecIn[i] = 1;
    //      G->ecOut[i] = 1;
    //      G->ecIn[i] = edge_weights[i];
    //      G->ecOut[i] = edge_weights[i];
        }

     //   for(i=1;i<=my_nVrtx;++i){
     //     for(j=G->ecIn[])
    //    }

        for(i=0;i<my_nVrtx;++i){
     
            strcpy(G->vertices[i],vertexName[i]);
            //printf("vertices %d = %s\n",i,G->vertices[i]);
            G->vWeight[i+1] = vertexWeight[i];
            //printf("vWeight[%d] = %lf\n",i+1,G->vWeight[i+1]);
        }

//      for(i = 0 ; i < G->nEdge ; i++){
//          printf("ecIn[%d] = %lf\n",i,G->ecIn[i]);
//      }

     //   printf("before fillout for min\n");
    //    for(i = 0 ; i < G->nEdge ; i++){
    //      printf("out[%d] = %d\n",i,G->out[i]);
    //    }

        // FILE* indegree_file;
        // indegree_file = fopen("indegree.txt","w");
        // for(i = 1 ; i <= my_nVrtx ; i++){
        //     fprintf(indegree_file, "%s %d\n", G->vertices[i-1],G->inEnd[i] - G->inStart[i]);
        // }


        // fclose(indegree_file);

        printf("before calling filloutfrom\n");

        FILE* entire_graph;
        entire_graph = fopen("entire_graph.txt","w");
        for(i = 1 ; i <= my_nVrtx ; i++){
            for (j = G->inStart[i]; j <= G->inEnd[i]; ++j)
            {
                /* code */
                fprintf(entire_graph, "%s ---> %s\n", G->vertices[G->in[j]-1],G->vertices[i-1]);
            }
        }
        fclose(entire_graph);
        
        fillOutFromIn(G);

    // FILE *initial_graph;
    // initial_graph = fopen("initial_graph.dot","w");
    // fprintf(initial_graph,"digraph {\n");

    // for (i = 1;i<=G->nVrtx ; i++){
    //     for(j = G->inStart[i] ; j <= G->inEnd[i]; j++){
    //         //printf("%d ---> %d\n",G->in[j],i);
    //         fprintf(initial_graph,"%d -> %d;\n",G->in[j],i);
    //     }
    // }
    // fprintf(initial_graph, "}\n" );

    // fclose(initial_graph);



        printf("filloutfrom in done\n");







        //exit(1);
       // printf("total vweight = %lf\n",G->totvw); 
        
        if(use_binary_input){
            FILE *fp = fopen(fnbin,"wb");

            if(fp == NULL){
               u_errexit("Cannot open file");
            }

            fwrite(&G->frmt,sizeof(G->frmt),1,fp);
            fwrite(&G->nVrtx,sizeof(G->nVrtx),1,fp);
            fwrite(&G->nEdge,sizeof(G->nEdge),1,fp);
            fwrite(&G->totvw,sizeof(G->totvw),1,fp);
            fwrite(&G->maxindegree,sizeof(G->maxindegree),1,fp);
            fwrite(&G->maxoutdegree,sizeof(G->maxoutdegree),1,fp);
            fwrite(G->inStart,sizeof(G->inStart[0]),(nVrtx+2),fp);
            fwrite(G->inEnd,sizeof(G->inEnd[0]),(nVrtx+2),fp);
            fwrite(G->in,sizeof(G->in[0]),nEdge,fp);
            fwrite(G->vw,sizeof(G->vw[0]),(nVrtx+1),fp);
            fwrite(G->outStart,sizeof(G->outStart[0]),(nVrtx+2),fp);
            fwrite(G->outEnd,sizeof(G->outEnd[0]),(nVrtx+2),fp);
            fwrite(G->out,sizeof(G->out[0]),nEdge,fp);
            fwrite(G->ecIn,sizeof(ecType),nEdge,fp);
            fwrite(G->ecOut,sizeof(ecType),nEdge,fp);

            //anik add
            fwrite(G->vertices,sizeof(G->vertices[0]),(nVrtx+1),fp);
            fwrite(G->vWeight,sizeof(G->vWeight[0]),(nVrtx+1),fp);


            fclose(fp);
        }

//      for(i=0;i<my_nVrtx+2;++i){
//          printf("G1->inStart[%d] = %d\n",i,G->inStart[i]);
//      }
//      for(i=0;i<my_nVrtx+2;++i){
//          printf("G1->inEnd[%d] = %d\n",i,G->inEnd[i]);
//      }
//      for(i=0;i<my_nEdge;++i){
//          printf("G1->in[%d] = %d\n",i,G->in[i]);
//      }
//      for(i=0;i<my_nVrtx+2;++i){
//          printf("G1->inStart[%d] = %d\n",i,G->outStart[i]);
//      }
//      for(i=0;i<my_nVrtx+2;++i){
//          printf("G1->inEnd[%d] = %d\n",i,G->outEnd[i]);
//      }
//      for(i=0;i<my_nEdge;++i){
//          printf("G1->in[%d] = %d\n",i,G->out[i]);
//      }
//
        /*FILE* outdegree_file;
        outdegree_file = fopen("outdegree.txt","w");
        for(i = 1 ; i <= my_nVrtx ; i++){
            fprintf(outdegree_file, "%s %d\n", G->vertices[i-1],G->outEnd[i] - G->outStart[i]);
        }
        fclose(outdegree_file);*/



        for (i = 1; i <= nVrtx; i++)
            free(inneighbors[i]);
        free(nbneighbors);
        free(inneighbors);



}

void task_name(const char *node_name, char** task){
    //char *task = (char*) malloc(100*sizeof(char));
    //char task[100];
    int i = 0 ; 

    while(node_name[i]!= ','){
        (*task)[i] = node_name[i];
        i++;
    }
    if(i<=strlen(node_name))
        (*task)[i] = '\0';
    //printf("task name %s\n",*task);
    //return task;
    return;
}


int node_type(const char* node){
    //printf("inside type function %s\n",node);

    //char* tsk_name = task_name(node);
    char* task = (char*)malloc(500*sizeof(char));
    //strcpy(temp_node_name,node);
    task_name(node,&task);

    //printf("inside type taskname = %s\n", task);

    //if(!strcmp(task_name(node),"SPMM"))return 3;
    //if(node[0] == 'S' && node[0] == 'P' && node[0] == 'M' && node[0] == 'M' && node[0] == ',')return 3;
    if(!strcmp(task,"SPMM")) {free(task);return 3;}
    else if(!strcmp(task,"SPMV")) {free(task);return 3;}


    else if(!strcmp(node,"_lambda")) {free(task);return 0;}
    else if(!strcmp(node,"RNRED,RNBUF")) {free(task);return 0;}
    else if(!strcmp(node,"RESET,RN")) {free(task);return 0;}
    else if(!strcmp(node,"SQRT,RN")) {free(task);return 0;}
    else if(!strcmp(node,"RESET,actMask")) {free(task);return 0;}
    else if(!strcmp(node,"CONV,actMaks")) {free(task);return 0;}
    else if(!strcmp(node,"RED,temp2BUF,0")) {free(task);return 0;}
    else if(!strcmp(node,"RED,RBRBUF,0")) {free(task);return 0;}
    else if(!strcmp(node,"CHOL,RBR")) {free(task);return 0;}
    else if(!strcmp(node,"INV,RBR")) {free(task);return 0;}
    else if(!strcmp(node,"RED,PBPBUF,0")) {free(task);return 0;}
    else if(!strcmp(node,"CHOL,PBP")) {free(task);return 0;}
    else if(!strcmp(node,"INV,PBP")) {free(task);return 0;}
    else if(!strcmp(node,"EIGEN")) {free(task);return 0;}
    else if(!strcmp(node,"CONSTRUCTGB")) {free(task);return 0;}
    else if(!strcmp(node,"CONSTRUCTGA1")) {free(task);return 0;}
    else if(!strcmp(node,"CONSTRUCTGA2")) {free(task);return 0;}
    else if(!strcmp(node,"RED,XAPBUF,0")) {free(task);return 0;}
    else if(!strcmp(node,"SPEUPDATE,RAR")) {free(task);return 0;}
    else if(!strcmp(node,"SPEUPDATE,PAP")) {free(task);return 0;}
    else if(!strcmp(node,"TRANS,RAR")) {free(task);return 0;}
    else if(!strcmp(node,"RED,RAPBUF,0")) {free(task);return 0;}
    else if(!strcmp(node,"RED,XARBUF,0")) {free(task);return 0;}
    else if(!strcmp(node,"RED,RARBUF,0")) {free(task);return 0;}
    else if(!strcmp(node,"RED,PAPBUF,0")) {free(task);return 0;}
    else if(!strcmp(node,"RED,XBPBUF,0")) {free(task);return 0;}
    else if(!strcmp(node,"RED,RBPBUF,0")) {free(task);return 0;}
    else if(!strcmp(node,"MAX")) {free(task);return 0;}

//these are constant nodes for nonloop version
    else if(!strcmp(node,"RED,XAXBUF,0")) {free(task);return 0;}
    else if(!strcmp(node,"RED,XBXBUF,0")) {free(task);return 0;}
    else if(!strcmp(node,"CHOL,XBX")) {free(task);return 0;}
    else if(!strcmp(node,"INV,XBX")) {free(task);return 0;}
    else if(!strcmp(node,"DLACPY,0,20")) {free(task);return 0;}





    else if(!strcmp(task,"_X")){free(task);return 1;}
    else if(!strcmp(task,"_AX")){free(task);return 1;}
    else if(!strcmp(task,"_P")){free(task);return 1;}
    else if(!strcmp(task,"_AP")){free(task);return 1;}
    else if(!strcmp(task,"MULT")){free(task);return 1;}
    else if(!strcmp(task,"SPMMRED")){free(task);return 1;}
    else if(!strcmp(task,"NORM")){free(task);return 1;}
    else if(!strcmp(task,"_Y")){free(task);return 1;}



    else if(!strcmp(task,"XTY")){free(task);return 2;}
    else if(!strcmp(task,"XY")){free(task);return 2;}
    else if(!strcmp(task,"DLACPY")){free(task);return 2;}
    else if(!strcmp(task,"SUB")){free(task);return 2;}
    else if(!strcmp(task,"COL")){free(task);return 2;}
    else if(!strcmp(task,"GET")){free(task);return 2;}
    else if(!strcmp(task,"UPDATE")){free(task);return 2;}
    else if(!strcmp(task,"SETZERO")){free(task);return 2;}
    else if(!strcmp(task,"ADD")){free(task);return 2;}
    else if(!strcmp(task,"SUBMAX")){free(task);return 2;}


    //else if(node[0]=='A' && node[1] == 'D')return 2;

    free(task);

    
    return 0;
}

int get_row_num(const char* node){
    int i = 0;
    int row_num = 0;
    //printf("node1 = %s\n",node);
    while(node[i] != ','){
        i++;
    }
    i++;
    while(node[i] != ',' && i < strlen(node)){
      //  printf("node[%d] = %c\n",i,node[i]);
        row_num = row_num*10 + node[i] - 48;
        i++;
    }
    //printf("row num = %d\n");
    return row_num;
}

int get_spmm_col_num(const char* node){
    int i = 0 ; 
    int col_num = 0;
    while(node[i] != ','){
        i++;
    }
    i++;
    while(node[i] != ','){
        i++;
    }
    i++;

    while(node[i] != ',' && strlen(node)){
        col_num = col_num*10 + node[i] - 48;
        i++;

    }
    return col_num;


}



/*void create_smallgraph_datastructure(int *edge_u, int *edge_v, double *edge_weight, int edgeCount, int vertexCount, const char** vertexName,double* vertexWeight, int block_divisor, char*** newVertexName, int new_vertexcount,
                                            int new_edgeCount, int **newEdge_u, int **newEdge_v, double** newEdge_weight, int** prev_vertex , double** newVertex_weight,int** nnzblock_matrix, int newRblock, int newCblock){
    int v1,v2;
    //printf("edgecount = %d\n",edgeCount);
    int k = 0,i,j;
    
    int type=0;
    int global_vertex_index = 0; 
    int local_row = 0;
    int local_col = 0;
    char loc_row_str_type1[10];
    char task_id[10];
    int vmap_index = 0;



    int **vertex_map;
    vertex_map = (int**)calloc((new_vertexcount+1),sizeof(int*));

    
    for(i = 0;i< (new_vertexcount);++i){
            vertex_map[i] = (int*)calloc((block_divisor*block_divisor+1),sizeof(int));
        }

    printf("new_vertexcount = %d\n",new_vertexcount);
    int buff = 0;
    while (k < vertexCount){
        //printf("%s %d\n", vertexName[k],global_vertex_index);
        type = node_type(vertexName[k]);

        char* task = (char*)malloc(500*sizeof(char));
        task_name(vertexName[k],&task);

        //printf("k = %d node = %s type %d global_vertex = %d\n", k,vertexName[k],type,global_vertex_index);
        //k++;
        //continue;
        
        if(type == 3){
            buff = 0;
            //k++;
            //continue;
            //printf("node= %s\n", vertexName[k]);
            vmap_index = 0;
            int spmm_row =0,spmm_col =0;
            int s = 5;
            while(vertexName[k][s]!= ','){
                spmm_row = spmm_row*10+vertexName[k][s++]-48;
            }
            s++;
            while(vertexName[k][s]!= ','){
                spmm_col = spmm_col*10+vertexName[k][s++]-48;
            }
            //printf("spmm_row = %d spmm_col = %d\n",spmm_row,spmm_col);

            //if(spmm_row == 7 && spmm_col == 0)
            
            for(i=0;i<block_divisor;i++){
                char loc_row[10];
                local_row = spmm_row*block_divisor+i;
                sprintf(loc_row,"%d",local_row);
                //printf("local row = %d\n",local_row);
                for(j=0;j<block_divisor;j++){

                    char loc_col[10];
                    local_col = spmm_col*block_divisor+j;

                    //if(!nnzblock_matrix[local_row][local_col]) continue;
                    //printf("nnzblock_matrix[%d][%d] = %d\n",local_row,local_col,nnzblock_matrix[local_row][local_col]);

                    if(local_row < newRblock && local_col < newCblock && nnzblock_matrix[local_row][local_col]) 
                    {
                    sprintf(loc_col,"%d",local_col);
                    char total_node[1000];

                    strcpy(total_node,"SPMM,");
                    strcat(total_node,loc_row);
                    strcat(total_node,",");
                    strcat(total_node,loc_col);
                    strcat(total_node,",");


                    char buff_c[2];
                    sprintf(buff_c,"%d",buff%6);
                    buff++;
                    strcat(total_node,buff_c);
                    //printf("totalnode made\n");
                    strcpy((*newVertexName)[global_vertex_index],total_node);
                    //printf("%s %d\n", (*newVertexName)[global_vertex_index],global_vertex_index);
                    //(*newVertexName)[global_vertex_index] = strdup(total_node);
                    (*prev_vertex)[global_vertex_index] = k;
                    vertex_map[k][i*block_divisor+j] = global_vertex_index;
                    //printf("vertex_map[%d][%d] = %d(%s)\n",k,i*block_divisor+j , global_vertex_index,(*newVertexName)[global_vertex_index]);
                    (*newVertex_weight)[global_vertex_index] = vertexWeight[k]/block_divisor;
                    vmap_index++;
                    global_vertex_index++;
                    //printf("added %s prev %d\n",(*newVertexName)[global_vertex_index-1],(*prev_vertex)[global_vertex_index-1]);
                }
                }
            }

        }



        else if(type == 0) {
            //printf("global_vertex_index %d vertex %s\n",global_vertex_index,vertexName[k]);
            vmap_index = 0;
            strcpy((*newVertexName)[global_vertex_index],vertexName[k]);
            for(i = 0 ; i < block_divisor ; i++)
                vertex_map[k][i] = global_vertex_index;
            (*prev_vertex)[global_vertex_index] = k;
            (*newVertex_weight)[global_vertex_index] = vertexWeight[k]/block_divisor;
            global_vertex_index++;
            //printf("added %s prev %d\n",(*newVertexName)[global_vertex_index-1],(*prev_vertex)[global_vertex_index-1]);

        }

        else if(type == 1) {
            //k++;
            //continue;
            vmap_index = 0;
            int row_num = 0;
            int s = strlen(task)+1;
            //printf("type 1 task %s s value %d char %c\n",task,s,vertexName[k][s]);
            while(vertexName[k][s]!= '\0'){
                row_num = row_num*10+vertexName[k][s]-48;
                s++;
            }
            //printf("row num = %d\n",row_num);
            for(i=0;i<block_divisor ;i++){
                int loc;
                loc= row_num*block_divisor+i;
                if(loc >= newRblock) break;
                //char* loc_row = (char*)malloc(10*sizeof(char));
                
                sprintf(loc_row_str_type1,"%d",loc);
              //  printf("loc row str %s\n", loc_row_str_type1);
                char total_node[1000];
                strcpy(total_node,task);
                strcat(total_node,",");
                strcat(total_node,loc_row_str_type1);

                
              //  printf("type 1 total node %s\n",total_node);

                vertex_map[k][i] = global_vertex_index;
                vmap_index++;

                (*prev_vertex)[global_vertex_index] = k;

                strcpy((*newVertexName)[global_vertex_index],total_node);
                (*newVertex_weight)[global_vertex_index] = vertexWeight[k]/block_divisor;
              //  printf("strdup e problem????\n");
                global_vertex_index++;
             //   printf("added %s prev %d\n",(*newVertexName)[global_vertex_index-1],(*prev_vertex)[global_vertex_index-1]);

            }
            
           
         
        }
        else if(type == 2){



            //special case col node
            if(!strcmp(task,"COL")){

            vmap_index = 0;

            int row_num = 0;
            buff =0;
            int s = strlen(task)+1;
            //printf("type 1 task %s s value %d char %c\n",task,s,vertexName[k][s]);
            while(vertexName[k][s]!= ','){
                row_num = row_num*10+vertexName[k][s]-48;
                s++;
            }
            //printf("row num = %d\n",row_num);
            for(i=0;i<block_divisor;i++){
                int loc;
                loc= row_num*block_divisor+i;
                if(loc >= newRblock) break;
                //char* loc_row = (char*)malloc(10*sizeof(char));
                
                sprintf(loc_row_str_type1,"%d",loc);
                //printf("loc row str %s\n", loc_row_str_type1);
                char total_node[1000];
                strcpy(total_node,task);
                strcat(total_node,",");
                strcat(total_node,loc_row_str_type1);
                strcat(total_node,",");


                char buff_c[2];
                    sprintf(buff_c,"%d",buff%6);
                    buff++;
                    strcat(total_node,buff_c);
                //printf("type 1 total node %s\n",total_node);

                vertex_map[k][i] = global_vertex_index;
                    vmap_index++;
                (*prev_vertex)[global_vertex_index] = k;

                strcpy((*newVertexName)[global_vertex_index],total_node);
                (*newVertex_weight)[global_vertex_index] = vertexWeight[k]/block_divisor;
                //printf("strdup e problem????\n");
                global_vertex_index++;
                //printf("added %s prev %d\n",(*newVertexName)[global_vertex_index-1],(*prev_vertex)[global_vertex_index-1]);

            }

            }

            else if(!strcmp(task,"XTY")){

                vmap_index = 0;
                int task_id_index=strlen(vertexName[k])-1;
                while(vertexName[k][task_id_index]!= ',')
                    task_id_index--;
                task_id_index++;
                int t = 0;
                while(vertexName[k][t]!= '\0')
                    task_id[t++] = vertexName[k][task_id_index++];
                task_id[t] = '\0';

                int row_num = 0;
            buff =0;
            int s = strlen(task)+1;
            //printf("type 1 task %s s value %d char %c\n",task,s,vertexName[k][s]);
            while(vertexName[k][s]!= ','){
                row_num = row_num*10+vertexName[k][s]-48;
                s++;
            }
            //printf("row num = %d\n",row_num);
            for(i=0;i<block_divisor;i++){
                int loc;
                loc= row_num*block_divisor+i;
                if(loc >= newRblock) break;
                //char* loc_row = (char*)malloc(10*sizeof(char));
                
                sprintf(loc_row_str_type1,"%d",loc);
                //printf("loc row str %s\n", loc_row_str_type1);
                char total_node[1000];
                strcpy(total_node,task);
                strcat(total_node,",");
                strcat(total_node,loc_row_str_type1);
                strcat(total_node,",");


                char buff_c[2];
                    sprintf(buff_c,"%d",buff%6);
                    buff++;
                    strcat(total_node,buff_c);
                strcat(total_node,",");
                strcat(total_node,task_id);

                vertex_map[k][i] = global_vertex_index;
                    vmap_index++;
                //printf("type 1 total node %s\n",total_node);
                (*prev_vertex)[global_vertex_index] = k;

                strcpy((*newVertexName)[global_vertex_index],total_node);
                (*newVertex_weight)[global_vertex_index] = vertexWeight[k]/block_divisor;
                //printf("strdup e problem????\n");
                global_vertex_index++;
                //printf("added %s prev %d\n",(*newVertexName)[global_vertex_index-1],(*prev_vertex)[global_vertex_index-1]);

            }




            }



        else{
            vmap_index = 0;

            int task_id_index=strlen(vertexName[k])-1;
                while(vertexName[k][task_id_index]!= ',')
                    task_id_index--;
                task_id_index++;
                int t = 0;
                while(vertexName[k][t]!= '\0')
                    task_id[t++] = vertexName[k][task_id_index++];
                task_id[t] = '\0';

                int row_num = 0;
            buff =0;
            int s = strlen(task)+1;
            //printf("type 1 task %s s value %d char %c\n",task,s,vertexName[k][s]);
            while(vertexName[k][s]!= ','){
                row_num = row_num*10+vertexName[k][s]-48;
                s++;
            }
            //printf("row num = %d\n",row_num);
            for(i=0;i<block_divisor;i++){
                int loc;
                loc= row_num*block_divisor+i;
                if(loc >= newRblock) break;
                //char* loc_row = (char*)malloc(10*sizeof(char));
                
                sprintf(loc_row_str_type1,"%d",loc);
                //printf("loc row str %s\n", loc_row_str_type1);
                char total_node[1000];
                strcpy(total_node,task);
                strcat(total_node,",");
                strcat(total_node,loc_row_str_type1);
                strcat(total_node,",");


                
                strcat(total_node,task_id);
                //printf("type 1 total node %s\n",total_node);


                vertex_map[k][i] = global_vertex_index;
                    vmap_index++;
                (*prev_vertex)[global_vertex_index] = k;

                strcpy((*newVertexName)[global_vertex_index],total_node);
                (*newVertex_weight)[global_vertex_index] = vertexWeight[k]/block_divisor;
                //printf("strdup e problem????\n");
                global_vertex_index++;
                //printf("added %s prev %d\n",(*newVertexName)[global_vertex_index-1],(*prev_vertex)[global_vertex_index-1]);

            }


        }

        }


        k++;
        free(task); 
        //free(vertex_map);
    }

    printf("updated vertex count = %d\n",global_vertex_index);

    for(i = 0; i < global_vertex_index ; i++){
     //   printf("new node %d = %s %d\n",i,(*newVertexName)[i],(*prev_vertex)[i]);
    }

    // for(i = 0; i < global_vertex_index ; i++){
    //     //printf("new node %d = %s %d\n",i,(*newVertexName)[i],(*prev_vertex)[i]);
    // }

    // for(i = 0 ; i < vertexCount ; i++){
    //     //printf("%d = ",i);
    //     for(j = 0 ; j < block_divisor*block_divisor ; j++){
    //         //printf(" %d",vertex_map[i][j]);
    //     }
    //     //printf("\n");
    // }

    k = 0;
    int global_edge_index = 0;
    //char* tsk1 = (char*)malloc(100*sizeof(char));
    //char* tsk2 = (char*)malloc(100*sizeof(char));
    while(k < edgeCount){
        //printf("k = %d %d %d\n",k,edge_u[k],edge_v[k]);

        v1 = edge_u[k];
        v2 = edge_v[k];

        //printf("k = %d %d %d\n",k,edge_u[k],edge_v[k]);


        
        //task_name(vertexName[v1],&task1);
        //task_name(vertexName[v2],&task2);

        //printf("k = %d %d %d \n",k,v1,v2);

        int nd_type1 = node_type(vertexName[v1]);
        int nd_type2 = node_type(vertexName[v2]);

        //printf("k = %d %s(%d) %s(%d)\n",k,vertexName[v1],nd_type1,vertexName[v2],nd_type2);

        int sp_row,sp_col;


        //if(node_type(vertexName[v1]) == 3 )
            //printf("k = %d node 1 type = %s(%d) node 2 = %s(%d)\n",k,vertexName[v1],nd_type1, vertexName[v2],nd_type2);

        if(nd_type1 == 0 && nd_type2 == 0){

            (*newEdge_u)[global_edge_index] = vertex_map[v1][0];
            (*newEdge_v)[global_edge_index] = vertex_map[v2][0];
            (*newEdge_weight)[global_edge_index] = edge_weight[k];
            //printf("%s --> %s new added %s --> %s\n",vertexName[v1],vertexName[v2],(*newVertexName)[(*newEdge_u)[global_edge_index]],(*newVertexName)[(*newEdge_v)[global_edge_index]]);
            global_edge_index++;
        }
        else if(nd_type1 == 3 || nd_type2 == 3){
            
            

                if(nd_type1 == 3){


                    
                    

                    for(i = 0 ; i < block_divisor ; i++){
                        sp_row = get_row_num(vertexName[v1])*block_divisor+i;


                        for(j = 0 ; j < block_divisor ; j++){
                            sp_col = get_spmm_col_num(vertexName[v1])*block_divisor+j;
                            if(sp_row < newRblock && sp_col < newCblock && vertex_map[v1][i*block_divisor+j])
                            {
                            (*newEdge_u)[global_edge_index] = vertex_map[v1][i*block_divisor+j];
                            (*newEdge_v)[global_edge_index] = vertex_map[v2][i];
                            (*newEdge_weight)[global_edge_index] = edge_weight[k]/block_divisor;
                            //printf("added %s --> %s\n",(*newVertexName)[(*newEdge_u)[global_edge_index]],(*newVertexName)[(*newEdge_v)[global_edge_index]]);
                            global_edge_index++;
                        }
                        }
                    }
                }
                else if(vertexName[v1][0] == 'S'){
                 for(i = 0 ; i < block_divisor ; i++){
                    sp_row = get_row_num(vertexName[v1])*block_divisor+i;
                        for(j = 0 ; j < block_divisor ; j++){
                            sp_col = get_spmm_col_num(vertexName[v1])*block_divisor+j;
                            if(sp_row < newRblock && sp_col < newCblock && vertex_map[v2][i*block_divisor+j])
                            {
                            (*newEdge_u)[global_edge_index] = vertex_map[v1][i];
                            (*newEdge_v)[global_edge_index] = vertex_map[v2][i*block_divisor+j];
                            (*newEdge_weight)[global_edge_index] = edge_weight[k]/block_divisor;
                            //printf("added %s --> %s\n",(*newVertexName)[(*newEdge_u)[global_edge_index]],(*newVertexName)[(*newEdge_v)[global_edge_index]]);
                            global_edge_index++;
                        }
                        }
                    }   
                }
                else if(vertexName[v1][0] == 'D'){
                //printf("%s --> %s\n",vertexName[v1],vertexName[v2]);
                  for(j = 0 ; j < block_divisor ; j++){
                        sp_col = get_spmm_col_num(vertexName[v2])*block_divisor+j;
                        for(i = 0 ; i < block_divisor ; i++){
                            sp_row = get_row_num(vertexName[v2])*block_divisor+i;
                            if(!strcmp((*newVertexName)[vertex_map[v1][j]],"DLACPY,28,1"))
                            //printf("%s\n",vertexName[])
                            printf("sp_row = %d sp_col = %d vmap[%d][%d] = %d\n", sp_row,sp_col,v2,i*block_divisor+j,vertex_map[v2][i*block_divisor+j]);
                            if(sp_row < newRblock && sp_col < newCblock && vertex_map[v2][i*block_divisor+j] > 0 )//&& vertex_map[v2][i*block_divisor+j] <= global_vertex_index)
                            {
                            (*newEdge_u)[global_edge_index] = vertex_map[v1][j];
                            (*newEdge_v)[global_edge_index] = vertex_map[v2][i*block_divisor+j];
                            (*newEdge_weight)[global_edge_index] = edge_weight[k]/block_divisor;
                            if(!strcmp((*newVertexName)[vertex_map[v1][j]],"DLACPY,28,1"))
                            printf("added %s --> %s\n",(*newVertexName)[(*newEdge_u)[global_edge_index]],(*newVertexName)[(*newEdge_v)[global_edge_index]]);
                            global_edge_index++;
                        }
                        }
                    }  
                }


        }

        //else if(node_type(vertexName[v1]) == 1 && node_type(vertexName[v2]) == 2){

        //}
        else {
            //if(nd_type1 == )



            for(i = 0 ; i < block_divisor ; i++){
               // if(!strcmp(task1,"SETZERO") && !strcmp(task2,"SPMMRED")){
                if(nd_type1 == 0){
                    
                        sp_row = get_row_num(vertexName[v2])*block_divisor+i;
                    
            }
            else 
                sp_row = get_row_num(vertexName[v1])*block_divisor+i;
            //printf("%s --> %s  %d\n",vertexName[v1],vertexName[v2], sp_row);


                if(vertexName[v1][0] == 'S' && vertexName[v1][1] == 'E' && vertexName[v1][2] == 'T' && vertexName[v2][3] == 'M' &&
                        vertexName[v2][4] == 'R' && vertexName[v2][5] == 'E' && vertexName[v2][6] == 'D' ){
                    for(j = 0 ; j < block_divisor ; j++){


                        sp_col = get_spmm_col_num(vertexName[edge_u[k-1]])*block_divisor+j;

                        //printf("spmm node = %s col no = %d cal col no = %d\n",vertexName[edge_u[k-1]],get_spmm_col_num(vertexName[edge_u[k-1]]),sp_col);
                        if(sp_row < newRblock && sp_col < newCblock && vertex_map[edge_u[k-1]][i*block_divisor+j])
                        {
                        (*newEdge_u)[global_edge_index] = vertex_map[v1][i];
                        (*newEdge_v)[global_edge_index] = vertex_map[v2][i];
                        (*newEdge_weight)[global_edge_index] = edge_weight[k]/block_divisor;
                        //printf("added %s --> %s\n",(*newVertexName)[(*newEdge_u)[global_edge_index]],(*newVertexName)[(*newEdge_v)[global_edge_index]]);
                        global_edge_index++;
                    }

                    }
                }
                else{
                if(sp_row < newRblock){
                (*newEdge_u)[global_edge_index] = vertex_map[v1][i];
                (*newEdge_v)[global_edge_index] = vertex_map[v2][i];
                (*newEdge_weight)[global_edge_index] = edge_weight[k]/block_divisor;
                //printf("%s --> %s new added %s --> %s\n",vertexName[v1],vertexName[v2],(*newVertexName)[(*newEdge_u)[global_edge_index]],(*newVertexName)[(*newEdge_v)[global_edge_index]]);
                global_edge_index++;
            }
            }
                
             
            }
        }

        //free(task1);
        //free(task2);

        k++;
    }

    printf("updated edge count = %d\n", global_edge_index);

    FILE* new_edges;
    new_edges = fopen("new_graph_edges.txt","w");

    for(i = 0 ; i < global_edge_index ; i++){
        fprintf(new_edges,"%s ---> %s\n",(*newVertexName)[(*newEdge_u)[i]],(*newVertexName)[(*newEdge_v)[i]]);
        //printf(" edge no %d %s(%d) --> %s(%d)\n",i,(*newVertexName)[(*newEdge_u)[i]],(*newEdge_u)[i],(*newVertexName)[(*newEdge_v)[i]],(*newEdge_v)[i]);
    }
    fclose(new_edges);
    printf("loop sesh\n");
    //free(tsk1);
    //free(tsk2);
    //free(vertex_map);
}

*/


// generate small graph by breaking up the large graph
void create_smallgraph_datastructure_sparse(int *edge_u, int *edge_v, double *edge_weight, int edgeCount, int vertexCount, const char** vertexName,double* vertexWeight, int block_divisor, char*** newVertexName, int new_vertexcount,
                                            int new_edgeCount, int **newEdge_u, int **newEdge_v, double** newEdge_weight, int** prev_vertex , double** newVertex_weight, int** nnzblock_matrix, int newRblock, int newCblock,
                                            int* updatedVertexCount,int *updatedEdgeCount, int ***vmap){

    int v1,v2;
    //printf("edgecount = %d\n",edgeCount);
    printf("block divisor = %d\n",block_divisor);
    int k = 0,i,j;
    
    int type=0;
    int global_vertex_index = 0; 
    int local_row = 0;
    int local_col = 0;
    char loc_row_str_type1[10];
    char task_id[10];
    int vmap_index = 0;



    int **vertex_map;
    vertex_map = (int**)calloc((vertexCount+1),sizeof(int*));

    
    for(i = 0;i< (vertexCount);++i){
            vertex_map[i] = (int*)calloc((block_divisor*block_divisor+1),sizeof(int));
        }

    printf("new_vertexcount = %d\n",new_vertexcount);
    int buff = 0;
    while (k < vertexCount){
        //printf("%s %d\n", vertexName[k],global_vertex_index);
        type = node_type(vertexName[k]);

        char* task = (char*)malloc(500*sizeof(char));
        task_name(vertexName[k],&task);

        //printf("k = %d node = %s type %d global_vertex = %d\n", k,vertexName[k],type,global_vertex_index);
        //k++;
        //continue;
        
        if(type == 3){
            buff = 0;
            //k++;
            //continue;
            //printf("node= %s\n", vertexName[k]);
            vmap_index = 0;
            int spmm_row =0,spmm_col =0;
            int s = 5;
            while(vertexName[k][s]!= ','){
                spmm_row = spmm_row*10+vertexName[k][s++]-48;
            }
            s++;
            while(vertexName[k][s]!= ','){
                spmm_col = spmm_col*10+vertexName[k][s++]-48;
            }
            //printf("spmm_row = %d spmm_col = %d\n",spmm_row,spmm_col);

            //if(spmm_row == 7 && spmm_col == 0)
            
            for(i=0;i<block_divisor;i++){
                char loc_row[10];
                local_row = spmm_row*block_divisor+i;
                sprintf(loc_row,"%d",local_row);
                //printf("local row = %d\n",local_row);
                for(j=0;j<block_divisor;j++){

                    char loc_col[10];
                    local_col = spmm_col*block_divisor+j;

                    //if(!nnzblock_matrix[local_row][local_col]) continue;
                    //printf("nnzblock_matrix[%d][%d] = %d\n",local_row,local_col,nnzblock_matrix[local_row][local_col]);

                    if(local_row < newRblock && local_col < newCblock && nnzblock_matrix[local_row][local_col]) 
                    {
                    sprintf(loc_col,"%d",local_col);
                    char total_node[1000];

                    strcpy(total_node,task);
                    strcat(total_node,",");
                    strcat(total_node,loc_row);
                    strcat(total_node,",");
                    strcat(total_node,loc_col);
                    strcat(total_node,",");


                    char buff_c[2];
                    //sprintf(buff_c,"%d",local_row%7);
                    buff++;
                    strcat(total_node,"0");
                    //printf("totalnode made\n");
                    strcpy((*newVertexName)[global_vertex_index],total_node);
                    //printf("%s %d\n", (*newVertexName)[global_vertex_index],global_vertex_index);
                    //(*newVertexName)[global_vertex_index] = strdup(total_node);
                    (*prev_vertex)[global_vertex_index] = k;
                    //printf("prev_vertex[%s] = %s\n",(*newVertexName)[global_vertex_index],vertexName[k]);
                    vertex_map[k][i*block_divisor+j] = global_vertex_index;
                    //printf("vertex_map[%d][%d] = %d(%s)\n",k,i*block_divisor+j , global_vertex_index,(*newVertexName)[global_vertex_index]);
                    (*newVertex_weight)[global_vertex_index] = vertexWeight[k]/block_divisor;
                    vmap_index++;
                    global_vertex_index++;
                    //printf("added %s prev %d\n",(*newVertexName)[global_vertex_index-1],(*prev_vertex)[global_vertex_index-1]);
                }
                }
            }

        }



        else if(type == 0) {
            //printf("global_vertex_index %d vertex %s\n",global_vertex_index,vertexName[k]);
            vmap_index = 0;
            strcpy((*newVertexName)[global_vertex_index],vertexName[k]);
            for(i = 0 ; i < block_divisor ; i++)
                vertex_map[k][i] = global_vertex_index;
            (*prev_vertex)[global_vertex_index] = k;
            //printf("prev_vertex[%s] = %s\n",(*newVertexName)[global_vertex_index],vertexName[k]);
            (*newVertex_weight)[global_vertex_index] = vertexWeight[k]/block_divisor;
            global_vertex_index++;
            //printf("added %s prev %d\n",(*newVertexName)[global_vertex_index-1],(*prev_vertex)[global_vertex_index-1]);

        }

        else if(type == 1) {
            //k++;
            //continue;
            vmap_index = 0;
            int row_num = 0;
            int s = strlen(task)+1;
            //printf("type 1 task %s s value %d char %c\n",task,s,vertexName[k][s]);
            while(vertexName[k][s]!= '\0'){
                row_num = row_num*10+vertexName[k][s]-48;
                s++;
            }
            //printf("row num = %d\n",row_num);
            for(i=0;i<block_divisor ;i++){
                int loc;
                loc= row_num*block_divisor+i;
                if(loc >= newRblock) break;
                //char* loc_row = (char*)malloc(10*sizeof(char));
                
                sprintf(loc_row_str_type1,"%d",loc);
              //  printf("loc row str %s\n", loc_row_str_type1);
                char total_node[1000];
                strcpy(total_node,task);
                strcat(total_node,",");
                strcat(total_node,loc_row_str_type1);

                
              //  printf("type 1 total node %s\n",total_node);

                vertex_map[k][i] = global_vertex_index;
                vmap_index++;

                (*prev_vertex)[global_vertex_index] = k;


                strcpy((*newVertexName)[global_vertex_index],total_node);
                //printf("prev_vertex[%s] = %s\n",(*newVertexName)[global_vertex_index],vertexName[k]);
                (*newVertex_weight)[global_vertex_index] = vertexWeight[k]/block_divisor;
              //  printf("strdup e problem????\n");
                global_vertex_index++;
             //   printf("added %s prev %d\n",(*newVertexName)[global_vertex_index-1],(*prev_vertex)[global_vertex_index-1]);

            }
            
           
         
        }
        else if(type == 2){



            //special case col node
            if(!strcmp(task,"COL")){

            vmap_index = 0;

            int row_num = 0;
            buff =0;
            int s = strlen(task)+1;
            //printf("type 1 task %s s value %d char %c\n",task,s,vertexName[k][s]);
            while(vertexName[k][s]!= ','){
                row_num = row_num*10+vertexName[k][s]-48;
                s++;
            }
            //printf("row num = %d\n",row_num);
            for(i=0;i<block_divisor;i++){
                int loc;
                loc= row_num*block_divisor+i;
                if(loc >= newRblock) break;
                //char* loc_row = (char*)malloc(10*sizeof(char));
                
                sprintf(loc_row_str_type1,"%d",loc);
                //printf("loc row str %s\n", loc_row_str_type1);
                char total_node[1000];
                strcpy(total_node,task);
                strcat(total_node,",");
                strcat(total_node,loc_row_str_type1);
                strcat(total_node,",");


                char buff_c[2];
                    sprintf(buff_c,"%d",loc%16);
                    buff++;
                    strcat(total_node,buff_c);
                //printf("type 1 total node %s\n",total_node);

                vertex_map[k][i] = global_vertex_index;
                    vmap_index++;
                (*prev_vertex)[global_vertex_index] = k;

                strcpy((*newVertexName)[global_vertex_index],total_node);
                //printf("prev_vertex[%s] = %s\n",(*newVertexName)[global_vertex_index],vertexName[k]);
                (*newVertex_weight)[global_vertex_index] = vertexWeight[k]/block_divisor;
                //printf("strdup e problem????\n");
                global_vertex_index++;
                //printf("added %s prev %d\n",(*newVertexName)[global_vertex_index-1],(*prev_vertex)[global_vertex_index-1]);

            }

            }

            else if(!strcmp(task,"SUBMAX")){

            vmap_index = 0;

            int row_num = 0;
            buff =0;
            int s = strlen(task)+1;
            //printf("type 1 task %s s value %d char %c\n",task,s,vertexName[k][s]);
            while(vertexName[k][s]!= ','){
                row_num = row_num*10+vertexName[k][s]-48;
                s++;
            }
            //printf("row num = %d\n",row_num);
            for(i=0;i<block_divisor;i++){
                int loc;
                loc= row_num*block_divisor+i;
                if(loc >= newRblock) break;
                //char* loc_row = (char*)malloc(10*sizeof(char));
                
                sprintf(loc_row_str_type1,"%d",loc);
                //printf("loc row str %s\n", loc_row_str_type1);
                char total_node[1000];
                strcpy(total_node,task);
                strcat(total_node,",");
                strcat(total_node,loc_row_str_type1);
                strcat(total_node,",");


                char buff_c[2];
                    sprintf(buff_c,"%d",loc%16);
                    buff++;
                    strcat(total_node,buff_c);
                //printf("type 1 total node %s\n",total_node);

                vertex_map[k][i] = global_vertex_index;
                    vmap_index++;
                (*prev_vertex)[global_vertex_index] = k;

                strcpy((*newVertexName)[global_vertex_index],total_node);
                //printf("prev_vertex[%s] = %s\n",(*newVertexName)[global_vertex_index],vertexName[k]);
                (*newVertex_weight)[global_vertex_index] = vertexWeight[k]/block_divisor;
                //printf("strdup e problem????\n");
                global_vertex_index++;
                //printf("added %s prev %d\n",(*newVertexName)[global_vertex_index-1],(*prev_vertex)[global_vertex_index-1]);

            }

            }


            else if(!strcmp(task,"XTY")){

                vmap_index = 0;
                int task_id_index=strlen(vertexName[k])-1;
                while(vertexName[k][task_id_index]!= ',')
                    task_id_index--;
                task_id_index++;
                int t = 0;
                while(vertexName[k][t]!= '\0')
                    task_id[t++] = vertexName[k][task_id_index++];
                task_id[t] = '\0';

                int row_num = 0;
            buff =0;
            int s = strlen(task)+1;
            //printf("type 1 task %s s value %d char %c\n",task,s,vertexName[k][s]);
            while(vertexName[k][s]!= ','){
                row_num = row_num*10+vertexName[k][s]-48;
                s++;
            }
            //printf("row num = %d\n",row_num);
            for(i=0;i<block_divisor;i++){
                int loc;
                loc= row_num*block_divisor+i;
                if(loc >= newRblock) break;
                //char* loc_row = (char*)malloc(10*sizeof(char));
                
                sprintf(loc_row_str_type1,"%d",loc);
                //printf("loc row str %s\n", loc_row_str_type1);
                char total_node[1000];
                strcpy(total_node,task);
                strcat(total_node,",");
                strcat(total_node,loc_row_str_type1);
                strcat(total_node,",");


                char buff_c[2];
                sprintf(buff_c,"%d",loc%16);
                buff++;
                strcat(total_node,buff_c);
                strcat(total_node,",");
                strcat(total_node,task_id);

                vertex_map[k][i] = global_vertex_index;
                    vmap_index++;
                //printf("type 1 total node %s\n",total_node);
                (*prev_vertex)[global_vertex_index] = k;

                strcpy((*newVertexName)[global_vertex_index],total_node);
                (*newVertex_weight)[global_vertex_index] = vertexWeight[k]/block_divisor;
                //printf("prev_vertex[%s] = %s\n",(*newVertexName)[global_vertex_index],vertexName[k]);
                //printf("strdup e problem????\n");
                global_vertex_index++;
                //printf("added %s prev %d\n",(*newVertexName)[global_vertex_index-1],(*prev_vertex)[global_vertex_index-1]);

            }




            }



        else{
            vmap_index = 0;

            int task_id_index=strlen(vertexName[k])-1;
                while(vertexName[k][task_id_index]!= ',')
                    task_id_index--;
                task_id_index++;
                int t = 0;
                while(vertexName[k][t]!= '\0')
                    task_id[t++] = vertexName[k][task_id_index++];
                task_id[t] = '\0';

                int row_num = 0;
            buff =0;
            int s = strlen(task)+1;
            //printf("type 1 task %s s value %d char %c\n",task,s,vertexName[k][s]);
            while(vertexName[k][s]!= ','){
                row_num = row_num*10+vertexName[k][s]-48;
                s++;
            }
            //printf("row num = %d\n",row_num);
            for(i=0;i<block_divisor;i++){
                int loc;
                loc= row_num*block_divisor+i;
                if(loc >= newRblock) break;
                //char* loc_row = (char*)malloc(10*sizeof(char));
                
                sprintf(loc_row_str_type1,"%d",loc);
                //printf("loc row str %s\n", loc_row_str_type1);
                char total_node[1000];
                strcpy(total_node,task);
                strcat(total_node,",");
                strcat(total_node,loc_row_str_type1);
                strcat(total_node,",");


                
                strcat(total_node,task_id);
                //printf("type 1 total node %s\n",total_node);


                vertex_map[k][i] = global_vertex_index;
                    vmap_index++;
                (*prev_vertex)[global_vertex_index] = k;

                strcpy((*newVertexName)[global_vertex_index],total_node);
                //printf("prev_vertex[%s] = %s\n",(*newVertexName)[global_vertex_index],vertexName[k]);
                (*newVertex_weight)[global_vertex_index] = vertexWeight[k]/block_divisor;
                //printf("strdup e problem????\n");
                global_vertex_index++;
                //printf("added %s prev %d\n",(*newVertexName)[global_vertex_index-1],(*prev_vertex)[global_vertex_index-1]);

            }


        }

        }


        k++;
        free(task); 
        //free(vertex_map);
    }

    printf("updated vertex count = %d\n",global_vertex_index);
    *updatedVertexCount = global_vertex_index;

    for(i = 0; i < global_vertex_index ; i++){
     //   printf("new node %d = %s %d\n",i,(*newVertexName)[i],(*prev_vertex)[i]);
    }

    // for(i = 0; i < global_vertex_index ; i++){
    //     //printf("new node %d = %s %d\n",i,(*newVertexName)[i],(*prev_vertex)[i]);
    // }

    // for(i = 0 ; i < vertexCount ; i++){
    //     //printf("%d = ",i);
    //     for(j = 0 ; j < block_divisor*block_divisor ; j++){
    //         //printf(" %d",vertex_map[i][j]);
    //     }
    //     //printf("\n");
    // }

    k = 0;
    int global_edge_index = 0;
    //char* tsk1 = (char*)malloc(100*sizeof(char));
    //char* tsk2 = (char*)malloc(100*sizeof(char));
    while(k < edgeCount){
        //printf("k = %d %d %d\n",k,edge_u[k],edge_v[k]);

        v1 = edge_u[k];
        v2 = edge_v[k];

        //printf("k = %d %d %d\n",k,edge_u[k],edge_v[k]);


        
        //task_name(vertexName[v1],&task1);
        //task_name(vertexName[v2],&task2);

        //printf("k = %d %d %d \n",k,v1,v2);

        int nd_type1 = node_type(vertexName[v1]);
        int nd_type2 = node_type(vertexName[v2]);

        //printf("k = %d %s(%d) %s(%d)\n",k,vertexName[v1],nd_type1,vertexName[v2],nd_type2);

        int sp_row,sp_col;


        //if(node_type(vertexName[v1]) == 3 )
            //printf("k = %d node 1 type = %s(%d) node 2 = %s(%d)\n",k,vertexName[v1],nd_type1, vertexName[v2],nd_type2);

        if(nd_type1 == 0 && nd_type2 == 0){

            (*newEdge_u)[global_edge_index] = vertex_map[v1][0];
            (*newEdge_v)[global_edge_index] = vertex_map[v2][0];
            (*newEdge_weight)[global_edge_index] = edge_weight[k];
            //printf("%s --> %s new added %s --> %s\n",vertexName[v1],vertexName[v2],(*newVertexName)[(*newEdge_u)[global_edge_index]],(*newVertexName)[(*newEdge_v)[global_edge_index]]);
            global_edge_index++;
        }
        else if(nd_type1 == 3 || nd_type2 == 3){
            
            

                if(nd_type1 == 3){


                    
                    

                    for(i = 0 ; i < block_divisor ; i++){
                        sp_row = get_row_num(vertexName[v1])*block_divisor+i;


                        for(j = 0 ; j < block_divisor ; j++){
                            sp_col = get_spmm_col_num(vertexName[v1])*block_divisor+j;
                            if(sp_row < newRblock && sp_col < newCblock && vertex_map[v1][i*block_divisor+j])
                            {
                            (*newEdge_u)[global_edge_index] = vertex_map[v1][i*block_divisor+j];
                            (*newEdge_v)[global_edge_index] = vertex_map[v2][i];
                            (*newEdge_weight)[global_edge_index] = edge_weight[k]/block_divisor;
                            //printf("added %s --> %s\n",(*newVertexName)[(*newEdge_u)[global_edge_index]],(*newVertexName)[(*newEdge_v)[global_edge_index]]);
                            global_edge_index++;
                        }
                        }
                    }
                }
                else if(vertexName[v1][0] == 'S'){
                //printf("%s --> %s\n",vertexName[v1],vertexName[v2]);
                 for(i = 0 ; i < block_divisor ; i++){
                    sp_row = get_row_num(vertexName[v2])*block_divisor+i;
                        for(j = 0 ; j < block_divisor ; j++){
                            sp_col = get_spmm_col_num(vertexName[v2])*block_divisor+j;
                            if(sp_row < newRblock && sp_col < newCblock && vertex_map[v2][i*block_divisor+j])
                            {
                            (*newEdge_u)[global_edge_index] = vertex_map[v1][i];
                            (*newEdge_v)[global_edge_index] = vertex_map[v2][i*block_divisor+j];
                            (*newEdge_weight)[global_edge_index] = edge_weight[k]/block_divisor;
                            //printf("added %s --> %s\n",(*newVertexName)[(*newEdge_u)[global_edge_index]],(*newVertexName)[(*newEdge_v)[global_edge_index]]);
                            global_edge_index++;
                        }
                        }
                    }   
                }
                else if(vertexName[v1][0] == 'D'){
                //printf("%s --> %s\n",vertexName[v1],vertexName[v2]);
                  for(j = 0 ; j < block_divisor ; j++){
                        sp_col = get_spmm_col_num(vertexName[v2])*block_divisor+j;
                        for(i = 0 ; i < block_divisor ; i++){
                            sp_row = get_row_num(vertexName[v2])*block_divisor+i;
                            //if(!strcmp((*newVertexName)[vertex_map[v1][j]],"DLACPY,28,1"))
                            //printf("%s\n",vertexName[])
                            //printf("sp_row = %d sp_col = %d vmap[%d][%d] = %d\n", sp_row,sp_col,v2,i*block_divisor+j,vertex_map[v2][i*block_divisor+j]);
                            if(sp_row < newRblock && sp_col < newCblock && vertex_map[v2][i*block_divisor+j] > 0 )//&& vertex_map[v2][i*block_divisor+j] <= global_vertex_index)
                            {
                            (*newEdge_u)[global_edge_index] = vertex_map[v1][j];
                            (*newEdge_v)[global_edge_index] = vertex_map[v2][i*block_divisor+j];
                            (*newEdge_weight)[global_edge_index] = edge_weight[k]/block_divisor;
                            //if(!strcmp((*newVertexName)[vertex_map[v1][j]],"DLACPY,28,1"))
                            //printf("added %s --> %s\n",(*newVertexName)[(*newEdge_u)[global_edge_index]],(*newVertexName)[(*newEdge_v)[global_edge_index]]);
                            global_edge_index++;
                        }
                        }
                    }  
                }


        }

        //else if(node_type(vertexName[v1]) == 1 && node_type(vertexName[v2]) == 2){

        //}
        else {
            //if(nd_type1 == )



            for(i = 0 ; i < block_divisor ; i++){
               // if(!strcmp(task1,"SETZERO") && !strcmp(task2,"SPMMRED")){
                if(nd_type1 == 0){
                    
                        sp_row = get_row_num(vertexName[v2])*block_divisor+i;
                    
            }
            else 
                sp_row = get_row_num(vertexName[v1])*block_divisor+i;
            //printf("%s --> %s  %d\n",vertexName[v1],vertexName[v2], sp_row);


                if(vertexName[v1][0] == 'S' && vertexName[v1][1] == 'E' && vertexName[v1][2] == 'T' && vertexName[v2][3] == 'M' &&
                        vertexName[v2][4] == 'R' && vertexName[v2][5] == 'E' && vertexName[v2][6] == 'D' ){
                    for(j = 0 ; j < block_divisor ; j++){


                        sp_col = get_spmm_col_num(vertexName[edge_u[k-1]])*block_divisor+j;

                        //printf("spmm node = %s col no = %d cal col no = %d\n",vertexName[edge_u[k-1]],get_spmm_col_num(vertexName[edge_u[k-1]]),sp_col);
                        if(sp_row < newRblock && sp_col < newCblock && vertex_map[edge_u[k-1]][i*block_divisor+j])
                        {
                        (*newEdge_u)[global_edge_index] = vertex_map[v1][i];
                        (*newEdge_v)[global_edge_index] = vertex_map[v2][i];
                        (*newEdge_weight)[global_edge_index] = edge_weight[k]/block_divisor;
                        //printf("added %s --> %s\n",(*newVertexName)[(*newEdge_u)[global_edge_index]],(*newVertexName)[(*newEdge_v)[global_edge_index]]);
                        global_edge_index++;
                    }

                    }
                }
                else{
                if(sp_row < newRblock){
                (*newEdge_u)[global_edge_index] = vertex_map[v1][i];
                (*newEdge_v)[global_edge_index] = vertex_map[v2][i];
                (*newEdge_weight)[global_edge_index] = edge_weight[k]/block_divisor;
                //printf("%s --> %s new added %s --> %s\n",vertexName[v1],vertexName[v2],(*newVertexName)[(*newEdge_u)[global_edge_index]],(*newVertexName)[(*newEdge_v)[global_edge_index]]);
                global_edge_index++;
            }
            }
                
             
            }
        }

        //free(task1);
        //free(task2);

        k++;
    }

    printf("updated edge count = %d\n", global_edge_index);
    *updatedEdgeCount = global_edge_index;

    // FILE* new_edges;
    // new_edges = fopen("new_graph_edges.txt","w");

    // for(i = 0 ; i < global_edge_index ; i++){
    //     fprintf(new_edges,"%s ---> %s\n",(*newVertexName)[(*newEdge_u)[i]],(*newVertexName)[(*newEdge_v)[i]]);
    //     //printf(" edge no %d %s(%d) --> %s(%d)\n",i,(*newVertexName)[(*newEdge_u)[i]],(*newEdge_u)[i],(*newVertexName)[(*newEdge_v)[i]],(*newEdge_v)[i]);
    // }
    // fclose(new_edges);
    

    for(i = 0 ; i < vertexCount ; i++){
        for(j = 0 ; j < block_divisor*block_divisor ; j++){
            (*vmap)[i][j] = vertex_map[i][j];
        }
        if(node_type(vertexName[i])==0)
            (*vmap)[i][1] = -1;
        else if((node_type(vertexName[i]) == 1) || (node_type(vertexName[i]) == 2)) 
            (*vmap)[i][block_divisor] = -1;
        /*else if(node_type(vertexName[i]) == 3){
            int t=0;
            while(t < block_divisor*block_divisor){
                if (vertex_map[i][t] == 0)
                {
                    (*vmap)[i][t] = -1;
                    break;
                }
                t++;
            }

        }*/

    }

    printf("loop sesh\n");
    //free(tsk1);
    //free(tsk2);
    //free(vertex_map);

}





void my_generate_smallgraph(dgraph *G, char* file_name, int use_binary_input, int *edge_u, int *edge_v, double *edge_weight, int edgeCount, int vertexCount, const char** vertexName,double* vertexWeight, int block_divisor){
        printf("\n\n\n\ngenerate small Dgraph called\n\n\n\n");

        //printf("\n\n%s\n\n",vertexName[0]);

        char fnbin[100];
        strcpy(fnbin,file_name);
        strcat(fnbin,".bin");

        if(use_binary_input){
            FILE *fp = fopen(fnbin,"rb");
            if(fp!=NULL){
                fclose(fp);
                // printf("Yes-ish\n");
                generateDGraphFromBinaryDot(G,fnbin);
                return;
            }
            else if (strstr(file_name,".bin")){
                generateDGraphFromBinaryDot(G,file_name);
                return;
            }
            // printf("No bin file found for input!\n");
        }

        char line[1000];
        char token[200];
        idxType nVrtx = 0, nEdge = 0;
        idxType i, j, k = 0 ;
        idxType* nbneighbors; //how many neighbours of each vertex
        idxType** inneighbors; //adjacency list (in-neighbours) , parent tracking


        //idxType my_nVrtx = 45*block_divisor+block_divisor*block_divisor+27 , my_nEdge = 0 ;
        idxType my_nVrtx = vertexCount, my_nEdge = 0;
        idxType* my_nbneighbors;
        idxType** my_inneighbors;
        //char vertices[12000][100]; //vertex name
        //double vweights[12000]; 
       // ecType my_inedgeweights[12000][10]; //ecType double incoming edge weights
        ecType** my_inedgeweights;

        my_nbneighbors = (idxType*) calloc((vertexCount+1), sizeof(idxType));
//        my_nbneighbors = (idxType*) malloc((vertexCount+1) * sizeof(idxType));
        my_inneighbors = (idxType**) malloc((vertexCount+1) * sizeof(idxType*));
        my_inedgeweights = (ecType**) malloc((my_nVrtx+1) * sizeof(ecType*));
        
        for (i = 0; i <= my_nVrtx; i++)
        {
            my_inneighbors[i] = (idxType*) malloc(100 * sizeof(idxType)); //say ever vertex has max 100 parents
            my_inedgeweights[i] = (ecType*) malloc(100 * sizeof(ecType));
        }

        while(k < edgeCount)
        {
      
            int my_v1, my_v2; //(my_v1, my_v2) edge
            double edge_w;
           
            
            my_v2 = edge_u[k];
            my_v1 = edge_v[k];
            edge_w = edge_weight[k];

            my_nEdge++;

            if ((my_nbneighbors[my_v1+1] + 1) % 100 == 0)
            {
                my_inneighbors[my_v1+1] = (idxType*) realloc (my_inneighbors[my_v1+1], (my_nbneighbors[my_v1+1] + 101) * sizeof(idxType));
                my_inedgeweights[my_v1+1] = (ecType*) realloc (my_inedgeweights[my_v1+1], (my_nbneighbors[my_v1+1] + 101) * sizeof(ecType));
            }
    

            my_inneighbors[my_v1+1][my_nbneighbors[my_v1+1]] = my_v2+1;
            my_inedgeweights[my_v1+1][my_nbneighbors[my_v1+1]] = edge_w;
            my_nbneighbors[my_v1+1]++;
            
            k++;
        }
        printf("my vertex count = %d my edge count= %d\n", vertexCount, my_nEdge);

        //exit(1);

        G->frmt = 2;
        G->nVrtx = my_nVrtx;
        G->nEdge = my_nEdge;
        G->totvw = my_nVrtx;
        G->maxindegree = 0;
        G->hollow  = (int * ) calloc(my_nVrtx + 2, sizeof(int));
        G->inStart  = (idxType * ) calloc(my_nVrtx + 2, sizeof(idxType));
        G->inEnd= (idxType * ) malloc(sizeof(idxType) * (my_nVrtx + 2));
        G->in = (idxType * ) malloc(sizeof(idxType) * my_nEdge);

        //anik add
        G->vertices = (char**)malloc(sizeof(char*)*(my_nVrtx+1));
        for(i = 0;i<my_nVrtx;++i){
            G->vertices[i] = (char*)malloc(100*sizeof(char));
        }
        G->ecIn = (ecType *) malloc(sizeof(ecType) * my_nEdge);
        G->ecOut = (ecType *) malloc(sizeof(ecType) * my_nEdge);

        idxType idx = 0, degree;

        for (i=1; i<=my_nVrtx; i++){
            G->inStart[i] = idx;
            G->inEnd[i-1] = idx-1;
            if (i>1){
                degree = G->inEnd[i-1] - G->inStart[i-1] + 1;
                G->maxindegree = G->maxindegree < degree ? degree : G->maxindegree;
            }
            for (j=0; j< my_nbneighbors[i]; j++){
                G->in[idx] = my_inneighbors[i][j];
                G->ecIn[idx] = my_inedgeweights[i][j];
                idx++;
            }

        }
        G->inStart[0] = 0;
        G->inEnd[0] = -1;
        G->inEnd[my_nVrtx] = idx-1;
        G->inEnd[my_nVrtx+1] = my_nEdge;
        G->inStart[my_nVrtx+1] = my_nEdge;
        degree = G->inEnd[my_nVrtx] - G->inStart[my_nVrtx] + 1;
        G->maxindegree = G->maxindegree < degree ? degree : G->maxindegree;
        if (my_nEdge <= 0)
            u_errexit("allocateDGraphData: empty edge set\n");

        if (my_nVrtx <=-1)
            u_errexit("allocateDGraphData: empty vertex set\n");

        G->outStart  = (idxType * ) malloc(sizeof(idxType) * (my_nVrtx + 2));
        G->outEnd  = (idxType * ) malloc(sizeof(idxType) * (my_nVrtx + 2));
        G->out = (idxType * ) malloc(sizeof(idxType) * my_nEdge);

        G->vw = (vwType *) malloc(sizeof(vwType) * (my_nVrtx+1));
        G->vWeight = (ecType*)malloc(sizeof(ecType)*(my_nVrtx+1));
    //    G->totvw = 0;
        for (i=1; i<=my_nVrtx; i++){
      //      G->vw[i] = vweights[i-1];
            G->vw[i] = 1.0;
     //       G->totvw += G->vw[i];
    //      printf("G.vw[%d] = %lf totvw = %lf\n",i,G->vw[i],G->totvw);
        }

        for (i=0; i< my_nEdge; i++){
    //      G->ecIn[i] = 1;
    //      G->ecOut[i] = 1;
    //      G->ecIn[i] = edge_weights[i];
    //      G->ecOut[i] = edge_weights[i];
        }

     //   for(i=1;i<=my_nVrtx;++i){
     //     for(j=G->ecIn[])
    //    }

        for(i=0;i<my_nVrtx;++i){
     
            strcpy(G->vertices[i],vertexName[i]);
            //printf("vertices %d = %s\n",i,G->vertices[i]);
            G->vWeight[i+1] = vertexWeight[i+1];
            //printf("vWeight[%d] = %lf\n",i+1,G->vWeight[i+1]);
        }

//      for(i = 0 ; i < G->nEdge ; i++){
//          printf("ecIn[%d] = %lf\n",i,G->ecIn[i]);
//      }

     //   printf("before fillout for min\n");
    //    for(i = 0 ; i < G->nEdge ; i++){
    //      printf("out[%d] = %d\n",i,G->out[i]);
    //    }

        // FILE* indegree_file;
        // indegree_file = fopen("indegree.txt","w");
        // for(i = 1 ; i <= my_nVrtx ; i++){
        //     //fprintf(indegree_file, "%s %d\n", G->vertices[i-1],G->inEnd[i] - G->inStart[i]);
        // }


        // fclose(indegree_file);

        printf("before calling filloutfrom\n");

        FILE* entire_small_graph;
        entire_small_graph = fopen("entire_small_graph.txt","w");
        for(i = 1 ; i <= my_nVrtx ; i++){
            for (j = G->inStart[i]; j <= G->inEnd[i]; ++j)
            {
                /* code */
                fprintf(entire_small_graph, "%s ---> %s\n", G->vertices[G->in[j]-1],G->vertices[i-1]);
            }
        }
        fclose(entire_small_graph);



        
        fillOutFromIn(G);

    // FILE *initial_graph;
    // initial_graph = fopen("initial_graph.dot","w");
    // fprintf(initial_graph,"digraph {\n");

    // for (i = 1;i<=G->nVrtx ; i++){
    //     for(j = G->inStart[i] ; j <= G->inEnd[i]; j++){
    //         //printf("%d ---> %d\n",G->in[j],i);
    //         //fprintf(initial_graph,"%d -> %d;\n",G->in[j],i);
    //     }
    // }
    // fprintf(initial_graph, "}\n" );

    // fclose(initial_graph);



        printf("filloutfrom in done\n");







        //exit(1);
       // printf("total vweight = %lf\n",G->totvw); 
        
        if(use_binary_input){
            FILE *fp = fopen(fnbin,"wb");

            if(fp == NULL){
               u_errexit("Cannot open file");
            }

            fwrite(&G->frmt,sizeof(G->frmt),1,fp);
            fwrite(&G->nVrtx,sizeof(G->nVrtx),1,fp);
            fwrite(&G->nEdge,sizeof(G->nEdge),1,fp);
            fwrite(&G->totvw,sizeof(G->totvw),1,fp);
            fwrite(&G->maxindegree,sizeof(G->maxindegree),1,fp);
            fwrite(&G->maxoutdegree,sizeof(G->maxoutdegree),1,fp);
            fwrite(G->inStart,sizeof(G->inStart[0]),(nVrtx+2),fp);
            fwrite(G->inEnd,sizeof(G->inEnd[0]),(nVrtx+2),fp);
            fwrite(G->in,sizeof(G->in[0]),nEdge,fp);
            fwrite(G->vw,sizeof(G->vw[0]),(nVrtx+1),fp);
            fwrite(G->outStart,sizeof(G->outStart[0]),(nVrtx+2),fp);
            fwrite(G->outEnd,sizeof(G->outEnd[0]),(nVrtx+2),fp);
            fwrite(G->out,sizeof(G->out[0]),nEdge,fp);
            fwrite(G->ecIn,sizeof(ecType),nEdge,fp);
            fwrite(G->ecOut,sizeof(ecType),nEdge,fp);

            //anik add
            fwrite(G->vertices,sizeof(G->vertices[0]),(nVrtx+1),fp);
            fwrite(G->vWeight,sizeof(G->vWeight[0]),(nVrtx+1),fp);


            fclose(fp);
        }




        for (i = 1; i <= nVrtx; i++)
            free(inneighbors[i]);
        free(nbneighbors);
        free(inneighbors);



}


void my_BFS(dgraph* G,idxType* visited){
	int head,tail;
	idxType* active_nodes = (idxType*)malloc(G->nVrtx*sizeof(idxType));
	int i;
	int active_node_count,visited_head = 1;
	active_node_count = sourcesList(G,active_nodes);
	int out_vertex;
	idxType* already_visited = (idxType*)calloc(G->nVrtx+1,sizeof(idxType));
	head = 0;
	tail = active_node_count - 1;
	//printf("inside my BFS\n");
	while(head<=tail){
		int curr = active_nodes[head++];
		if(tail>G->nVrtx)
			printf("something wrong, tail > vertex count\n");
		visited[visited_head++] = curr;
//		printf("visited[%d] = %d head = %d\n",visited_head-1,visited[visited_head-1],head-1);
		for(i = G->outStart[curr] ; i <= G->outEnd[curr] ; ++i){
			out_vertex = G->out[i];

			if(already_visited[out_vertex]==1)
				continue;
			active_nodes[++tail] = out_vertex;
			already_visited[out_vertex] = 1;
	//		printf("out vertex = %d tail = %d\n",out_vertex,tail);

		}

	}
	if(visited_head != G->nVrtx+1)
	{
		printf("graph is not connected\n");
	}
	free(active_nodes);
	free(already_visited);
	//printf("\nfreed\n");
}


/*lowest common ancestor*/
idxType my_LCA(dgraph* G, idxType node1, idxType node2){

    idxType* visited = (idxType*)calloc(G->nVrtx,sizeof(idxType));
    int i,j,current_ancestor;
    idxType* added = (idxType*)calloc(G->nVrtx, sizeof(idxType));
    idxType* ancestorList_node1 = (idxType*)malloc(G->nVrtx*sizeof(idxType));

    idxType* ancestorList_node2 = (idxType*)malloc(G->nVrtx*sizeof(idxType));
    int ancestor_list_index =  0;
    visited[node1] = 1;
    for (i = G->inStart[node1] ; i<= G->inEnd[node1] ; i++){
        ancestorList_node1[ancestor_list_index++] = G->in[i];
    }
    while(ancestor_list_index>0){
        current_ancestor = ancestorList_node1[ancestor_list_index-1];
        ancestor_list_index--;
        visited[current_ancestor] = 1;
        //printf("curr ancestor of node %d = %d\n",node1,current_ancestor);
        for(j = G->inStart[current_ancestor] ; j<= G->inEnd[current_ancestor] ; j++){
            if(added[j]) continue;
            added[j] = 1;
            ancestorList_node1[ancestor_list_index++] = G->in[j];
        }

    }

    for (i = G->inStart[node2] ; i<= G->inEnd[node2] ; i++){
        ancestorList_node2[ancestor_list_index++] = G->in[i];
    }

    /*if node2 is the ancestor then return*/
    if(visited[node2]){
        //free(added);
        //free(ancestorList_node2);
        //free(ancestorList_node1);
        //free(visited);

        return node2;
    }

    while(ancestor_list_index>0){
        current_ancestor = ancestorList_node2[ancestor_list_index-1];
        ancestor_list_index--;
        if(visited[current_ancestor]){
            //free(added);
            //free(ancestorList_node2);
            //free(ancestorList_node1);
            //free(visited);
            return current_ancestor;
        }
        visited[current_ancestor] = 1;
        //printf("curr ancestor of node %d = %d\n",node2,current_ancestor);
        for(j = G->inStart[current_ancestor] ; j<= G->inEnd[current_ancestor] ; j++){
            if(added[j]) continue;
            added[j] = 1;
            ancestorList_node2[ancestor_list_index++] = G->in[j];
        }

    }
    //free(added);
    //free(ancestorList_node2);
    //free(ancestorList_node1);
    //free(visited);


    return 0;


}

/* MY cycle detection in a graph*/





