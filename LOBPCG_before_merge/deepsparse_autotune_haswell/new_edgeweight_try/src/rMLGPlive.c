#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <sys/stat.h>

#include <getopt.h>


#include "dgraph.h"
#include "bmmio.h"
#include "utils.h"
#include "debug.h"
#include "vcycle.h"
#include "rvcycle.h"
#include "partDGraphAlgos.h"
#include "matching.h"
#include "infoPart.h"

/*
char *infilename;
int K=2;
float eps = 0.03;
*/

/*********************************************************************************/
void printUsage_rMLGPlive(char *exeName)
{
    printf("Usage: %s <input-file-name> <MaxLiveSet> [options...]\n", exeName);
    printf("Option:\n");

    //seed
    printf("\t-seed int \t(default = 0)\n");
    printf("\t\tSeed for the random generator\n");

    //ratio
    printf("\t--ratio float \t(default = 1.03)\n");
    printf("\t\tImbalance ration between parts\n");

    //debug
    printf("\t--debug int \t(default = 0)\n");
    printf("\t\tDebug value\n");

    //print
    printf("\t--print int \t(default = 0)\n");
    printf("\t\tValue to output graphs\n");

    //runs
    printf("\t--runs int \t(default = 1)\n");
    printf("\t\tNumber of runs for the graph\n");

    //co_stop
    printf("\t-co_stop int \t(default = 0)\n");
    printf("\t\tSet the stoping criteria pour the coarsening step\n");
    printf("\t\t= 0 : Stop when the size of graph is < co_stop_size\n");
    printf("\t\t= 1 : Stop after co_stop_step steps\n");

    //co_stop_size
    printf("\t-co_stop_size int \t(default = 50 * #parts)\n");
    printf("\t\tSet co_stop_size when co_stop = 0\n");
    
    //co_stop_step
    printf("\t-co_stop_step int \t(default = 10)\n");
    printf("\t\tSet co_stop_step when co_stop = 1\n");

    //co_match
    printf("\t--co_match int \t(default = 0)\n");
    printf("\t\tSet the matching technic for the coarsening step\n");
    printf("\t\t= 0 : Pairwise matching with top level difference 1\n");
    printf("\t\t= 1 : Aggregative matching with top level difference 1\n");
    printf("\t\t= 2 : Aggregative matching with out top level constraint\n");

    //co_match_norder
    printf("\t--co_match_norder int \t(default = 1)\n");
    printf("\t\tSet the node traversal order for the matching phase\n");
    printf("\t\t= 0 : Random traversal\n");
    printf("\t\t= 1 : Depth First Topological order\n");
    printf("\t\t= 2 : Random Depth First Topological order\n");
    printf("\t\t= 3 : Breadth First Topological order\n");
    printf("\t\t= 4 : Random Breadth First Topological order\n");
    printf("\t\t= 5 : Depth First order\n");
    printf("\t\t= 6 : Random Depth First order\n");
    printf("\t\t= 7 : Breadth First order\n");
    printf("\t\t= 8 : Random Breadth First order\n");

    //co_match_eorder
    printf("\t--co_match_eorder int \t(default = 1)\n");
    printf("\t\tSet the edge traversal order for the matching phase\n");
    printf("\t\t= 0 : Natural construction order\n");
    printf("\t\t= 1 : Edge weight priority\n");
    printf("\t\t= 2 : Edge weight / node weight priority\n");

    //co_match_onedegreefirst
    printf("\t--co_match_onedegreefirst int \t(default = 1)\n");
    printf("\t\tDo we traverse the nodes with degree 1 first in the matching?\n");
    printf("\t\t= 0 : No\n");
    printf("\t\t= 1 : Yes\n");

    //co_init
    printf("\t--co_inipart int \t(default = 0)\n");
    printf("\t\tDetermine the initial partitioning\n");
    printf("\t\t= 0 : try all partitioning and pick the best one\n");
    printf("\t\t= 1 : Kernighan with random DFS\n");
    printf("\t\t= 2 : Kernighan with random BFS\n");
    printf("\t\t= 3 : Kernighan with random mix BFS-DFS\n");
    printf("\t\t= 4 : Greedy graph growing approach\n");
    printf("\t\t= 5 : Forced balance initial partitioning\n");
    printf("\t\t= 6 : Metis with fixing acyclicity and forced balance\n");
    printf("\t\t= 7 : BFS Growing\n");

    //co_nbinit
    printf("\t--co_nbinipart int \t(default = 5)\n");
    printf("\t\tNumber of runs for the initial partitioning\n");

    //co_refinement
    printf("\t-co_refinement int \t(default = 5)\n");
    printf("\t\tDetermine the refinement in uncoarsening step\n");
    printf("\t\t= 0 : no refinement\n");
    printf("\t\t= 1 : toward the largest partition in topological order\n");
    printf("\t\t= 2 : toward the best partition if it creates no cycle\n");
    printf("\t\t= 3 : refinement using method 1 and 2 consecutively\n");
    printf("\t\t= 4 : refinement by swaping nodes (for bisection only)\n");
    printf("\t\t= 5 : refinement from largest part to smallest (for bisection only)\n");
    printf("\t\t= 6 : refinement using method 5 and 4 consecutively (for bisection only)\n");

    //co_obj
    printf("\t--co_obj int \t(default = 0)\n");
    printf("\t\tDetermine the objective we are trying to minimize\n");
    printf("\t\t= 0 : minimize edge-cut\n");
    printf("\t\t= 1 : minimize communication volume\n");

    //live_cut
    printf("\t--live_cut int \t(default = 0)\n");
    printf("\t\tDetermine the cuting strategy for rMLGPlive\n");
    printf("\t\t= 0 : recursively cut in half until the live set value is smaller than opt.maxliveset\n");
    printf("\t\t= 1 : cut in half and recut in 2/3 1/3 if liveset smaller than opt.maxliveset / 2\n");

    //live_cut_merge
    printf("\t--live_cut_merge int \t(default = 1)\n");
    printf("\t\tDetermine if we merge parts if the max live set is not violated after the recursive cut\n");
    printf("\t\t= 0 : no\n");
    printf("\t\t= 1 : yes\n");

    //part_ub
    printf("\t-part_ub double \t(default = (tot_weight/nbpart) * 1.03)\n");
    printf("\t\tUpper bound for the partition\n");

    //part_lb
    printf("\t-part_lb double \t(default = (tot_weight/nbpart) / 1.03)\n");
    printf("\t\tLower bound for the partition\n");
}


int processArgs_rMLGPlive(int argc, char **argv, MLGP_option* opt)
{
    int i;
    if (argc < 3){
	    printUsage_rMLGPlive(argv[0]);
	    u_errexit("%s: There is a problem with the arguments.\n", argv[0]);
    }

    strncpy(opt->file_name, argv[1], PATH_MAX);    

    opt->maxliveset = atoi(argv[2]);
/*
    if ((opt->nbPart != 2)&&(opt->nbPart != 4)&&(opt->nbPart != 8)&&(opt->nbPart != 16)&&(opt->nbPart != 32)&&(opt->nbPart != 64)&&(opt->nbPart != 128)&&(opt->nbPart != 256)&&(opt->nbPart != 512)&&(opt->nbPart != 1024)){
	printUsage_rMLGP(argv[0]);
	u_errexit("%s: This number of partitions is not xsupported for now. Try a power of 2.\n", argv[0]);
    }
*/
    opt->nbPart = opt->maxliveset;
    opt->seed = 0;
    opt->co_stop = 0;
    opt->co_stop_size = 100;//50*opt->nbPart;
    opt->co_stop_step = 10;
    opt->co_match = 0;
    opt->co_match_norder = 1;
    opt->co_match_eorder = 1;
    opt->co_match_onedegreefirst = 1;
    opt->co_inipart = 0;
    opt->co_nbinipart = 5;
    opt->co_refinement = 5;
    opt->live_cut = 0;
    opt->live_cut_merge = 1;


    opt->ub = (double*)malloc((opt->nbPart)*sizeof(double));
    opt->lb = (double*)malloc((opt->nbPart)*sizeof(double));

    for (i=0;i<opt->nbPart;i++){
        opt->ub[i] = -1;
        opt->lb[i] = -1;
    }

    opt->ref_step = 10;
    opt->co_obj = 0;
    opt->ratio = 1.5;
    opt->runs = 1;
    opt->debug = 0;
    opt->print = 0;
    
    static struct option long_options[] =
    {
        {"seed",  required_argument, 0, 's'},
	{"co_stop",  required_argument, 0, 't'},
	{"co_stop_size",  required_argument, 0, 'z'},
	{"co_stop_step",  required_argument, 0, 'p'},
	{"co_match",  required_argument, 0, 'm'},
        {"co_match_norder",  required_argument, 0, 'o'},
        {"co_match_eorder",  required_argument, 0, 'g'},
	{"co_match_onedegreefirst",  required_argument, 0, '1'},
	{"co_inipart",  required_argument, 0, 'i'},
	{"co_nbinipart",  required_argument, 0, 'b'},
	{"co_refinement",  required_argument, 0, 'r'},
	{"co_obj",  required_argument, 0, 'j'},
	{"part_ub",  required_argument, 0, 'u'},
	{"part_lb",    required_argument, 0, 'l'},
	{"ratio",    required_argument, 0, 'a'},
	{"runs",    required_argument, 0, 'n'},
	{"debug",    required_argument, 0, 'd'},
    {"print",    required_argument, 0, 'q'},
    {"live_cut",    required_argument, 0, 'c'},
    {"live_cut_merge",    required_argument, 0, 'w'},

    };

    const char *delims = "s:t:z:p:m:o:g:1:i:r:j:u:l:a:n:d:q:c:w:";
    char o;
    int option_index = 0;
    while ((o = getopt_long(argc, argv, delims, long_options, &option_index)) != -1) {
        switch (o) {
            case 's':
                opt->seed = atoi(optarg);
                break;
            case 't':
                opt->co_stop = atoi(optarg);
                break;
            case 'z':
                opt->co_stop_size = atoi(optarg);
                break;
            case 'p':
                opt->co_stop_step = atoi(optarg);
                break;
            case 'm':
                opt->co_match = atoi(optarg);
                break;
            case 'o':
                opt->co_match_norder = atoi(optarg);
                break;
            case 'g':
                opt->co_match_eorder = atoi(optarg);
                break;
            case '1':
                opt->co_match_onedegreefirst = atoi(optarg);
                break;
            case 'i':
                opt->co_inipart = atoi(optarg);
                break;
            case 'b':
                opt->co_nbinipart = atoi(optarg);
                break;
            case 'r':
                opt->co_refinement = atoi(optarg);
                break;
            case 'j':
                opt->co_obj = atoi(optarg);
                break;
            case 'c':
                opt->live_cut = atoi(optarg);
                break;
            case 'u':
                opt->ub[0] = atof(optarg);
                break;
            case 'l':
                opt->lb[0] = atof(optarg);
                break;
            case 'a':
                opt->ratio = atof(optarg);
                break;
            case 'd':
                opt->debug = atoi(optarg);
                break;
	    case 'q':
                opt->print = atoi(optarg);
                break;
            case 'n':
                opt->runs = atof(optarg);
                break;
            case 'w':
                opt->live_cut_merge = atof(optarg);
                break;
            default:
                printUsage_rMLGPlive(argv[0]);
		u_errexit("Option %s not recognized.", o);
                return 1;
        }
    }

    for(i=1;i<opt->nbPart;i++){
        opt->ub[i]=opt->ub[0];
        opt->lb[i]=opt->lb[0];
    }
    char suf[200];
    char fname[200];
    char* split;    
    char fname_sav[200];
    sprintf(fname_sav, "%s", opt->file_name);
    if (opt->print > 0){
	split = strtok(fname_sav, "/");
	while(split != NULL){
	    sprintf(fname, "%s", split);
	    split = strtok(NULL, "/");
	}
	sprintf(opt->file_name_dot, "out/");
	mkdir(opt->file_name_dot, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IWOTH | S_IXOTH);
	strcat(opt->file_name_dot, fname);
	sprintf(suf, "__%d_%d", opt->nbPart, opt->seed);
	strcat(opt->file_name_dot, suf);
	mkdir(opt->file_name_dot, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IWOTH | S_IXOTH);
	sprintf(suf, "/graph");
	strcat(opt->file_name_dot, suf);
    }    

    return 0;
}


void run_rMLGPlive(char* file_name, MLGP_option opt)
{
    MLGP_info info;
    dgraph G;

//    generateDGraphFromDot(&G, file_name,1);
    generateDGraphFromDot(&G , file_name,0); //dont generate d graph from binary anik
    
    //opt.maxliveset=400;

    printf("maxliveset: %d debug:%d\n", opt.maxliveset,opt.debug);
    printf("Nb node: %d\nNb Edges: %d\n\n", G.nVrtx, G.nEdge);

    ecType* edgecut = (ecType*) malloc(sizeof(ecType) * opt.runs);
    ecType* nbcomm = (ecType*) malloc(sizeof(ecType) * opt.runs);
    double* latencies = (double*) malloc(sizeof(double) * opt.runs);
    int r,i;
    rcoarsen* rcoars;
    int dumpnbpartMain=-1;
    int ls;
    idxType* toporder = (idxType*) malloc(sizeof(idxType)*(G.nVrtx + 2));
    for (r = 0; r<opt.runs; r++){
    	printf("########################## RUN %d ########################\n", r);
    	srand((unsigned) opt.seed + r);
        if(opt.debug > -1){
            ls =calcLiveSizeMixMulti(&G, 1);
            printf("Graph LiveSize: %d\n", ls);
    	}
        rcoars = rVCycleLive(&G, opt, &info,&dumpnbpartMain);
        if(opt.debug > 3)
            printf("dumpnbpartMain:%d\n",dumpnbpartMain );
    	edgecut[r] = edgeCut(&G, rcoars->coars->part);
    	nbcomm[r] = nbCommunications(&G, rcoars->coars->part);
    	latencies[r] = computeLatency(&G, rcoars->coars->part, 1.0, 11.0);
    	printf("Final run %d\t%d\t%d\t%d\t%d\t%f\n", r, G.nVrtx, G.nEdge, edgecut[r], nbcomm[r], latencies[r]);
        print_info_part(rcoars->coars->graph, rcoars->coars->part, opt);
    	opt.seed++;
        /*
        char a[20]="rectpart.dot";
        dgraph_to_dot(&G,rcoars->coars->part,a);
        */
        int* partsize = (int*) malloc(sizeof(int) * dumpnbpartMain);
        int nn = nbPart(&G, rcoars->coars->part, partsize);
        if(opt.debug > 0) {
            DFStopsort(&G, toporder);
	        printf("Done topsort\n");
            ls = calcLiveSizeMix_with_part_Multi(&G, rcoars->coars->part, nn, toporder, 10);
            printf("Partitioned LiveSize: %d\n", ls);
        }

        if(opt.debug > 0) {
            printf("==================\n");
            printf("Nbpart = %d\nPartsize: ", nn);
            int k;
            for (k = 0; k<nn; k++)
                printf("%d ", partsize[k]);
            printf("\n");
            printf("==================\n");

            printf("==================\n");
            printf("Nbpart = %d\nPartLiveSet: ", nn);
            for (k = 0; k<nn; k++)
                printf("%d ", calcLiveSizePartMixMulti(&G, rcoars->coars->part, k, 10));
            printf("\n");
            printf("==================\n");

        }

    	printf("######################## END RUN %d ########################\n\n", r);
    	FILE *fin=fopen("partassign.txt","w");
        for (i=1; i<=rcoars->coars->graph->nVrtx; i++){
            fprintf(fin,"part[%d] %d\n",i, rcoars->coars->part[i]);
        }
        fclose(fin);

        free(partsize);
	}
    double edgecutave = 0.0, nbcommave = 0.0, edgecutsd = 0.0, nbcommsd = 0.0;
    for (r = 0; r<opt.runs; r++){
    	edgecutave += edgecut[r];
    	nbcommave += nbcomm[r];
    }
    edgecutave = edgecutave / opt.runs;
    nbcommave = nbcommave / opt.runs;
    for (r = 0; r<opt.runs; r++){
    	edgecutsd = edgecut[r] - edgecutave < 0? edgecutave - edgecut[r] : edgecut[r] - edgecutave;
    	nbcommsd = nbcomm[r] - nbcommave < 0? nbcommave - nbcomm[r] : nbcomm[r] - nbcommave;
    }
    edgecutsd = edgecutsd / opt.runs;
    nbcommsd = nbcommsd / opt.runs;
    printf("Final runs\t%d\t%d\t%f\t%f\t%f\t%f\n", G.nVrtx, G.nEdge, edgecutave, edgecutsd, nbcommave, nbcommsd);

    /*
    FILE *file;
    char name_tmp[200];
    sprintf(name_tmp,".rMLGP.part.%d.seed.%d", nbpart, opt.seed);
    char res_name[200];
    strcpy(res_name, file_name);
    strcat(res_name, name_tmp);
    file = fopen(res_name, "w");
    idxType i;
    for (i=1; i<=G.nVrtx; i++){
	fprintf(file, "%d\n", (int) rcoars->coars->part[i]);
    }
    fclose(file);
    */
    free(toporder);
    free(G.inStart);
    free(G.inEnd);
    free(G.in);
    free(G.outStart);
    free(G.outEnd);
    free(G.out);
    free(G.hollow);
    free(G.vw);
    free(G.ecIn);
    free(G.ecOut);
    free(edgecut);
    free(nbcomm);
    free(latencies);
    return;
}

int main(int argc, char *argv[])
{
    MLGP_option opt;

    processArgs_rMLGPlive(argc, argv, &opt);

    run_rMLGPlive(opt.file_name, opt);

    return 0;
}
