#include <stdio.h>
#include <stdlib.h>

#include "refinement.h"
#include "infoPart.h"
#include "debug.h"
#include "dgraph.h"
#include "infoPart.h"
#include "utils.h"
#include "vcycle.h"
#include <limits.h>
#include <math.h>


// #define _CHECK_COMM
void printPartMatrix(ecType** partmatrix, idxType nbpart){
   idxType i,j;
   idxType cut = 0, tot = 0;
   printf("Partmatrix:\n");
   for (i=0; i<nbpart; i++){
       for (j=0; j<nbpart; j++){
	   printf("%d ", partmatrix[i][j]);
	   tot += partmatrix[i][j];
	   if (i!=j)
	       cut += partmatrix[i][j];
       }
       printf("\n");
   }
   printf("Tot edge: %d\n", tot);
   printf("Edge cut: %d\n", cut);
}


void buildPartmatrix(dgraph *G, idxType * part, idxType nbpart, ecType** partmatrix)
{
    idxType i,j;
    for (i=0; i< nbpart; i++)
	    for (j=0; j<nbpart; j++)
	        partmatrix[i][j] = 0;
    for (i=1; i<= G->nVrtx; i++){
	    for (j=G->outStart[i]; j<=G->outEnd[i]; j++){
	        idxType node = G->out[j];
	        partmatrix[part[i]][part[node]] += G->ecOut[j];
	    }
    }
    //printPartMatrix(partmatrix, nbpart);
}


int computePartmatrix(dgraph *G, idxType *part, ecType** partmatrix, idxType node, idxType movetopart)
{
    idxType j;
    //idxType nbpart = 4;
    /*
    ecType** partmatrix_save = (ecType**) malloc(sizeof(ecType*) * nbpart);
    for (i=0; i< nbpart; i++)
	partmatrix_save[i] = (ecType*) malloc(sizeof(ecType) * nbpart);
    for (i=0; i< nbpart; i++)
	for (j=0; j< nbpart; j++)
	    partmatrix_save[i][j] = partmatrix[i][j];
    */
    int flag = 0;
    for (j=G->outStart[node]; j<=G->outEnd[node]; j++) {
        idxType out = G->out[j];
        
        if (partmatrix[part[node]][part[out]] - G->ecOut[j]== 0 )
            flag=1;
        partmatrix[part[node]][part[out]] -= G->ecOut[j];
        
        if (partmatrix[movetopart][part[out]] == 0)
            flag=1;
        partmatrix[movetopart][part[out]] += G->ecOut[j];
    }
    for (j=G->inStart[node]; j<=G->inEnd[node]; j++) {
		idxType in = G->in[j];
        if (partmatrix[part[in]][part[node]] - G->ecIn[j] == 0)
            flag = 1;
        partmatrix[part[in]][part[node]] -= G->ecIn[j];
        if (partmatrix[part[in]][movetopart] == 0)
            flag =1;
        partmatrix[part[in]][movetopart] += G->ecIn[j];
    }
    /*
    for (i=0; i< nbpart; i++)
	for (j=0; j< nbpart; j++)
	    if ((partmatrix_save[i][j] >= 0)&&(partmatrix[i][j] < 0)){
		printPartMatrix(partmatrix_save, nbpart);
		printf("ERROR!!!! when moved node %d from %d to %d partmatrix[%d][%d] = %d < 0\n",node,part[node],movetopart,i,j,partmatrix[i][j]);
		printPartMatrix(partmatrix, nbpart);
	    }
    */
    return flag;
}

/*
void moveNode(dgraph *G, idxType inode, idxType *part, ecType* gain, idxType* movableto, idxType* topsortpart)
{
    //This function moves node to movableto[node] in part and updates the gain vector (only if movable[i] != -1 because otherwise it's unmovable
    idxType i,j;
    idxType dest = movableto[inode];
    part[inode] = dest;
    gain[inode] = 0;
    for (i=G->inStart[inode]; i<=G->inEnd[inode]; i++){
	idxType node = G->in[i];
	if (topsortpart[movableto[node]] > topsortpart[dest]){
	    //printf("-> Cange movableto[%d] from %d to  %d\n", node, movableto[node], dest);
	    movableto[node] = dest;
	}
	ecType newgain = 0;
	for (j=G->inStart[node]; j<=G->inEnd[node]; j++){
	    idxType innode = G->in[j];
	    if (part[innode] == part[node])
		newgain -= G->ecIn[j];
	    else if (part[innode] == movableto[node])
		newgain += G->ecIn[j];		
	}
	for (j=G->outStart[node]; j<=G->outEnd[node]; j++){
	    idxType outnode = G->out[j];
	    if (part[outnode] == part[node])
		newgain -= G->ecOut[j];
	    else if (part[outnode] == movableto[node])
		newgain += G->ecOut[j];		
	}
	gain[node] = newgain;
    }
    for (i=G->outStart[inode]; i<=G->outEnd[inode]; i++){
	idxType node = G->out[i];
	if (topsortpart[movableto[node]] < topsortpart[dest]){
	    //printf("-> Cange movableto[%d] from %d to  %d\n", node, movableto[node], dest);
	    movableto[node] = dest;
	}
	ecType newgain = 0;
	for (j=G->inStart[node]; j<=G->inEnd[node]; j++){
	    idxType innode = G->in[j];
	    if (part[innode] == part[node])
		newgain -= G->ecIn[j];
	    else if (part[innode] == movableto[node])
		newgain += G->ecIn[j];		
	}
	for (j=G->outStart[node]; j<=G->outEnd[node]; j++){
	    idxType outnode = G->out[j];
	    if (part[outnode] == part[node])
		newgain -= G->ecOut[j];
	    else if (part[outnode] == movableto[node])
		newgain += G->ecOut[j];		
	}
	gain[node] = newgain;
    }
}
*/

idxType computeMovabletoUpNode(dgraph* G, idxType* part, int nbpart, idxType* topsortpart, idxType* toporderpart, idxType* partsize, idxType* movableto, idxType i) {
    idxType moveup = -1, j;
    for (j = G->inStart[i]; j <= G->inEnd[i]; j++) {
        idxType innode = G->in[j];
        idxType parti = part[innode];
        if (moveup == -1 && (topsortpart[parti] <= topsortpart[part[i]]))
            moveup = parti;
        if ((topsortpart[parti] > topsortpart[moveup]) && (topsortpart[parti] <= topsortpart[part[i]]))
            moveup = parti;
    }
    if (moveup == -1)
        moveup = topsortpart[part[i]] == 0 ? part[i] : toporderpart[topsortpart[part[i]] - 1];
    return moveup;
}

idxType computeMovabletoDownNode(dgraph* G, idxType* part, int nbpart, idxType* topsortpart, idxType* toporderpart, idxType* partsize, idxType* movableto, idxType i) {
    idxType movedown = -1, j;
    for (j = G->outStart[i]; j <= G->outEnd[i]; j++) {
        idxType outnode = G->out[j];
        idxType parti = part[outnode];
        if (movedown == -1 && (topsortpart[parti] >= topsortpart[part[i]]))
            movedown = parti;
        if ((topsortpart[parti] < topsortpart[movedown]) && (topsortpart[parti] >= topsortpart[part[i]]))
            movedown = parti;
    }
    if (movedown == -1)
        movedown = topsortpart[part[i]] == nbpart - 1 ? part[i] : toporderpart[topsortpart[part[i]] + 1];
    return movedown;
}

void computeMovabletoNode(dgraph* G, idxType* part, int nbpart, idxType* topsortpart, idxType* toporderpart, idxType* partsize, idxType* movableto, idxType i) {
    idxType moveup = computeMovabletoUpNode(G, part, nbpart, topsortpart, toporderpart, partsize, movableto, i);
    idxType movedown = computeMovabletoDownNode(G, part, nbpart, topsortpart, toporderpart, partsize, movableto, i);
    if ((movedown == part[i])&&(moveup == part[i]))
		movableto[i] = part[i];
    else if ((movedown != part[i])&&(moveup != part[i]))
		if (partsize[movedown] < partsize[moveup])
		    movableto[i] = movedown;
		else
		    movableto[i] = moveup;
    else if (moveup != part[i])
		movableto[i] = moveup;
    else
		movableto[i] = movedown;
}


ecType computeGainNode(dgraph *G, idxType *part, idxType movableto, idxType node, MLGP_option opt)
{
    idxType j;
    ecType newgain = 0;
    if (movableto == part[node])
	    return 0;
	
    switch (opt.co_obj){
        case CO_OBJ_EC :
            for (j=G->inStart[node]; j<=G->inEnd[node]; j++){
                idxType innode = G->in[j];
                if (part[innode] == part[node])
                    newgain -= G->ecIn[j];
                else if (part[innode] == movableto)
                    newgain += G->ecIn[j];
            }
            for (j=G->outStart[node]; j<=G->outEnd[node]; j++){
                idxType outnode = G->out[j];
                if (part[outnode] == part[node])
                    newgain -= G->ecOut[j];
                else if (part[outnode] == movableto)
                    newgain += G->ecOut[j];
            }
            break;
        case CO_OBJ_CV :
            //int* partdone = (int*) calloc(opt.nbPart, sizeof(int));
            for (j=G->inStart[node]; j<=G->inEnd[node]; j++){
                idxType innode = G->in[j];
                /*
                 if (partdone[part[innode]] == 1)
                 continue;
                 partdone[part[innode]] = 1;
                 */
                if (part[innode] == part[node])
                    newgain -= G->ecIn[j];
                else if (part[innode] == movableto)
                    newgain += G->ecIn[j];
            }
            for (j=G->outStart[node]; j<=G->outEnd[node]; j++){
                idxType outnode = G->out[j];
                /*
                 if (partdone[part[outnode]] == 1)
                 continue;
                 partdone[part[outnode]] = 1;
                 */
                if (part[outnode] == part[node])
                    newgain -= G->ecOut[j];
                else if (part[outnode] == movableto)
                    newgain += G->ecOut[j];
            }
            //free(partdone);
            break;
    }
    return newgain;

}

void moveAndUpdateGainNode(dgraph *G, ecType* gain, idxType *part, idxType nbpart, idxType* movableto, idxType* topsortpart, idxType* toporderpart, idxType* partsize, idxType node, MLGP_option opt)
{
    idxType j;
    //First we update movableto of neighbors
    for (j=G->inStart[node]; j<=G->inEnd[node]; j++){
        idxType innode = G->in[j];
        if (topsortpart[movableto[innode]] < topsortpart[part[innode]])
            continue;
        if (topsortpart[movableto[innode]] > topsortpart[part[innode]]){
            if (topsortpart[movableto[innode]] > topsortpart[movableto[node]])
                movableto[innode] = movableto[node];
            else
                continue;
        }
        if (movableto[innode] == part[innode]) {
            movableto[innode] = computeMovabletoDownNode(G, part, nbpart, topsortpart, toporderpart, partsize, movableto, innode);
        }
    }
    for (j=G->outStart[node]; j<=G->outEnd[node]; j++){
        idxType outnode = G->out[j];
        if (topsortpart[movableto[outnode]] > topsortpart[part[outnode]])
            continue;
        if (topsortpart[movableto[outnode]] < topsortpart[part[outnode]]){
            if (topsortpart[movableto[outnode]] < topsortpart[movableto[node]])
                movableto[outnode] = movableto[node];
            else
                continue;
        }
        if (movableto[outnode] == part[outnode]) {
            movableto[outnode] = computeMovabletoUpNode(G, part, nbpart, topsortpart, toporderpart, partsize, movableto, outnode);
        }
    }

    //Now we update gain
    switch (opt.co_obj){
        case CO_OBJ_EC :
            for (j=G->inStart[node]; j<=G->inEnd[node]; j++){
                idxType innode = G->in[j];
                if (movableto[innode] == part[innode]) {
                    gain[innode] = 0;
                    continue;
                }
                if ((part[node] == movableto[innode])&&(movableto[node] != movableto[innode]))
                    gain[innode] -= G->ecIn[node];
                if ((part[node] != movableto[innode])&&(movableto[node] == movableto[innode]))
                    gain[innode] += G->ecIn[node];
            }
            for (j=G->outStart[node]; j<=G->outEnd[node]; j++){
                idxType outnode = G->out[j];
                if (movableto[outnode] == part[outnode]) {
                    gain[outnode] = 0;
                    continue;
                }
                if ((part[node] == movableto[outnode])&&(movableto[node] != movableto[outnode]))
                    gain[outnode] -= G->ecOut[node];
                if ((part[node] != movableto[outnode])&&(movableto[node] == movableto[outnode]))
                    gain[outnode] += G->ecOut[node];
            }
            break;

        case CO_OBJ_CV :
            //int* partdone = (int*) calloc(opt.nbPart, sizeof(int));
            for (j=G->inStart[node]; j<=G->inEnd[node]; j++){
                idxType innode = G->in[j];
                if (movableto[innode] == part[innode])
                    gain[innode] = 0;
                else
                    gain[innode] = computeGainNode(G, part, movableto[innode], innode, opt);

            }
            for (j=G->outStart[node]; j<=G->outEnd[node]; j++){
                idxType outnode = G->out[j];
                if (movableto[outnode] == part[outnode])
                    gain[outnode] = 0;
                else
                gain[outnode] = computeGainNode(G, part, movableto[outnode], outnode, opt);
            }
            break;
    }
    part[node] = movableto[node];
    gain[node] = 0;
}


int thereIsCycle(idxType nbpart, ecType** partmatrix)
{
    idxType* ready = (idxType*) malloc(nbpart * sizeof(idxType));
    idxType* traversed = (idxType*) calloc(nbpart, sizeof(idxType));
    idxType nbready = 0, i, j, nbtraversed = 0;
 
    /*
    for (i=0; i<nbpart; i++){
	for (j=0; j<nbpart; j++)
	    printf("%d ", partmatrix[i][j]);
	printf("\n");
    }
    */

    for (i = 0; i < nbpart; i++){
	int nbin = 0;
	for (j = 0; j < nbpart; j++)
	    if ((partmatrix[j][i] > 0)&&(j!=i))
		nbin++;
	if (nbin == 0)
	    ready[nbready++] = i;
    }
    //printf("nbready %d\n", nbready);
    while (nbready > 0){
	idxType node = ready[--nbready];
	//printf("node %d\n",node);
	traversed[node] = 1;
	nbtraversed++;
	for (i = 0; i <nbpart; i++){
	    if (partmatrix[node][i] == 0)
		continue;
	    if (traversed[i] == 1)
		continue;
	    int is_ready = 1;
            for (j = 0; j < nbpart; j++){
		if ((partmatrix[j][i] == 0)||(j==i))
		    continue;
                if (traversed[j] == 0){
                    is_ready = 0;
		    //printf("income %d not traversed\n",j);
		}
	    }
	    //printf("i = %d is ready: %d\n", i, is_ready);
            if (is_ready == 1)
                ready[nbready++] = i;
	}
    }
    //printf("nbtraversed %d\n", nbtraversed);
    free(ready);
    free(traversed);
    return (nbtraversed != nbpart);
}


int createCycleSwap(dgraph* G, idxType* part, idxType nbpart, ecType** partmatrix,
				idxType node0, idxType node1, idxType movableto0,idxType movableto1)
{
    idxType part_save0 = part[node0];
    idxType part_save1 = part[node1];
    /*
    idxType i,j;
    
    ecType** partmatrix_save = (ecType**) malloc(sizeof(ecType*) * nbpart);
    for (i=0; i< nbpart; i++)
	partmatrix_save[i] = (ecType*) malloc(sizeof(ecType) * nbpart);
    for (i=0; i< nbpart; i++)
	for (j=0; j< nbpart; j++)
	    partmatrix_save[i][j] = partmatrix[i][j];
    */
    int res=0;
    computePartmatrix(G, part, partmatrix, node0, movableto0); // don't care about intermediate matrix since we are swapping, it may be fixed
    part[node0] = movableto0;
    int flag =computePartmatrix(G, part, partmatrix, node1, movableto1);
    part[node1] = movableto1;
    if (flag>0)
        res = thereIsCycle(nbpart, partmatrix);
    computePartmatrix(G, part, partmatrix, node1, part_save1);
    part[node1] = part_save1;
    computePartmatrix(G, part, partmatrix, node0, part_save0);
    part[node0] = part_save0;
    /*
    for (i=0; i< nbpart; i++)
	for (j=0; j< nbpart; j++)
	    if (partmatrix_save[i][j] != partmatrix[i][j]){
		printf("ERROR!!!! partmatrix[%d][%d] changed\n",i,j);
		free(partmatrix_save);
		free(partmatrix_save);
	    }
    */
    return res;
}

int createCycle(dgraph* G, idxType* part, idxType nbpart, ecType** partmatrix, idxType node, idxType movableto)
{
    idxType part_save = part[node];
    //idxType i,j;
    /*
    ecType** partmatrix_save = (ecType**) malloc(sizeof(ecType*) * nbpart);
    for (i=0; i< nbpart; i++)
	partmatrix_save[i] = (ecType*) malloc(sizeof(ecType) * nbpart);
    for (i=0; i< nbpart; i++)
	for (j=0; j< nbpart; j++)
	    partmatrix_save[i][j] = partmatrix[i][j];
    */
    int flag = computePartmatrix(G, part, partmatrix, node, movableto);
    part[node] = movableto;
    int res = 0;
    if (flag)
        res = thereIsCycle(nbpart, partmatrix);
    computePartmatrix(G, part, partmatrix, node, part_save);
    part[node] = part_save;
    /*
    for (i=0; i< nbpart; i++)
	for (j=0; j< nbpart; j++)
	    if (partmatrix_save[i][j] != partmatrix[i][j]){
		printf("ERROR!!!! partmatrix[%d][%d] changed\n",i,j);
		free(partmatrix_save);
		free(partmatrix_save);
	    }
    */
    return res;
}

/*
int checkConstraints(dgraph* G,idxType *part, ecTpe** partmatrix, idxType node0,idxType node1,idxType movableto0, idxType movableto1){
		
}
*/

// SWAPS nodes between parts. Move one from first to second, move one from second to first. 
void refinementPostOrder_Swap(dgraph *G, ecType** partmatrix, idxType *toporderpart,
                              double* lb_pw, double* ub_pw,
                              idxType *part, idxType nbpart, ecType* edgecut, MLGP_option opt)
{
  if (opt.nbPart != 2){
	u_errexit("Refinement refinementPostOrder_Swap only works for nbpart = 2\n");
	      }
    idxType* heap[2];
    idxType i,j;
    idxType* movableto = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    idxType* partsize = (idxType*) malloc(sizeof(idxType)*(nbpart+1));
    idxType* partcopy = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    idxType* topsortpart = (idxType*) malloc (sizeof(idxType) * nbpart);
    idxType* moved = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    ecType* gain = (ecType*) malloc(sizeof(ecType)*(G->nVrtx+1));
    heap[0] = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    heap[1] = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    int* inheap = (int*) malloc(sizeof(int)*(G->nVrtx+1));
    int* hasbeenmoved = (int*) malloc(sizeof(idxType)*(G->nVrtx+1));
    int frmt = G->frmt;
    idxType hsize[2] = {0,0};
    idxType maxpartsize = 0;

    for (i=0; i<nbpart; i++){
        partsize[i] = 0;
        topsortpart[toporderpart[i]] = i;
    }
    for (i=1; i<=G->nVrtx; i++){
        inheap[i] = 0;
        hasbeenmoved[i] = 0;
        partcopy[i] = part[i];
        computeMovabletoNode(G, part, nbpart, topsortpart, toporderpart, partsize, movableto, i);
        gain[i] = computeGainNode(G, part, movableto[i], i, opt);
        if (movableto[i] != part[i]){
            heap[part[i]][++hsize[part[i]]] = i;
            inheap[i] = 1;
        }
        if (frmt & DG_FRMT_VW) // if it has vertex weights
            partsize[part[i]] += G->vw[i];
        else
            partsize[part[i]] += 1;
    }
    for (i=0; i<nbpart; i++)
        maxpartsize = maxpartsize < partsize[i] ? partsize[i] : maxpartsize;
    heapBuild(G, heap[0], gain, hsize[0]);
    heapBuild(G, heap[1], gain, hsize[1]);
    int flagger=0; // 1 for first if, 2 for second if, 3 for third, 4 for fourth.
    idxType nbmoved = 0, minmaxpartsize = maxpartsize;
    int without_impro = 0, minindex = 0;
    ecType minedgecut = *edgecut;
    int isThereCycle=0;
    int secondNodes[2]={-1,-1};
    idxType node[2]={-1,-1};
    idxType newsize[2]={-1,-1};

    while ((hsize[0] > 0 && hsize[1] > 0)&&(without_impro <= G->nVrtx/4)&&(nbmoved<G->nVrtx)){
        //first heap
        node[0] = heapExtractMax(G, heap[0], gain, &hsize[0]);
        inheap[node[0]] = 0;
        while ((hsize[0] > 0)&&(movableto[node[0]]==part[node[0]])){
            node[0] = heapExtractMax(G, heap[0], gain, &hsize[0]);
            inheap[node[0]] = 0;
        }
        if (movableto[node[0]]==part[node[0]]) //there is no more movable node[0]s
            break;
        //second heap
        node[1] = heapExtractMax(G, heap[1], gain, &hsize[1]);
        inheap[node[1]] = 0;
        while ((hsize[1] > 0)&&(movableto[node[1]]==part[node[1]])){
            node[1] = heapExtractMax(G, heap[1], gain, &hsize[1]);
            inheap[node[1]] = 0;
        }
        if (movableto[node[1]]==part[node[1]]) //there is no more movable node[0]s
            break;

        flagger=0;
        // Check cycle and constraint for top pair
        {
            isThereCycle = createCycleSwap(G,part,nbpart,partmatrix,node[0],node[1],movableto[node[0]],movableto[node[1]]);
            if (!isThereCycle){
                //check new sizes to see if we can move one node from the first part to second one
                newsize[0]=partsize[0];
                newsize[part[node[0]]] -= G->vw[node[0]];
                newsize[movableto[node[0]]] += G->vw[node[0]];

                newsize[1]=partsize[1];
                newsize[part[node[1]]] -= G->vw[node[1]];
                newsize[movableto[node[1]]] += G->vw[node[1]];

                if (!((newsize[0] < lb_pw[0])||(newsize[0] > ub_pw[0])||(newsize[1] < lb_pw[1])||(newsize[1] > ub_pw[1])))
                    flagger=1;
            }
        }

        if(flagger==0){
            if(hsize[0]>0){
                //Get the second best from heaps
                secondNodes[0] = heapExtractMax(G, heap[0], gain, &hsize[0]);
                inheap[secondNodes[0]] = 0;
                while ((hsize[0] > 0)&&(movableto[secondNodes[0]]==part[secondNodes[0]])){
                    secondNodes[0] = heapExtractMax(G, heap[0], gain, &hsize[0]);
                    inheap[secondNodes[0]] = 0;
                }
                if (movableto[secondNodes[0]]==part[secondNodes[0]]) //there is no more movable node[0]s
                    secondNodes[0]=-1;
            }
            else{
                secondNodes[0]=-1;
            }
            //Get the second best from heaps
            if(hsize[1]>0){
                secondNodes[1] = heapExtractMax(G, heap[1], gain, &hsize[1]);
                inheap[secondNodes[1]] = 0;
                while ((hsize[1] > 0)&&(movableto[secondNodes[1]]==part[secondNodes[1]])){
                    secondNodes[1] = heapExtractMax(G, heap[1], gain, &hsize[1]);
                    inheap[secondNodes[1]] = 0;
                }
                if (movableto[secondNodes[1]]==part[secondNodes[1]]) //there is no more movable node[0]s
                    secondNodes[1]=-1;
            }
            else{
                secondNodes[1]=-1;
            }
        }
        // Check cycle and constraint for topFirstHeap and secondSecondHeap
        if(flagger==0 && secondNodes[1]!=-1)
        {
            isThereCycle = createCycleSwap(G,part,nbpart,partmatrix,node[0],secondNodes[1],movableto[node[0]],movableto[secondNodes[1]]);
            if (!isThereCycle){

                newsize[0]=partsize[0];
                newsize[part[node[0]]] -= G->vw[node[0]];
                newsize[movableto[node[0]]] += G->vw[node[0]];

                newsize[1]=partsize[1];
                newsize[part[secondNodes[1]]] -= G->vw[secondNodes[1]];
                newsize[movableto[secondNodes[1]]] += G->vw[secondNodes[1]];

                if (!((newsize[0] < lb_pw[0])||(newsize[0] > ub_pw[0])||(newsize[1] < lb_pw[1])||(newsize[1] > ub_pw[1])))
                    flagger=2;
            }
        }
        // Check cycle and constraint for secondFirstHeap and topSecondHeap

        if(flagger==0 && secondNodes[0]!=-1)
        {
            isThereCycle = createCycleSwap(G,part,nbpart,partmatrix,secondNodes[0],node[1],movableto[secondNodes[0]],movableto[node[1]]);
            if (!isThereCycle){

                newsize[0]=partsize[0];
                newsize[part[secondNodes[0]]] -= G->vw[secondNodes[0]];
                newsize[movableto[secondNodes[0]]] += G->vw[secondNodes[0]];

                newsize[1]=partsize[1];
                newsize[part[node[1]]] -= G->vw[node[1]];
                newsize[movableto[node[1]]] += G->vw[node[1]];

                if (!((newsize[0] < lb_pw[0])||(newsize[0] > ub_pw[0])||(newsize[1] < lb_pw[1])||(newsize[1] > ub_pw[1])))
                    flagger=3;
            }
        }
        // Check cycle and constraint for secondFirstHeap and secondSecondHeap
        if(flagger==0 && secondNodes[0]!=-1 && secondNodes[1]!=-1)
        {
            isThereCycle = createCycleSwap(G,part,nbpart,partmatrix,secondNodes[0],secondNodes[1],movableto[secondNodes[0]],movableto[secondNodes[1]]);
            if (!isThereCycle){

                //check new sizes to see if we can move one secondNodes from the first part to second one
                newsize[0]=partsize[0];
                newsize[part[secondNodes[0]]] -= G->vw[secondNodes[0]];
                newsize[movableto[secondNodes[0]]] += G->vw[secondNodes[0]];

                newsize[1]=partsize[1];
                newsize[part[secondNodes[1]]] -= G->vw[secondNodes[1]];
                newsize[movableto[secondNodes[1]]] += G->vw[secondNodes[1]];

                if (!((newsize[0] < lb_pw[0])||(newsize[0] > ub_pw[0])||(newsize[1] < lb_pw[1])||(newsize[1] > ub_pw[1])))
                    flagger=4;
            }

        }
        if(flagger == 0)
            continue;
        if(flagger == 1){
            if(secondNodes[0]!=-1){
                gain[secondNodes[0]] = computeGainNode(G, part, movableto[secondNodes[0]], secondNodes[0], opt);
                heapInsert(G, heap[part[secondNodes[0]]], gain, &hsize[part[secondNodes[0]]], secondNodes[0]);
                inheap[secondNodes[0]] = 1;
            }
            if(secondNodes[1]!=-1){
                gain[secondNodes[1]] = computeGainNode(G, part, movableto[secondNodes[1]], secondNodes[1], opt);
                heapInsert(G, heap[part[secondNodes[1]]], gain, &hsize[part[secondNodes[1]]], secondNodes[1]);
                inheap[secondNodes[1]] = 1;
            }
        }
        if(flagger ==2){
            if(secondNodes[0]!=-1){
                gain[secondNodes[0]] = computeGainNode(G, part, movableto[secondNodes[0]], secondNodes[0], opt);
                heapInsert(G, heap[part[secondNodes[0]]], gain, &hsize[part[secondNodes[0]]], secondNodes[0]);
                inheap[secondNodes[0]] = 1;
            }
            node[1]=secondNodes[1];

        }
        if(flagger == 3){
            if(secondNodes[1]!=-1){
                gain[secondNodes[1]] = computeGainNode(G, part, movableto[secondNodes[1]], secondNodes[1], opt);
                heapInsert(G, heap[part[secondNodes[1]]], gain, &hsize[part[secondNodes[1]]], secondNodes[1]);
                inheap[secondNodes[1]] = 1;
            }
            node[0]=secondNodes[0];
        }
        if(flagger ==4){
            node[0]=secondNodes[0];
            node[1]=secondNodes[1];
        }
        //printf("swapped %d and %d. wololooo\n",node[0],node[1] );
        //printf("from %d to %d and from %d to %d\n",part[node[0]],movableto[node[0]],part[node[1]],movableto[node[1]] );

        computePartmatrix(G, part, partmatrix, node[0], movableto[node[0]]);
        computePartmatrix(G, part, partmatrix, node[1], movableto[node[1]]);

        //update the weights
        partsize[part[node[0]]] -= G->vw[node[0]];
        partsize[part[node[1]]] -= G->vw[node[1]];
        partsize[movableto[node[0]]] += G->vw[node[0]];
        partsize[movableto[node[1]]] += G->vw[node[1]];

        maxpartsize = 0;
        for (j=0; j<nbpart; j++)
            maxpartsize = maxpartsize < partsize[j] ? partsize[j] : maxpartsize;

        *edgecut -= gain[node[0]];
        part[node[0]] = movableto[node[0]];
        hasbeenmoved[node[0]] = 1;
        moved[nbmoved] = node[0];
        nbmoved++;

        *edgecut -= gain[node[1]];
        part[node[1]] = movableto[node[1]];
        hasbeenmoved[node[1]] = 1;
        moved[nbmoved] = node[1];
        nbmoved++;

        for (j=G->inStart[node[0]]; j<=G->inEnd[node[0]]; j++){
            idxType father = G->in[j];
            computeMovabletoNode(G, part, nbpart, topsortpart, toporderpart, partsize, movableto, father);
            gain[father] = computeGainNode(G, part, movableto[father], father, opt);
            if ((movableto[father] != part[father])&&(inheap[father] == 0)&&(hasbeenmoved[father] == 0)){
                heapInsert(G, heap[part[father]], gain, &hsize[part[father]], father);
                //printf("inserted node %d, its part is %d and movableto %d\n",father,part[father],movableto[father] );
                inheap[father] = 1;
            }
        }
        for (j=G->outStart[node[0]]; j<=G->outEnd[node[0]]; j++){
            idxType child = G->out[j];
            computeMovabletoNode(G, part, nbpart, topsortpart, toporderpart, partsize, movableto, child);
            gain[child] = computeGainNode(G, part, movableto[child], child, opt);
            if ((movableto[child] != part[child])&&(inheap[child] == 0)&&(hasbeenmoved[child] == 0)){
                heapInsert(G, heap[part[child]], gain, &hsize[part[child]], child);
                //printf("inserted node %d, its part is %d and movableto %d\n",child,part[child],movableto[child] );
                inheap[child] = 1;
            }
        }
        for (j=G->inStart[node[1]]; j<=G->inEnd[node[1]]; j++){
            idxType father = G->in[j];
            computeMovabletoNode(G, part, nbpart, topsortpart, toporderpart, partsize, movableto, father);
            gain[father] = computeGainNode(G, part, movableto[father], father, opt);
            if ((movableto[father] != part[father])&&(inheap[father] == 0)&&(hasbeenmoved[father] == 0)){
                heapInsert(G, heap[part[father]], gain, &hsize[part[father]], father);
                //printf("inserted node %d, its part is %d and movableto %d\n",father,part[father],movableto[father] );
                inheap[father] = 1;
            }
        }
        for (j=G->outStart[node[1]]; j<=G->outEnd[node[1]]; j++){
            idxType child = G->out[j];
            computeMovabletoNode(G, part, nbpart, topsortpart, toporderpart, partsize, movableto, child);
            gain[child] = computeGainNode(G, part, movableto[child], child, opt);
            if ((movableto[child] != part[child])&&(inheap[child] == 0)&&(hasbeenmoved[child] == 0)){
                heapInsert(G, heap[part[child]], gain, &hsize[part[child]], child);
                //printf("inserted node %d, its part is %d and movableto %d\n",child,part[child],movableto[child] );
                inheap[child] = 1;
            }
        }
        if ((*edgecut < minedgecut)||((*edgecut == minedgecut)&&(maxpartsize <= minmaxpartsize))){
            minedgecut = *edgecut;
            minmaxpartsize = maxpartsize;
            minindex = nbmoved;
            without_impro = -1;
        }
        without_impro ++;
    }

    for (i = nbmoved-1; i >= minindex; i--){
        part[moved[i]] = partcopy[moved[i]];
    }
    /*
    for (i = 1; i <=G->nVrtx; i++){
    	printf("%d ", part[i]);
    }
	*/
    //printf("\nNb moved %d\n",minindex);
    *edgecut = minedgecut;
    free(movableto);
    free(partsize);
    free(partcopy);
    free(topsortpart);
    free(moved);
    free(gain);
    free(heap[0]);
    free(heap[1]);
    free(inheap);
    free(hasbeenmoved);
}

//ONLY MOVE from bigger to smaller part
void refinementPostOrder_Max(dgraph *G, idxType *toporderpart,
			  double* lb_pw, double* ub_pw,
			 idxType *part, idxType nbpart, ecType* edgecut, MLGP_option opt)
{

  if (opt.nbPart != 2){
	u_errexit("RefinementPostOrder_Max only works for nbpart = 2\n");
  }

  //printf("refinementPostOrder_Max before allocation done vertex = %d\n",G->nVrtx);
    int frmt = G->frmt;
    idxType i,j;
    idxType hsize[2] = {0,0};
    idxType maxpartsize = 0;
    idxType* movableto = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    idxType* partsize = (idxType*) malloc(sizeof(idxType)*(nbpart+1));
    idxType* partcopy = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    idxType* topsortpart = (idxType*) malloc (sizeof(idxType) * nbpart);
    idxType* moved = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    ecType* gain = (ecType*) malloc(sizeof(ecType)*(G->nVrtx+1));
    idxType* heap[2];
    heap[0] = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    heap[1] = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    int* inheap = (int*) malloc(sizeof(int)*(G->nVrtx+1));
    int* hasbeenmoved = (int*) malloc(sizeof(idxType)*(G->nVrtx+1));

    //printf("refinementPostOrder_Max allocation done vertex = %d\n",G->nVrtx);

    for (i=0; i<nbpart; i++){
		partsize[i] = 0;
		topsortpart[toporderpart[i]] = i;
    }
    for (i=1; i<=G->nVrtx; i++){
		inheap[i] = 0;
		hasbeenmoved[i] = 0;
		partcopy[i] = part[i];
		computeMovabletoNode(G, part, nbpart, topsortpart, toporderpart, partsize, movableto, i);
		gain[i] = computeGainNode(G, part, movableto[i], i, opt);
		if (movableto[i] != part[i]){
		    heap[part[i]][++hsize[part[i]]] = i;
		    inheap[i] = 1;
		}
		if (frmt & DG_FRMT_VW) // if it has vertex weights
		    partsize[part[i]] += G->vw[i];	
		else
		    partsize[part[i]] += 1;
    }

    for (i=0; i<nbpart; i++)
	maxpartsize = maxpartsize < partsize[i] ? partsize[i] : maxpartsize;

    heapBuild(G, heap[0], gain, hsize[0]);
    heapBuild(G, heap[1], gain, hsize[1]);

    idxType nbmoved = 0, newsize1, newsize2, minmaxpartsize = maxpartsize;
    idxType node[2];
    node[0] =-1;
    node[1]=-1;
    int without_impro = 0, minindex = 0;
    //double target = (ub_pw - lb_pw)/2;
    ecType minedgecut = *edgecut;
    int biggerPart=-1;
    while ((hsize[0] > 0 || hsize[1] > 0)&&(without_impro <= G->nVrtx/4)&&(nbmoved<G->nVrtx)){
    	if (partsize[0]>partsize[1]){
			biggerPart = 0;
			//first heap
			if(hsize[0] == 0 ){
				break;
			}
		}
		else{
			biggerPart = 1;
			//second heap
			if(hsize[1] == 0 ){
				break;
			}
		}

		//if(node[0] < 0 || node[1] < 0)
		//	printf("HAHAAAAAAAAAAA %d ::: %d\n",node[0],node[1]);

		node[biggerPart] = heapExtractMax(G, heap[biggerPart], gain, &hsize[biggerPart]);
		inheap[node[biggerPart]] = 0;
		while ((hsize[biggerPart] > 0)&&(movableto[node[biggerPart]]==part[node[biggerPart]])){
		    node[biggerPart] = heapExtractMax(G, heap[biggerPart], gain, &hsize[biggerPart]);
		    inheap[node[biggerPart]] = 0;
		}
		if (movableto[node[biggerPart]]==part[node[biggerPart]]) //there is no more movable node[biggerPart]s
		    break;

		//check new sizes to see if we can move one node from the first part to second one
		newsize1 = partsize[part[node[biggerPart]]] - G->vw[node[biggerPart]];	
		newsize2 = partsize[movableto[node[biggerPart]]] + G->vw[node[biggerPart]];

		if ((newsize1 < lb_pw[part[node[biggerPart]]])||(newsize1 > ub_pw[part[node[biggerPart]]])||(newsize2 < lb_pw[movableto[node[biggerPart]]])||(newsize2 > ub_pw[movableto[node[biggerPart]]]))
		    continue;
		//update the weights
		partsize[part[node[biggerPart]]] -= G->vw[node[biggerPart]];
		partsize[movableto[node[biggerPart]]] += G->vw[node[biggerPart]];

		maxpartsize = 0;
		for (j=0; j<nbpart; j++)
		    maxpartsize = maxpartsize < partsize[j] ? partsize[j] : maxpartsize;
		*edgecut -= gain[node[biggerPart]];
		part[node[biggerPart]] = movableto[node[biggerPart]];
		hasbeenmoved[node[biggerPart]] = 1;
		moved[nbmoved++] = node[biggerPart];

		for (j=G->inStart[node[biggerPart]]; j<=G->inEnd[node[biggerPart]]; j++){
		    idxType father = G->in[j];
		    computeMovabletoNode(G, part, nbpart, topsortpart, toporderpart, partsize, movableto, father);
		    gain[father] = computeGainNode(G, part, movableto[father], father, opt);
		    if ((movableto[father] != part[father])&&(inheap[father] == 0)&&(hasbeenmoved[father] == 0)){
				heapInsert(G, heap[part[father]], gain, &hsize[part[father]], father);
				inheap[father] = 1;
		    }	
		}
		for (j=G->outStart[node[biggerPart]]; j<=G->outEnd[node[biggerPart]]; j++){
		    idxType child = G->out[j];
		    computeMovabletoNode(G, part, nbpart, topsortpart, toporderpart, partsize, movableto, child);
		    gain[child] = computeGainNode(G, part, movableto[child], child, opt);
		    if ((movableto[child] != part[child])&&(inheap[child] == 0)&&(hasbeenmoved[child] == 0)){
				heapInsert(G, heap[part[child]], gain, &hsize[part[child]], child);
				inheap[child] = 1;
		    }		
		}
		if ((*edgecut < minedgecut)||((*edgecut == minedgecut)&&(maxpartsize <= minmaxpartsize))){
		    minedgecut = *edgecut;
		    minmaxpartsize = maxpartsize;
		    minindex = nbmoved;
		    without_impro = -1;
		}
		without_impro ++;
    }
    for (i = nbmoved-1; i >= minindex; i--){
		part[moved[i]] = partcopy[moved[i]];
    }
    //printf("Nb moved %d\n",minindex);


    //printf("refinementPostOrder_Max minindex = %d\n", minindex);
    *edgecut = minedgecut;
    free(movableto);
    free(partsize);        
    free(partcopy);
    free(topsortpart);
    free(moved);
    free(gain);
    free(heap[0]);
    free(heap[1]);
    free(inheap);
    free(hasbeenmoved);
}

void refinementPostOrder(dgraph *G, idxType *toporderpart,
			  double* lb_pw, double* ub_pw,
			 idxType *part, idxType nbpart, ecType* edgecut, MLGP_option opt)
{
    // printf("========================================================Starting refinementPostOrder\n");
    int frmt = G->frmt;
    idxType i,j, hsize = 0;
    idxType maxpartsize = 0;
    idxType* movableto = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    idxType* partsize = (idxType*) malloc(sizeof(idxType)*(nbpart+1));
    idxType* partcopy = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    idxType* topsortpart = (idxType*) malloc (sizeof(idxType) * nbpart);
    idxType* moved = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    ecType* gain = (ecType*) malloc(sizeof(ecType)*(G->nVrtx+1));
    idxType* heap = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    int* inheap = (int*) malloc(sizeof(int)*(G->nVrtx+1));
    idxType* hasbeenmoved = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));

    // printf("topsort toporder\n");
    for (i=0; i<nbpart; i++){
	    partsize[i] = 0;
	    topsortpart[toporderpart[i]] = i;
        // printf("%d %d\n", i,toporderpart[i]);
    }
    // printf("topsort toporder done\n\n");
    for (i=1; i<=G->nVrtx; i++){
    	inheap[i] = 0;
    	hasbeenmoved[i] = 0;
    	partcopy[i] = part[i];
    	computeMovabletoNode(G, part, nbpart, topsortpart, toporderpart, partsize, movableto, i);
    	gain[i] = computeGainNode(G, part, movableto[i], i, opt);
    	if (movableto[i] != part[i]){
    	    heap[++hsize] = i;
    	    inheap[i] = 1;
    	}
    	if (frmt & DG_FRMT_VW) /* if it has vertex weights */
    	    partsize[part[i]] += G->vw[i];	
    	else
    	    partsize[part[i]] += 1;
    }
    for (i=0; i<nbpart; i++)
	    maxpartsize = maxpartsize < partsize[i] ? partsize[i] : maxpartsize;

    heapBuild(G, heap, gain, hsize);

    idxType nbmoved = 0, newsize1, newsize2, node = 1, minmaxpartsize = maxpartsize;
    int without_impro = 0, minindex = 0;
    //double target = (ub_pw - lb_pw)/2;
    ecType minedgecut = *edgecut;

    while ((hsize > 0)&&(without_impro <= G->nVrtx/4)&&(nbmoved<G->nVrtx)){
        // printNodeInfo(G,part,5915);
    	node = heapExtractMax(G, heap, gain, &hsize);
    	inheap[node] = 0;
    	while ((hsize > 0)&&(movableto[node]==part[node])){
    	    node = heapExtractMax(G, heap, gain, &hsize);
    	    inheap[node] = 0;
    	}

		if (movableto[node]==part[node]) //there is no more movable nodes
		    break;

		newsize1 = partsize[part[node]] - G->vw[node];	
		newsize2 = partsize[movableto[node]] + G->vw[node];	
		if ((newsize1 < lb_pw[part[node]])||(newsize1 > ub_pw[part[node]])||(newsize2 < lb_pw[movableto[node]])||(newsize2 > ub_pw[movableto[node]]))
		    continue;	    
		
        partsize[part[node]] = newsize1;
		partsize[movableto[node]] = newsize2;


        maxpartsize = 0;
        for (j=0; j<nbpart; j++)
            maxpartsize = maxpartsize < partsize[j] ? partsize[j] : maxpartsize;
        // printf("%4d: %d moved from %d to %d\n",nbmoved+1, node,part[node],movableto[node]);  
        *edgecut -= gain[node];
        part[node] = movableto[node];
        hasbeenmoved[node] = 1;
        moved[nbmoved++] = node;

    	for (j=G->inStart[node]; j<=G->inEnd[node]; j++){
    	    idxType father = G->in[j];
    	    computeMovabletoNode(G, part, nbpart, topsortpart, toporderpart, partsize, movableto, father);
    	    gain[father] = computeGainNode(G, part, movableto[father], father, opt);
    	    if ((movableto[father] != part[father])&&(inheap[father] == 0)&&(hasbeenmoved[father] == 0)){
    		heapInsert(G, heap, gain, &hsize, father);
    		inheap[father] = 1;
    	    }	
    	}

    	for (j = G->outStart[node]; j <= G->outEnd[node]; j++){
    	    idxType child = G->out[j];
    	    computeMovabletoNode(G, part, nbpart, topsortpart, toporderpart, partsize, movableto, child);
    	    gain[child] = computeGainNode(G, part, movableto[child], child, opt);
    	    if ((movableto[child] != part[child])&&(inheap[child] == 0)&&(hasbeenmoved[child] == 0)){
    		heapInsert(G, heap, gain, &hsize, child);
    		inheap[child] = 1;
    	    }		
    	}

    	if ((*edgecut < minedgecut)||((*edgecut == minedgecut)&&(maxpartsize <= minmaxpartsize))){
                minedgecut = *edgecut;
    	    minmaxpartsize = maxpartsize;
    	    minindex = nbmoved;
    	    without_impro = -1;
    	}
    	without_impro ++;
        /*
        if(checkAcyclicity(G,part,nbpart)){
            printf("CHECK FAIL ON\n");
            printNodeInfo(G,part,node);
        }*/
    }
    for (i = nbmoved-1; i >= minindex; i--){
	    part[moved[i]] = partcopy[moved[i]];
    }

    // printf("========================================================Nb moved %d\n",minindex);
    *edgecut = minedgecut;
    free(movableto);
    free(partsize);        
    free(partcopy);
    free(topsortpart);
    free(moved);
    free(gain);
    free(heap);
    free(inheap);
    free(hasbeenmoved);
}



void computeMovabletoBestPartNode(dgraph* G, idxType* part, idxType* partsize, int nbpart, idxType i, idxType* movableto, ecType* gain, MLGP_option opt)
{
    idxType j,k,idx = 0;
    
    for (j=0; j<nbpart; j++){
    	if (j==part[i])
    	    continue;
    	movableto[idx] = j;
    	gain[idx] = computeGainNode(G, part, j, i, opt);
    	for (k=idx; k>=1; k--){
    	    if (gain[k] > gain[k-1]){
    		idxType movable_save = movableto[k];
    		idxType gain_save = gain[k];
    		movableto[k] = movableto[k-1];
    		gain[k] = gain[k-1];
    		movableto[k-1] = movable_save;
    		gain[k-1] = gain_save;
    	    }
    	}
    	idx++;
    }
}


int isIncomingBoundary(dgraph* G, idxType* part, idxType node)
{
    idxType i;
    for (i=G->inStart[node]; i<=G->inEnd[node]; i++)
	if (part[G->in[i]] == part[node])
	    return 0;
    return 1;
}

int isOutgoingBoundary(dgraph* G, idxType* part, idxType node)
{
    idxType i;
    for (i=G->outStart[node]; i<=G->outEnd[node]; i++)
	if (part[G->out[i]] == part[node])
	    return 0;
    return 1;
}

int partSizeChecker(idxType* partsize, double ub_pw[], int nbpart){
	//as long as at least one of them is bigger than bound...
	int i;
	for ( i=0; i<nbpart; ++i ) {
		//if (partsize[i] > ub_pw[nbpart-i-1]){
        if (partsize[i] > ub_pw[i]){

                return 1;
		}
	}
	//printf("noproblem-->forcedBalance ends\n");
	return 0;
	//(maxpartsize > ub_pw)
}

idxType* forcedBalance(dgraph *G, idxType *toporderpart,
		   double* ub_pw,
		      idxType *part, idxType nbpart, MLGP_option opt, MLGP_info* info)
{
    int frmt = G->frmt;
    idxType i,j, hsize = 0;

    idxType* movableto = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    idxType* nbmovableto = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    idxType* partsize = (idxType*) malloc(sizeof(idxType)*(nbpart+1));
    idxType* topsortpart = (idxType*) calloc (nbpart, sizeof(idxType));
    idxType* moved = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    ecType* gain = (ecType*) malloc(sizeof(ecType)*(G->nVrtx+1));
    idxType* heap = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    int* inheap = (int*) malloc(sizeof(int)*(G->nVrtx+1));
    int* hasbeenmoved = (int*) malloc(sizeof(idxType)*(G->nVrtx+1));

    for (i=0; i<nbpart; i++){
		partsize[i] = 0;
		topsortpart[toporderpart[i]] = i;
    }
    for (i=1; i<=G->nVrtx; i++){
	inheap[i] = 0;
	hasbeenmoved[i] = 0;
	computeMovabletoNode(G, part, nbpart, topsortpart, toporderpart, partsize, movableto, i);
	gain[i] = computeGainNode(G, part, movableto[i], i, opt);
	if (movableto[i] != part[i]){
	    heap[++hsize] = i;
	    inheap[i] = 1;
	}
	if (frmt & DG_FRMT_VW) /* if it has vertex weights */
	    partsize[part[i]] += G->vw[i];	
	else
	    partsize[part[i]] += 1;
    }

    heapBuild(G, heap, gain, hsize);

    idxType nbmoved = 0, newsize1, newsize2, node = 1;

    while ((partSizeChecker(partsize, ub_pw, opt.nbPart))&&(hsize > 0)){
	    node = heapExtractMax(G, heap, gain, &hsize);
	    inheap[node] = 0;
	    while ((hsize > 0)&&(partsize[movableto[node]]-ub_pw[movableto[node]] >= partsize[part[node]]-ub_pw[part[node]])){
	        node = heapExtractMax(G, heap, gain, &hsize);
	        inheap[node] = 0;
	    }
	    if (partsize[movableto[node]]-ub_pw[movableto[node]] >= partsize[part[node]]-ub_pw[part[node]]) //there is no more movable nodes
	        break;
	    newsize1 = partsize[part[node]] - G->vw[node];
	    newsize2 = partsize[movableto[node]] + G->vw[node];
	    if (newsize2 > ub_pw[movableto[node]]){
	        continue;
	        idxType new_movableto;
	        if (toporderpart[part[node]] < toporderpart[movableto[node]])
		        new_movableto = topsortpart[toporderpart[movableto[node]]-1];
	        if (toporderpart[part[node]] > toporderpart[movableto[node]])
		        new_movableto = topsortpart[toporderpart[movableto[node]]+1];
	        if (new_movableto != part[node]){
		        if(opt.debug > 10)
			        printf("!!!Movableto of node %d is moved from %d to %d\n",node, movableto[node], new_movableto);
		        movableto[node] = new_movableto;
		        gain[node] = computeGainNode(G, part, movableto[node], node, opt);
		        if ((movableto[node] != part[node])&&(inheap[node] == 0)&&(hasbeenmoved[node] == 0)){
		            heapInsert(G, heap, gain, &hsize, node);
		            inheap[node] = 1;
		        }
	        }
	        continue;
	    }

	    partsize[part[node]] = newsize1;
	    partsize[movableto[node]] = newsize2;
	    //maxpartsize = 0;
	    //for (j=0; j<nbpart; j++)
	    //    maxpartsize = maxpartsize < partsize[j] ? partsize[j] : maxpartsize;
	    part[node] = movableto[node];
	    hasbeenmoved[node] = 1;
	    moved[nbmoved++] = node;
        info->current_edgecut -= gain[node];

	    for (j=G->inStart[node]; j<=G->inEnd[node]; j++){
	        idxType father = G->in[j];
	        computeMovabletoNode(G, part, nbpart, topsortpart, toporderpart, partsize, movableto, father);
	        gain[father] = computeGainNode(G, part, movableto[father], father, opt);
	        if ((inheap[father] == 1)||(hasbeenmoved[father] == 1))
		        continue;
	        if (movableto[father] != part[father]){
		        heapInsert(G, heap, gain, &hsize, father);
		        inheap[father] = 1;
	        }
	    }
	    for (j=G->outStart[node]; j<=G->outEnd[node]; j++){
	        idxType child = G->out[j];
	        computeMovabletoNode(G, part, nbpart, topsortpart, toporderpart, partsize, movableto, child);
	        gain[child] = computeGainNode(G, part, movableto[child], child, opt);
	        if ((inheap[child] == 1)||(hasbeenmoved[child] == 1))
		        continue;
	        if (movableto[child] != part[child]){
		        heapInsert(G, heap, gain, &hsize, child);
		        inheap[child] = 1;
	        }
	    }
    }
    if(opt.debug > 3)
    	printf("moved weight %d\n",nbmoved );

    free(movableto);
    free(nbmovableto);
    //free(partsize);        
    free(topsortpart);
    free(moved);
    free(gain);
    free(heap);
    free(inheap);
    free(hasbeenmoved);
    return partsize;
}

void refinementRandomizedSelectTowardsBest(dgraph *G, ecType** partmatrix,
              double* lb_pw, double* ub_pw,
              idxType *part, idxType nbpart, ecType* edgecut, MLGP_option opt)
{
    idxType i,j,k, qsize = 0;
    idxType maxpartsize = 0;
    ecType** gain = (ecType**) umalloc(sizeof(ecType*)*(G->nVrtx+1),"gain");
    idxType* queue = (idxType*) umalloc(sizeof(idxType)*(G->nVrtx+1),"queue");
    int* inqueue = (int*) umalloc(sizeof(int)*(G->nVrtx+1),"inqueue");
    int* hasbeenmoved = (int*) umalloc(sizeof(idxType)*(G->nVrtx+1),"hasbeenmoved");
    idxType* moved = (idxType*) umalloc(sizeof(idxType)*(G->nVrtx+1),"moved");
    idxType** movableto = (idxType**) umalloc(sizeof(idxType*)*(G->nVrtx+1),"movableto");
    idxType* partsize = (idxType*) calloc(nbpart+1, sizeof(idxType));
    idxType* partcopy = (idxType*) umalloc(sizeof(idxType)*(G->nVrtx+1),"partcopy");
    
    for (i = 1; i <= G->nVrtx; i++){
        partsize[part[i]] += G->vw[i];
        partcopy[i] = part[i];
        hasbeenmoved[i] = 0;
        inqueue[i] = 0;
        movableto[i] = (idxType*) umalloc(sizeof(idxType) * (nbpart-1),"movableto[i]");
        gain[i] = (ecType*) umalloc(sizeof(ecType) * (nbpart-1),"gain[i]");
    }

    for (i = 1; i <= G->nVrtx; i++){
        if ((isIncomingBoundary(G, part, i))||(isOutgoingBoundary(G, part, i))){
            queue[qsize++] = i;
            inqueue[i] = 1;
        }
    }

    for (i = 0; i < nbpart; i++){
        maxpartsize = maxpartsize < partsize[i] ? partsize[i] : maxpartsize;
    }


    idxType nbmoved = 0, newsize1, newsize2, node = 1, minmaxpartsize = maxpartsize;
    int without_impro = 0, minindex = 0;
    //double target = (ub_pw - lb_pw)/2;
    ecType minedgecut = *edgecut;
    if(qsize>0)
        shuffleArray(qsize, queue, qsize/4+1);
    while ((qsize > 0)&&(without_impro <= G->nVrtx/4)&&(nbmoved<G->nVrtx)){

        node = queue[qsize-1];
        --qsize;
        inqueue[node] = 0;
        computeMovabletoBestPartNode(G, part, partsize, nbpart, node, movableto[node], gain[node], opt);

        if ((movableto[node][0] == -1)||(hasbeenmoved[node]))
            continue;

        if(gain[node][0]<0)
            continue;

        newsize1 = partsize[part[node]] - G->vw[node];  
        newsize2 = partsize[movableto[node][0]] + G->vw[node];  
        int cycle = createCycle(G, part, nbpart, partmatrix, node, movableto[node][0]);

        k=0;
        while (((newsize1 < lb_pw[part[node]])||(newsize1 > ub_pw[part[node]])||(newsize2 < lb_pw[movableto[node][k]])||(newsize2 > ub_pw[movableto[node][k]])||(cycle))&&(k<nbpart-2)){
            //Let's try second best partition for this node
            k++;
            if (gain[node][k] < gain[node][1])
                break;
            if(gain[node][k]<0)
                continue;
            newsize2 = partsize[movableto[node][k]] + G->vw[node];
            cycle = createCycle(G, part, nbpart, partmatrix, node, movableto[node][k]);
        }
        if ((newsize1 < lb_pw[part[node]])||(newsize1 > ub_pw[part[node]])||(newsize2 < lb_pw[movableto[node][k]])||(newsize2 > ub_pw[movableto[node][k]])||(cycle)||(k>=nbpart)||(gain[node][k] < gain[node][1]))
            continue;

        if(qsize>0)
            shuffleArray(qsize, queue, qsize/4+1);
        computePartmatrix(G, part, partmatrix, node, movableto[node][k]);

        partsize[part[node]] = newsize1;
        partsize[movableto[node][k]] = newsize2;
        maxpartsize = 0;
        for (j=0; j<nbpart; j++)
            maxpartsize = maxpartsize < partsize[j] ? partsize[j] : maxpartsize;
        *edgecut -= gain[node][k];
        part[node] = movableto[node][k];
        hasbeenmoved[node] = 1;
        moved[nbmoved++] = node;

        for (j=G->inStart[node]; j<=G->inEnd[node]; j++){
            idxType father = G->in[j];
            if (!((isIncomingBoundary(G, part, father))||(isOutgoingBoundary(G, part, father))))
                continue;
            if ((inqueue[father] == 0)&&(hasbeenmoved[father] == 0)){
                queue[qsize++] = father;
                inqueue[father] = 1;
            }
        }
        for (j=G->outStart[node]; j<=G->outEnd[node]; j++){
            idxType child = G->out[j];
            if (!((isIncomingBoundary(G, part, child))||(isOutgoingBoundary(G, part, child))))
                continue;
            if ((inqueue[child] == 0)&&(hasbeenmoved[child] == 0)){
                queue[qsize++] = child;
                inqueue[child] = 1;
            }       
        }   

        if ((*edgecut < minedgecut)||((*edgecut == minedgecut)&&(maxpartsize <= minmaxpartsize))){
            minedgecut = *edgecut;
            minmaxpartsize = maxpartsize;
            minindex = nbmoved;
            without_impro = -1;
        }
        without_impro ++;

    }

    for (i = nbmoved-1; i >= minindex; i--){
        part[moved[i]] = partcopy[moved[i]];
    }

    //printf("Nb moved %d\n",minindex);
    *edgecut = minedgecut;
    for (i = 1; i <= G->nVrtx; i++){
        free(gain[i]);
        free(movableto[i]);
    }
    free(gain);
    free(queue);
    free(inqueue);
    free(hasbeenmoved);
    free(moved);
    free(movableto);
    free(partsize);
    free(partcopy);
}
void refinementTowardBest(dgraph *G, ecType** partmatrix,
			  double* lb_pw, double* ub_pw,
			  idxType *part, idxType nbpart, ecType* edgecut, MLGP_option opt)
{
    idxType i,j,k, hsize = 0;
    idxType maxpartsize = 0;
    ecType** gain = (ecType**) malloc(sizeof(ecType*)*(G->nVrtx+1));
    ecType* gain_0 = (ecType*) malloc(sizeof(ecType)*(G->nVrtx+1));
    idxType* heap = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    int* inheap = (int*) malloc(sizeof(int)*(G->nVrtx+1));
    int* hasbeenmoved = (int*) malloc(sizeof(idxType)*(G->nVrtx+1));
    idxType* moved = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    idxType** movableto = (idxType**) malloc(sizeof(idxType*)*(G->nVrtx+1));
    idxType* partsize = (idxType*) calloc(nbpart+1, sizeof(idxType));
    idxType* partcopy = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    for (i = 1; i <= G->nVrtx; i++){
    	partsize[part[i]] += G->vw[i];
    	partcopy[i] = part[i];
    	hasbeenmoved[i] = 0;
    	inheap[i] = 0;
    	movableto[i] = (idxType*) malloc(sizeof(idxType) * (nbpart-1));
    	gain[i] = (ecType*) malloc(sizeof(ecType) * (nbpart-1));
    }
    for (i = 1; i <= G->nVrtx; i++){
    	if ((isIncomingBoundary(G, part, i))||(isOutgoingBoundary(G, part, i))){
    	    computeMovabletoBestPartNode(G, part, partsize, nbpart, i, movableto[i], gain[i], opt);
    	    gain_0[i] = gain[i][0];
    	    heap[++hsize] = i;
    	    inheap[i] = 1;
    	}
    }
    heapBuild(G, heap, gain_0, hsize);
    for (i = 0; i < nbpart; i++){
	    maxpartsize = maxpartsize < partsize[i] ? partsize[i] : maxpartsize;
    }

    idxType nbmoved = 0, newsize1, newsize2, node = 1, minmaxpartsize = maxpartsize;
    int without_impro = 0, minindex = 0;
    //double target = (ub_pw - lb_pw)/2;
    ecType minedgecut = *edgecut;

    while ((hsize > 0)&&(without_impro <= G->nVrtx/4)&&(nbmoved<G->nVrtx)){
    	node = heapExtractMax(G, heap, gain_0, &hsize);
    	//printf("Concider node %d\n",node);
    	//printPartMatrix(partmatrix, nbpart);
    	inheap[node] = 0;

    	if ((movableto[node][0] == -1)||(hasbeenmoved[node]))
    	    continue;
    	newsize1 = partsize[part[node]] - G->vw[node];	
    	newsize2 = partsize[movableto[node][0]] + G->vw[node];	

    	int cycle = createCycle(G, part, nbpart, partmatrix, node, movableto[node][0]);
    	//printf("node %d part %d moveto %d cycle %d\n", node, part[node], movableto[node], cycle);
    	k=0;
    	while (((newsize1 < lb_pw[part[node]])||(newsize1 > ub_pw[part[node]])||(newsize2 < lb_pw[movableto[node][k]])||(newsize2 > ub_pw[movableto[node][k]])||(cycle))&&(k<nbpart-2)){
    	    //Let's try second best partition for this node
    	    k++;
    	    if (gain[node][k] < gain[node][1])
    		break;
    	    newsize2 = partsize[movableto[node][k]] + G->vw[node];
    	    cycle = createCycle(G, part, nbpart, partmatrix, node, movableto[node][k]);
    	}
    	if ((newsize1 < lb_pw[part[node]])||(newsize1 > ub_pw[part[node]])||(newsize2 < lb_pw[movableto[node][k]])||(newsize2 > ub_pw[movableto[node][k]])||(cycle)||(k>=nbpart)||(gain[node][k] < gain[node][1]))
    	    continue;

    	//printf("Move node %d from %d to %d with gain %d\n", node, part[node], movableto[node], gain[node]);

    	computePartmatrix(G, part, partmatrix, node, movableto[node][k]);

    	partsize[part[node]] = newsize1;
    	partsize[movableto[node][k]] = newsize2;
    	maxpartsize = 0;
    	for (j=0; j<nbpart; j++)
    	    maxpartsize = maxpartsize < partsize[j] ? partsize[j] : maxpartsize;
    	*edgecut -= gain[node][k];
    	part[node] = movableto[node][k];
    	hasbeenmoved[node] = 1;
    	moved[nbmoved++] = node;

    	for (j=G->inStart[node]; j<=G->inEnd[node]; j++){
    	    idxType father = G->in[j];
    	    if (!((isIncomingBoundary(G, part, father))||(isOutgoingBoundary(G, part, father))))
    		continue;
    	    computeMovabletoBestPartNode(G, part, partsize, nbpart, father, movableto[father], gain[father], opt);
    	    gain_0[father] = gain[father][0];
    	    if ((inheap[father] == 0)&&(hasbeenmoved[father] == 0)){
    		heapInsert(G, heap, gain_0, &hsize, father);
    		inheap[father] = 1;
    	    }	
    	}
    	for (j=G->outStart[node]; j<=G->outEnd[node]; j++){
    	    idxType child = G->out[j];
    	    if (!((isIncomingBoundary(G, part, child))||(isOutgoingBoundary(G, part, child))))
    		continue;
    	    computeMovabletoBestPartNode(G, part, partsize, nbpart, child, movableto[child], gain[child], opt);
    	    gain_0[child] = gain[child][0];
    	    if ((inheap[child] == 0)&&(hasbeenmoved[child] == 0)){
    		heapInsert(G, heap, gain_0, &hsize, child);
    		inheap[child] = 1;
    	    }
    	}   

    	if ((*edgecut < minedgecut)||((*edgecut == minedgecut)&&(maxpartsize <= minmaxpartsize))){
    	    minedgecut = *edgecut;
    	    minmaxpartsize = maxpartsize;
    	    minindex = nbmoved;
    	    without_impro = -1;
    	}
    	without_impro ++;
    }

    for (i = nbmoved-1; i >= minindex; i--){
	part[moved[i]] = partcopy[moved[i]];
    }

    //printf("Nb moved %d\n",minindex);
    *edgecut = minedgecut;
    for (i = 1; i <= G->nVrtx; i++){
	free(gain[i]);
	free(movableto[i]);
    }
    free(gain);
    free(gain_0);
    free(heap);
    free(inheap);
    free(hasbeenmoved);
    free(moved);
    free(movableto);
    free(partsize);
    free(partcopy);
}



void refinementStep(MLGP_option opt, coarsen* coars, MLGP_info* info)
{
	//printf("\n\n\nrefinement called with opt.step = %d\n\n\n\n",opt.step);
    idxType nbpart = coars->nbpart;
    idxType j,i;
    idxType* partsize = (idxType*) calloc(opt.nbPart+1, sizeof(idxType));
    double bound[nbpart];
    idxType maxpartsize = 0, previous_maxpartsize;
    int nbbalance = 1;
    idxType* toporder = (idxType*) malloc(sizeof(idxType)*nbpart);
    idxType* fBpartsize;
    info->nbref_tab[opt.step] = 0;

    ecType lastobjvalue = ecType_MAX;

/*  by UVC
    idxType nbpart = 0, i;
    for (i=1; i<=coars->graph->nVrtx; i++)
	if (coars->part[i] > nbpart)
	    nbpart = coars->part[i];
    nbpart++;
*/
    //printf("refinementStep1\n");
    double sum = 0;
    for (i=0;i<nbpart;++i)
    	sum+=opt.ub[i];
    //printf("%lf\n",sum );
    for(i=0;i<nbpart;++i)
    	//bound[i] = (coars->graph->totvw/nbpart) + 0.9*(opt.ub[i] - (coars->graph->totvw/nbpart));
    	bound[i] = coars->graph->totvw*opt.ub[i]/sum + 0.9*(opt.ub[i]-coars->graph->totvw*opt.ub[i]/sum);

    for (i=1; i<=coars->graph->nVrtx; i++){
		if (coars->graph->frmt & DG_FRMT_VW) /* if it has vertex weights */
	   	 	partsize[coars->part[i]] += coars->graph->vw[i];
		else
	    	partsize[coars->part[i]] += 1;
    }
    
    if (opt.debug > 10){
		printf("PARTSIZES\n");
		for(i=0;i<nbpart;++i)
			printf("%d, ",partsize[i] );
 		printf("\nUPPERBOUNDS\n");
		for(i=0;i<nbpart;++i)
			printf("%lf, ",bound[i] );
 	   printf("\n============\n");
	}
    //printf("refinementStep2\n");

    for (i=0; i<nbpart; i++){
		maxpartsize = maxpartsize < partsize[i] ? partsize[i] : maxpartsize;
    }

    info->timing_forced_tab[opt.step] -= u_wseconds();
    randTopSortOnParts(coars->graph, coars->part, toporder, nbpart);

    //printf("refinementStep3\n");

    for(i=0; i<nbpart ; ++i ){
    	//printf("toporder on parts[%d] = %d\n",i,toporder[i]);
    }
    fBpartsize = forcedBalance(coars->graph, toporder, bound, coars->part, nbpart, opt, info);
    //printf("refinementStep4\n");
	
    while ((nbbalance < 4)&&partSizeChecker(fBpartsize,bound,nbpart)){
		randTopSortOnParts(coars->graph, coars->part, toporder, nbpart);
		previous_maxpartsize = maxpartsize;
		free(fBpartsize);
		fBpartsize = forcedBalance(coars->graph, toporder, bound, coars->part, nbpart, opt, info);
		nbbalance++;
    }
    info->ec_afterforced_tab[opt.step] = info->current_edgecut;
    info->timing_forced_tab[opt.step] += u_wseconds();

    //printf("refinementStep5\n");


    if (opt.debug > 10){
		printf("We ran %d Forced Balance iterations\n", nbbalance);
		for(i=0;i<nbpart;++i)
			printf("%d, ",fBpartsize[i] );
		printf("PARTSIZES\n");
    }
    free(fBpartsize);
    
    ecType objvalue = -1;
    switch(opt.co_obj) {
    	case CO_OBJ_EC :
			objvalue = edgeCut(coars->graph, coars->part);
            //printf("edgecut done inside refinement objvalue = %lf\n",objvalue);
			break;
    	case CO_OBJ_CV :
			objvalue = nbCommunications(coars->graph, coars->part);
			break;
    }
    //printf("refinementStep6\n");
    if(objvalue < 0)
        u_errexit("The objective value has not been set correctly\n");

    info->nbref_tab[opt.step] = 0;

    ecType** partmatrix;

    //printf("refinementStep7\n");

    //printf("co refinement %d Info length = %d\n", opt.co_refinement, INFO_LENGTH);

    switch(opt.co_refinement) {
        
    	case CO_REFINEMENT_NOTHING :
			break;

    	case CO_REFINEMENT_PO :
			lastobjvalue = objvalue;

			for (i=1; i<= opt.ref_step; i++) {
                //printf("refinementStep6\n");
	    		if (opt.print > 10){
					//print_graph(coars->graph, coars->part, opt.file_name_dot, opt.step, i);
	    		}
                info->timing_refinement_tab_tab[opt.step][i] -= u_wseconds();

                //printf("This is 2 step %d edgecut = %d\n",i,edgeCut(coars->graph,coars->part));
	    		//printf("part[1312] = %d\n", coars->part[1312]);
	    		//print_info_part(coars->graph, coars->part, opt.part_ub);
	    		refinementPostOrder(coars->graph, toporder, opt.lb, opt.ub, coars->part, nbpart, &objvalue, opt);
                info->current_edgecut = objvalue;

                if(opt.debug>4)
                {
                    ecType cutComputed;
                    printf("\twill verify the edge cut for refinement %d (CO_REFINEMENT_PO)\n", CO_REFINEMENT_PO);
                    cutComputed = edgeCut(coars->graph, coars->part);
                    if(cutComputed !=  info->current_edgecut )
                        u_errexit("edge cut computer %.2f but stored %.2f\n", 1.0*cutComputed, 1.0*info->current_edgecut );
                }
	    		if (opt.debug > 10)
					printf("   ref %d\t%d\t%d\t%d\t%d\n", i, coars->graph->nVrtx, coars->graph->nEdge, (int) objvalue, 0);

                info->timing_refinement_tab_tab[opt.step][i] += u_wseconds();

                info->ec_afterref_tab[opt.step][i] = info->current_edgecut;

                info->nbref_tab[opt.step] = i;

                if (objvalue >= 0.99*lastobjvalue) {
                    //printf("We break step %d beacause objvalue = %d and lastobjvalue = %d\n", i, (int) objvalue, (int) lastobjvalue);
                    break;
                }
                lastobjvalue = objvalue;

                //char filename[100];
				//sprintf(filename, "test/test_ref_%d.dot", i);
				//dgraph_to_dot(coars->graph, coars->part, filename);
				//printf("part[1312] = %d\n", coars->part[1312]);
			}
			//printf("This is 2 step %d edgecut = %d\n",i,edgeCut(coars->graph,coars->part));
			break;

        case CO_REFINEMENT_TB :
            lastobjvalue = objvalue;

            partmatrix = (ecType**) malloc(opt.nbPart * sizeof(ecType*));
            for (j=0; j<opt.nbPart; j++)
                partmatrix[j] = (ecType*) malloc(opt.nbPart * sizeof(ecType));


            for (i=1; i<= opt.ref_step; i++) {
                info->timing_refinement_tab_tab[opt.step][i] -= u_wseconds();

                buildPartmatrix(coars->graph, coars->part, opt.nbPart, partmatrix);
                //printf("Before refinement step %d:\n",i);
                //printPartMatrix(partmatrix, nbpart);
                refinementTowardBest(coars->graph, partmatrix, opt.lb, opt.ub, coars->part, nbpart, &objvalue, opt);
                info->current_edgecut = objvalue;

                
                if(opt.debug>4)
                {
                    ecType cutComputed;
                    printf("\twill verify the edge cut for refinement %d (CO_REFINEMENT_TB)\n", CO_REFINEMENT_TB);
                    cutComputed = edgeCut(coars->graph, coars->part);
                    if(cutComputed !=  info->current_edgecut )
                        u_errexit("edge cut computer %.2f but stored %.2f\n", 1.0*cutComputed, 1.0*info->current_edgecut );
                }
                
                if (opt.debug > 10){
                    printf("   ref %d\t%d\t%d\t%d\t%d\n", i, coars->graph->nVrtx, coars->graph->nEdge, objvalue, 0);
                }

                info->timing_refinement_tab_tab[opt.step][i] += u_wseconds();
                info->ec_afterref_tab[opt.step][i] = info->current_edgecut;
                info->nbref_tab[opt.step] = i;

                if (objvalue >= 0.99*lastobjvalue)
                    break;
                lastobjvalue = objvalue;


            }

            for (i=0; i<opt.nbPart; i++)
                free(partmatrix[i]);
            free(partmatrix);
            break;

        case CO_REFINEMENT_POTB :
            lastobjvalue = objvalue;
            partmatrix = (ecType**) malloc(opt.nbPart * sizeof(ecType*));
            for (j=0; j<opt.nbPart; j++)
                partmatrix[j] = (ecType*) malloc(opt.nbPart * sizeof(ecType));

            for (i=1; i<= opt.ref_step; i++) {
                info->timing_refinement_tab_tab[opt.step][i] -= u_wseconds();
                //printf("This is 2 step %d edgecut = %d\n",i,edgeCut(coars->graph,coars->part));
                //printf("part[1312] = %d\n", coars->part[1312]);
                //print_info_part(coars->graph, coars->part, opt.part_ub);
                randTopSortOnParts(coars->graph, coars->part, toporder, nbpart);

                refinementPostOrder(coars->graph, toporder, opt.lb, opt.ub, coars->part, nbpart, &objvalue, opt);
                info->current_edgecut = objvalue;
                
                buildPartmatrix(coars->graph, coars->part, opt.nbPart, partmatrix);
                //printPartMatrix(partmatrix, nbpart);
                refinementTowardBest(coars->graph, partmatrix, opt.lb, opt.ub, coars->part, nbpart, &objvalue, opt);
                info->current_edgecut = objvalue;
                if(opt.debug>4)
                {
                    ecType cutComputed;
                    printf("\twill verify the edge cut for refinement %d (CO_REFINEMENT_POTB) \n", CO_REFINEMENT_POTB);
                    cutComputed = edgeCut(coars->graph, coars->part);
                    if(cutComputed !=  info->current_edgecut )
                        u_errexit("edge cut computer %.2f but stored %.2f\n", 1.0*cutComputed, 1.0*info->current_edgecut );
                }
                
                if (opt.debug > 0){
                    printf("   ref %d\t%d\t%d\t%d\t%d\n", i, coars->graph->nVrtx, coars->graph->nEdge, objvalue, 0);
                }

                info->timing_refinement_tab_tab[opt.step][i] += u_wseconds();
                info->ec_afterref_tab[opt.step][i] = info->current_edgecut;
                info->nbref_tab[opt.step] = i;

                if (objvalue >= 0.99*lastobjvalue)
                    break;
                lastobjvalue = objvalue;


                //char filename[100];
                //sprintf(filename, "test/test_ref_%d.dot", i);
                //dgraph_to_dot(coars->graph, coars->part, filename);

                //printf("part[1312] = %d\n", coars->part[1312]);
            }
            //printf("This is 2 step %d edgecut = %d\n",i,edgeCut(coars->graph,coars->part));
            for (i=0; i<opt.nbPart; i++)
                free(partmatrix[i]);
            free(partmatrix);
            break;



    case CO_REFINEMENT_SWAP :

		partmatrix = (ecType**) malloc(opt.nbPart * sizeof(ecType*));
		for (j=0; j<opt.nbPart; j++)
		    partmatrix[j] = (ecType*) malloc(opt.nbPart * sizeof(ecType));

		lastobjvalue = objvalue;

		for (i=1; i<= opt.ref_step; i++) {
            info->timing_refinement_tab_tab[opt.step][i] -= u_wseconds();

            buildPartmatrix(coars->graph, coars->part, opt.nbPart, partmatrix);

			//printf("\n\nref step %d starting\n\n", i);
		    if (opt.print > 10) {
				//print_graph(coars->graph, coars->part, opt.file_name_dot, opt.step, i);
		    }
		    refinementPostOrder_Swap(coars->graph, partmatrix, toporder, opt.lb, opt.ub, coars->part, nbpart, &objvalue, opt);
            info->current_edgecut = objvalue;

            
            if(opt.debug>4)
            {
                ecType cutComputed;
                printf("will verify the edge cut\n");
                cutComputed = edgeCut(coars->graph, coars->part);
                if(cutComputed !=  info->current_edgecut )
                    u_errexit("edge cut computer %.2f but stored %.2f\n", 1.0*cutComputed, 1.0*info->current_edgecut );
            }
            
            if (opt.debug > 10){
			printf("   ref %d\t%d\t%d\t%d\t%d\n", i, coars->graph->nVrtx, coars->graph->nEdge, objvalue, 0);
		    }

            info->timing_refinement_tab_tab[opt.step][i] += u_wseconds();
            info->ec_afterref_tab[opt.step][i] = info->current_edgecut;
            info->nbref_tab[opt.step] = i;

            if (objvalue >= 0.99*lastobjvalue)
                break;
            lastobjvalue = objvalue;

        }
		
        for (i=0; i<opt.nbPart; i++)
            free(partmatrix[i]);
		free(partmatrix);
	break;

    case CO_REFINEMENT_MAX :
	
      lastobjvalue = objvalue;

		for (i=1; i<= opt.ref_step; i++) {
		    if (opt.print > 10){
				//print_graph(coars->graph, coars->part, opt.file_name_dot, opt.step, i);
		    }
            info->timing_refinement_tab_tab[opt.step][i] -= u_wseconds();

            //printf("This is 2 step %d edgecut = %d\n",i,edgeCut(coars->graph,coars->part));
		    //printf("part[1312] = %d\n", coars->part[1312]);
		    //print_info_part(coars->graph, coars->part, opt.part_ub);
		    

		    refinementPostOrder_Max(coars->graph, toporder, opt.lb, opt.ub, coars->part, nbpart, &objvalue, opt);
            info->current_edgecut = objvalue;

            
            if(opt.debug>4)
            {
                ecType cutComputed;
                printf("will verify the edge cut\n");
                cutComputed = edgeCut(coars->graph, coars->part);
                if(cutComputed !=  info->current_edgecut )
                    u_errexit("edge cut computer %.2f but stored %.2f\n", 1.0*cutComputed, 1.0*info->current_edgecut );
            }
            
            if (opt.debug > 0){
			printf("   ref %d\t%d\t%d\t%d\t%d\n", i, coars->graph->nVrtx, coars->graph->nEdge, objvalue, 0);
		    }

            info->timing_refinement_tab_tab[opt.step][i] += u_wseconds();
            info->ec_afterref_tab[opt.step][i] = info->current_edgecut;
            info->nbref_tab[opt.step] = i;

            if (objvalue >= 0.99*lastobjvalue)
                break;
            lastobjvalue = objvalue;


            //char filename[100];
		    //sprintf(filename, "test/test_ref_%d.dot", i);
		    //dgraph_to_dot(coars->graph, coars->part, filename);

		    //printf("part[1312] = %d\n", coars->part[1312]);
		}
		//printf("This is 2 step %d edgecut = %d\n",i,edgeCut(coars->graph,coars->part));
	break;

   case CO_REFINEMENT_COMBINED :

		partmatrix = (ecType**) malloc(opt.nbPart * sizeof(ecType*));
		for (j=0; j<opt.nbPart; j++)
		    partmatrix[j] = (ecType*) malloc(opt.nbPart * sizeof(ecType));

        //printf("partmatrix allocation done step = %d opt_step = %d timing_refinement_tab_tab[1000][1] = %lf\n",opt.ref_step,opt.step,info->timing_refinement_tab_tab[1000][1]);



		lastobjvalue = objvalue;
		for (i=1; i<= opt.ref_step; i++) {
		    if (opt.print > 0){
				//print_graph(coars->graph, coars->part, opt.file_name_dot, opt.step, i);
		    }
            //printf("info size = %lf\n",sizeof(info->timing_refinement_tab_tab)/sizeof(info->timing_refinement_tab_tab[0][0]));
            //printf("Refinement step loop i = %d timing_refinement_tab_tab[%d][%d] = %lf\n", i,99,0,info->timing_refinement_tab_tab[99][0]);
            info->timing_refinement_tab_tab[opt.step][i] -= u_wseconds();
            //printf("info->timing_refinement_tab_tab[%d][%d] = %lf\n",opt.step,i,info->timing_refinement_tab_tab[opt.step][i]);
            //printf("before refinementPostOrder_Max call\n");

		    refinementPostOrder_Max(coars->graph, toporder, opt.lb, opt.ub, coars->part, nbpart, &objvalue, opt);

            //printf("co refinement combined postordermax done\n");

            //printf("step %d refinementPostOrder_Max objvalue = %lf",i,objvalue);
            info->current_edgecut = objvalue;
            // checkAcyclicity(coars->graph, coars->part, nbpart);
		    buildPartmatrix(coars->graph, coars->part, opt.nbPart, partmatrix);
            //printf("buildpartmatrix done\n");
		    refinementPostOrder_Swap(coars->graph, partmatrix, toporder, opt.lb, opt.ub, coars->part, nbpart, &objvalue, opt);
            //printf("postorder swap done\n");
            info->current_edgecut = objvalue;
            
            if(opt.debug>4)
            {
                ecType cutComputed;
                printf("will verify the edge cut\n");
                cutComputed = edgeCut(coars->graph, coars->part);
                if(cutComputed !=  info->current_edgecut )
                    u_errexit("edge cut computer %.2f but stored %.2f\n", 1.0*cutComputed, 1.0*info->current_edgecut );
            }
            
            // checkAcyclicity(coars->graph, coars->part, nbpart);

		    if (opt.debug > 10){
			    printf("   ref %d\t%d\t%d\t%d\t%d\n", i, coars->graph->nVrtx, coars->graph->nEdge, objvalue, 0);
		    }

            info->timing_refinement_tab_tab[opt.step][i] += u_wseconds();
            info->ec_afterref_tab[opt.step][i] = info->current_edgecut;
            info->nbref_tab[opt.step] = i;

            //printf("laster dike\n");

            if (objvalue >= 0.99*lastobjvalue)
                break;
            lastobjvalue = objvalue;

            //char filename[100];
		    //sprintf(filename, "test/test_ref_%d.dot", i);
		    //dgraph_to_dot(coars->graph, coars->part, filename);

		    //printf("part[1312] = %d\n", coars->part[1312]);
		}
		//printf("This is 2 step %d edgecut = %d\n",i,edgeCut(coars->graph,coars->part));
		for (j=0; j<opt.nbPart; j++)
		  free(partmatrix[j]);
		free(partmatrix);

	break;


        case CO_REFINEMENT_RANDOMIZED:
            lastobjvalue = objvalue;

            partmatrix = (ecType**) malloc(opt.nbPart * sizeof(ecType*));
            for (j=0; j<opt.nbPart; j++)
                partmatrix[j] = (ecType*) malloc(opt.nbPart * sizeof(ecType));


            for (i=1; i<= opt.ref_step; i++) {
                info->timing_refinement_tab_tab[opt.step][i] -= u_wseconds();

                buildPartmatrix(coars->graph, coars->part, opt.nbPart, partmatrix);
                
                refinementRandomizedSelectTowardsBest(coars->graph, partmatrix, opt.lb, opt.ub, coars->part, nbpart, &objvalue, opt);
                info->current_edgecut = objvalue;

                
                if(opt.debug>4)
                {
                    ecType cutComputed;
                    printf("will verify the edge cut\n");
                    cutComputed = edgeCut(coars->graph, coars->part);
                    if(cutComputed !=  info->current_edgecut )
                        u_errexit("edge cut computer %.2f but stored %.2f\n", 1.0*cutComputed, 1.0*info->current_edgecut );
                }
                
                if (opt.debug > 10){
                    printf("   ref %d\t%d\t%d\t%d\t%d\n", i, coars->graph->nVrtx, coars->graph->nEdge, objvalue, 0);
                }

                info->timing_refinement_tab_tab[opt.step][i] += u_wseconds();
                info->ec_afterref_tab[opt.step][i] = info->current_edgecut;
                info->nbref_tab[opt.step] = i;

                if (objvalue >= 0.99*lastobjvalue)
                    break;
                lastobjvalue = objvalue;

            }

            for (i=0; i<opt.nbPart; i++)
                free(partmatrix[i]);
            free(partmatrix);

            break;
        default:
            u_errexit("not a defined refinement!\n");
            break;
    }

    free(toporder);
    free(partsize);
}
