#include "utils.h"
#include <sys/time.h>
#define U_MB     (1024*1024)


void* umalloc(long size, char* msg)
{
void* ptr = NULL;

if (size <= 0)
    return NULL;

ptr = (void*) malloc(size);
if (!ptr)
    {
    u_errexit("Memory allocation failed for '%s'.\nRequested size: %ld byte(s) [%.2lf MB].\n", msg, size, (double)size/(double)U_MB);
    }

return ptr;
}
void printNodeInfo(dgraph *G,idxType* part,idxType node){
    int j=0;
        printf("Node: %d (%d): [", node,part[node],G->nVrtx);
        for (j=G->inStart[node]; j<=G->inEnd[node]; j++){
            idxType father = G->in[j];
            printf("%d (%d), ", father,part[father] );
        }
        printf("]<" );
        for (j=G->outStart[node]; j<=G->outEnd[node]; j++){
            idxType child = G->out[j];
            printf("%d (%d),", child,part[child] );
        }
        printf(">\n");
}

void shuffleArray(idxType size, idxType* toBeShuffled, idxType cnt){
    // printf("%d %d\n",size,cnt );
    idxType i,j,tmp;
    while(cnt>0){
        j = rand()%size;
        i = rand()%size;
        tmp = toBeShuffled[i];
        toBeShuffled[i] = toBeShuffled[j];
        toBeShuffled[j] = tmp;
        --cnt;
    }
}

void heapRemove(dgraph *G, idxType *heapIds, ecType* key, idxType *sz, idxType i)
{
    idxType idx = -1, tmp;
    for (idx = 1; idx<=*sz; idx++)
	if (heapIds[idx] == i)
	    break;
    heapIds[idx] = heapIds[*sz];
    *sz = *sz - 1;
    while ((idx != 1)&&(key[idx] > key[idx/2])){
	tmp = heapIds[idx];
	heapIds[idx] = heapIds[idx/2];
	heapIds[idx/2] = heapIds[idx];
	idx = idx/2;
    }
    heapSort(G, heapIds, key, *sz, idx);
}

void randheapRemove(dgraph *G, idxType *heapIds, ecType* key, idxType *sz, idxType i)
{
    idxType idx = -1, tmp;
    for (idx = 1; idx<=*sz; idx++)
    if (heapIds[idx] == i)
        break;
    heapIds[idx] = heapIds[*sz];
    *sz = *sz - 1;
    //if the key is equal to the one we are comparing, flip a coin and decide the order.
    while ((idx != 1)&&(key[idx] > key[idx/2] || (key[idx] > key[idx/2] && rand()%2==1 ))){
        tmp = heapIds[idx];
        heapIds[idx] = heapIds[idx/2];
        heapIds[idx/2] = heapIds[idx];
        idx = idx/2;
    }
    heapSort(G, heapIds, key, *sz, idx);
}

void heapInsert(dgraph *G, idxType *heapIds, ecType* key, idxType *sz, idxType i)
{  
    idxType j, kv = key[i];
    *sz = *sz+1;
    heapIds[*sz] = i;
    j = *sz;

    while(j > 1 && key[heapIds[j/2]] < kv)
    {
        heapIds[j] = heapIds[j/2] ;
        j = j/2;
    }
    heapIds[j] = i;    
}

void randHeapInsert(dgraph *G, idxType *heapIds, ecType* key, idxType *sz, idxType i)
{  
    idxType j, kv = key[i];
    *sz = *sz+1;
    heapIds[*sz] = i;
    j = *sz;
    //if the key is equal to the one we are comparing, flip a coin and decide the order.
    while(j > 1 && ((key[heapIds[j/2]] < kv) || ( kv == key[heapIds[j/2]] && rand()%2==1) ) )
    {
        heapIds[j] = heapIds[j/2] ;
        j = j/2;
    }
    heapIds[j] = i;    
}

void heapSort(dgraph *G, idxType *heapIds, ecType* key, idxType sz, idxType i)
{
    idxType largest, j, l,r, tmp;
    
    largest = j = i;
    while(j<=sz/2)
    {
        l = 2*j;
        r = 2*j + 1;
        
        if(l<=sz && key[heapIds[l]]>key[heapIds[j]])
            largest = l;
        else
            largest = j;
        
        if(r<=sz && key[heapIds[r]]>key[heapIds[largest]])
            largest = r;
        
        if(largest != j)
        {
            tmp = heapIds[largest];
            heapIds[largest] = heapIds[j];
            heapIds[j] = tmp;
            j = largest;
        }
        else
            break;
    }
}

void randHeapSort(dgraph *G, idxType *heapIds, ecType* key, idxType sz, idxType i)
{
    idxType largest, j, l,r, tmp;
    
    largest = j = i;
    while(j<=sz/2)
    {
        l = 2*j;
        r = 2*j + 1;
        
        if(l<=sz && ((key[heapIds[l]]>key[heapIds[j]]) || (key[heapIds[l]]==key[heapIds[j]] && rand()%2==1)))
            largest = l;
        else
            largest = j;
        
        if(r<=sz && ((key[heapIds[r]]>key[heapIds[largest]]) || (key[heapIds[l]]==key[heapIds[largest]] && rand()%2==1)))
            largest = r;
        
        if(largest != j)
        {
            tmp = heapIds[largest];
            heapIds[largest] = heapIds[j];
            heapIds[j] = tmp;
            j = largest;
        }
        else
            break;
    }
}

idxType heapExtractMax(dgraph *G, idxType *heapIds, ecType* key, idxType *sz)
{
    idxType maxind ;
    if( *sz < 1)
        u_errexit("heap underflow\n");
    
    maxind = heapIds[1];
    heapIds[1] = heapIds[*sz];
    *sz = *sz - 1;
    heapSort(G, heapIds, key, *sz, 1);
    return maxind;
    
}

idxType randHeapExtractMax(dgraph *G, idxType *heapIds, ecType* key, idxType *sz)
{
    idxType maxind ;
    if( *sz < 1)
        u_errexit("heap underflow\n");
    
    maxind = heapIds[1];
    heapIds[1] = heapIds[*sz];
    *sz = *sz - 1;
    randHeapSort(G, heapIds, key, *sz, 1);
    return maxind;
}

void heapBuild(dgraph *G, idxType *heapIds, ecType* key, idxType sz)
{
    idxType i;
    for (i=sz/2; i>=1; i--)
        heapSort(G, heapIds, key, sz, i);
}

void randHeapBuild(dgraph *G, idxType *heapIds, ecType* key, idxType sz)
{
    idxType i;
    for (i=sz/2; i>=1; i--)
        randHeapSort(G, heapIds, key, sz, i);
}

void u_errexit(char * f_str,...)
{
    /*copied from Umit's*/
    va_list argp;
    
    fflush(stdout);
    fflush(stderr);
    fprintf(stderr, "\n****** Error ******\n");
    va_start(argp, f_str);
    vfprintf(stderr, f_str, argp);
    va_end(argp);
    
    fprintf(stderr,"*******************\n");
    fflush(stderr);
    
    printf("Error in Execution\n");
    exit(12);
}

double u_wseconds(void)
{
    struct timeval tp;
    
    gettimeofday(&tp, NULL);
    
    return (double) tp.tv_sec + (double) tp.tv_usec / 1000000.0;
}

