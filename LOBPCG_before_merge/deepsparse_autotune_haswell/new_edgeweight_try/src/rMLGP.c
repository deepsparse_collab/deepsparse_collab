#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <sys/stat.h>

#include <getopt.h>


#include "rMLGP.h"


/*#include "dgraph.h"
#include "bmmio.h"
#include "utils.h"
#include "debug.h"
#include "vcycle.h"
#include "rvcycle.h"
#include "partDGraphAlgos.h"
#include "matching.h"
#include "infoPart.h"*/

/*
char *infilename;
int K=2;
float eps = 0.03;
*/

dgraph main_graph;

/*********************************************************************************/
void printUsage_rMLGP(char *exeName)
{
    printf("Usage: %s <input-file-name> <#parts> [options...]\n", exeName);
    printf("Option:\n");

    //seed
    printf("\t-seed int \t(default = 0)\n");
    printf("\t\tSeed for the random generator\n");

    //ratio
    printf("\t--ratio float \t(default = 1.03)\n");
    printf("\t\tImbalance ration between parts\n");

    //debug
    printf("\t--debug int \t(default = 0)\n");
    printf("\t\tDebug value\n");

    //print
    printf("\t--print int \t(default = 0)\n");
    printf("\t\tValue to output graphs\n");

    //runs
    printf("\t--runs int \t(default = 1)\n");
    printf("\t\tNumber of runs for the graph\n");

    //co_stop
    printf("\t-co_stop int \t(default = 0)\n");
    printf("\t\tSet the stoping criteria pour the coarsening step\n");
    printf("\t\t= 0 : Stop when the size of graph is < co_stop_size\n");
    printf("\t\t= 1 : Stop after co_stop_step steps\n");

    //co_stop_size
    printf("\t-co_stop_size int \t(default = 50 * #parts)\n");
    printf("\t\tSet co_stop_size when co_stop = 0\n");
    
    //co_stop_step
    printf("\t-co_stop_step int \t(default = 10)\n");
    printf("\t\tSet co_stop_step when co_stop = 1\n");

    //co_match
    printf("\t--co_match int \t(default = 0)\n");
    printf("\t\tSet the matching technic for the coarsening step\n");
    printf("\t\t= 0 : Pairwise matching with top level difference 1\n");
    printf("\t\t= 1 : Aggregative matching with top level difference 1\n");
    printf("\t\t= 2 : Aggregative matching with out top level constraint\n");

    //co_match_norder
    printf("\t--co_match_norder int \t(default = 1)\n");
    printf("\t\tSet the node traversal order for the matching phase\n");
    printf("\t\t= 0 : Random traversal\n");
    printf("\t\t= 1 : Depth First Topological order\n");
    printf("\t\t= 2 : Random Depth First Topological order\n");
    printf("\t\t= 3 : Breadth First Topological order\n");
    printf("\t\t= 4 : Random Breadth First Topological order\n");
    printf("\t\t= 5 : Depth First order\n");
    printf("\t\t= 6 : Random Depth First order\n");
    printf("\t\t= 7 : Breadth First order\n");
    printf("\t\t= 8 : Random Breadth First order\n");

    //co_match_eorder
    printf("\t--co_match_eorder int \t(default = 1)\n");
    printf("\t\tSet the edge traversal order for the matching phase\n");
    printf("\t\t= 0 : Natural construction order\n");
    printf("\t\t= 1 : Edge weight priority\n");
    printf("\t\t= 2 : Edge weight / node weight priority\n");

    //co_match_onedegreefirst
    printf("\t--co_match_onedegreefirst int \t(default = 1)\n");
    printf("\t\tDo we traverse the nodes with degree 1 first in the matching?\n");
    printf("\t\t= 0 : No\n");
    printf("\t\t= 1 : Yes\n");

    //co_init
    printf("\t--co_inipart int \t(default = 0)\n");
    printf("\t\tDetermine the initial partitioning\n");
    printf("\t\t= 0 : try all partitioning and pick the best one\n");
    printf("\t\t= 1 : Kernighan with random DFS\n");
    printf("\t\t= 2 : Kernighan with random BFS\n");
    printf("\t\t= 3 : Kernighan with random mix BFS-DFS\n");
    printf("\t\t= 4 : Greedy graph growing approach\n");
    printf("\t\t= 5 : Forced balance initial partitioning\n");
    printf("\t\t= 6 : Metis with fixing acyclicity and forced balance\n");
    printf("\t\t= 7 : BFS Growing - Fan Out\n");
    printf("\t\t= 8 : Cocktail Shake Growing - Fan In Fan Out\n");



    //co_nbinit
    printf("\t--co_nbinipart int \t(default = 5)\n");
    printf("\t\tNumber of runs for the initial partitioning\n");

    //co_refinement
    printf("\t-co_refinement int \t(default = 1)\n");
    printf("\t\tDetermine the refinement in uncoarsening step\n");
    printf("\t\t= 0 : no refinement\n");
    printf("\t\t= 1 : toward the largest partition in topological order\n");
    printf("\t\t= 2 : toward the best partition if it creates no cycle\n");
    printf("\t\t= 3 : refinement using method 1 and 2 consecutively\n");
    printf("\t\t= 4 : refinement by swaping nodes (for bisection only)\n");
    printf("\t\t= 5 : refinement from largest part to smallest (for bisection only)\n");
    printf("\t\t= 6 : refinement using method 5 and 4 consecutively (for bisection only)\n");
    printf("\t\t= 7 : refinement randomized selection of boundary vertices to move\n");

    //co_obj
    printf("\t--co_obj int \t(default = 0)\n");
    printf("\t\tDetermine the objective we are trying to minimize\n");
    printf("\t\t= 0 : minimize edge-cut\n");
    printf("\t\t= 1 : minimize communication volume\n");

    //part_ub
    printf("\t-part_ub double \t(default = (tot_weight/nbpart) * 1.03)\n");
    printf("\t\tUpper bound for the partition\n");

    //part_lb
    printf("\t-part_lb double \t(default = (tot_weight/nbpart) / 1.03)\n");
    printf("\t\tLower bound for the partition\n");

    //use_binary_input
    printf("\t--use_binary_input (-x) int \t(default = 1)\n");
    printf("\t\tCreate/Read binary version of the input graph.\n");
}



int my_processArgs_rMLGP(int partCount,  MLGP_option* opt)
{
    int i;
    

    opt->nbPart = partCount;

    printf("\n\nmy processArgs_rMLGP nbpart value = %d\n", opt->nbPart);

    if ((opt->nbPart != 2)&&(opt->nbPart != 4)&&(opt->nbPart != 8)&&(opt->nbPart != 16)&&(opt->nbPart != 32)&&(opt->nbPart != 64)&&(opt->nbPart != 128)&&(opt->nbPart != 256)&&(opt->nbPart != 512)&&(opt->nbPart != 1024)&&
                        (opt->nbPart != 2048)&&(opt->nbPart != 3000)&&(opt->nbPart != 4096)&&(opt->nbPart != 8192)){
        //printUsage_rMLGP(argv[0]);
        u_errexit(" This number of partitions is not supported for now. Try a power of 2.\n");
    }

    opt->seed = 0;
    opt->co_stop = 0;
//    opt->co_stop_size = 50*opt->nbPart;
//    opt->co_stop_size = 500*opt->nbPart;
//    opt->co_stop_size = 2000000*opt->nbPart;
    opt->co_stop_size = 3000;
    opt->co_stop_step = 50;
    opt->co_match = 0;
    opt->co_match_norder = 1;
    opt->co_match_eorder = 1;
    opt->co_match_onedegreefirst = 1;
    opt->co_inipart = 0;
    opt->co_nbinipart = 5;
    opt->co_refinement = 6;
    //opt->co_refinement = 1;
    
    opt->ub = (double*)malloc((opt->nbPart)*sizeof(double));
    opt->lb = (double*)malloc((opt->nbPart)*sizeof(double));

    for (i=0;i<opt->nbPart;i++){
        opt->ub[i] = -1;
        opt->lb[i] = -1;
    }
    opt->ref_step = 10;
    opt->co_obj = 0;
    opt->ratio = 1.03;
    opt->runs = 1;
    opt->debug = 1;
    opt->print = 20;
    opt->use_binary_input = 0;
    static struct option long_options[] =
    {
        {"seed",  required_argument, 0, 's'},
    {"co_stop",  required_argument, 0, 't'},
    {"co_stop_size",  required_argument, 0, 'z'},
    {"co_stop_step",  required_argument, 0, 'p'},
    {"co_match",  required_argument, 0, 'm'},
        {"co_match_norder",  required_argument, 0, 'o'},
        {"co_match_eorder",  required_argument, 0, 'g'},
    {"co_match_onedegreefirst",  required_argument, 0, '1'},
    {"co_inipart",  required_argument, 0, 'i'},
    {"co_nbinipart",  required_argument, 0, 'b'},
    {"co_refinement",  required_argument, 0, 'r'},
    {"co_obj",  required_argument, 0, 'j'},
    {"part_ub",  required_argument, 0, 'u'},
    {"part_lb",    required_argument, 0, 'l'},
    {"ratio",    required_argument, 0, 'a'},
    {"runs",    required_argument, 0, 'n'},
    {"debug",    required_argument, 0, 'd'},
    {"print",    required_argument, 0, 'q'},
    {"ref_step",    required_argument, 0, 'f'},
    {"use_binary_input", required_argument, 0, 'x'},
    };

    const char *delims = "s:t:z:p:m:o:g:1:i:b:r:j:u:l:a:n:d:q:f:x:";
    char o;
    int option_index = 0;
    /*while ((o = getopt_long(argc, argv, delims, long_options, &option_index)) != -1) {
        switch (o) {
            case 's':
                opt->seed = atoi(optarg);
                break;
            case 't':
                opt->co_stop = atoi(optarg);
                break;
            case 'z':
                opt->co_stop_size = atoi(optarg);
                break;
            case 'p':
                opt->co_stop_step = atoi(optarg);
                break;
            case 'm':
                opt->co_match = atoi(optarg);
                break;
            case 'o':
                opt->co_match_norder = atoi(optarg);
                break;
            case 'g':
                opt->co_match_eorder = atoi(optarg);
                break;
            case '1':
                opt->co_match_onedegreefirst = atoi(optarg);
                break;
            case 'i':
                opt->co_inipart = atoi(optarg);
                break;
            case 'b':
                opt->co_nbinipart = atoi(optarg);
                break;
            case 'r':
                opt->co_refinement = atoi(optarg);
                break;
            case 'j':
                opt->co_obj = atoi(optarg);
                break;
            case 'u':
                opt->ub[0] = atof(optarg);
                break;
            case 'l':
                opt->lb[0] = atof(optarg);
                break;
            case 'a':
                opt->ratio = atof(optarg);
                break;
            case 'd':
                opt->debug = atoi(optarg);
                break;
        case 'q':
                opt->print = atoi(optarg);
                break;
            case 'n':
                opt->runs = atof(optarg);
                break;
            case 'f':
                opt->ref_step = atoi(optarg);
                break;
            case 'x':
                opt->use_binary_input = atoi(optarg);
                break;
            default:
                printUsage_rMLGP(argv[0]);
        u_errexit("Option %s not recognized.", o);
                return 1;
        }
    }*/
    
    for(i=1;i<opt->nbPart;i++){
        opt->ub[i]=opt->ub[0];
        opt->lb[i]=opt->lb[0];
    }
    char suf[200];
    char fname[200];
    char* split;    
    char fname_sav[200];
    sprintf(fname_sav, opt->file_name);
    if (opt->print > 0){
    split = strtok(fname_sav, "/");
    while(split != NULL){
        sprintf(fname, split);
        split = strtok(NULL, "/");
    }
    sprintf(opt->file_name_dot, "out/");
    mkdir(opt->file_name_dot, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IWOTH | S_IXOTH);
    strcat(opt->file_name_dot, fname);
    sprintf(suf, "__%d_%d", opt->nbPart, opt->seed);
    strcat(opt->file_name_dot, suf);
    mkdir(opt->file_name_dot, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IWOTH | S_IXOTH);
    sprintf(suf, "/graph");
    strcat(opt->file_name_dot, suf);
    }    

    return 0;
}



int processArgs_rMLGP(int argc, char **argv, MLGP_option* opt)
{
    int i;
    if (argc < 3)
    {
	   printUsage_rMLGP(argv[0]);
	   u_errexit("%s: There is a problem with the arguments.\n", argv[0]);
    }

    strncpy(opt->file_name, argv[3], PATH_MAX);    

    opt->nbPart = atoi(argv[2]);

    printf("\n\nprocessArgs_rMLGP nbpart value = %d\n", opt->nbPart);

    if ((opt->nbPart != 2)&&(opt->nbPart != 4)&&(opt->nbPart != 8)&&(opt->nbPart != 16)&&(opt->nbPart != 32)&&(opt->nbPart != 64)&&(opt->nbPart != 128)&&(opt->nbPart != 256)&&(opt->nbPart != 512)&&(opt->nbPart != 1024)&&
                        (opt->nbPart != 2048)&&(opt->nbPart != 3000)&&(opt->nbPart != 4096)&&(opt->nbPart != 8192)){
	    printUsage_rMLGP(argv[0]);
	    u_errexit("%s: This number of partitions is not supported for now. Try a power of 2.\n", argv[0]);
    }

    opt->seed = 0;
    opt->co_stop = 0;
//    opt->co_stop_size = 50*opt->nbPart;
//    opt->co_stop_size = 500*opt->nbPart;
//    opt->co_stop_size = 2000000*opt->nbPart;
    opt->co_stop_size = 3000;
    opt->co_stop_step = 50;
    opt->co_match = 0;
    opt->co_match_norder = 1;
    opt->co_match_eorder = 1;
    opt->co_match_onedegreefirst = 1;
    opt->co_inipart = 0;
    opt->co_nbinipart = 5;
    opt->co_refinement = 6;
    //opt->co_refinement = 1;
    
    opt->ub = (double*)malloc((opt->nbPart)*sizeof(double));
    opt->lb = (double*)malloc((opt->nbPart)*sizeof(double));

    for (i=0;i<opt->nbPart;i++){
        opt->ub[i] = -1;
        opt->lb[i] = -1;
    }
    opt->ref_step = 10;
    opt->co_obj = 0;
    opt->ratio = 1.03;
    opt->runs = 1;
    opt->debug = 1;
    opt->print = 20;
    opt->use_binary_input = 0;
    static struct option long_options[] =
    {
        {"seed",  required_argument, 0, 's'},
	{"co_stop",  required_argument, 0, 't'},
	{"co_stop_size",  required_argument, 0, 'z'},
	{"co_stop_step",  required_argument, 0, 'p'},
	{"co_match",  required_argument, 0, 'm'},
        {"co_match_norder",  required_argument, 0, 'o'},
        {"co_match_eorder",  required_argument, 0, 'g'},
	{"co_match_onedegreefirst",  required_argument, 0, '1'},
	{"co_inipart",  required_argument, 0, 'i'},
	{"co_nbinipart",  required_argument, 0, 'b'},
	{"co_refinement",  required_argument, 0, 'r'},
	{"co_obj",  required_argument, 0, 'j'},
	{"part_ub",  required_argument, 0, 'u'},
	{"part_lb",    required_argument, 0, 'l'},
	{"ratio",    required_argument, 0, 'a'},
	{"runs",    required_argument, 0, 'n'},
	{"debug",    required_argument, 0, 'd'},
	{"print",    required_argument, 0, 'q'},
    {"ref_step",    required_argument, 0, 'f'},
    {"use_binary_input", required_argument, 0, 'x'},
    };

    const char *delims = "s:t:z:p:m:o:g:1:i:b:r:j:u:l:a:n:d:q:f:x:";
    char o;
    int option_index = 0;
    while ((o = getopt_long(argc, argv, delims, long_options, &option_index)) != -1) {
        switch (o) {
            case 's':
                opt->seed = atoi(optarg);
                break;
            case 't':
                opt->co_stop = atoi(optarg);
                break;
            case 'z':
                opt->co_stop_size = atoi(optarg);
                break;
            case 'p':
                opt->co_stop_step = atoi(optarg);
                break;
            case 'm':
                opt->co_match = atoi(optarg);
                break;
            case 'o':
                opt->co_match_norder = atoi(optarg);
                break;
            case 'g':
                opt->co_match_eorder = atoi(optarg);
                break;
            case '1':
                opt->co_match_onedegreefirst = atoi(optarg);
                break;
            case 'i':
                opt->co_inipart = atoi(optarg);
                break;
            case 'b':
                opt->co_nbinipart = atoi(optarg);
                break;
            case 'r':
                opt->co_refinement = atoi(optarg);
                break;
            case 'j':
                opt->co_obj = atoi(optarg);
                break;
            case 'u':
                opt->ub[0] = atof(optarg);
                break;
            case 'l':
                opt->lb[0] = atof(optarg);
                break;
            case 'a':
                opt->ratio = atof(optarg);
                break;
            case 'd':
                opt->debug = atoi(optarg);
                break;
	    case 'q':
                opt->print = atoi(optarg);
                break;
            case 'n':
                opt->runs = atof(optarg);
                break;
            case 'f':
                opt->ref_step = atoi(optarg);
                break;
            case 'x':
                opt->use_binary_input = atoi(optarg);
                break;
            default:
                printUsage_rMLGP(argv[0]);
		u_errexit("Option %s not recognized.", o);
                return 1;
        }
    }
    
    for(i=1;i<opt->nbPart;i++){
        opt->ub[i]=opt->ub[0];
        opt->lb[i]=opt->lb[0];
    }
    char suf[200];
    char fname[200];
    char* split;    
    char fname_sav[200];
    sprintf(fname_sav, opt->file_name);
    if (opt->print > 0){
	split = strtok(fname_sav, "/");
	while(split != NULL){
	    sprintf(fname, split);
	    split = strtok(NULL, "/");
	}
	sprintf(opt->file_name_dot, "out/");
	mkdir(opt->file_name_dot, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IWOTH | S_IXOTH);
	strcat(opt->file_name_dot, fname);
	sprintf(suf, "__%d_%d", opt->nbPart, opt->seed);
	strcat(opt->file_name_dot, suf);
	mkdir(opt->file_name_dot, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IWOTH | S_IXOTH);
	sprintf(suf, "/graph");
	strcat(opt->file_name_dot, suf);
    }    

    return 0;
}


void run_rMLGP(char* file_name, MLGP_option opt, int *edge_u, int *edge_v, double *edge_weight, int edgeCount, int vertexCount,const char** vertexName,double* vertexWeight, int loopType, char* matrix_name)
{
    rMLGP_info info;
    info.depth = 0;
    dgraph G;
    int partcount = -1;
    int i,j;
    ecType* memory_from_out;
    ecType* memory_from_in;

    //printf("%s\n",vertexName[0]);

    /*if (strstr(file_name,".dot")){
        //generateDGraphFromDot(&G, file_name,opt.use_binary_input);
        //my_generate_graph(&G, file_name,opt.use_binary_input);
        my_generate_graph_fazlay(&G, file_name,opt.use_binary_input, edge_u, edge_v, edge_weight, edgeCount, vertexCount,vertexName,vertexWeight);
    }

    else if(strstr(file_name,".mtx")){   
        generateDGraphFromMtx(&G,file_name,opt.use_binary_input);
    }*/

    my_generate_graph_fazlay(&G, file_name,opt.use_binary_input, edge_u, edge_v, edge_weight, edgeCount, vertexCount,vertexName,vertexWeight);
    for(int i = 0 ; i < G.nEdge ; i++){
    //	printf("out[%d] = %d\n",i,G.out[i]);
    }

    printf("run_rMLGP function edge = %d node = %d \n",edgeCount,vertexCount);

    set_dgraph_info(&G);




    main_graph = G;

    printf("G.nvrtx = %d\n",G.nVrtx);
    G.totvw = G.nVrtx;  
     printf("set_dgraph_info done\n");
    // double x = -u_wseconds();
    // x+=u_wseconds();
    // printf("read time %lf\n",x);
    //exit(1);
    int maxindegree, minindegree, maxoutdegree, minoutdegree;
    double aveindegree, aveoutdegree;
    dgraph_info(&G, &maxindegree, &minindegree, &aveindegree, &maxoutdegree, &minoutdegree, &aveoutdegree);
    printf("dgraph_info done\n");
    G.maxindegree = maxindegree;
    G.maxoutdegree = maxoutdegree;
    
    printf("maxIndegree = %d\t minindegree =  %d\n maxoutdegree =  %d\t  minoutdegree = %d\n avgindegree = %lf\t avgutdegree =  %lf\n", maxindegree,minindegree, maxoutdegree,minoutdegree,aveindegree,aveoutdegree);
    //double ratio = opt.ratio;
    //double eca = ((ratio-1.)*G.nVrtx)/opt.nbPart;

   /* FILE* graph_edges = fopen("graph_edges.txt","w");

    for (i = 1;i<=G.nVrtx ; i++){
        //printf("%s = %d  %d\n",G.vertices[i-1],G.inEnd[i] - G.inStart[i]+1,G.outEnd[i] - G.outStart[i]+1);
        for(j = G.inStart[i] ; j <= G.inEnd[i]; j++){
            //printf("%s ---> %s\n",G->vertices[G->in[j]-1],G->vertices[i-1]);
            fprintf(graph_edges,"%s --> %s\n",G.vertices[G.in[j]-1],G.vertices[i-1]);
        }
    }
    fclose(graph_edges);
    //return;
*/

    char* input1 = (char*)malloc(200*sizeof(char));
    char* input2 = (char*)malloc(200*sizeof(char));
    char* output = (char*)malloc(200*sizeof(char));

//    get_input_output(G.vertices[100],input1,input2,output);

    printf("\n\n\n node %s inp_1 %s inp_2 %s output %s \n\n",G.vertices[100],input1,input2,output);
    //return;


    for (i = 1;i<=G.nVrtx ; i++){
        for(j = G.inStart[i] ; j <= G.inEnd[i]; j++){
            //printf("%d ---> %d\n",G->in[j],i);
            fill_allinout_memory_map(G.vertices[i-1],G.vertices[G.in[j]-1],G.ecIn[j]);
            //fprintf(initial_graph,"%d -> %d;\n",G->in[j],i);
        }
    }

    printf("graph total weight = %lf\n",G.totvw);
    for(i = 0 ; i < G.nEdge ; i++){
    //	printf("out[%d] = %d\n",i,G.out[i]);
    }
    for (i=0; i<opt.nbPart; i++){
        if (opt.lb[i] < 0)
            opt.lb[i] = 1;
        //Warning: Here the upper bound is the targeted value for rvcycle
        if (opt.ub[i] < 0)
 //           opt.ub[i] = G.nVrtx/(opt.nbPart*1.0);
        	opt.ub[i] = G.totvw/(opt.nbPart*1.0);

        if ((floor(opt.lb[i]) < opt.lb[i])&&(floor(opt.lb[i]) == floor(opt.ub[i]))){
            printf("WARNING: The balance can not be matched!!!\n");
            opt.lb[i] = floor(opt.lb[i]);
            opt.ub[i] = floor(opt.ub[i])+1; 
        }
    }

      /*  FILE* indegree_file;
        indegree_file = fopen("indegree.txt","w");
        for(i = 1 ; i <= G.nVrtx ; i++){

            fprintf(indegree_file, "%s %d\n", G.vertices[i-1],G.inEnd[i] - G.inStart[i]+1);
        }
        fclose(indegree_file);
*/
  /*      FILE* outdegree_file;
        outdegree_file = fopen("outdegree.txt","w");
        for(i = 1 ; i <= G.nVrtx ; i++){
            if(!strcmp(G.vertices[i-1],"DLACPY,1,1")){
                for (j = G.outStart[i] ; j <= G.outEnd[i] ; j++ ){
                    //printf("%s --> %s\n",G.vertices[i-1],G.vertices[G.out[j]-1]);
                }
            }
            if(!strcmp(G.vertices[i-1],"INV,RBR")){
                for (j = G.outStart[i] ; j <= G.outEnd[i] ; j++ ){
                    //printf("%s --> %s\n",G.vertices[i-1],G.vertices[G.out[j]-1]);
                }
            }
            fprintf(outdegree_file, "%s %d\n", G.vertices[i-1],G.outEnd[i] - G.outStart[i]+1);
        }
        fclose(outdegree_file);
*/

    /*FILE* large_vname = fopen("large_vname.txt","w");
    for(i = 0 ; i < G.nVrtx ; i++){
        fprintf(large_vname, "%s 0 %d\n",G.vertices[i],G.nVrtx-i);
    }
    fclose(large_vname);
*/



    //return;


    //opt.ub[0] = 1.5*opt.ub[0];
    //opt.ub[1] = 0.8*opt.ub[1];

    double* save_ub = (double*) malloc(sizeof(double) * opt.nbPart);
    for (i=0; i<opt.nbPart; i++)
        save_ub[i] = opt.ub[i];

        //printf("Nb node: %d\nNb Edges: %d\nNb part: %d\nLower Bound[0]: %f\nUpper Bound[0]: %f\n\n", G.nVrtx, G.nEdge, opt.nbPart, opt.part_lb[0], opt.part_ub[0]);
    printf("Nb node: %d\nNb Edges: %d\nNb part: %d\nLower Bound[0]: %f\nTargeted Bound[0]: %f\n\n", G.nVrtx, G.nEdge, opt.nbPart, opt.lb[0], opt.ub[0]);
    ecType * edgecut = (ecType *) malloc(sizeof(ecType) * opt.runs);
    int* nbcomm = (int*) malloc(sizeof(int) * opt.runs);
    double* latencies = (double*) malloc(sizeof(double) * opt.runs);
    int r;
    rcoarsen* rcoars;

    for (r = 0; r<opt.runs; r++){
        initRInfoPart(&info);
        printf("########################## RUN %d ########################\n", r);

        srand((unsigned) opt.seed + r);
        //double t = -u_wseconds();
//        opt.debug = 0;

        //rcoars = rVCycle(&G, opt, &info);
        //printf("rMLGP first vertex = %s\n",G.vertices[87]);
        rcoars = my_rVCycle(&G,opt,&info,&partcount);
        printf("\n\n\nresulting part count = %d\n\n",partcount);
        for (i=0; i<G.nVrtx; i++){
    //	fprintf(file, "%s = part %d\n",G.vertices[i], (int) rcoars->coars->part[i]);
    //	printf("%s = part %d\n",G.vertices[i], (int) rcoars->coars->part[i]);
        }



        edgecut[r] = edgeCut(&G, rcoars->coars->part);
        nbcomm[r] = nbCommunications(&G, rcoars->coars->part);
        latencies[r] = computeLatency(&G, rcoars->coars->part, 1.0, 11.0);
	    printf("Final run %d\t%d\t%d\t%lf\t%d\t%f\n", r, G.nVrtx, G.nEdge, edgecut[r], nbcomm[r], latencies[r]);
        printf("Times\t%.3lf\t%.3lf\t%.3lf\t%.3lf\n", info.timing_coars, info.timing_inipart, info.timing_uncoars,info.timing_global);

        for (i=0; i<opt.nbPart; i++)
            opt.ub[i] = opt.ub[i] * opt.ratio;

        print_info_part(rcoars->coars->graph, rcoars->coars->part, opt);
        print_info_part_file(rcoars->coars->graph, rcoars->coars->part, opt);
        checkAcyclicity(rcoars->coars->graph, rcoars->coars->part, opt.nbPart);


        for (i=0; i<opt.nbPart; i++)
            opt.ub[i] = save_ub[i];
	    opt.seed++;
	    printf("######################## END RUN %d ########################\n\n", r);
        if (opt.print > 0) {
            printf("######################## OUTPUT %d ########################\n\n", r);
            printRInfoPart(&info, rcoars, opt);
            printf("\n###########################################################\n\n");
        }
        freeRInfoPart(&info);
	    /*
	    int* partsize = (int*) malloc(sizeof(int) * opt.nbPart);
	    int nn = nbPart(&G, rcoars->coars->part, partsize);
	    printf("Nbpart = %d\nPartsize: ", nn);
	    int k;
	    for (k = 0; k<nn; k++)
	        printf("%d ", partsize[k]);
	    printf("\n");
        free(partsize);
        */
	    
    }

        //temp return, double pointer free error, look later
    //return;



    double edgecutave = 0.0, nbcommave = 0.0, edgecutsd = 0.0, nbcommsd = 0.0;
    for (r = 0; r<opt.runs; r++){
	edgecutave += edgecut[r];
	nbcommave += nbcomm[r];
    }
    edgecutave = edgecutave / opt.runs;
    nbcommave = nbcommave / opt.runs;
    for (r = 0; r<opt.runs; r++){
	edgecutsd = edgecut[r] - edgecutave < 0? edgecutave - edgecut[r] : edgecut[r] - edgecutave;
	nbcommsd = nbcomm[r] - nbcommave < 0? nbcommave - nbcomm[r] : nbcomm[r] - nbcommave;
    }
    edgecutsd = edgecutsd / opt.runs;
    nbcommsd = nbcommsd / opt.runs;
    printf("Final runs\t%d\t%d\t%lf\t%f\t%f\t%f\n", G.nVrtx, G.nEdge, edgecutave, edgecutsd, nbcommave, nbcommsd);    


    FILE *file;
    FILE *different_part;
    FILE *same_part;
    FILE *graph_lookup_100;
    FILE *graph_lookup_99;
    char name_tmp[200];
    sprintf(name_tmp,".rMLGP.part.%d.seed.%d", opt.nbPart, opt.seed);
    char res_name[200];
    strcpy(res_name, file_name);
    strcat(res_name, name_tmp);
    strcat(res_name,".txt");
    file = fopen(res_name, "w");
    different_part = fopen("different_part.txt","w");
    same_part = fopen("same_part.txt","w");
    graph_lookup_100 = fopen("graph_lookup_100.txt","w");
    graph_lookup_99 = fopen("graph_lookup_99.txt","w");
//    idxType i;

    FILE *original_graph_partition  = fopen("original_graph_partition.txt","w");
    for (i=1; i<=rcoars->coars->graph->nVrtx; i++){
//	fprintf(file, "%s = part %d\n",G.vertices[i], (int) rcoars->coars->part[i]);
//	fprintf(original_graph_partition, "%s = part %d\n",rcoars->coars->graph->vertices[i-1], (int) rcoars->coars->part[i]);
    }

    fprintf(file, "%d\n", partcount);
    int p_part = -1;
    int distinct_part_count = 0;

    idxType* my_partition_topsort = (idxType*)malloc((rcoars->coars->graph->nVrtx+1)*sizeof(idxType));
    //DFStopsort_with_part(&G,rcoars->coars->part,opt.nbPart,my_partition_topsort);
    DFStopsort_with_part(&G,rcoars->coars->part,partcount,my_partition_topsort);
    for(i=1;i<=rcoars->coars->graph->nVrtx;i++){
        if(rcoars->coars->part[my_partition_topsort[i]] != p_part){
            distinct_part_count++;
            p_part = rcoars->coars->part[my_partition_topsort[i]];
        }
         fprintf(original_graph_partition,"%s %d %d\n",vertexName[my_partition_topsort[i]-1],rcoars->coars->part[my_partition_topsort[i]],rcoars->coars->graph->nVrtx-i);
        // //printf("partition top sort[%d] = %d part = %d nodename = %s\n",i,my_partition_topsort[i],rcoars->coars->part[my_partition_topsort[i]],rcoars->coars->graph->vertices[my_partition_topsort[i]-1]);
        // if(vertexName[my_partition_topsort[i]-1][0] == 'S' && vertexName[my_partition_topsort[i]-1][1] == 'P' &&vertexName[my_partition_topsort[i]-1][2] == 'M' &&
        //     vertexName[my_partition_topsort[i]-1][3] == 'M' && vertexName[my_partition_topsort[i]-1][4]==',')
    	   // //fprintf(file,"topsort[%d] = %d part = %d noden = %s\n",i,my_partition_topsort[i],rcoars->coars->part[my_partition_topsort[i]],vertexName[my_partition_topsort[i]-1]);
        //     fprintf(file,"%s %d\n",vertexName[my_partition_topsort[i]-1],rcoars->coars->part[my_partition_topsort[i]]);
        // //fprintf(file,"%d %d\n",my_partition_topsort[i],rcoars->coars->part[my_partition_topsort[i]]);
    }
    fclose(original_graph_partition);

    



    //////memory calculation for each partition/////////////////
    memory_from_out = (ecType*)calloc(partcount,sizeof(ecType));
    memory_from_in = (ecType*)calloc(partcount,sizeof(ecType));

    for(i=0;i<partcount;++i){
        memory_from_in[i] = 0;
        memory_from_out[i] = 0;
    }


    // for(i=1;i<=rcoars->coars->graph->nVrtx;i++){


    //             //printf("%s %d\n",vertexName[my_partition_topsort[i]-1],rcoars->coars->part[my_partition_topsort[i]]);
    //             //fprintf(same_part, "%s\n", vertexName[my_partition_topsort[i]-1]);
    //             memory_from_in[rcoars->coars->part[my_partition_topsort[i]]] += rcoars->coars->graph->vWeight[my_partition_topsort[i]]; //at this moment 1
    //             for(j=rcoars->coars->graph->inStart[my_partition_topsort[i]];j<=rcoars->coars->graph->inEnd[my_partition_topsort[i]];++j)
    //             {
    //                 //printf("vertex = %s(%d) j = %s(%d) part = %d\n",vertexName[my_partition_topsort[i]-1],my_partition_topsort[i]-1,vertexName[rcoars->coars->graph->in[j]-1],j,rcoars->coars->part[rcoars->coars->graph->in[j]-1]);
    //                 //incoming_edge_weight_in_part += igraph->ecIn[j];
    //                 if(rcoars->coars->part[rcoars->coars->graph->in[j]] != rcoars->coars->part[my_partition_topsort[i]])
    //                 {   
    //                     //fprintf(different_part, "%s %d\n", vertexName[rcoars->coars->graph->in[j]-1],rcoars->coars->part[rcoars->coars->graph->in[j]-1]);
    //                     memory_from_out[rcoars->coars->part[my_partition_topsort[i]]] += rcoars->coars->graph->ecIn[j];
    //                 }
    //             }
            


    //     }
    FILE* mem_out = fopen("mem_out.txt","w");
    FILE* mem_in = fopen("mem_in.txt","w");

    // for(i = 0 ; i < partcount ; i++){
    //     fprintf(mem_out, "%d %lf\n",i,memory_from_out[i] );
    //     fprintf(mem_in, "%d %lf\n",i,memory_from_in[i] );

    // }



    myprint();

    //get the new block info
    int **nnzblock_matrix = NULL;
    int nrowblocks,ncolblocks; 

    printf("wblk = %d\n", wblk);
    int starting_block_size = wblk;




    get_new_csb_block(small_block,&nnzblock_matrix, &nrowblocks, &ncolblocks);

    for(i = 0 ; i < nrowblocks ; i++){
        for(j = 0 ; j < ncolblocks ; j++){
            //printf("%d ",nnzblock_matrix[i][j]);
        }
        //printf("\n");
    }

/*temporary placement, move to the last of the function later*/
    //break large blocked graph into small blocked graph
    //my_generate_smallgraph(&G, file_name,opt.use_binary_input, edge_u, edge_v, edge_weight, edgeCount, vertexCount,vertexName,vertexWeight,8);
    int block_divisor = starting_block_size/small_block;
    //int old_blocksize = (sqrt(1917+4*vertexCount)-45)/2;
    int old_blocksize = nrowblocks;

    printf("old blocksize = %d\n",old_blocksize);
    int new_blocksize = nrowblocks;

    //printf("vertexcount = %d edgeCount\n", );

    printf("new_blocksize = %d\n", new_blocksize);

    int newVertexCount = new_blocksize*45+new_blocksize*new_blocksize+27;
    int newEdgeCount = 84*new_blocksize+25+new_blocksize*new_blocksize*4;
    char **newVertexName;
    printf("new vertexcount = %d newEdgeCount = %d\n",newVertexCount,newEdgeCount);

    int* prev_vertex = ((int*)malloc((newVertexCount+1)*sizeof(int)));
    double* newVWeight = (double*)malloc((newVertexCount+1)*sizeof(double));


    newVertexName = (char**)malloc((newVertexCount+1)*sizeof(char*));

    
    for(i = 0;i< (newVertexCount);++i){
            newVertexName[i] = (char*)malloc(200*sizeof(char));
        }


    int *newEdge_u = ((int*)malloc((newEdgeCount+1)*sizeof(int)));
    int *newEdge_v = ((int*)malloc((newEdgeCount+1)*sizeof(int)));
    double *newEdge_weight = ((double*)malloc((newEdgeCount+1)*sizeof(double)));

    printf("all allocation done\n");

    int updatedVertexCount, updatedEdgeCount;


// getting the vmap from dgraph
    int **v_map;
    v_map = (int**)calloc((vertexCount+1),sizeof(int*));

    
    for(i = 0;i< (vertexCount);++i){
            v_map[i] = (int*)calloc((block_divisor*block_divisor+1),sizeof(int));
    }


    create_smallgraph_datastructure_sparse(edge_u, edge_v, edge_weight, edgeCount, vertexCount,vertexName,vertexWeight,block_divisor,&newVertexName,newVertexCount, newEdgeCount,
                                    &newEdge_u,&newEdge_v,&newEdge_weight, &prev_vertex, &newVWeight, nnzblock_matrix,nrowblocks,ncolblocks,&updatedVertexCount,&updatedEdgeCount,&v_map);
    //create_smallgraph_datastructure(edge_u, edge_v, edge_weight, edgeCount, vertexCount,vertexName,vertexWeight,block_divisor,&newVertexName,newVertexCount, newEdgeCount,
    //                                &newEdge_u,&newEdge_v,&newEdge_weight, &prev_vertex, &newVWeight, nnzblock_matrix,nrowblocks,ncolblocks);
    printf("returned %s\n",newVertexName[0]);
    printf("rmlgp te asche abar\n");
    printf("updatedVertexCount = %d updatedEdgeCount = %d\n",updatedVertexCount,updatedEdgeCount);

    for(i = 0 ; i < updatedVertexCount ; i++){
        //printf("prev[%s] = %s\n",newVertexName[i], G.vertices[prev_vertex[i]]);
    }

 /*   for(i = 0 ; i < vertexCount ; i++){
        
        if(vertexName[i][0] == 'S' && vertexName[i][1] == 'P' && vertexName[i][2] == 'M' && vertexName[i][3] == 'M' && vertexName[i][4] == ','){
            j = 0 ;
            while(j< block_divisor*block_divisor){
                if(v_map[i][j] > 0)

                //printf("prev[%s] = new[%s]\n", G.vertices[i],newVertexName[v_map[i][j]]);
                j++;
            }
        }
        else{
            j = 0 ;
        while(v_map[i][j] != -1){
                 

                //printf("prev[%s] = new[%s]\n", G.vertices[i],newVertexName[v_map[i][j]]);
                j++;
            }
        }
            
        
    }*/
/*    FILE* refinement_without_topo_order = fopen("refinement_without_topo_order.txt","w");
    int m;
    for(m = 1 ; m <= vertexCount ; m++){
        i = my_partition_topsort[m]-1;
        //printf("%s\n",vertexName[i]);
        if(!strcmp(vertexName[i],"_lambda")){
            fprintf(refinement_without_topo_order, "%s %d %d\n",  newVertexName[v_map[i][j]],rcoars->coars->part[i+1],updatedVertexCount - m);
        }
        
        else if(vertexName[i][0] == 'S' && vertexName[i][1] == 'P' && vertexName[i][2] == 'M' && vertexName[i][3] == 'M' && vertexName[i][4] == ','){
            j = 0 ;
            while(j< block_divisor*block_divisor){
                if(v_map[i][j] > 0)
                {
                //printf("prev[%s] = new[%s]\n", G.vertices[i],newVertexName[v_map[i][j]]);
                fprintf(refinement_without_topo_order, "%s %d %d\n",  newVertexName[v_map[i][j]],rcoars->coars->part[i+1],updatedVertexCount - m);
            }
                j++;
            }
        }
        else{
            j = 0 ;
        while(v_map[i][j] > 0){
                 

                //printf("prev[%s] = new[%s]\n", G.vertices[i],newVertexName[v_map[i][j]]);
                fprintf(refinement_without_topo_order, "%s %d %d\n",  newVertexName[v_map[i][j]],rcoars->coars->part[i+1], updatedVertexCount - m);
                j++;
            }
        }
            
        
    }
    fclose(refinement_without_topo_order);*/

    //////////rfinement without topo order , later use

    // FILE* refinement_without_topo_order = fopen("refinement_without_topo_order.txt","w");
    // int m;
    // for(m = 1 ; m <= vertexCount ; m++){
    //     //printf("inside ref loop %s\n",vertexName[my_partition_topsort[m]-1]);
    //     i = my_partition_topsort[m]-1;
    //     //printf("%s\n",vertexName[i]);
    //     if(!strcmp(vertexName[i],"_lambda")){
    //         fprintf(refinement_without_topo_order, "%s %d %d\n",  newVertexName[v_map[i][0]],rcoars->coars->part[i+1],updatedVertexCount - m);
    //     }

    //     else if(vertexName[i][0] == 'S' && vertexName[i][1] == 'P' && vertexName[i][2] == 'M' && vertexName[i][3] == 'M' && vertexName[i][4] == ','){
    //         j = 0 ;
    //         while(j< block_divisor*block_divisor){
    //             if(v_map[i][j] > 0)
    //             {
    //             //printf("prev[%s] = new[%s]\n", G.vertices[i],newVertexName[v_map[i][j]]);
    //             fprintf(refinement_without_topo_order, "%s %d %d\n",  newVertexName[v_map[i][j]],rcoars->coars->part[i+1],updatedVertexCount - m);
    //         }
    //             j++;
    //         }
    //     }
    //     else{
    //         j = 0 ;
    //     while(v_map[i][j] > 0){


    //             //printf("prev[%s] = new[%s]\n", G.vertices[i],newVertexName[v_map[i][j]]);
    //             fprintf(refinement_without_topo_order, "%s %d %d\n",  newVertexName[v_map[i][j]],rcoars->coars->part[i+1], updatedVertexCount - m);
    //             j++;
    //         }
    //     }
    //      //printf("inside ref loop last %s\n", vertexName[i]);   

    // }
    // fclose(refinement_without_topo_order);

    // printf("\n\nrefinement without topoorder done \n\n");





    dgraph small_G;

    my_generate_smallgraph(&small_G, file_name,opt.use_binary_input, newEdge_u, newEdge_v, newEdge_weight, updatedEdgeCount, updatedVertexCount,newVertexName,newVWeight,block_divisor);

    coarsen* small_C;

    small_C = initializeCoarsen(&small_G);
    rcoarsen* small_rcoarse;
    small_rcoarse = initializeRCoarsen(small_C);

    FILE *refined_graph_partition = fopen("refined_graph_partition.txt","w");

    for(i = 1 ; i <= small_rcoarse->coars->graph->nVrtx ; i++){
        small_rcoarse->coars->part[i] = rcoars->coars->part[prev_vertex[i-1]+1];
        //fprintf(refined_graph_partition,"old = %s(%d) new = %s(%d)  \n",G.vertices[prev_vertex[i-1]],rcoars->coars->part[prev_vertex[i-1]], small_rcoarse->coars->graph->vertices[i-1],small_rcoarse->coars->part[i]);
    }
    //fclose(refined_graph_partition);

    for(i = 0 ; i < newEdgeCount ; i++){
        //printf("edge %d %s(%d) --> %s(%d)\n",i,newVertexName[newEdge_u[i]],newEdge_u[i],newVertexName[newEdge_v[i]],newEdge_v[i]);
    }



    FILE* small_file = fopen("small_file.txt","w");
    fprintf(small_file, "%d\n", distinct_part_count);


    print_info_part(small_rcoarse->coars->graph, small_rcoarse->coars->part, opt);






    idxType* my_small_partition_topsort = (idxType*)malloc((small_rcoarse->coars->graph->nVrtx+1)*sizeof(idxType));
    //DFStopsort_with_part(&G,rcoars->coars->part,opt.nbPart,my_partition_topsort);

    for (i = 1 ; i <= small_G.nVrtx ; i++){
        //printf("%s %d\n",small_G.vertices[i-1],small_rcoarse->coars->part[i]);
    }
    DFStopsort_with_part(&small_G,small_rcoarse->coars->part,partcount,my_small_partition_topsort);


   /* for(i=1, j = small_rcoarse->coars->graph->nVrtx;i<=small_rcoarse->coars->graph->nVrtx;i++,j--){
        //fprintf(refined_graph_partition,"%s %d\n",newVertexName[my_small_partition_topsort[i]-1],small_rcoarse->coars->part[my_small_partition_topsort[i]]);
        fprintf(refined_graph_partition,"%s %d %d\n",newVertexName[my_small_partition_topsort[i]-1],small_rcoarse->coars->part[my_small_partition_topsort[i]], j);
        //printf("partition top sort[%d] = %d part = %d nodename = %s\n",i,my_partition_topsort[i],rcoars->coars->part[my_partition_topsort[i]],rcoars->coars->graph->vertices[my_partition_topsort[i]-1]);
        if(newVertexName[my_small_partition_topsort[i]-1][0] == 'S' && newVertexName[my_small_partition_topsort[i]-1][1] == 'P' && newVertexName[my_small_partition_topsort[i]-1][2] == 'M' &&
            newVertexName[my_small_partition_topsort[i]-1][3] == 'M' && newVertexName[my_small_partition_topsort[i]-1][4]==',')
           //fprintf(file,"topsort[%d] = %d part = %d noden = %s\n",i,my_partition_topsort[i],rcoars->coars->part[my_partition_topsort[i]],vertexName[my_partition_topsort[i]-1]);
            fprintf(small_file,"%s %d\n",newVertexName[my_small_partition_topsort[i]-1],small_rcoarse->coars->part[my_small_partition_topsort[i]]);
        //fprintf(file,"%d %d\n",my_partition_topsort[i],rcoars->coars->part[my_partition_topsort[i]]);
    }*/

    printf("\n\nloop type = %d large_block = %d small block %d\n", loopType,starting_block_size,small_block);
    FILE* refined_new;
    int prev_part = -1;
    char new_fname[100];
    char loopname[100];
    if(loopType == 0 ) strcpy(loopname,"nonloop");
    else if(loopType == 1 )strcpy(loopname,"firstloop");
    else if(loopType == 2 )strcpy(loopname,"secondloop");
    //refined_new = fopen("refined_new.txt","w");
    FILE* part_topsort = fopen("part_topsort.txt","w");
    char partinfo_file_name[200];

    sprintf(partinfo_file_name,"%s_%dk_%dk_part%d_%s_partinfo.txt",matrix_name,starting_block_size/1024,wblk/1024,opt.nbPart,loopname);

    FILE* partInfo = fopen(partinfo_file_name,"w");
    fprintf(partInfo, "%d\n", distinct_part_count+1);

    for(i = 1, j = small_rcoarse->coars->graph->nVrtx ; i <= small_rcoarse->coars->graph->nVrtx ; i++, j--)
    {
        
        //non-priority
        //fprintf(refined_graph_partition,"%s %d\n",newVertexName[my_small_partition_topsort[i]-1],small_rcoarse->coars->part[my_small_partition_topsort[i]]);

        //priority
        if(small_rcoarse->coars->part[my_small_partition_topsort[i]] != prev_part){

            prev_part = small_rcoarse->coars->part[my_small_partition_topsort[i]];
            fprintf(partInfo,"%d\n", i-1);
            fprintf(part_topsort,"%d\n",prev_part);
            //sprintf(new_fname,"refined_new_graphs/refined_new_%d.txt",prev_part);
            //fclose(refined_new);
            //refined_new = fopen(new_fname,"w");

        }
        fprintf(refined_graph_partition,"%s %d %d\n",newVertexName[my_small_partition_topsort[i]-1],small_rcoarse->coars->part[my_small_partition_topsort[i]], j);
        // //fprintf(refined_new,"%s %d %d\n",newVertexName[my_small_partition_topsort[i]-1],small_rcoarse->coars->part[my_small_partition_topsort[i]], j);

        
        // //printf("partition top sort[%d] = %d part = %d nodename = %s\n",i,my_partition_topsort[i],rcoars->coars->part[my_partition_topsort[i]],rcoars->coars->graph->vertices[my_partition_topsort[i]-1]);
        // if(newVertexName[my_small_partition_topsort[i]-1][0] == 'S' && newVertexName[my_small_partition_topsort[i]-1][1] == 'P' && newVertexName[my_small_partition_topsort[i]-1][2] == 'M' &&
        //     newVertexName[my_small_partition_topsort[i]-1][3] == 'M' && newVertexName[my_small_partition_topsort[i]-1][4]==',')
        //    //fprintf(file,"topsort[%d] = %d part = %d noden = %s\n",i,my_partition_topsort[i],rcoars->coars->part[my_partition_topsort[i]],vertexName[my_partition_topsort[i]-1]);
        //     fprintf(small_file,"%s %d\n",newVertexName[my_small_partition_topsort[i]-1],small_rcoarse->coars->part[my_small_partition_topsort[i]]);
        // //fprintf(file,"%d %d\n",my_partition_topsort[i],rcoars->coars->part[my_partition_topsort[i]]);
    }
    fprintf(partInfo, "%d\n", small_rcoarse->coars->graph->nVrtx);
    //fclose(refined_new);
    fclose(part_topsort);
    fclose(partInfo);




    fclose(refined_graph_partition);

    make_new_table_file(small_rcoarse, my_small_partition_topsort,loopname,matrix_name,starting_block_size,wblk,opt.nbPart);





        //////memory calculation for each partition/////////////////
    ecType* small_memory_from_out = (ecType*)calloc(partcount,sizeof(ecType));
    ecType* small_memory_from_in = (ecType*)calloc(partcount,sizeof(ecType));

    for(i=0;i<partcount;++i){
        small_memory_from_in[i] = 0;
        small_memory_from_out[i] = 0;
    }


    // for(i=1;i<=small_rcoarse->coars->graph->nVrtx;i++){


    //             //printf("%s %d\n",vertexName[my_partition_topsort[i]-1],rcoars->coars->part[my_partition_topsort[i]]);
    //             //fprintf(same_part, "%s\n", vertexName[my_partition_topsort[i]-1]);
    //             small_memory_from_in[small_rcoarse->coars->part[i]] += small_rcoarse->coars->graph->vWeight[i]; //at this moment 1
    //             for(j=small_rcoarse->coars->graph->inStart[i];j<=small_rcoarse->coars->graph->inEnd[i];++j)
    //             {
    //                 //printf("vertex = %s(%d) j = %s(%d) part = %d\n",vertexName[my_partition_topsort[i]-1],my_partition_topsort[i]-1,vertexName[rcoars->coars->graph->in[j]-1],j,rcoars->coars->part[rcoars->coars->graph->in[j]-1]);
    //                 //incoming_edge_weight_in_part += igraph->ecIn[j];
    //                 if(small_rcoarse->coars->part[small_rcoarse->coars->graph->in[j]] != small_rcoarse->coars->part[i])
    //                 {   
    //                     //fprintf(different_part, "%s %d\n", vertexName[rcoars->coars->graph->in[j]-1],rcoars->coars->part[rcoars->coars->graph->in[j]-1]);
    //                     small_memory_from_out[small_rcoarse->coars->part[i]] += small_rcoarse->coars->graph->ecIn[j];
    //                 }
    //             }
            


    //     }
    FILE* small_mem_out = fopen("small_mem_out.txt","w");
    FILE* small_mem_in = fopen("small_mem_in.txt","w");

    // for(i = 0 ; i < partcount ; i++){
    //     fprintf(small_mem_out, "%d %lf\n",i,small_memory_from_out[i] );
    //     fprintf(small_mem_in, "%d %lf\n",i,small_memory_from_in[i] );

    // }

    /*FILE* small_vname = fopen("small_vname.txt","w");
    for(i = 0 ; i < small_G.nVrtx ; i++){
        fprintf(small_vname,"%s\n",small_G.vertices[i]);
    }
    fclose(small_vname);
*/






    fclose(file);
    fclose(different_part);
    fclose(same_part);
    fclose(mem_out);
    fclose(mem_in);
    fclose(small_mem_out);
    fclose(small_mem_in);
    fclose(graph_lookup_100);
    fclose(graph_lookup_99);
    fclose(small_file);

    free(save_ub);
    free(edgecut);
    free(nbcomm);
    free(latencies);
    freeDGraphData(&G);
}


void get_task_name(const char* node_name, char* task){
    printf("get_task_name func\n");

    //task = strtok(node_name,",");


}


void make_new_table_file(rcoarsen* rcoarse, int *my_partition_topsort, char* loopname, char* matrix_name, int large_blk, int small_blk, int partCount){

    int i,j;




    char* task = (char*) malloc(150*sizeof(char));
    char* strparam = (char*) malloc(150*sizeof(char));

    int opcode;
    int numparamsCount;
    int strparamsCount;
    int task_id;
    int partition_no;
    int priority;

//    FILE* vertexName_file ;
//    vertexName_file = fopen("vertexName.txt","w");


    FILE* vertexName_file ;
    char vertex_filename[150];

    sprintf(vertex_filename,"%s_%dk_%dk_part%d_%s.txt",matrix_name,large_blk/1024,small_blk/1024,partCount,loopname);

    vertexName_file = fopen(vertex_filename,"w");







    char taskinfo_file_name[200];

    sprintf(taskinfo_file_name,"%s_%dk_%dk_part%d_%s_taskinfo.txt",matrix_name,large_blk/1024,small_blk/1024,partCount,loopname);

    FILE* task_table = fopen(taskinfo_file_name,"w");



    fprintf(task_table,"%d\n",rcoarse->coars->graph->nVrtx);
    for(i = 1, j = rcoarse->coars->graph->nVrtx ; i <= rcoarse->coars->graph->nVrtx ; i++, j--)
    {
        
        //non-priority
        fprintf(vertexName_file,"%s 0\n",rcoarse->coars->graph->vertices[my_partition_topsort[i]-1]);

        //ptttttt
        //get_task_name(rcoarse->coars->graph->vertices[my_partition_topsort[i]-1],task);
        task = strtok(rcoarse->coars->graph->vertices[my_partition_topsort[i]-1],",");
        
        if(!strcmp(task,"RESET")){
            opcode = 1;
            numparamsCount = 0 ; 
            strparamsCount = 1;
            task_id = -1;
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            strparam = strtok(NULL,",");

            fprintf(task_table,"%d,%d,%d,%s,%d,%d,%d\n",opcode,numparamsCount,strparamsCount,strparam,task_id,partition_no,priority);

        }
        else if(!strcmp(task,"SPMM")){
            opcode = 2;
            numparamsCount = 3 ;
            strparam = strtok(NULL,"/");//ttttsav the whole row,col,buf of SPMM in this string and just print that 
            strparamsCount = 0;
            task_id = -1;
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            

            fprintf(task_table,"%d,%d,%s,%d,%d,%d,%d\n",opcode,numparamsCount,strparam,strparamsCount,task_id,partition_no,priority);

        }
        else if(!strcmp(task,"XTY")){
            opcode = 3;
            numparamsCount = 2 ;
            int rowblk = atoi(strtok(NULL,","));
            int buffid = atoi(strtok(NULL,","));

            strparamsCount = 0;
            task_id = atoi(strtok(NULL,","));
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            

            fprintf(task_table,"%d,%d,%d,%d,%d,%d,%d,%d\n",opcode,numparamsCount,rowblk,buffid, strparamsCount,task_id,partition_no,priority);

        }

        else if(!strcmp(task,"RED")){
            opcode = 4;
            numparamsCount = 1 ;
            strparam = strtok(NULL,",");
            int rowblk = atoi(strtok(NULL,","));


            strparamsCount = 1;
            task_id = -1;
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            

            fprintf(task_table,"%d,%d,%d,%d,%s,%d,%d,%d\n",opcode,numparamsCount,rowblk, strparamsCount,strparam, task_id,partition_no,priority);

        }
        else if(!strcmp(task,"XY")){
            opcode = 5;
            numparamsCount = 1 ;
            int rowblk = atoi(strtok(NULL,","));
            

            strparamsCount = 0;
            task_id = atoi(strtok(NULL,","));
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            

            fprintf(task_table,"%d,%d,%d,%d,%d,%d,%d\n",opcode,numparamsCount,rowblk, strparamsCount,task_id,partition_no,priority);

        }
        else if(!strcmp(task,"ADD")){
            opcode = 6;
            numparamsCount = 1 ;
            int rowblk = atoi(strtok(NULL,","));
            

            strparamsCount = 0;
            task_id = atoi(strtok(NULL,","));
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            

            fprintf(task_table,"%d,%d,%d,%d,%d,%d,%d\n",opcode,numparamsCount,rowblk, strparamsCount,task_id,partition_no,priority);

        }
        else if(!strcmp(task,"DLACPY")){
            opcode = 7;
            numparamsCount = 1 ;
            int rowblk = atoi(strtok(NULL,","));
            

            strparamsCount = 0;
            task_id = atoi(strtok(NULL,","));
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            

            fprintf(task_table,"%d,%d,%d,%d,%d,%d,%d\n",opcode,numparamsCount,rowblk, strparamsCount,task_id,partition_no,priority);

        }

        else if(!strcmp(task,"UPDATE")){
            opcode = 8;
            numparamsCount = 1 ;
            int rowblk = atoi(strtok(NULL,","));
            

            strparamsCount = 0;
            task_id = atoi(strtok(NULL,","));
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            

            fprintf(task_table,"%d,%d,%d,%d,%d,%d,%d\n",opcode,numparamsCount,rowblk, strparamsCount,task_id,partition_no,priority);

        }
        else if(!strcmp(task,"SUB")){
            opcode = 9;
            numparamsCount = 1 ;
            int rowblk = atoi(strtok(NULL,","));
            

            strparamsCount = 0;
            task_id = atoi(strtok(NULL,","));
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            

            fprintf(task_table,"%d,%d,%d,%d,%d,%d,%d\n",opcode,numparamsCount,rowblk, strparamsCount,task_id,partition_no,priority);

        }

        else if(!strcmp(task,"MULT")){
            opcode = 10;
            numparamsCount = 1 ;
            int rowblk = atoi(strtok(NULL,","));
            

            strparamsCount = 0;
            task_id = -1;
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            

            fprintf(task_table,"%d,%d,%d,%d,%d,%d,%d\n",opcode,numparamsCount,rowblk, strparamsCount,task_id,partition_no,priority);

        }

        else if(!strcmp(task,"COL")){
            opcode = 11;
            numparamsCount = 2 ;
            int rowblk = atoi(strtok(NULL,","));
            int buffid = atoi(strtok(NULL,","));

            strparamsCount = 0;
            task_id = -1;
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            

            fprintf(task_table,"%d,%d,%d,%d,%d,%d,%d,%d\n",opcode,numparamsCount,rowblk,buffid, strparamsCount,task_id,partition_no,priority);

        }

        else if(!strcmp(task,"RNRED")){
            opcode = 12;
            numparamsCount = 0 ; 
            strparamsCount = 1;
            task_id = -1;
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            strparam = strtok(NULL,",");

            fprintf(task_table,"%d,%d,%d,%s,%d,%d,%d\n",opcode,numparamsCount,strparamsCount,strparam,task_id,partition_no,priority);

        }

        else if(!strcmp(task,"SQRT")){
            opcode = 13;
            numparamsCount = 0 ; 
            strparamsCount = 1;
            task_id = -1;
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            strparam = strtok(NULL,",");

            fprintf(task_table,"%d,%d,%d,%s,%d,%d,%d\n",opcode,numparamsCount,strparamsCount,strparam,task_id,partition_no,priority);

        }

        //no 14

        else if(!strcmp(task,"GET")){
            opcode = 14;
            numparamsCount = 1 ;
            int rowblk = atoi(strtok(NULL,","));
            

            strparamsCount = 0;
            task_id = atoi(strtok(NULL,","));
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            

            fprintf(task_table,"%d,%d,%d,%d,%d,%d,%d\n",opcode,numparamsCount,rowblk, strparamsCount,task_id,partition_no,priority);

        }

        else if(!strcmp(task,"TRANS")){
            opcode = 15;
            numparamsCount = 0 ; 
            strparamsCount = 1;
            task_id = -1;
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            strparam = strtok(NULL,",");

            fprintf(task_table,"%d,%d,%d,%s,%d,%d,%d\n",opcode,numparamsCount,strparamsCount,strparam,task_id,partition_no,priority);

        }

        else if(!strcmp(task,"SPEUPDATE")){
            opcode = 16;
            numparamsCount = 0 ; 
            strparamsCount = 1;
            task_id = -1;
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            strparam = strtok(NULL,",");

            fprintf(task_table,"%d,%d,%d,%s,%d,%d,%d\n",opcode,numparamsCount,strparamsCount,strparam,task_id,partition_no,priority);

        }

        else if(!strcmp(task,"CHOL")){
            opcode = 17;
            numparamsCount = 0 ; 
            strparamsCount = 1;
            task_id = -1;
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            strparam = strtok(NULL,",");

            fprintf(task_table,"%d,%d,%d,%s,%d,%d,%d\n",opcode,numparamsCount,strparamsCount,strparam,task_id,partition_no,priority);

        }

        else if(!strcmp(task,"INV")){
            opcode = 18;
            numparamsCount = 0 ; 
            strparamsCount = 1;
            task_id = -1;
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            strparam = strtok(NULL,",");

            fprintf(task_table,"%d,%d,%d,%s,%d,%d,%d\n",opcode,numparamsCount,strparamsCount,strparam,task_id,partition_no,priority);

        }

        // no 19

        else if(!strcmp(task,"SETZERO")){
            opcode = 19;
            numparamsCount = 1 ;
            int rowblk = atoi(strtok(NULL,","));
            

            strparamsCount = 0;
            task_id = atoi(strtok(NULL,","));
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            

            fprintf(task_table,"%d,%d,%d,%d,%d,%d,%d\n",opcode,numparamsCount,rowblk, strparamsCount,task_id,partition_no,priority);

        }



        else if(!strcmp(task,"CONV")){
            opcode = 20;
            numparamsCount = 0 ; 
            strparamsCount = 1;
            task_id = -1;
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            strparam = strtok(NULL,",");

            fprintf(task_table,"%d,%d,%d,%s,%d,%d,%d\n",opcode,numparamsCount,strparamsCount,strparam,task_id,partition_no,priority);

        }
        else if(task[0] == '_'){
            opcode = 22;
            numparamsCount = 0 ; 
            strparamsCount = 1;
            task_id = -1;
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            strparam = task;

            fprintf(task_table,"%d,%d,%d,%s,%d,%d,%d\n",opcode,numparamsCount,strparamsCount,strparam,task_id,partition_no,priority);


        }



        else if(strtok(NULL,",")==NULL){
            opcode = 21;
            numparamsCount = 0 ; 
            strparamsCount = 1;
            task_id = -1;
            partition_no = rcoarse->coars->part[my_partition_topsort[i]];
            priority = j;
            strparam = task;

            fprintf(task_table,"%d,%d,%d,%s,%d,%d,%d\n",opcode,numparamsCount,strparamsCount,strparam,task_id,partition_no,priority);


        }
















    }

    fclose(task_table);
    fclose(vertexName_file);


}

/*int main(int argc, char *argv[])
{
    MLGP_option opt;

    processArgs_rMLGP(argc, argv, &opt);

    run_rMLGP(opt.file_name, opt);

    free_opt(&opt);
    return 0;
}*/
