// mtx to bin converter
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <sys/stat.h>

#include <getopt.h>


#include "dgraph.h"
#include "bmmio.h"
#include "utils.h"
#include "debug.h"
#include "vcycle.h"
#include "rvcycle.h"
#include "partDGraphAlgos.h"
#include "matching.h"
#include "infoPart.h"

/*********************************************************************************/
void printUsage_rMLGP(char *exeName)
{
    printf("Usage: %s <input-file-name>\n", exeName);

}


int processArgs_Conv(int argc, char **argv, MLGP_option* opt)
{
    int i;
    if (argc < 2){
		printUsage_rMLGP(argv[0]);
		u_errexit("%s: There is a problem with the arguments.\n", argv[0]);
    }

    strncpy(opt->file_name, argv[1], PATH_MAX);    
    opt->seed = 0;
    opt->co_stop = 0;
    opt->co_stop_size = 50*opt->nbPart;
    opt->co_stop_step = 10;
    opt->co_match = 0;
    opt->co_match_norder = 1;
    opt->co_match_eorder = 1;
    opt->co_match_onedegreefirst = 1;
    opt->co_inipart = 0;
    opt->co_nbinipart = 5;
    opt->co_refinement = 6;
    
    opt->ub = (double*)malloc((opt->nbPart)*sizeof(double));
    opt->lb = (double*)malloc((opt->nbPart)*sizeof(double));

    for (i=0;i<opt->nbPart;i++){
        opt->ub[i] = -1;
        opt->lb[i] = -1;
    }
    opt->ref_step = 10;
    opt->co_obj = 0;
    opt->ratio = 1.03;
    opt->runs = 1;
    opt->debug = 0;
    opt->print = 0;
    opt->use_binary_input = 1;
    
    for(i=1;i<opt->nbPart;i++){
        opt->ub[i]=opt->ub[0];
        opt->lb[i]=opt->lb[0];
    }
    return 0;
}


void run_Conv(char* file_name, MLGP_option opt)
{
    dgraph G;

    if (strstr(file_name,".dot")){
        generateDGraphFromDot(&G, file_name,opt.use_binary_input);
    }
    else if(strstr(file_name,".mtx")){   
        generateDGraphFromMtx(&G,file_name,opt.use_binary_input);
    }

    freeDGraphData(&G);
}

int main(int argc, char *argv[])
{
    MLGP_option opt;

    processArgs_Conv(argc, argv, &opt);

    run_Conv(opt.file_name, opt);

    free_opt(&opt);
    return 0;
}
