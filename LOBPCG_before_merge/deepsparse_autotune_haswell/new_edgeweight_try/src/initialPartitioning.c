#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <assert.h>

#include "metis.h"
#include "debug.h"
#include "dgraph.h"
#include "utils.h"
#include "initialBissection.h"
#include "initialPartitioning.h"
#include "partDGraphAlgos.h"
#include "refinement.h"

void iniPartKerDFS(MLGP_option opt, MLGP_info* info, coarsen*  coars, ecType *edgecutmin, int nbpass){
    idxType* toporder;
    idxType* partcopy;
    toporder = (idxType*) malloc(sizeof(idxType)*(coars->graph->nVrtx + 2));
    partcopy = (idxType*) malloc(sizeof(idxType)*(coars->graph->nVrtx + 1));
    double* bound = (double*) malloc(sizeof(double) * opt.nbPart);
    double* lb_pw = (double*) malloc(sizeof(double) * opt.nbPart);

    printf("\ncoarse graph vertex count = %d\n\n",coars->graph->nVrtx);

    int nbpartmin=-1;
    double totbound;
    int allSameUBflag = 1;
    ecType edgecut = ecType_MAX, locedgecut = ecType_MAX;

    int pass,i,j;
    
    printf("total vw = %lf\n",coars->graph->totvw);
    coars->graph->totvw = 1.0*(coars->graph->nVrtx);

    for (pass=1; pass<=nbpass; pass++){

        randDFStopsort(coars->graph, toporder);
        coars->nbpart = 0;
        totbound = 0;
        for (i=0; i < opt.nbPart; i++) {
            bound[i] = opt.ub[i];
 //           totbound += (int) bound[i];
            totbound += bound[i];
            printf("\ninit DFS totbound = %lf bound[%d] = %lf\n\n",totbound,i,bound[i]);
        }
        printf("totvw = %lf totbound = %lf\n",coars->graph->totvw,totbound);
        if(totbound < coars->graph->totvw) {
            totbound =0;
            for (i=0; i < opt.nbPart; i++) {
                bound[i] += 1;
                totbound += (int) bound[i];
            }
        }

        if(totbound < coars->graph->totvw)
            u_errexit("iniPartKerDFS: Big progblem, contact developers.\n");
        
        for (i=0; i < opt.nbPart; i++) {
            lb_pw[i] = ((2.0 - opt.ratio) / (opt.ratio)) * bound[i];
        }
        while (coars->nbpart != opt.nbPart){
            allSameUBflag=1;
            for(j=1;j<opt.nbPart;++j)
                if(fabs(bound[j]-bound[0])>0.0000001)
                    allSameUBflag=0;
            if (allSameUBflag){
                coars->nbpart = kernighanSequentialPart(coars->graph, toporder, lb_pw[0], bound[0],partcopy, &edgecut, opt);
            }
            if (coars->nbpart != opt.nbPart){
                
                coars->nbpart = kernighanSequentialExactPart(coars->graph, toporder, opt.nbPart, bound, partcopy, &edgecut, opt);
            }

            printf("initpartkerdfs part returned = %d\n",coars->nbpart);

            if ((coars->nbpart != 1)&&(coars->nbpart < opt.nbPart)){
                edgecut = ecType_MAX;
                break;
            }
            for (i=0; i < opt.nbPart; i++) {
                bound[i] = bound[i] * 1.1;
                lb_pw[i] = lb_pw[i] * 0.9;
            }
        }
        if (edgecut < locedgecut)
            locedgecut = edgecut;
        info->ec_inipart_tab[CO_INIPART_KERDFS] = locedgecut;
        printf("edgecut = %lf edgecutmin = %lf\n",edgecut,*edgecutmin);
        if (edgecut < *edgecutmin){
            *edgecutmin = edgecut;
            info->best_inipart = CO_INIPART_KERDFS;
            info->iniec = edgecut;
            nbpartmin = coars->nbpart;
            for (j = 0; j <=coars->graph->nVrtx; j++)
                {
            		coars->part[j] = partcopy[j];
       //         	printf("coarse>part[%d] = %d\n", j, coars->part[j]);
                }
        }
    }
    if(nbpartmin!=-1)
        coars->nbpart = nbpartmin;
    else{
        if(opt.debug)
            printf("iniPartKerDFS: something wrong!\n");/*BU2JH: is this error or warning*/
    }
    free(toporder);
    free(partcopy);

}
void iniPartKerBFS(MLGP_option opt, MLGP_info* info, coarsen*  coars, ecType *edgecutmin, int nbpass){
    idxType* toporder;
    idxType* partcopy;
    toporder = (idxType*) malloc(sizeof(idxType)*(coars->graph->nVrtx + 2));
    partcopy = (idxType*) malloc(sizeof(idxType)*(coars->graph->nVrtx + 1));
    double* bound = (double*) malloc(sizeof(double) * opt.nbPart);
    double* lb_pw = (double*) malloc(sizeof(double) * opt.nbPart);
    int nbpartmin=-1, totbound;
    int allSameUBflag = 1;
    ecType edgecut = ecType_MAX, locedgecut = ecType_MAX;
    int pass,i,j;
    for (pass=1; pass<=nbpass; pass++){
        randBFStopsort(coars->graph, toporder);
        coars->nbpart = 0;
        totbound = 0;
        for (i=0; i < opt.nbPart; i++) {
            bound[i] = opt.ub[i];
            totbound += (int) bound[i];
        }
        if(totbound < coars->graph->totvw) {
            totbound =0;
            for (i=0; i < opt.nbPart; i++) {
                bound[i] += 1;
                totbound += (int) bound[i];
            }
        }
        if(totbound < coars->graph->totvw)
            printf("BIG PROBLEM\n");/*BU2JH: is this error or warning*/
        for (i=0; i < opt.nbPart; i++) {
            lb_pw[i] = ((2.0 - opt.ratio) / (opt.ratio)) * bound[i];
        }
        //if (lb_pw >=bound)
            //printf("BIG PROBLEM\n");
        while (coars->nbpart != opt.nbPart){
            allSameUBflag=1;
            for(j=1;j<opt.nbPart;++j)
                if(fabs(bound[j]-bound[0])>0.0000001)
                    allSameUBflag=0;
            if (allSameUBflag){
                coars->nbpart = kernighanSequentialPart(coars->graph, toporder, lb_pw[0], bound[0],partcopy, &edgecut, opt);
            }
            if (coars->nbpart != opt.nbPart)
                coars->nbpart = kernighanSequentialExactPart(coars->graph, toporder, opt.nbPart, bound, partcopy, &edgecut, opt);
            if ((coars->nbpart != 1)&&(coars->nbpart < opt.nbPart)){
                edgecut = ecType_MAX;
                break;
            }
            for (i=0; i < opt.nbPart; i++) {
                bound[i] = bound[i] * 1.1;
                lb_pw[i] = lb_pw[i] * 0.9;
            }
        }
        if (edgecut < locedgecut)
            locedgecut = edgecut;
        info->ec_inipart_tab[CO_INIPART_KERBFS] = locedgecut;
        if (edgecut < *edgecutmin){
            *edgecutmin = edgecut;
            info->best_inipart = CO_INIPART_KERBFS;
            info->iniec = edgecut;
            nbpartmin = coars->nbpart;
            for (j = 0; j <=coars->graph->nVrtx; j++)
                coars->part[j] = partcopy[j];
        }
    }
    if(nbpartmin!=-1)
        coars->nbpart = nbpartmin;
    else{
        if(opt.debug)
            printf("iniPartKerBFS: something wrong!\n");/*BU2JH: is this error or warning*/
    }
    free(bound);
    free(lb_pw);
    free(toporder);
    free(partcopy);
}

void iniPartKerMix(MLGP_option opt, MLGP_info* info, coarsen*  coars, ecType *edgecutmin, int nbpass){
    idxType* toporder;
    idxType* partcopy;
    toporder = (idxType*) malloc(sizeof(idxType)*(coars->graph->nVrtx + 2));
    partcopy = (idxType*) malloc(sizeof(idxType)*(coars->graph->nVrtx + 1));
    double* bound = (double*) malloc(sizeof(double) * opt.nbPart);
    double* lb_pw = (double*) malloc(sizeof(double) * opt.nbPart);
    int nbpartmin=-1, totbound;
    int allSameUBflag = 1;
    ecType edgecut = ecType_MAX, locedgecut = ecType_MAX;
    int pass,i,j;
    for (pass=1; pass<=nbpass; pass++){
        switch(pass){
            case 1: mixtopsort(coars->graph, toporder,50,0); break;
            case 2: mixtopsort(coars->graph, toporder,25,0); break;
            case 3: mixtopsort(coars->graph, toporder,75,0); break;
            case 4: mixtopsort(coars->graph, toporder,25,1); break;
            case 5: mixtopsort(coars->graph, toporder,75,1); break;
            default: mixtopsort(coars->graph, toporder,50,0); break;
        }
        coars->nbpart = 0;
        totbound = 0;
        for (i=0; i < opt.nbPart; i++) {
            bound[i] = opt.ub[i];
            totbound += (int) bound[i];
        }
        if(totbound < coars->graph->totvw) {
            totbound =0;
            for (i=0; i < opt.nbPart; i++) {
                bound[i] += 1;
                totbound += (int) bound[i];
            }
        }
        if(totbound < coars->graph->totvw) printf("BIG PROBLEM\n");
        for (i=0; i < opt.nbPart; i++) {
            lb_pw[i] = ((2.0 - opt.ratio) / (opt.ratio)) * bound[i];
        }
        while (coars->nbpart != opt.nbPart){
            allSameUBflag=1;
            for(j=1;j<opt.nbPart;++j)
                if(fabs(bound[j]-bound[0])>0.0000001)
                    allSameUBflag=0;
            if (allSameUBflag){
                coars->nbpart = kernighanSequentialPart(coars->graph, toporder, lb_pw[0], bound[0],partcopy, &edgecut, opt);
            }
            if (coars->nbpart != opt.nbPart)
                coars->nbpart = kernighanSequentialExactPart(coars->graph, toporder, opt.nbPart, bound, partcopy, &edgecut, opt);
            if ((coars->nbpart != 1)&&(coars->nbpart < opt.nbPart)){
                edgecut = ecType_MAX;
                break;
            }
            for (i=0; i < opt.nbPart; i++) {
                bound[i] = bound[i] * 1.1;
                lb_pw[i] = lb_pw[i] * 0.9;
            }
        }
        if (edgecut < locedgecut)
            locedgecut = edgecut;
        info->ec_inipart_tab[CO_INIPART_KERDFSBFS] = locedgecut;
        if (edgecut < *edgecutmin){
            *edgecutmin = edgecut;
            info->best_inipart = CO_INIPART_KERDFSBFS;
            info->iniec = edgecut;
            nbpartmin = coars->nbpart;
            for (j = 0; j <=coars->graph->nVrtx; j++)
                coars->part[j] = partcopy[j];
        }
    }
    if(nbpartmin!=-1)
        coars->nbpart = nbpartmin;
    else{
        if(opt.debug)
            printf("iniPartKerMix: something wrong!\n");/*BU2JH: is this error or warning*/

    }
    free(bound);
    free(lb_pw);
    free(toporder);
    free(partcopy);
        
}

void iniPartGGG(MLGP_option opt, MLGP_info* info, coarsen*  coars, ecType *edgecutmin,int nbpass)
{
    ecType edgecut, locedgecut = ecType_MAX;
    idxType* partcopy;
    coars->nbpart = opt.nbPart;
    partcopy = (idxType*) malloc(sizeof(idxType)*(coars->graph->nVrtx + 1));
    int i,j;
    for (i=1; i<=nbpass; i++){
        greedyGraphGrowing(coars->graph, opt.ub, partcopy, opt.nbPart, opt);
        edgecut = edgeCut(coars->graph, partcopy);
        if (edgecut < locedgecut)
            locedgecut = edgecut;
        info->ec_inipart_tab[CO_INIPART_GREEDY] = locedgecut;
        if (edgecut < *edgecutmin){
            *edgecutmin = edgecut;
            info->best_inipart = CO_INIPART_GREEDY;
            info->iniec = edgecut;
            for (j = 0; j <=coars->graph->nVrtx; j++)
                coars->part[j] = partcopy[j];
        }
    }
    free(partcopy);

}

void iniPartForcedBalance(MLGP_option opt, MLGP_info* info, coarsen*  coars, ecType *edgecutmin,int nbpass)
{
    ecType edgecut, locedgecut = ecType_MAX;
    coars->nbpart = opt.nbPart;
    idxType* partcopy;
    partcopy = (idxType*) malloc(sizeof(idxType)*(coars->graph->nVrtx + 1));
    int i,j;
    for (i=1; i<=nbpass; i++){
        initialPartitioningForcedBalance(coars->graph, opt.ub, partcopy, opt.nbPart, opt);
        edgecut = edgeCut(coars->graph, partcopy);
        if (edgecut < locedgecut)
            locedgecut = edgecut;
        info->ec_inipart_tab[CO_INIPART_FORCED] = locedgecut;
        if (edgecut < *edgecutmin){
            *edgecutmin = edgecut;
            info->best_inipart = CO_INIPART_FORCED;
            info->iniec = edgecut;
            for (j = 0; j <=coars->graph->nVrtx; j++)
                coars->part[j] = partcopy[j];
        }
    }
    free(partcopy);

}
void iniPartBFSGrowing(MLGP_option opt, MLGP_info* info, coarsen*  coars, ecType *edgecutmin,int nbpass){

    ecType edgecut, locedgecut = ecType_MAX;
    idxType* partcopy;
    coars->nbpart = opt.nbPart;
    idxType* toporder = (idxType*) malloc(sizeof(idxType)*coars->nbpart);
    partcopy = (idxType*) malloc(sizeof(idxType)*(coars->graph->nVrtx + 1));
    int i,j;
    for (i=1; i<=nbpass; i++){
        BFSGrowing(coars->graph, opt.ub, partcopy, opt.nbPart, opt, info);
        randTopSortOnParts(coars->graph, partcopy, toporder, opt.nbPart);
        idxType* sizes = forcedBalance(coars->graph, toporder, opt.ub, partcopy, opt.nbPart, opt, info);
        free(sizes);
        edgecut = edgeCut(coars->graph, partcopy);
        //printf("initPerBFSGrowing edgeCut = %lf\n",edgecut);
        if (edgecut < locedgecut)
            locedgecut = edgecut;
        info->ec_inipart_tab[CO_INIPART_BFS] = locedgecut;
        if (edgecut < *edgecutmin){
            *edgecutmin = edgecut;
            info->best_inipart = CO_INIPART_BFS;
            info->iniec = edgecut;
            for (j = 0; j <=coars->graph->nVrtx; j++)
                coars->part[j] = partcopy[j];
        }
    }
    free(toporder);
    free(partcopy);
}

void iniPartCocktailShake(MLGP_option opt, MLGP_info* info, coarsen*  coars, ecType *edgecutmin,int nbpass){
    ecType edgecut, locedgecut = ecType_MAX;
    coars->nbpart = opt.nbPart;
    idxType* partcopy;
    partcopy = (idxType*) malloc(sizeof(idxType)*(coars->graph->nVrtx + 1));
    idxType* toporder = (idxType*) malloc(sizeof(idxType)*coars->nbpart);
    CocktailShake(coars->graph, opt.ub, partcopy, opt.nbPart, opt);
    randTopSortOnParts(coars->graph, partcopy, toporder, opt.nbPart);
    idxType* sizes = forcedBalance(coars->graph, toporder, opt.ub, partcopy, opt.nbPart, opt, info);
    free(sizes);
    edgecut = edgeCut(coars->graph, partcopy);
    int j;
    if (edgecut < locedgecut)
        locedgecut = edgecut;
    info->ec_inipart_tab[CO_INIPART_CS] = locedgecut;
    if (edgecut < *edgecutmin){
        *edgecutmin = edgecut;
        info->best_inipart = CO_INIPART_CS;
        info->iniec = edgecut;
        for (j = 0; j <=coars->graph->nVrtx; j++)
            coars->part[j] = partcopy[j];
    }
    free(partcopy);
    free(toporder);
}

void iniPartDiaGreedy(MLGP_option opt, MLGP_info* info, coarsen*  coars, ecType *edgecutmin,int nbpass)
{
    ecType edgecut;
    idxType* partcopy = (idxType*) malloc(sizeof(idxType)*(coars->graph->nVrtx + 1));

    coars->nbpart = opt.nbPart;
    peripheralRootGreedyGraphGrowing(coars->graph, opt.ub, partcopy, opt.nbPart, opt);

    edgecut = edgeCut(coars->graph, partcopy);
    info->ec_inipart_tab[CO_INIPART_DIA_GREEDY] = edgecut;
    if (edgecut < *edgecutmin) {
        *edgecutmin = edgecut;
        info->best_inipart = CO_INIPART_DIA_GREEDY;
        info->iniec = edgecut;
        for (int j = 0; j <=coars->graph->nVrtx; j++)
            coars->part[j] = partcopy[j];
    }
    free(partcopy);
}

void initialPartitioning(MLGP_option opt, coarsen*  coars, MLGP_info* info)
{
    idxType* partcopy;
    double* bound = (double*) malloc(sizeof(double) * opt.nbPart);
    double* lb_pw = (double*) malloc(sizeof(double) * opt.nbPart);
    ecType edgecutmin = ecType_MAX;
    //opt.co_inipart = CO_INIPART_KERDFS; //////anik temporary kernighan test
    //opt.co_inipart = CO_INIPART_METIS;
    //opt.co_inipart = CO_INIPART_GREEDY;
    //opt.co_inipart = CO_INIPART_DIA_GREEDY;
    //opt.co_inipart = CO_INIPART_CS;
    opt.co_inipart = CO_INIPART_BFS;
    //opt.co_inipart = CO_INIPART_KERBFS;
    //opt.co_inipart = CO_INIPART_KERDFSBFS;
    //opt.co_inipart = CO_INIPART_FORCED;


    //printf("initialPartitioning opt.nbPart = %d\n",opt.nbPart);
    switch (opt.co_inipart) {
        case CO_INIPART_ALL :
        {
        	printf("\n\ninit partition all algo\n\n");
            /* Kernighan DFS */
            info->timing_inipart_tab[CO_INIPART_KERDFS] -= u_wseconds();
            iniPartKerDFS(opt, info, coars, &edgecutmin, 1);
            printf("\nAfter initpartKerDFS partition :\n\n");
            print_info_part(coars->graph,coars->part,opt);

            info->timing_inipart_tab[CO_INIPART_KERDFS] += u_wseconds();
            // printf("dfs done\n");

            /* Kernighan BFS */
            info->timing_inipart_tab[CO_INIPART_KERBFS] -= u_wseconds();
            iniPartKerBFS(opt, info, coars, &edgecutmin, 1);
            printf("\nAfter initPerKerBFS partition :\n\n");
            print_info_part(coars->graph,coars->part,opt);
            info->timing_inipart_tab[CO_INIPART_KERBFS] += u_wseconds();
            // printf("bfs done\n");

            /* Kernighan MIX */
            info->timing_inipart_tab[CO_INIPART_KERDFSBFS] -= u_wseconds();
            iniPartKerMix(opt, info, coars, &edgecutmin, 1);
            printf("\nAfter initPerKerMix partition :\n\n");
            print_info_part(coars->graph,coars->part,opt);
            info->timing_inipart_tab[CO_INIPART_KERDFSBFS] += u_wseconds();
            // printf("mix done\n");

            /* Forced balanced */
            info->timing_inipart_tab[CO_INIPART_FORCED] -= u_wseconds();
            iniPartForcedBalance(opt, info, coars, &edgecutmin, 1);
            printf("\nAfter initPerForcedBalance partition :\n\n");
            print_info_part(coars->graph,coars->part,opt);
            info->timing_inipart_tab[CO_INIPART_FORCED] += u_wseconds();
            // printf("fb done\n");

            /* Greedy Graph Growing */
            if (opt.nbPart == 2) {
                info->timing_inipart_tab[CO_INIPART_GREEDY] -= u_wseconds();
                iniPartGGG(opt, info, coars, &edgecutmin, 1);
                printf("\nAfter initPerGGG partition :\n\n");
                print_info_part(coars->graph,coars->part,opt);
                info->timing_inipart_tab[CO_INIPART_GREEDY] += u_wseconds();
                // printf("ggg done\n");
            }

            /* BFS Growing */
           /* if (opt.nbPart == 2) {
                info->timing_inipart_tab[CO_INIPART_BFS] -= u_wseconds();
                iniPartBFSGrowing(opt, info, coars, &edgecutmin, 1);
                printf("\nAfter initPerBFSGrowing partition :\n\n");
                print_info_part(coars->graph,coars->part,opt);
                info->timing_inipart_tab[CO_INIPART_BFS] += u_wseconds();
                // printf("bfsg done\n");
            }*/

            /* Metis */
            if (opt.nbPart == 2) {
                info->timing_inipart_tab[CO_INIPART_METIS] -= u_wseconds();
                partcopy = (idxType*) malloc(sizeof(idxType)*(coars->graph->nVrtx + 1));
                if(opt.debug > 3)
                    printf("metis start\n");
                metisAcyclicBissection(&edgecutmin, info, coars, partcopy, opt);
                if (opt.debug > 0)
                    printf("Metis cost %d\n", edgecutmin);
                free(partcopy);
                printf("\nAfter Metis partition :\n\n");
                print_info_part(coars->graph,coars->part,opt);
                info->timing_inipart_tab[CO_INIPART_METIS] += u_wseconds();
            }
            // printf("metis done\n");
            /* Inipart DIA Greedy */
            /*
            for (i=1; i<=opt.co_nbinipart; i++) {
                peripheralRootGreedyGraphGrowing(coars->graph, opt.ub, partcopy, opt.nbPart, opt);
                edgecut = edgeCut(coars->graph, partcopy);
                if (opt.debug > 0)
                    printf("DIA Greedy cost %.0f\n", edgecut);
                if (edgecut < edgecutmin) {
                    edgecutmin = edgecut;
                    for (j = 0; j <= coars->graph->nVrtx; j++)
                        coars->part[j] = partcopy[j];
                }
            }*/
            /* CocktailShake */
            /*
                for (i=1; i<=opt.co_nbinipart; i++) {
                    CocktailShake(coars->graph, opt.ub, partcopy, opt.nbPart, opt);
                    edgecut = edgeCut(coars->graph, partcopy);
                    if (opt.debug > 0)
                        printf("Cocktail Check cost %.0f\n", edgecut);
                    if (edgecut < edgecutmin) {
                        edgecutmin = edgecut;
                        for (j = 0; j <= coars->graph->nVrtx; j++)
                            coars->part[j] = partcopy[j];
                    }
                }
            */
            break;
        }

        case CO_INIPART_KERDFS :
        {
            info->timing_inipart_tab[CO_INIPART_KERDFS] -= u_wseconds();
            //iniPartKerDFS(opt,info,coars,&edgecutmin, opt.co_nbinipart);
            iniPartKerDFS(opt,info,coars,&edgecutmin, 1);
            info->timing_inipart_tab[CO_INIPART_KERDFS] += u_wseconds();
            break;
        }

        case CO_INIPART_KERBFS :
        {
            info->timing_inipart_tab[CO_INIPART_KERBFS] -= u_wseconds();
            iniPartKerBFS(opt,info,coars,&edgecutmin, opt.co_nbinipart);
            info->timing_inipart_tab[CO_INIPART_KERBFS] += u_wseconds();
            break;
        }    

        case CO_INIPART_KERDFSBFS :
        {
            info->timing_inipart_tab[CO_INIPART_KERDFSBFS] -= u_wseconds();
            iniPartKerMix(opt,info,coars,&edgecutmin, opt.co_nbinipart);
            info->timing_inipart_tab[CO_INIPART_KERDFSBFS] += u_wseconds();
            break;
        }

        case CO_INIPART_GREEDY:
        {
            assert(opt.nbPart == 2);
            info->timing_inipart_tab[CO_INIPART_GREEDY] -= u_wseconds();
            iniPartGGG(opt,info,coars, &edgecutmin, opt.co_nbinipart);
            info->timing_inipart_tab[CO_INIPART_GREEDY] += u_wseconds();
            break;
        }    

        case CO_INIPART_FORCED :
        {
            info->timing_inipart_tab[CO_INIPART_FORCED] -= u_wseconds();
            iniPartForcedBalance(opt,info,coars, &edgecutmin, opt.co_nbinipart);
            info->timing_inipart_tab[CO_INIPART_FORCED] += u_wseconds();
            break;
        }

        case CO_INIPART_METIS :
        {
            info->timing_inipart_tab[CO_INIPART_METIS] -= u_wseconds();
            coars->nbpart = opt.nbPart;
            metisAcyclicBissection(&edgecutmin, info, coars, coars->part, opt);
            info->timing_inipart_tab[CO_INIPART_METIS] += u_wseconds();
            break;
        }

        case CO_INIPART_BFS:
        {
            assert(opt.nbPart == 2);
            info->timing_inipart_tab[CO_INIPART_BFS] -= u_wseconds();
            iniPartBFSGrowing(opt,info,coars, &edgecutmin, opt.co_nbinipart);
            info->timing_inipart_tab[CO_INIPART_BFS] += u_wseconds();
            break;
        }

        case CO_INIPART_CS:
        {
            assert(opt.nbPart == 2);
            info->timing_inipart_tab[CO_INIPART_CS] -= u_wseconds();
            iniPartCocktailShake(opt,info,coars, &edgecutmin, opt.co_nbinipart);
            info->timing_inipart_tab[CO_INIPART_CS] += u_wseconds();
            break;
        }

        case CO_INIPART_DIA_GREEDY: //NOTE: Don't use for testing
        {
            assert(opt.nbPart == 2);
            info->timing_inipart_tab[CO_INIPART_DIA_GREEDY] -= u_wseconds();
            iniPartDiaGreedy(opt,info,coars,&edgecutmin, opt.co_nbinipart);
            info->timing_inipart_tab[CO_INIPART_DIA_GREEDY] += u_wseconds();
            break;
        }
    }
    info->current_edgecut = edgecutmin;
    free(bound);
    free(lb_pw);
}
