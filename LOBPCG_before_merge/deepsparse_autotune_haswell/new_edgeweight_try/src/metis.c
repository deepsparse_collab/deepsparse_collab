#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <metis.h>

#include <getopt.h>


#include "dgraph.h"
#include "bmmio.h"
#include "utils.h"
#include "debug.h"
#include "partDGraphAlgos.h"
#include "infoPart.h"
#include "option.h"

/*
char *infilename;
int K=2;
float eps = 0.03;
*/

/*********************************************************************************/
void printUsage_metis(char *exeName)
{
    printf("Usage: %s <input-file-name> <#parts> [options...]\n", exeName);
    printf("Option:\n");

    //seed
    printf("\t--seed int \t(default = 0)\n");
    printf("\t\tSeed for the random generator\n");

    //runs
    printf("\t--runs int \t(default = 1)\n");
    printf("\t\tNumber of runs\n");

    //ratio
    printf("\t--ration float \t(default = 1.03)\n");
    printf("\t\tImbalance ration between parts\n");

    //co_stop
    printf("\t--co_stop int \t(default = 0)\n");
    printf("\t\tSet the stoping criteria pour the coarsening step\n");
    printf("\t\t= 0 : Stop when the size of graph is < co_stop_size\n");
    printf("\t\t= 1 : Stop after co_stop_step steps\n");

    //co_stop_size
    printf("\t--co_stop_size int \t(default = 50 * #parts)\n");
    printf("\t\tSet co_stop_size when co_stop = 0\n");
    
    //co_stop_step
    printf("\t--co_stop_step int \t(default = 10)\n");
    printf("\t\tSet co_stop_step when co_stop = 1\n");

    //co_match
    printf("\t--co_match int \t(default = 0)\n");
    printf("\t\tSet the matching technic for the coarsening step\n");
    printf("\t\t= 0 : Match as long as there is no u_i -> v_j edges\n");

    //co_init
    printf("\t--co_init int \t(default = 0)\n");
    printf("\t\tDetermine the initial partitioning\n");
    printf("\t\t= 0 : kernighan\n");

    //co_refinement
    printf("\t--co_refinement int \t(default = 1)\n");
    printf("\t\tDetermine the refinement in uncoarsening step\n");
    printf("\t\t= 0 : no refinement\n");
    printf("\t\t= 1 : toward the largest partition in topological order\n");

    //ref_step
    printf("\t--ref_step int \t(default = 4)\n");
    printf("\t\tNumber of steps in the refinement\n");

    //part_ub
    printf("\t--part_ub double \t(default = (tot_weight/nbpart) * 1.03)\n");
    printf("\t\tUpper bound for the partition\n");

    //part_lb
    printf("\t--part_lb double \t(default = (tot_weight/nbpart) / 1.03)\n");
    printf("\t\tLower bound for the partition\n");

    //out_file
    printf("\t--out_file filename \t(default = graph_algo.out)\n");
    printf("\t\tOutput filename\n");
}


int processArgs_metis(int argc, char **argv, MLGP_option* opt)
{
    int i;
    if (argc < 3){
	printUsage_metis(argv[0]);
	u_errexit("%s: There is a problem with the arguments.\n", argv[0]);
    }

    strncpy(opt->file_name, argv[1], PATH_MAX);
    strncpy(opt->out_file, opt->file_name, PATH_MAX);
    strcat(opt->out_file, "_metis.out");

    opt->nbPart = atoi(argv[2]);
    opt->seed = 0;
    opt->co_stop = 0;
    opt->co_stop_size = 50*opt->nbPart;
    opt->co_stop_step = 10;
    opt->co_match = 0;
    opt->co_inipart = 0;
    opt->co_refinement = 2;

    opt->ub = (double*)malloc((opt->nbPart)*sizeof(double));
    opt->lb = (double*)malloc((opt->nbPart)*sizeof(double));

    for (i=0;i<opt->nbPart;i++){
        opt->ub[i] = -1;
        opt->lb[i] = -1;
    }
    opt->ref_step = 10;
    opt->ratio = 1.03;
    opt->runs = 1;

    static struct option long_options[] =
    {
        {"seed",  required_argument, 0, 's'},
	{"co_stop",  required_argument, 0, 't'},
	{"co_stop_size",  required_argument, 0, 'z'},
	{"co_stop_step",  required_argument, 0, 'p'},
	{"co_match",  required_argument, 0, 'm'},
	{"co_inipart",  required_argument, 0, 'i'},
	{"co_refinement",  required_argument, 0, 'r'},
	{"ref_step",  required_argument, 0, 'e'},
	{"part_ub",  required_argument, 0, 'u'},
	{"part_lb",    required_argument, 0, 'l'},
	{"ratio",    required_argument, 0, 'a'},
	{"runs",    required_argument, 0, 'n'},
        {"debug",    required_argument, 0, 'd'},
        {"out_file",    required_argument, 0, 'o'},
    };

    const char *delims = "s:t:z:p:m:i:r:u:l:a";
    char o;
    int option_index = 0;

    while ((o = getopt_long(argc, argv, delims, long_options, &option_index)) != -1) {
        switch (o) {
            case 's':
                opt->seed = atoi(optarg);
                break;
            case 't':
                opt->co_stop = atoi(optarg);
                break;
            case 'z':
                opt->co_stop_size = atoi(optarg);
                break;
            case 'p':
                opt->co_stop_step = atoi(optarg);
                break;
            case 'm':
                opt->co_match = atoi(optarg);
                break;
            case 'i':
                opt->co_inipart = atoi(optarg);
                break;
            case 'r':
                opt->co_refinement = atoi(optarg);
                break;
            case 'e':
                opt->ref_step = atoi(optarg);
                break;
            case 'u':
                opt->ub[0] = atof(optarg);
                break;
            case 'l':
                opt->lb[0] = atof(optarg);
                break;
            case 'd':
                opt->debug = atoi(optarg);
                break;
            case 'a':
                opt->ratio = atof(optarg);
                break;
            case 'n':
                opt->runs = atoi(optarg);
                break;
	    case 'o':
	        strncpy(opt->out_file, optarg, PATH_MAX);
		break;
            default:
                printUsage_metis(argv[0]);
		u_errexit("Option %s not recognized.", o);
                return 1;
        }
    }
    for(i=1;i<opt->nbPart;i++){
        opt->ub[i]=opt->ub[0];
        opt->lb[i]=opt->lb[0];
    }
    return 0;
}

void dgraph_to_metis(dgraph* G, idx_t* in, idx_t* adj, idx_t* vw, idx_t* adjw)
{
    idxType i,j,k=0;

    for (i=1; i<=G->nVrtx; i++) {
        in[i-1] = k;
        vw[i-1] = G->vw[i];
        for (j = G->inStart[i]; j <= G->inEnd[i]; j++) {
            adj[k] = G->in[j]-1;
            adjw[k++] = G->ecIn[j];
        }
        for (j = G->outStart[i]; j <= G->outEnd[i]; j++) {
            adj[k] = G->out[j]-1;
            adjw[k++] = G->ecOut[j];
        }
    }
    in[G->nVrtx] = k;    
}

void run_metis(char* file_name, MLGP_option opt)
{
    srand((unsigned) opt.seed);
    
    dgraph G;
    printf("Read nat\n");

    generateDGraphFromDot(&G, file_name,1);

    idx_t nVertices = G.nVrtx;
    idx_t nEdges = G.nEdge;
    idx_t nWeights = 1;
    idx_t nParts = opt.nbPart;

    ecType* edgecut = (ecType*) malloc(sizeof(ecType) * opt.runs);
    ecType* nbcomm = (ecType*) malloc(sizeof(ecType) * opt.runs);
    double* latencies = (double*) malloc(sizeof(double) * opt.runs);
    int r,i;

    printf("Nb node: %d\nNb Edges: %d\nNb part: %d\nLower Bound: %f\nUper Bound: %f\n\n", G.nVrtx, G.nEdge, opt.nbPart, 0.0, 1.03*((float) G.nVrtx / (float) nParts));

    FILE * pFile = fopen(opt.out_file, "w");
    char *out_filep = opt.file_name;
    out_filep[strlen(out_filep)-4]='\0';

    idx_t objval;
    idx_t* xadj = (idx_t*) malloc(sizeof(idx_t) * (nVertices+1));
    idx_t* vw = (idx_t*) malloc(sizeof(idx_t) * (nVertices+1));
    idx_t* adjncy = (idx_t*) malloc(sizeof(idx_t) * (2*nEdges));
    idx_t* adjw = (idx_t*) malloc(sizeof(idx_t) * (2*nEdges));
    idx_t* part = (idx_t*) malloc(sizeof(idx_t) * nVertices);
    idxType* part_dgraph = (idxType*) malloc(sizeof(idxType) * (nVertices+1));

    dgraph_to_metis(&G, xadj, adjncy, vw, adjw);
  
    idx_t options[METIS_NOPTIONS];
    METIS_SetDefaultOptions(options);

    for (r = 0; r<opt.runs; r++){
	printf("########################## RUN %d ########################\n", r);
	options[METIS_OPTION_SEED] = opt.seed + r;
	double t = -u_wseconds();
	int ret = METIS_PartGraphKway(&nVertices, &nWeights, xadj, adjncy, 
				      vw, NULL, adjw, &nParts, NULL,
				      NULL, options, &objval, part);
    if (ret!=METIS_OK) {
        u_errexit("Metir returned error code: %d", ret);
    }
	t += u_wseconds();

	for (i=1; i<=G.nVrtx; i++)
	    part_dgraph[i] = part[i-1];
	
	edgecut[r] = edgeCut(&G, part_dgraph);
	nbcomm[r] = nbCommunications(&G, part_dgraph);
	latencies[r] = computeLatency(&G, part_dgraph, 1.0, 11.0);
	printf("Final run %d\t%d\t%d\t%d\t%d\t%f\n", r, nVertices, nEdges, edgecut[r], nbcomm[r], latencies[r]);
	printf("Time\t%.3lf\n", t);

	fprintf(pFile, "metis\t%s\t%d\t%d\t%d\t%d\t%.3lf\t%.3lf\n", out_filep, opt.nbPart, opt.seed + r, edgecut[r], nbcomm[r], latencies[r], t);
	printf("########################## RUN %d ########################\n\n", r);
	/*
	  FILE *file;
	  char name_tmp[200];
	  sprintf(name_tmp,".MLGP.metis.%d.seed.%d", nbpart, opt.seed);
	  char res_name[200];
	  strcpy(res_name, file_name);
	  strcat(res_name, name_tmp);
	  file = fopen(res_name, "w");
	  idxType i;
	  idxType* part_dgraph = (idxType*) malloc(sizeof(idxType) * (nVertices+1));
	  
	  //printf("Final part\n");
	  for (i=1; i<=G.nVrtx; i++){
	  part_dgraph[i] = part[i-1];
	  //printf("Node %d -> %d\n", i, C->part[i]); 
	  fprintf(file, "%d\n", (int) part_dgraph[i]);
	  }
	  fclose(file);

	  int* partsize = (int*) malloc(sizeof(int) * opt.nbPart);
	  int nn = nbPart(&G, part_dgraph, partsize);
	  printf("Nbpart = %d\nPartsize: ",nn);
	  int k;
	  for (k = 0; k<nn; k++)
	  printf("%d ", partsize[k]);
	  printf("\n");
	*/
    }
    free(part);
    free(part_dgraph);
    free(xadj);
    free(adjncy);
    free(vw);
    free(adjw);
    free(edgecut);
    free(nbcomm);
    free(latencies);
    fclose (pFile);
}


int main(int argc, char *argv[])
{
    MLGP_option opt;
    printf("Process arg\n");
    processArgs_metis(argc, argv, &opt);
    printf("Processed\n");

    run_metis(opt.file_name, opt);

    return 0;
}
