#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include <getopt.h>

#include "dgraph.h"
#include "bmmio.h"
#include "utils.h"
#include "debug.h"
#include "vcycle.h"
#include "partDGraphAlgos.h"
#include "matching.h"
#include "infoPart.h"
#include "option.h"

/*
char *infilename;
int K=2;
float eps = 0.03;
*/

/*********************************************************************************/
void printUsage_kernighan(char *exeName)
{
    printf("Usage: %s <input-file-name> <#parts> [options...]\n", exeName);
    printf("Option:\n");

    //seed
    printf("\t-seed int \t(default = 0)\n");
    printf("\t\tSeed for the random generator\n");

    //runs
    printf("\t-runs int \t(default = 1)\n");
    printf("\t\tNumber of runs\n");

    //debug
    printf("\t--debug int \t(default = 0)\n");
    printf("\t\tDebug value\n");

    //part_ub
    printf("\t-part_ub double \t(default = (tot_weight/nbpart) * 1.03)\n");
    printf("\t\tUpper bound for the partition\n");

    //part_lb
    printf("\t-part_lb double \t(default = (tot_weight/nbpart) / 1.03)\n");
    printf("\t\tLower bound for the partition\n");

    //kernighan
    printf("\t--kernighan int \t(default = 1)\n");
    printf("\t\tDetermine which variant of the Kernighan algorithm we use\n");
    printf("\t\t= 0 : regular kernighan (warning the number of partition might not be correct)\n");
    printf("\t\t= 1 : regular kernighan with strict lower bound and if it fails, kernighan with exact number of partition\n");

    //out_file
    printf("\t--out_file filename \t(default = graph_algo.out)\n");
    printf("\t\tOutput filename\n");
}

int processArgs_kernighan(int argc, char **argv, MLGP_option* opt)
{
    if (argc < 3){
	printUsage_kernighan(argv[0]);
	u_errexit("%s: There is a problem with the arguments.\n", argv[0]);
    }
	
    strncpy(opt->file_name, argv[1], PATH_MAX);
    strncpy(opt->out_file, opt->file_name, PATH_MAX);
    strcat(opt->out_file, "_kernighan.out");

    opt->nbPart = atoi(argv[2]);
    opt->seed = 0;
    opt->ub = (double*)malloc((opt->nbPart)*sizeof(double));
    opt->lb = (double*)malloc((opt->nbPart)*sizeof(double));

    idxType i;
    for (i=0;i<opt->nbPart;i++){
        opt->ub[i] = -1;
        opt->lb[i] = -1;
    }
    opt->runs = 1;
    opt->debug = 0;
    opt->kernighan = 1;

    static struct option long_options[] =
    {
        {"seed",  required_argument, 0, 's'},
	{"part_ub",  required_argument, 0, 'u'},
	{"part_lb",    required_argument, 0, 'l'},
	{"runs",    required_argument, 0, 'n'},
	{"out_file",    required_argument, 0, 'o'},
	{"debug",    required_argument, 0, 'd'},
	{"kernighan",    required_argument, 0, 'k'},

    };

    const char *delims = "s:u:l:n:d:k:";
    char o;
    int option_index = 0;
    while ((o = getopt_long(argc, argv, delims, long_options, &option_index)) != -1) {
        switch (o) {
            case 's':
                opt->seed = atoi(optarg);
                break;
            case 'u':
                opt->ub[0] = atof(optarg);
                break;
            case 'l':
                opt->lb[0] = atof(optarg);
                break;
            case 'n':
                opt->runs = atoi(optarg);
                break;
            case 'd':
                opt->debug = atoi(optarg);
                break;
	    case 'k':
                opt->kernighan = atoi(optarg);
                break;
	    case 'o':
	        strncpy(opt->out_file, optarg, PATH_MAX);
		break;
            default:
                printUsage_kernighan(argv[0]);
		u_errexit("Option %s not recognized.", o);
                return 1;
        }
    }
    for(i=1;i<opt->nbPart;i++){
        opt->ub[i]=opt->ub[0];
        opt->lb[i]=opt->lb[0];
    }
    return 0;
}


void runKernighan(char* file_name, MLGP_option opt)
{
    srand((unsigned) opt.seed);
    dgraph G;
    idxType i,j;
    int r;
    ecType* edgecut = (ecType*) malloc(sizeof(ecType) * opt.runs);
    ecType* nbcomm = (ecType*) malloc(sizeof(ecType) * opt.runs);
    double* latencies = (double*) malloc(sizeof(double) * opt.runs);

    generateDGraphFromDot(&G, file_name,1);
    print_dgraph(G);

//    double ratio = 1.03;
    //double eca = ((ratio-1.)*G.nVrtx)/opt.nbPart;

    for (i=0; i<opt.nbPart; i++){
        if (opt.lb[i] < 0)
            opt.lb[i] = 1;
        //Warning: Here the upper bound is the targeted value for rvcycle
        if (opt.ub[i] < 0)
            opt.ub[i] = G.nVrtx/(opt.nbPart*1.0);

        if ((floor(opt.lb[i]) < opt.lb[i])&&(floor(opt.lb[i]) == floor(opt.ub[i]))){
            printf("WARNING: The balance can not be matched!!!\n");
            opt.lb[i] = floor(opt.lb[i]);
            opt.ub[i] = floor(opt.ub[i])+1;
        }
    }

    printf("Nb node: %d\nNb Edges: %d\nNb part: %d\nLower Bound: %f\nUper Bound: %f\n\n", G.nVrtx, G.nEdge, opt.nbPart, opt.lb[0], opt.ub[0]);

    FILE * pFile = fopen(opt.out_file, "w");
    char *out_filep = opt.file_name;
    out_filep[strlen(out_filep)-4]='\0';
    for (r=0; r<opt.runs; r++){
	printf("########################## RUN %d ########################\n", r);
	idxType *toporder, *partvec;
	toporder = (idxType*) malloc(sizeof(idxType)*(G.nVrtx+1));
	partvec = (idxType*) malloc(sizeof(idxType)*(G.nVrtx+1));

	randDFStopsort(&G, toporder);
        for (i=0; i<=G.nVrtx; i++)
            printf("topdorder[%d] = %d\n", i, toporder[i]);

	double *lb_pw = opt.lb, *ub_pw = opt.ub;

	double t = -u_wseconds();
	int nbpart_found;
    int allSameUBflag=1;
	if (opt.kernighan == 0){
	    nbpart_found = kernighanSequentialPart(&G, toporder, lb_pw[0], ub_pw[0], partvec, &edgecut[r], opt);
	}
	if (opt.kernighan == 1){
	    //lb_pw = G.nVrtx/(opt.nbPart*1.0) - (opt.nbPart - 1)*eca;
        allSameUBflag=1;
        for(j=1;j<opt.nbPart;++j)
            if(fabs(opt.ub[j]-opt.ub[0])>0.0000001)
                allSameUBflag=0;
        if (allSameUBflag){
            nbpart_found = kernighanSequentialPart(&G, toporder, opt.lb[0], opt.ub[0], partvec, &edgecut[r], opt);
        }	    //print_info_part(&G, partvec, opt);
	    if (nbpart_found != opt.nbPart)
		    nbpart_found = kernighanSequentialExactPart(&G, toporder, opt.nbPart, opt.ub, partvec, &edgecut[r], opt);
	    //printf("Found ec %f\treal ec %f\n", edgecut[r], (double) edgeCut(&G, partvec));
	}
	t += u_wseconds();

	if (nbpart_found != opt.nbPart)
	    printf("KERNIGHAN FAILED! nbpart found = %d\n",nbpart_found);
	
	edgecut[r] = edgeCut(&G, partvec);
	nbcomm[r] = nbCommunications(&G, partvec);
	latencies[r] = computeLatency(&G, partvec, 1.0, 11.0);
	printf("Final run %d\t%d\t%d\t%d\t%d\t%f\n", r, G.nVrtx, G.nEdge, (int) edgecut[r], nbcomm[r], latencies[r]);
	printf("Time\t%.3lf\n", t);
	//int valid = print_info_part(&G, partvec, opt);
	fprintf(pFile, "kernighan\t%s\t%d\t%d\t%d\t%d\t%.3lf\t%.3lf\n", opt.file_name, opt.nbPart, opt.seed, (int) edgecut[r], nbcomm[r], latencies[r], t);

	/*
      FILE *file;
      char name_tmp[200];
      sprintf(name_tmp,".kernighan.part.%d.seed.%d", opt.nbPart, opt.seed);
      char res_name[200];
      strcpy(res_name, file_name);
      strcat(res_name, name_tmp);
      file = fopen(res_name, "w");
      for (i=1; i<=G.nVrtx; i++)
      fprintf(file, "%d\n", (int) partvec[i]);
      fclose(file);
	*/
    char filename[100];
    sprintf(filename, "test/test_k.dot");
    dgraph_to_dot(&G, partvec, filename);


	free(toporder);
	free(partvec);
	opt.seed++;
	printf("######################## END RUN %d ########################\n\n", r);
    }
    fclose (pFile);
    free(edgecut);
    free(nbcomm);
    free(latencies);
    freeDGraphData(&G); 
}


int main(int argc, char *argv[])
{
    MLGP_option opt;
    processArgs_kernighan(argc, argv, &opt);

    runKernighan(opt.file_name, opt);

    return 0;
}

