#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "infoPart.h"
#include <limits.h>


void initInfoPart(MLGP_info* info){
    idxType i;

    info->timing_global = 0.0;
    info->timing_coars = 0.0;
    info->timing_inipart = 0.0;
    info->timing_uncoars = 0.0;

    //printf("initInfoPart info length = %d\n",INFO_LENGTH);

    info->timing_coars_tab = (double*) calloc(INFO_LENGTH, sizeof(double));
    info->timing_matching_tab = (double*) calloc(INFO_LENGTH, sizeof(double));
    info->timing_buildgraph_tab = (double*) calloc(INFO_LENGTH, sizeof(double));

    info->timing_inipart_tab = (double*) calloc(INFO_LENGTH, sizeof(double));
    info->ec_inipart_tab = (ecType*) calloc(INFO_LENGTH, sizeof(ecType));

    info->timing_uncoars_tab = (double*) calloc(INFO_LENGTH, sizeof(double));
    info->timing_refinement_tab = (double*) calloc(INFO_LENGTH, sizeof(double));
    info->timing_project_tab = (double*) calloc(INFO_LENGTH, sizeof(double));
    info->timing_forced_tab = (double*) calloc(INFO_LENGTH, sizeof(double));

    info->ec_afterforced_tab = (ecType*) calloc(INFO_LENGTH, sizeof(ecType));
    info->nbref_tab = (int*) calloc(INFO_LENGTH, sizeof(int));
    info->ec_afterref_tab = (ecType**) calloc(INFO_LENGTH, sizeof(ecType*));
    info->timing_refinement_tab_tab = (double**) calloc(INFO_LENGTH, sizeof(double*));
    for (i=0; i < INFO_LENGTH; i++) {
        info->ec_afterref_tab[i] = (ecType *) calloc(INFO_LENGTH, sizeof(ecType));
        info->timing_refinement_tab_tab[i] = (double *) calloc(INFO_LENGTH, sizeof(double));
    }

    //printf("initInfoPart timing_refinement_tab_tab[1000][1] = %lf\n",info->timing_refinement_tab_tab[1000][1]);

    info->nbnodes_coars_tab = (idxType*) calloc(INFO_LENGTH, sizeof(idxType));
    info->nbedges_coars_tab = (idxType*) calloc(INFO_LENGTH, sizeof(idxType));

    info->coars_depth = -1;
    info->best_inipart = -1;
    info->iniec = -1;
    info->finalec = -1;
    info->current_edgecut = 0.0;

    info->depth = 0;
}

void freeInfoPart(MLGP_info* info){
    idxType i;

    free(info->timing_coars_tab);
    free(info->timing_matching_tab);
    free(info->timing_buildgraph_tab);

    free(info->timing_inipart_tab);
    free(info->ec_inipart_tab);

    free(info->timing_uncoars_tab);
    free(info->timing_refinement_tab);
    free(info->timing_project_tab);
    free(info->timing_forced_tab);

    free(info->ec_afterforced_tab);
    free(info->nbref_tab);
    for (i=0; i < INFO_LENGTH; i++) {
        free(info->ec_afterref_tab[i]);
        free(info->timing_refinement_tab_tab[i]);
    }
    free(info->timing_refinement_tab_tab);
    free(info->ec_afterref_tab);

    free(info->nbnodes_coars_tab);
    free(info->nbedges_coars_tab);
}

void initRInfoPart(rMLGP_info* info){
    info->timing_global = 0.0;
    info->timing_coars = 0.0;
    info->timing_inipart = 0.0;
    info->timing_uncoars = 0.0;

    initInfoPart(&info->info);
    info->rec1 = NULL;
    info->rec2 = NULL;
}

void freeRInfoPart(rMLGP_info* info){
    if(info->rec1 != NULL)
        freeRInfoPart(info->rec1);
    if(info->rec2 != NULL)
        freeRInfoPart(info->rec2);
    freeInfoPart(&info->info);
}

void printInfoPart(MLGP_info* info, coarsen* C, MLGP_option opt){
    int i,j;
    FILE* infopart_file;
    //infopart_file = fopen("infopart_file.txt","w");
    dgraph* G = C->graph;
    printf("Initial Graph : %d nodes %d edges\n", (int) G->nVrtx, (int) G->nEdge);
    printf("Final edgecut : %d \n", info->finalec);
    printf("Total time\t :\t %.2f seconds\n", info->timing_global);

    printf("\nCoarsening [depth : %d]\t :\t %.2f seconds\t (%.2f %%)\n", info->coars_depth, info->timing_coars, 100*info->timing_coars/info->timing_global);

    //fprintf(infopart_file,"Initial Graph : %d nodes %d edges\n", (int) G->nVrtx, (int) G->nEdge);
    //fprintf(infopart_file,"Final edgecut : %d \n", info->finalec);
    //fprintf(infopart_file,"Total time\t :\t %.2f seconds\n", info->timing_global);

    //fprintf(infopart_file,"\nCoarsening [depth : %d]\t :\t %.2f seconds\t (%.2f %%)\n", info->coars_depth, info->timing_coars, 100*info->timing_coars/info->timing_global);

    double timing_matching = 0.0, timing_buildgraph = 0.0;
    for (i=0; i < INFO_LENGTH; i++) {
        timing_matching += info->timing_matching_tab[i];
        timing_buildgraph += info->timing_buildgraph_tab[i];
    }
    printf("\tMatching\t :\t %.2f seconds\t (%.2f %%)\n", timing_matching, 100*timing_matching/info->timing_coars);
    printf("\tBuilding Graph\t :\t %.2f seconds\t (%.2f %%)\n", timing_buildgraph, 100*timing_buildgraph/info->timing_coars);


    //fprintf(infopart_file,"\tMatching\t :\t %.2f seconds\t (%.2f %%)\n", timing_matching, 100*timing_matching/info->timing_coars);
    //fprintf(infopart_file,"\tBuilding Graph\t :\t %.2f seconds\t (%.2f %%)\n", timing_buildgraph, 100*timing_buildgraph/info->timing_coars);
    if (opt.print > 1){
        for (i=1; i <= info->coars_depth; i++){
            printf("\t\tCoarsening Step %d\t :\t %.2f seconds (%.2f %%)\t %d nodes %d edges\n", i, info->timing_coars_tab[i], 100*info->timing_coars_tab[i]/info->timing_coars, (int) info->nbnodes_coars_tab[i], (int) info->nbedges_coars_tab[i]);
            //fprintf(infopart_file,"\t\tCoarsening Step %d\t :\t %.2f seconds (%.2f %%)\t %d nodes %d edges\n", i, info->timing_coars_tab[i], 100*info->timing_coars_tab[i]/info->timing_coars, (int) info->nbnodes_coars_tab[i], (int) info->nbedges_coars_tab[i]);

        }
    }

    printf("\nInitial Partitioning\t :\t %.2f seconds\t (%.2f %%)\n", info->timing_inipart, 100*info->timing_inipart/info->timing_global);
    //fprintf(infopart_file,"\nInitial Partitioning\t :\t %.2f seconds\t (%.2f %%)\n", info->timing_inipart, 100*info->timing_inipart/info->timing_global);

    if (info->timing_inipart_tab[CO_INIPART_KERDFS] != 0.0) {
        printf("\tKernighan DFS           \t :\t %.2f seconds\t (%.2f %%)\t(EdgeCut %lf)\n", info->timing_inipart_tab[CO_INIPART_KERDFS],
               100 * info->timing_inipart_tab[CO_INIPART_KERDFS] / info->timing_inipart,  info->ec_inipart_tab[CO_INIPART_KERDFS]);
        //fprintf(infopart_file,"\tKernighan DFS           \t :\t %.2f seconds\t (%.2f %%)\t(EdgeCut %lf)\n", info->timing_inipart_tab[CO_INIPART_KERDFS],
        //       100 * info->timing_inipart_tab[CO_INIPART_KERDFS] / info->timing_inipart,  info->ec_inipart_tab[CO_INIPART_KERDFS]);

    }
    if (info->timing_inipart_tab[CO_INIPART_KERBFS] != 0.0) {
        printf("\tKernighan BFS           \t :\t %.2f seconds\t (%.2f %%)\t(EdgeCut %lf)\n", info->timing_inipart_tab[CO_INIPART_KERBFS],
               100 * info->timing_inipart_tab[CO_INIPART_KERBFS] / info->timing_inipart, (int) info->ec_inipart_tab[CO_INIPART_KERBFS]);

        //fprintf(infopart_file,"\tKernighan BFS           \t :\t %.2f seconds\t (%.2f %%)\t(EdgeCut %lf)\n", info->timing_inipart_tab[CO_INIPART_KERBFS],
        //       100 * info->timing_inipart_tab[CO_INIPART_KERBFS] / info->timing_inipart, (int) info->ec_inipart_tab[CO_INIPART_KERBFS]);
    }
    if (info->timing_inipart_tab[CO_INIPART_KERDFSBFS] != 0.0) {
        printf("\tKernighan Mix DFS-BFS   \t :\t %.2f seconds\t (%.2f %%)\t(EdgeCut %lf)\n",
               info->timing_inipart_tab[CO_INIPART_KERDFSBFS],
               100 * info->timing_inipart_tab[CO_INIPART_KERDFSBFS] / info->timing_inipart, (int) info->ec_inipart_tab[CO_INIPART_KERDFSBFS]);

        //fprintf(infopart_file,"\tKernighan Mix DFS-BFS   \t :\t %.2f seconds\t (%.2f %%)\t(EdgeCut %lf)\n",
        //       info->timing_inipart_tab[CO_INIPART_KERDFSBFS],
        //       100 * info->timing_inipart_tab[CO_INIPART_KERDFSBFS] / info->timing_inipart, (int) info->ec_inipart_tab[CO_INIPART_KERDFSBFS]);
    }
    if (info->timing_inipart_tab[CO_INIPART_GREEDY] != 0.0) {
        printf("\tGreedy Graph Growing    \t :\t %.2f seconds\t (%.2f %%)\t(EdgeCut %lf)\n", info->timing_inipart_tab[CO_INIPART_GREEDY],
               100 * info->timing_inipart_tab[CO_INIPART_GREEDY] / info->timing_inipart, (int) info->ec_inipart_tab[CO_INIPART_GREEDY]);

        //fprintf(infopart_file,"\tGreedy Graph Growing    \t :\t %.2f seconds\t (%.2f %%)\t(EdgeCut %lf)\n", info->timing_inipart_tab[CO_INIPART_GREEDY],
        //       100 * info->timing_inipart_tab[CO_INIPART_GREEDY] / info->timing_inipart, (int) info->ec_inipart_tab[CO_INIPART_GREEDY]);
    }
    if (info->timing_inipart_tab[CO_INIPART_FORCED] != 0.0) {
        printf("\tForced Balance Inipart  \t :\t %.2f seconds\t (%.2f %%)\t(EdgeCut %lf)\n", info->timing_inipart_tab[CO_INIPART_FORCED],
               100 * info->timing_inipart_tab[CO_INIPART_FORCED] / info->timing_inipart, (int) info->ec_inipart_tab[CO_INIPART_FORCED]);

        //fprintf(infopart_file,"\tForced Balance Inipart  \t :\t %.2f seconds\t (%.2f %%)\t(EdgeCut %lf)\n", info->timing_inipart_tab[CO_INIPART_FORCED],
        //       100 * info->timing_inipart_tab[CO_INIPART_FORCED] / info->timing_inipart, (int) info->ec_inipart_tab[CO_INIPART_FORCED]);
    }
    if (info->timing_inipart_tab[CO_INIPART_METIS] != 0.0) {
        printf("\tMetis + fix             \t :\t %.2f seconds\t (%.2f %%)\t(EdgeCut %lf)\n", info->timing_inipart_tab[CO_INIPART_METIS],
               100 * info->timing_inipart_tab[CO_INIPART_METIS] / info->timing_inipart, (int) info->ec_inipart_tab[CO_INIPART_METIS]);

        //fprintf(infopart_file,"\tMetis + fix             \t :\t %.2f seconds\t (%.2f %%)\t(EdgeCut %lf)\n", info->timing_inipart_tab[CO_INIPART_METIS],
        //       100 * info->timing_inipart_tab[CO_INIPART_METIS] / info->timing_inipart, (int) info->ec_inipart_tab[CO_INIPART_METIS]);
    }
    if (info->timing_inipart_tab[CO_INIPART_BFS] != 0.0) {
        printf("\tBFS Growing             \t :\t %.2f seconds\t (%.2f %%)\t(EdgeCut %lf)\n", info->timing_inipart_tab[CO_INIPART_BFS],
               100 * info->timing_inipart_tab[CO_INIPART_BFS] / info->timing_inipart,  info->ec_inipart_tab[CO_INIPART_BFS]);

        //fprintf(infopart_file,"\tBFS Growing             \t :\t %.2f seconds\t (%.2f %%)\t(EdgeCut %lf)\n", info->timing_inipart_tab[CO_INIPART_BFS],
        //       100 * info->timing_inipart_tab[CO_INIPART_BFS] / info->timing_inipart, (int) info->ec_inipart_tab[CO_INIPART_BFS]);
    }
    
    if (info->timing_inipart_tab[CO_INIPART_CS] != 0.0) {
        printf("\tCocktailShake           \t :\t %.2f seconds\t (%.2f %%)\t(EdgeCut %lf)\n", info->timing_inipart_tab[CO_INIPART_CS],
               100 * info->timing_inipart_tab[CO_INIPART_CS] / info->timing_inipart, (int) info->ec_inipart_tab[CO_INIPART_CS]);

        //fprintf(infopart_file,"\tCocktailShake           \t :\t %.2f seconds\t (%.2f %%)\t(EdgeCut %lf)\n", info->timing_inipart_tab[CO_INIPART_CS],
        //       100 * info->timing_inipart_tab[CO_INIPART_CS] / info->timing_inipart, (int) info->ec_inipart_tab[CO_INIPART_CS]);
    }
    if (info->timing_inipart_tab[CO_INIPART_DIA_GREEDY] != 0.0) {
        printf("\tDia Greedy Graph Growing\t :\t %.2f seconds\t (%.2f %%)\t(EdgeCut %lf)\n",
               info->timing_inipart_tab[CO_INIPART_DIA_GREEDY],
               100 * info->timing_inipart_tab[CO_INIPART_DIA_GREEDY] / info->timing_inipart, (int) info->ec_inipart_tab[CO_INIPART_DIA_GREEDY]);

        //fprintf(infopart_file,"\tDia Greedy Graph Growing\t :\t %.2f seconds\t (%.2f %%)\t(EdgeCut %lf)\n",
        //       info->timing_inipart_tab[CO_INIPART_DIA_GREEDY],
        //       100 * info->timing_inipart_tab[CO_INIPART_DIA_GREEDY] / info->timing_inipart, (int) info->ec_inipart_tab[CO_INIPART_DIA_GREEDY]);
    }
    switch (info->best_inipart) {
        case CO_INIPART_KERDFS :
            printf("Best Initial Partition : Kernighan DFS (edgecut %lf)\n", info->iniec);

            //fprintf(infopart_file,"Best Initial Partition : Kernighan DFS (edgecut %lf)\n", info->iniec);
            break;
        case CO_INIPART_KERBFS :
            printf("Best Initial Partition : Kernighan BFS (EdgeCut %lf)\n", info->iniec);
            //fprintf(infopart_file,"Best Initial Partition : Kernighan BFS (EdgeCut %lf)\n", info->iniec);
            break;
        case CO_INIPART_KERDFSBFS :
            printf("Best Initial Partition : Kernighan Mix DFS-BFS (EdgeCut %lf)\n", info->iniec);

            //fprintf(infopart_file,"Best Initial Partition : Kernighan Mix DFS-BFS (EdgeCut %lf)\n", info->iniec);
            break;
        case CO_INIPART_GREEDY :
            printf("Best Initial Partition : Greedy Graph Growing (EdgeCut %lf)\n", info->iniec);

            //fprintf(infopart_file,"Best Initial Partition : Greedy Graph Growing (EdgeCut %lf)\n", info->iniec);
            break;
        case CO_INIPART_FORCED :
            printf("Best Initial Partition : Forced Balance Inipart (EdgeCut %lf)\n", info->iniec);
            //fprintf(infopart_file,"Best Initial Partition : Forced Balance Inipart (EdgeCut %lf)\n", info->iniec);
            break;
        case CO_INIPART_METIS :
            printf("Best Initial Partition : Metis + fix (EdgeCut %lf)\n", info->iniec);
            //fprintf(infopart_file,"Best Initial Partition : Metis + fix (EdgeCut %lf)\n", info->iniec);
            
            break;
        case CO_INIPART_BFS :
            printf("Best Initial Partition : BFS Growing (EdgeCut %lf)\n", info->iniec);

            //fprintf(infopart_file,"Best Initial Partition : BFS Growing (EdgeCut %lf)\n", info->iniec);
            break;
        case CO_INIPART_CS :
            printf("Best Initial Partition : CocktailShake (EdgeCut %lf)\n", info->iniec);

            //fprintf(infopart_file,"Best Initial Partition : CocktailShake (EdgeCut %lf)\n", info->iniec);

            break;
        case CO_INIPART_DIA_GREEDY :
            printf("Best Initial Partition : Diameter Greedy Graph Growing (EdgeCut %lf)\n", info->iniec);

            //fprintf(infopart_file,"Best Initial Partition : Diameter Greedy Graph Growing (EdgeCut %lf)\n", info->iniec);
            break;
    }

    printf("\nUncoarsening [depth : %d]\t :\t %.2f seconds\t (%.2f %%)\n", info->coars_depth, info->timing_uncoars, 100*info->timing_uncoars/info->timing_global);
    //fprintf(infopart_file,"\nUncoarsening [depth : %d]\t :\t %.2f seconds\t (%.2f %%)\n", info->coars_depth, info->timing_uncoars, 100*info->timing_uncoars/info->timing_global);

    double timing_refinement = 0.0, timing_project = 0.0, timing_forced = 0.0;
    for (i=0; i < INFO_LENGTH; i++) {
        timing_forced += info->timing_forced_tab[i];
        timing_project += info->timing_project_tab[i];
        for (j= 1; j <= info->nbref_tab[i]; j++)
            timing_refinement += info->timing_refinement_tab_tab[i][j];
    }
    printf("\tForce Balance\t :\t %.2f seconds\t (%.2f %%)\n", timing_forced, 100*timing_forced/info->timing_uncoars);
    printf("\tRefinement\t :\t %.2f seconds\t (%.2f %%)\n", timing_refinement, 100*timing_refinement/info->timing_uncoars);
    printf("\tProject Back\t :\t %.2f seconds\t (%.2f %%)\n", timing_project, 100*timing_project/info->timing_uncoars);


    //fprintf(infopart_file,"\tForce Balance\t :\t %.2f seconds\t (%.2f %%)\n", timing_forced, 100*timing_forced/info->timing_uncoars);
    //fprintf(infopart_file,"\tRefinement\t :\t %.2f seconds\t (%.2f %%)\n", timing_refinement, 100*timing_refinement/info->timing_uncoars);
    //fprintf(infopart_file,"\tProject Back\t :\t %.2f seconds\t (%.2f %%)\n", timing_project, 100*timing_project/info->timing_uncoars);
    if (opt.print > 1) {
        for (j = info->coars_depth; j >= 0; j--) {
            printf("\t\tUncoarsening Step %d\t :\t %.2f seconds (%.2f %%)\n", j,
                   info->timing_uncoars_tab[j],
                   100 * info->timing_uncoars_tab[j] / info->timing_uncoars);
            printf("\t\t\tForce Balance pass\t :\t %.2f seconds (%.2f %%) (edgecut %lf)\n",
                   info->timing_forced_tab[j],
                   100 * info->timing_forced_tab[j] / info->timing_uncoars_tab[j],
                    info->ec_afterforced_tab[j]);

            //fprintf(infopart_file,"\t\tUncoarsening Step %d\t :\t %.2f seconds (%.2f %%)\n", j,
            //       info->timing_uncoars_tab[j],
            //       100 * info->timing_uncoars_tab[j] / info->timing_uncoars);
            //fprintf(infopart_file,"\t\t\tForce Balance pass\t :\t %.2f seconds (%.2f %%) (edgecut %lf)\n",
            //       info->timing_forced_tab[j],
            //       100 * info->timing_forced_tab[j] / info->timing_uncoars_tab[j],
            //        info->ec_afterforced_tab[j]);
            for (i = 1; i <= info->nbref_tab[j]; i++) {
                printf("\t\t\tRefinement pass %d\t :\t %.2f seconds (%.2f %%)\t (edgecut %lf)\n", i,
                       info->timing_refinement_tab_tab[j][i],
                       100 * info->timing_refinement_tab_tab[j][i] / info->timing_uncoars_tab[j],
                        info->ec_afterref_tab[j][i]);
                //fprintf(infopart_file,"\t\t\tRefinement pass %d\t :\t %.2f seconds (%.2f %%)\t (edgecut %lf)\n", i,
                //       info->timing_refinement_tab_tab[j][i],
                //       100 * info->timing_refinement_tab_tab[j][i] / info->timing_uncoars_tab[j],
                //        info->ec_afterref_tab[j][i]);
            }
        }
    }
    //fclose(infopart_file);
}

void printRInfoPart(rMLGP_info* info, rcoarsen* C, MLGP_option opt)
{
    printInfoPart(&info->info, C->coars, opt);
    /*
    if(info->rec1 != NULL)
        printRInfoPart(info->rec1, C->next_rcoarsen1, opt);
    if(info->rec2 != NULL)
        printRInfoPart(info->rec2, C->next_rcoarsen2, opt);
     */
}


int nbPart(dgraph* G, idxType* part, vwType* partsize)
{
    /*Assume partsize in already allocated
      Compute partsize[i] = number of vertex is part i
      and return the number of partition in part*/
    int nbpart = 0;
    int frmt = G->frmt;
    idxType i;
    for (i = 1; i<=G->nVrtx; i++)
	if (nbpart < part[i]){
	    //printf("%d ", i);
	    //printf("%d\n", part[i]);
	    nbpart = part[i];
	}
    nbpart++;
    
    for (i = 0; i<nbpart; i++)
	partsize[i] = 0;
    
    for (i = 1; i<=G->nVrtx; i++)
	if (frmt & DG_FRMT_VW)
	    partsize[part[i]] += G->vw[i];
	else
	    partsize[part[i]]++;    
    return nbpart;
} 

double partImbalance(vwType* partsize, int nbpart)
{
    double max = partsize[0], min_avg=partsize[0];
    int i;
    for (i=1; i<nbpart; i++){
	if (partsize[i] > max)
	    max = partsize[i];
#ifdef IMBAL_IS_MAXIMBAL
        min_avg += partsize[i]; /* it is avg */
#else
	if (partsize[i] < min)  /* check min */
	    min_avg = partsize[i];
#endif
    }
#ifdef IMBAL_IS_MAXIMBAL
    min_avg = min_avg / (double) nbpart; /* compute avg */
#endif
    return max / min_avg;
}

ecType edgeCut(dgraph* G, idxType* part)
{
	//printf("edgecut is called\n");

    int frmt = G->frmt;
    /*Return the edge cut of the partition*/
    ecType edgecut = 0;
    idxType i,j;
        ///////////print instart inend in of G1 and G2////////
/*
        for(i=0;i<G->nVrtx+2;++i){
        	printf("G1->outStart[%d] = %d\n",i,G->outStart[i]);
        }
        for(i=0;i<G->nVrtx+2;++i){
        	printf("G1->outEnd[%d] = %d\n",i,G->outEnd[i]);
        }
        for(i=0;i<G->nEdge;++i){
        	printf("G1->out[%d] = %d\n",i,G->out[i]);
        }
        for(i=0;i<G->nEdge;++i){
        	printf("G1->ecout[%d] = %d\n",i,G->ecOut[i]);
        }
        for(i=0;i<G->nEdge;++i){
        	printf("G1->ecin[%d] = %d\n",i,G->ecIn[i]);
        }
*/



    for (i = 1; i<=G->nVrtx; i++)
	for (j=G->outStart[i]; j<=G->outEnd[i]; j++) {
//	    printf("i=%d, G->out[j]=%d, part[i]=%d, part[G->out[j]]=%d\n",i,G->out[j],part[i],part[G->out[j]]);
	    if (part[G->out[j]] != part[i]) {
//		printf("\tEdgecut++ pour (%d,%d)\n", i, j);
		if (frmt & DG_FRMT_EC){
		    edgecut += G->ecOut[j];
//			edgecut += 1;
//		    printf("ecout[%d] = %d\n",j,G->ecOut[j]);
		}
		else
		    edgecut++;
	    }
	}
//    printf("Edgecut calculated = %d\n",edgecut);
    return edgecut;
}

ecType nbCommunications(dgraph* G, idxType* part)
{
    /*Return the number of communication in the partition
      that is the edge cut by counting the edges coming from
      a same node toward a partition only once*/
	printf("nbcommunication called\n");
    ecType nbcomm = 0;
    int frmt = G->frmt;
    idxType i,j,k;
    for (i = 1; i<=G->nVrtx; i++)
	for (j=G->outStart[i]; j<=G->outEnd[i]; j++) {
	    if (part[G->out[j]] != part[i]) {
		int is_new = 1;
		for (k=G->outStart[i]; k<j; k++)
		    if (part[G->out[j]] == part[G->out[k]])
			is_new = 0;
		if (is_new){
		    if (frmt & DG_FRMT_EC)
			nbcomm += G->ecOut[j];
		    else
			nbcomm++;
		}
	    }
	}
    return nbcomm;	    
}

int print_info_part_old(dgraph* G, idxType* part, MLGP_option opt)
{
    int nbpart = 0;
    idxType maxpart = part[1];
    idxType i;
    for (i = 1; i<= G->nVrtx; i++){
        maxpart = maxpart < part[i] ? part[i] : maxpart;
    }

    int* partidx = (int*) malloc((maxpart+1) * sizeof(int));
    int* partidxrev = (int*) malloc((maxpart+1) * sizeof(int));
    for (i = 0; i<= maxpart; i++){
        partidx[i]=-1;
    }
    for (i = 1; i<= G->nVrtx; i++){
        if(partidx[part[i]]==-1){
            partidx[part[i]]=nbpart;
            partidxrev[nbpart++]=part[i];
            if (opt.debug > 3)
                printf("PARTIDX UPDATED\t\t%d -> %d\n",part[i], partidx[part[i]] );
        }
    }
    idxType* partsize = (idxType*) calloc(nbpart, sizeof(idxType));
    idxType minsize = INT_MAX, maxsize = 0;

    for (i = 1; i <= G->nVrtx; i++)
        partsize[partidx[part[i]]] += G->vw[i];
    	printf("\n\n\npart[%d] = %d\npartidx[part[%d]] = %d\nvw[%d] = %d\n\n\n",i,part[i],i,partidx[part[i]],i,G->vw[i]);


    for (i = 0; i < nbpart; i++){
        minsize = minsize < partsize[i] ? minsize : partsize[i];
        maxsize = maxsize < partsize[i] ? partsize[i] : maxsize;
    }

    printf("Info:\tp:%d\tmin:%d\tmax:%d\t-->", nbpart, (int) minsize, (int) maxsize);
    if (opt.debug > 3) {
        printf("\n");
        for (i = 0; i < nbpart; ++i) {
            printf("\ti-%d\tpart size %d - upperboud %lf\n", i, partsize[partidxrev[i]], opt.ub[i]);
        }
    }
    for (i=0; i< nbpart; ++i){
        if (partsize[i]>opt.ub[partidxrev[i]]){
            printf(" bad\n");
            free(partidx);
            free(partsize);
            return -1;
        }
    }
    if(nbpart==opt.nbPart){
        printf(" good\n");
        free(partidx);
        free(partsize);
        return 0;
    }
    printf("bad\n");
    free(partidx);
    free(partidxrev);
    free(partsize);
    return -1;
}

int print_info_part(dgraph* G, idxType* part, MLGP_option opt)
{
    int nbpart = 0;

    idxType maxpart = part[1];
    idxType i;
    for (i = 1; i<= G->nVrtx; i++){
    	//printf("print info part part[%d] = %d\n",i,part);
        maxpart = maxpart < part[i] ? part[i] : maxpart;
    }
    nbpart = maxpart+1;

    idxType* partsize = (idxType*) calloc(nbpart, sizeof(idxType));
    idxType minsize = INT_MAX, maxsize = 0;

    for (i = 1; i <= G->nVrtx; i++) {
        partsize[part[i]] += G->vw[i];
 //   	printf("\n\n\npart[%d] = %d\npartsize[part[%d]] = %d\nvw[%d] = %d\n\n\n",i,part[i],i,partsize[part[i]],i,G->vw[i]);
    }

    for (i = 0; i < nbpart; i++){
        minsize = minsize < partsize[i] ? minsize : partsize[i];
        maxsize = maxsize < partsize[i] ? partsize[i] : maxsize;
    }

    //printf("Info:\tp:%d\tmin:%d\tmax:%d\t-->", nbpart, (int) minsize, (int) maxsize);
    if (opt.debug > 10) {
        printf("\n");
        printf("nbpart = %d\n",nbpart);
        for (i = 0; i < (nbpart<1000)?nbpart:1000; ++i) {
            printf("\ti-%d\tpart size %d - upperboud %lf\n", i, partsize[i], opt.ub[i]);
        }
    }
    for(i = 0 ; i< nbpart ; ++i){
    	//printf("\npartsize[%d] = %d\n",i,partsize[i]);
    }
    for (i=0; i< nbpart; ++i){

        if (partsize[i]>opt.ub[i]){
            //printf(" bad\n");
            free(partsize);
            return -1;
        }
    }
    if(nbpart==opt.nbPart){
        //printf(" good\n");
        free(partsize);
        return 0;
    }
    //printf("bad\n");
    free(partsize);
    return -1;
}


int print_info_part_file(dgraph* G, idxType* part, MLGP_option opt)
{
    int nbpart = 0;
    FILE *fp;
    int new_part = 0;
    idxType* new_partsize ;
    //fp = fopen("partsize.txt","w");
    new_partsize = (idxType*)malloc(20*sizeof(idxType));
    idxType maxpart = part[1];
    idxType i;
    for (i = 1; i<= G->nVrtx; i++){
        //printf("print info part part[%d] = %d\n",i,part);
        maxpart = maxpart < part[i] ? part[i] : maxpart;
    }
    nbpart = maxpart+1;

    idxType* partsize = (idxType*) calloc(nbpart, sizeof(idxType));
    idxType minsize = INT_MAX, maxsize = 0;

    for (i = 1; i <= G->nVrtx; i++) {
        partsize[part[i]] += G->vw[i];
 //     printf("\n\n\npart[%d] = %d\npartsize[part[%d]] = %d\nvw[%d] = %d\n\n\n",i,part[i],i,partsize[part[i]],i,G->vw[i]);
    }

    for (i = 0; i < nbpart; i++){
        minsize = minsize < partsize[i] ? minsize : partsize[i];
        maxsize = maxsize < partsize[i] ? partsize[i] : maxsize;
    }

    ////fprintf(fp,"Info:\tp:%d\tmin:%d\tmax:%d\t-->\n", nbpart, (int) minsize, (int) maxsize);
    if (opt.debug > 10) {
        printf("\n");
        printf("nbpart = %d\n",nbpart);
        for (i = 0; i < (nbpart<1000)?nbpart:1000; ++i) {
            printf("\ti-%d\tpart size %d - upperboud %lf\n", i, partsize[i], opt.ub[i]);
        }
    }

    for(i = 0 ; i< nbpart ; ++i){
        if(partsize[i] > 0){
            new_part ++;

            //fprintf(fp,"%d  %d\n",i,partsize[i]);
        }
    }
    //new_partsize = (idxType*)calloc(new_part,sizeof(idxType));


    for (i=0; i< nbpart; ++i){

        if (partsize[i]>opt.ub[i]){
            printf(" bad\n");
            free(partsize);
            return -1;
        }
    }

    if(nbpart==opt.nbPart){
        printf(" good\n");
        free(partsize);
        return 0;
    }
    //fclose(fp);
    printf("bad\n");
    free(partsize);
    return -1;
}
