/*
 *   Matrix Market I/O library for ANSI C
 *
 *   See http://math.nist.gov/MatrixMarket for details.
 *
 *
 */
/*
 ***************************************************************
 *   modifications by By Bora Ucar.                            *
 ***************************************************************
 *  symmetric case is handled in such a way that, I and J      *
 *            now contains both symmetric entries, nz is       *
 *            updated accordingly.                             *
 ***************************************************************
 */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <memory.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>
#include <fcntl.h>

#include "bmmio.h"
/*#include "dgraph.h"*/
#include "utils.h"


#define PRINT_PROGRESS




int mm_is_valid(MM_typecode matcode)
{
    if (!mm_is_matrix(matcode)) return 0;
    if (mm_is_dense(matcode) && mm_is_pattern(matcode)) return 0;
    if (mm_is_real(matcode) && mm_is_hermitian(matcode)) return 0;
    if (mm_is_pattern(matcode) && (mm_is_hermitian(matcode) ||
                                   mm_is_skew(matcode))) return 0;
    return 1;
}

int mm_read_banner(FILE *f, MM_typecode *matcode)
{
    char line[MM_MAX_LINE_LENGTH];
    char banner[MM_MAX_TOKEN_LENGTH];
    char mtx[MM_MAX_TOKEN_LENGTH];
    char crd[MM_MAX_TOKEN_LENGTH];
    char data_type[MM_MAX_TOKEN_LENGTH];
    char storage_scheme[MM_MAX_TOKEN_LENGTH];
    char *p;
    /*    int ret_code;*/
    
    
    mm_clear_typecode(matcode);
    
    if (fgets(line, MM_MAX_LINE_LENGTH, f) == NULL)
        return MM_PREMATURE_EOF;
    
    if (sscanf(line, "%s %s %s %s %s", banner, mtx, crd, data_type,
               storage_scheme) != 5)
        return MM_PREMATURE_EOF;
    
    for (p=mtx; *p!='\0'; *p=tolower(*p),p++);  /* convert to lower case */
    for (p=crd; *p!='\0'; *p=tolower(*p),p++);
    for (p=data_type; *p!='\0'; *p=tolower(*p),p++);
    for (p=storage_scheme; *p!='\0'; *p=tolower(*p),p++);
    
    /* check for banner */
    if (strncmp(banner, MatrixMarketBanner, strlen(MatrixMarketBanner)) != 0)
        return MM_NO_HEADER;
    
    /* first field should be "mtx" */
    if (strcmp(mtx, MM_MTX_STR) != 0)
        return  MM_UNSUPPORTED_TYPE;
    mm_set_matrix(matcode);
    
    
    /* second field describes whether this is a sparse matrix (in coordinate
     storgae) or a dense array */
    
    
    if (strcmp(crd, MM_SPARSE_STR) == 0)
        mm_set_sparse(matcode);
    else
        if (strcmp(crd, MM_DENSE_STR) == 0)
            mm_set_dense(matcode);
        else
            return MM_UNSUPPORTED_TYPE;
    
    
    /* third field */
    
    if (strcmp(data_type, MM_REAL_STR) == 0)
        mm_set_real(matcode);
    else
        if (strcmp(data_type, MM_COMPLEX_STR) == 0)
            mm_set_complex(matcode);
        else
            if (strcmp(data_type, MM_PATTERN_STR) == 0)
                mm_set_pattern(matcode);
            else
                if (strcmp(data_type, MM_INT_STR) == 0)
                    mm_set_integer(matcode);
                else
                    return MM_UNSUPPORTED_TYPE;
    
    
    /* fourth field */
    
    if (strcmp(storage_scheme, MM_GENERAL_STR) == 0)
        mm_set_general(matcode);
    else
        if (strcmp(storage_scheme, MM_SYMM_STR) == 0)
            mm_set_symmetric(matcode);
        else
            if (strcmp(storage_scheme, MM_HERM_STR) == 0)
                mm_set_hermitian(matcode);
            else
                if (strcmp(storage_scheme, MM_SKEW_STR) == 0)
                    mm_set_skew(matcode);
                else
                    return MM_UNSUPPORTED_TYPE;
    
    
    return 0;
}

int mm_write_mtx_crd_size(FILE *f, idxType M, idxType N, idxType nz)
{
    if(sizeof(idxType) == sizeof(int))
    {
        if (fprintf(f, "%d %d %d\n", M, N, nz) != 3)
            return MM_COULD_NOT_WRITE_FILE;
        else
            return 0;
    }
    else
    {
        if (fprintf(f, "%ld %ld %ld\n", M, N, nz) != 3)
            return MM_COULD_NOT_WRITE_FILE;
        else
            return 0;
    }
}

int mm_read_mtx_crd_size(FILE *f, idxType *M, idxType *N, idxType *nz )
{
    char line[MM_MAX_LINE_LENGTH];
    /*    int ret_code;*/
    int num_items_read;
    
    /* set return null parameter values, in case we exit with errors */
    *M = *N = *nz = 0;
    
    /* now continue scanning until you reach the end-of-comments */
    do
    {
        if (fgets(line,MM_MAX_LINE_LENGTH,f) == NULL)
            return MM_PREMATURE_EOF;
    }while (line[0] == '%');
    
    /* line[] is either blank or has M,N, nz */
    if (sizeof(idxType) == sizeof(int))
    {
        if (sscanf(line, "%d %d %d", M, N, nz) == 3)
            return 0;
        
        else
            do
            {
                num_items_read = fscanf(f, "%d %d %d", M, N, nz);
                if (num_items_read == EOF) return MM_PREMATURE_EOF;
            }
        while (num_items_read != 3);
    }
    else
    {
        if (sscanf(line, "%ld %ld %ld", M, N, nz) == 3)
            return 0;
        
        else
            do
            {
                num_items_read = fscanf(f, "%ld %ld %ld", M, N, nz);
                if (num_items_read == EOF) return MM_PREMATURE_EOF;
            }
        while (num_items_read != 3);
    }
    return 0;
}


int mm_read_mtx_array_size(FILE *f, idxType *M, idxType *N)
{
    char line[MM_MAX_LINE_LENGTH];
    /*    int ret_code;*/
    int num_items_read;
    
    /* set return null parameter values, in case we exit with errors */
    *M = *N = 0;
    
    /* now continue scanning until you reach the end-of-comments */
    do
    {
        if (fgets(line,MM_MAX_LINE_LENGTH,f) == NULL)
            return MM_PREMATURE_EOF;
    }while (line[0] == '%');
    
    /* line[] is either blank or has M,N, nz */
    if(sizeof(idxType) == sizeof(int))
    {
        if (sscanf(line, "%d %d", M, N) == 2)
            return 0;
        
        else /* we have a blank line */
            do
            {
                num_items_read = fscanf(f, "%d %d", M, N);
                if (num_items_read == EOF) return MM_PREMATURE_EOF;
            }
        while (num_items_read != 2);
    }
    else
    {
        if (sscanf(line, "%ld %ld", M, N) == 2)
            return 0;
        
        else /* we have a blank line */
            do
            {
                num_items_read = fscanf(f, "%ld %ld", M, N);
                if (num_items_read == EOF) return MM_PREMATURE_EOF;
            }
        while (num_items_read != 2);
        
    }
    return 0;
}

int mm_write_mtx_array_size(FILE *f, idxType M, idxType N)
{
    if(sizeof(idxType) == sizeof(int))
    {
        if (fprintf(f, "%d %d\n", M, N) != 2)
            return MM_COULD_NOT_WRITE_FILE;
        else
            return 0;
    }
    else
    {
        if (fprintf(f, "%ld %ld\n", M, N) != 2)
            return MM_COULD_NOT_WRITE_FILE;
        else
            return 0;
    }
}



/*-------------------------------------------------------------------------*/

/******************************************************************/
/* use when I[], J[], and val[]J, and val[] are already allocated */
/******************************************************************/

int mm_read_mtx_crd_data(FILE *f, idxType M, idxType N, idxType nz, idxType I[], idxType J[],
                         double val[], MM_typecode matcode)
{
    idxType i;
    if (mm_is_complex(matcode))
    {
        for (i=0; i<nz; i++)
        {
            if(sizeof(idxType) == sizeof(int))
            {
                if (fscanf(f, "%d %d %lg %lg", &I[i], &J[i], &val[2*i], &val[2*i+1])
                    != 4) return MM_PREMATURE_EOF;
            }
            else
            {
                if (fscanf(f, "%ld %ld %lg %lg", &I[i], &J[i], &val[2*i], &val[2*i+1])
                    != 4) return MM_PREMATURE_EOF;
            }
        }
    }
    else if (mm_is_real(matcode))
    {
        for (i=0; i<nz; i++)
        {
            if(sizeof(idxType) == sizeof(int))
            {
                if (fscanf(f, "%d %d %lg\n", &I[i], &J[i], &val[i])
                    != 3) return MM_PREMATURE_EOF;
            }
            else
            {
                if (fscanf(f, "%ld %ld %lg\n", &I[i], &J[i], &val[i])
                    != 3) return MM_PREMATURE_EOF;
            }
            
        }
    }
    
    else if (mm_is_pattern(matcode))
    {
        for (i=0; i<nz; i++)
        {
            if (sizeof(idxType) == sizeof(int)){
                if (fscanf(f, "%d %d", &I[i], &J[i])!= 2) return MM_PREMATURE_EOF;
            }
            else
                if (fscanf(f, "%ld %ld", &I[i], &J[i])!= 2) return MM_PREMATURE_EOF;
            
        }
    }
    else if (mm_is_integer(matcode))
    {
        for (i=0; i<nz; i++)
        {
            if(sizeof(idxType) == sizeof(int))
            {
                if (fscanf(f, "%d %d %lf\n", &I[i], &J[i], &val[i])
                    != 3) return MM_PREMATURE_EOF;
            }
            else
            {
                if (fscanf(f, "%ld %ld %lf\n", &I[i], &J[i], &val[i])
                    != 3) return MM_PREMATURE_EOF;
            }
        }
    }
    else
        return MM_UNSUPPORTED_TYPE;
    
    return 0;
    
}

int mm_read_mtx_crd_entry(FILE *f, idxType *I, idxType *J,
                          double *real, double *imag, MM_typecode matcode)
{
    /*    int i;*/
    if (mm_is_complex(matcode))
    {
        if(sizeof(I[0]) == sizeof(int))
        {
            if (fscanf(f, "%d %d %lg %lg", I, J, real, imag)
                != 4) return MM_PREMATURE_EOF;
        }
        else
        {
            if (fscanf(f, "%ld %ld %lg %lg", I, J, real, imag)
                != 4) return MM_PREMATURE_EOF;
        }
    }
    else if (mm_is_real(matcode))
    {
        if(sizeof(I[0]) == sizeof(int))
        {
            if (fscanf(f, "%d %d %lg\n", I, J, real)
                != 3) return MM_PREMATURE_EOF;
        }
        else
            if (fscanf(f, "%ld %ld %lg\n", I, J, real)
                != 3) return MM_PREMATURE_EOF;
    }
    
    else if (mm_is_pattern(matcode))
    {
        if(sizeof(I[0]) == sizeof(int))
        {
            if (fscanf(f, "%d %d", I, J) != 2) return MM_PREMATURE_EOF;
        }
        else
            if (fscanf(f, "%ld %ld", I, J) != 2) return MM_PREMATURE_EOF;
        
    }
    else
        return MM_UNSUPPORTED_TYPE;
    
    return 0;
    
}

int mm_num_all_nnz(char *fname)
{
    int ret_code;
    idxType M, N, nz;
    idxType add = 0;
    MM_typecode matcode;
    
    FILE *f;
    if (strcmp(fname, "stdin") == 0) f=stdin;
    else
        if ((f = fopen(fname, "r")) == NULL)
            return MM_COULD_NOT_READ_FILE;
    
    
    if ((ret_code = mm_read_banner(f, &matcode)) != 0)
        return ret_code;
    
    if (!(mm_is_valid(matcode) && mm_is_sparse(matcode) &&
          mm_is_matrix(matcode)))
        return MM_UNSUPPORTED_TYPE;
    if(mm_is_complex(matcode))
        return MM_UNSUPPORTED_TYPE;
    if(mm_is_pattern(matcode))
        return MM_UNSUPPORTED_TYPE;
    
    if ((ret_code = mm_read_mtx_crd_size(f, &M, &N, &nz)) != 0)
        return ret_code;
    
    if(mm_is_symmetric(matcode))
    {
        if (mm_is_real(matcode))
        {
            idxType i;
            for (i=0; i<nz; i++)
            {
                idxType tmpI, tmpJ;
                double tmpVal;
                if(sizeof(tmpI) == sizeof(int))
                {
                    if (fscanf(f, "%d %d %lg\n", &tmpI, &tmpJ, &tmpVal)
                        != 3) return MM_PREMATURE_EOF;
                }
                else
                    if (fscanf(f, "%ld %ld %lg\n", &tmpI, &tmpJ, &tmpVal)
                        != 3) return MM_PREMATURE_EOF;
                
                if(tmpI != tmpJ)
                    add ++;
                
            }
        }
    }
    else
    {
        u_errexit("should not call from outside for nonsymmat\n");
    }
    return nz + add;
    
}

/************************************************************************
 mm_read_mtx_crd()  fills M, N, nz, array of values, and return
 type code, e.g. 'MCRS'
 
 if matrix is complex, values[] is of size 2*nz,
 (nz pairs of real/imaginary values)
 ************************************************************************/

int mm_read_mtx_crd(char *fname, idxType *M, idxType *N, idxType *nz, idxType **I, idxType **J,
                    double **val, MM_typecode *matcode)
{
    int ret_code;
    idxType bsz;
    
    FILE *f;
    
    idxType *lI;
    idxType *lJ;
    double *lval;
    
    idxType addind;
    if (strcmp(fname, "stdin") == 0) f=stdin;
    else
        if ((f = fopen(fname, "r")) == NULL)
            return MM_COULD_NOT_READ_FILE;
    
    
    if ((ret_code = mm_read_banner(f, matcode)) != 0)
        return ret_code;
    
    if (!(mm_is_valid(*matcode) && mm_is_sparse(*matcode) &&
          mm_is_matrix(*matcode)))
        return MM_UNSUPPORTED_TYPE;
    if(mm_is_complex(*matcode))
        return MM_UNSUPPORTED_TYPE;
    
    if ((ret_code = mm_read_mtx_crd_size(f, M, N, nz)) != 0)
        return ret_code;
    
    if(mm_is_symmetric(*matcode))
        bsz = 2 * (*nz);
    else
        bsz = *nz;
    addind = *nz;
    
    lI= *I = (idxType *)  malloc(bsz * sizeof(idxType));
    lJ= *J = (idxType *)  malloc(bsz * sizeof(idxType));
    lval=*val = NULL;
    
#ifdef PRINT_PROGRESS
    if(sizeof(idxType) == sizeof(int))
        printf("size is read as %d, bsz=%d\n", *nz, bsz);
    else
        printf("size is read as %ld, bsz=%ld\n", *nz, bsz);
#endif
    if (mm_is_complex(*matcode))
    {
        u_errexit("did not consider the complex case\n");
    }
    else if (mm_is_real(*matcode))
    {
        lval=*val = (double *) malloc(bsz * sizeof(double));
        ret_code = mm_read_mtx_crd_data(f, *M, *N, *nz, *I, *J, *val,
                                        *matcode);
        if (ret_code != 0) return ret_code;
    }
    
    else if (mm_is_pattern(*matcode))
    {
        ret_code = mm_read_mtx_crd_data(f, *M, *N, *nz, *I, *J, *val,
                                        *matcode);
        
        if (ret_code != 0) return ret_code;
    }
    else if (mm_is_integer(*matcode))
    {
        lval=*val = (double *) malloc(bsz * sizeof(double));
        ret_code = mm_read_mtx_crd_data(f, *M, *N, *nz, *I, *J, *val,
                                        *matcode);
        if (ret_code != 0) return ret_code;
    }
    else
        return MM_UNSUPPORTED_TYPE;
    
    if (f != stdin) fclose(f);
    
    
    
    if(mm_is_symmetric(*matcode))/*add the symmetric elements*/
    {
        idxType i;
        addind = *nz;
        
#ifdef PRINT_PROGRESS
        printf("will add symmetric elements\n");
#endif
        
        for(i = 0; i < *nz; i++)
        {
            if(lI[i] != lJ[i])
            {
                lI[addind] = lJ[i];
                lJ[addind] = lI[i];
                if(!mm_is_pattern(*matcode))
                    lval[addind] = lval[i];
                addind ++;
            }
#ifdef PRINT_PROGRESS
            if(i == (*nz -1))
            {
                if(sizeof(idxType) == sizeof(int))
                    printf("addind=%d, *nz=%d, bsz=%d\n", addind, *nz, bsz);
                else
                    printf("addind=%ld, *nz=%ld, bsz=%l d\n", addind, *nz, bsz);
                
            }
#endif
        }
    }
    *nz = addind;
    
    return 0;
}

int mm_write_banner(FILE *f, MM_typecode matcode)
{
    char *str = mm_typecode_to_str(matcode);
    int ret_code;
    
    ret_code = fprintf(f, "%s %s\n", MatrixMarketBanner, str);
    free(str);
    if (ret_code !=2 )
        return MM_COULD_NOT_WRITE_FILE;
    else
        return 0;
}

int mm_write_mtx_crd(char fname[], idxType M, idxType N, idxType nz, idxType I[], idxType J[],
                     double val[], MM_typecode matcode)
{
    FILE *f;
    idxType i;
    
    if (strcmp(fname, "stdout") == 0)
        f = stdout;
    else
        if ((f = fopen(fname, "w")) == NULL)
            return MM_COULD_NOT_WRITE_FILE;
    
    /* print banner followed by typecode */
    fprintf(f, "%s ", MatrixMarketBanner);
    fprintf(f, "%s\n", mm_typecode_to_str(matcode));
    
    /* print matrix sizes and nonzeros */
    if(sizeof(idxType) == sizeof(int))
        fprintf(f, "%d %d %d\n", M, N, nz);
    else
        fprintf(f, "%ld %ld %ld\n", M, N, nz);
    
    /* print values */
    if (mm_is_pattern(matcode))
    {
        for (i=0; i<nz; i++)
        {
            if(sizeof(idxType) == sizeof(int))
                fprintf(f, "%d %d\n", I[i], J[i]);
            else
                fprintf(f, "%ld %ld\n", I[i], J[i]);
        }
    }
    else if (mm_is_real(matcode))
    {
        for (i=0; i<nz; i++)
        {
            if(sizeof(idxType) == sizeof(int))
                fprintf(f, "%d %d %20.16g\n", I[i], J[i], val[i]);
            else
                fprintf(f, "%ld %ld %20.16g\n", I[i], J[i], val[i]);
        }
    }
    else if (mm_is_complex(matcode))
    {
        for (i=0; i<nz; i++)
        {
            if(sizeof(idxType) == sizeof(int))
                fprintf(f, "%d %d %20.16g %20.16g\n", I[i], J[i], val[2*i],
                        val[2*i+1]);
            else
                fprintf(f, "%ld %ld %20.16g %20.16g\n", I[i], J[i], val[2*i],
                        val[2*i+1]);
        }
    }
    else
    {
        if (f != stdout) fclose(f);
        return MM_UNSUPPORTED_TYPE;
    }
    
    if (f !=stdout) fclose(f);
    
    return 0;
}


char  *mm_typecode_to_str(MM_typecode matcode)
{
    char buffer[MM_MAX_LINE_LENGTH];
    char *types[4];
    /*    int error =0;*/
    /*    int i;*/
    
    /* check for MTX type */
    if (mm_is_matrix(matcode))
        types[0] = MM_MTX_STR;
    else
        return NULL;
    /* check for CRD or ARR matrix */
    if (mm_is_sparse(matcode))
        types[1] = MM_SPARSE_STR;
    else
        if (mm_is_dense(matcode))
            types[1] = MM_DENSE_STR;
        else
            return NULL;
    
    /* check for element data type */
    if (mm_is_real(matcode))
        types[2] = MM_REAL_STR;
    else
        if (mm_is_complex(matcode))
            types[2] = MM_COMPLEX_STR;
        else
            if (mm_is_pattern(matcode))
                types[2] = MM_PATTERN_STR;
            else
                if (mm_is_integer(matcode))
                    types[2] = MM_INT_STR;
                else
                    return NULL;
    
    
    /* check for symmetry type */
    if (mm_is_general(matcode))
        types[3] = MM_GENERAL_STR;
    else
        if (mm_is_symmetric(matcode))
            types[3] = MM_SYMM_STR;
        else
            if (mm_is_hermitian(matcode))
                types[3] = MM_HERM_STR;
            else
                if (mm_is_skew(matcode))
                    types[3] = MM_SKEW_STR;
                else
                    return NULL;
    
    sprintf(buffer,"%s %s %s %s", types[0], types[1], types[2], types[3]);
    return strdup(buffer);
}
int qsortfnct(const void *i1, const void *i2)
{
    idxType ii1=*(idxType*)i1, ii2 = *(idxType*)i2;
    if( ii1 < ii2) return -1;
    else if(ii1 > ii2) return 1;
    else return 0;
}
void fillCSC(idxType *xpins, idxType *pins, idxType _n, idxType _c, idxType _p, idxType *nids, idxType *cids)
{
    idxType i;
    memset(xpins, 0, sizeof(idxType)*(_n+2));
    
    for(i=0; i < _p; i++)
    {
        idxType nid = nids[i];
        idxType cid = cids[i];
        if(nid > _n || nid <= 0)
        {
            if(sizeof(idxType) == sizeof(int))
                u_errexit("nid=%d out of range [1...%d] from file\n", nid, _n);
            else
                u_errexit("nid=%ld out of range [1...%ld] from file\n", nid, _n);
            
        }
        if(cid > _c || cid <= 0)
        {
            if(sizeof(idxType) == sizeof(int))
                u_errexit("cid=%d out of range [1...%d] from file\n", cid, _c);
            else
                u_errexit("cid=%ld out of range [1...%ld] from file\n", cid, _c);
            
        }
        xpins[nid]++;
    }
    for(i=1; i <= _n+1; i++)
        xpins[i] += xpins[i-1];
    
    for(i=_p-1; i>=0; i--)
    {
        idxType nid = nids[i];
        idxType cid = cids[i];
        xpins[nid] = xpins[nid]-1;
        pins[xpins[nid]] = cid;
        
    }
    /*we like the indices sorted*/
    for(i=1; i<=_n; i++)
    {
        qsort(&pins[xpins[i]], xpins[i+1]-xpins[i], sizeof(idxType), qsortfnct);
    }
    if(xpins[0] != 0){
        if(sizeof(idxType) == sizeof(int))
            printf("xpins[0]=%d ~= 0\n", xpins[0]);
        else
            printf("xpins[0]=%ld ~= 0\n", xpins[0]);
        
        fflush(stdout);
        exit(14);
    }
    if(xpins[_n+1] != _p){
        if(sizeof(idxType) == sizeof(int))
        {
            printf("xpins[_n]=%d ~= _p=%d\n", xpins[_n+1], _p);fflush(stdout);
        }
        else
        {
            printf("xpins[_n]=%ld ~= _p=%ld\n", xpins[_n+1], _p);fflush(stdout);
        }
        exit(15);
    }
    return ;
    
}
void fillCSCvals(idxType *xpins, idxType *pins, ecType *vals, idxType _n, idxType _c, idxType _p, idxType *nids, idxType *cids, double *pvals)
{
    idxType i;
    memset(xpins, 0, sizeof(idxType)*(_n+2));
    
    for(i=0; i < _p; i++)
    {
        idxType nid = nids[i];
        idxType cid = cids[i];
        if(nid > _n || nid <= 0)
            u_errexit("nid=%d out of range [1...%d] from file\n", nid, _n);
        
        if(cid > _c || cid < 0)
            u_errexit("cid=%d out of range [1...%d] from file\n", cid, _c);
        
        xpins[nid]++;
    }
    for(i=1; i <= _n+1; i++)
        xpins[i] += xpins[i-1];
    
    for(i=_p-1; i>=0; i--)
    {
        idxType nid = nids[i];
        idxType cid = cids[i];
        xpins[nid] = xpins[nid]-1;
        pins[xpins[nid]] = cid;
        if (pvals)
            vals[xpins[nid]] = (ecType)pvals[i];
    }
    
    if(xpins[1] != 0){
        printf("xpins[0]=%d ~= 0\n", xpins[1]);
        fflush(stdout);
        exit(14);
    }
    if(xpins[_n+1] != _p){
        printf("xpins[_n]=%d ~= _p=%d\n", xpins[_n+1], _p);fflush(stdout);
        exit(15);
    }
    return ;
    
}
int mm_read_into_csc_pattern(char *fname, idxType *M, idxType *N, idxType *nnz, idxType **col_ptrs, idxType **row_inds)
{
    idxType *rindx=NULL, *cindx=NULL;
    double *vals=NULL;
    int  rcode;
    MM_typecode matcode;
    rcode = mm_read_mtx_crd(fname, M, N, nnz, &rindx, &cindx,
                            &vals, &matcode);
    if(rcode != 0)
        return rcode;
#ifdef PRINT_PROGRESS
    if(sizeof(idxType) == sizeof(int))
        printf("bmmio: will fill in csc storage arrays %d %d %d\n", *M, *N, *nnz);
    else
        printf("bmmio: will fill in csc storage arrays %ld %ld %ld\n", *M, *N, *nnz);
    
#endif
    
    *col_ptrs= (idxType *) malloc(sizeof(idxType)*(*N+2));
    *row_inds = (idxType *) malloc(sizeof(idxType)*(*nnz));
    fillCSC(*col_ptrs, *row_inds, *N, *M, *nnz, cindx, rindx);
    free(rindx);
    free(cindx);
    free(vals);
    return 0;
}

int mm_read_into_csc(char *fname, idxType *M, idxType *N, idxType *nnz, idxType **col_ptrs, idxType **row_inds, ecType **vals)
{
    idxType *rindx=NULL, *cindx=NULL;
    double *lvals=NULL;
    int  rcode;
    MM_typecode matcode;
    rcode = mm_read_mtx_crd(fname, M, N, nnz, &rindx, &cindx,
                            &lvals, &matcode);
    if(rcode != 0)
        return rcode;
    

#ifdef PRINT_PROGRESS
    if(sizeof(idxType) == sizeof(int))
        printf("bmmio: will fill in csc storage arrays %d %d %d\n", *M, *N, *nnz);
    else
        printf("bmmio: will fill in csc storage arrays %ld %ld %ld\n", *M, *N, *nnz);
    
#endif
    
    *col_ptrs= (idxType *) malloc(sizeof(idxType)*(*N+2));
    *row_inds = (idxType *) malloc(sizeof(idxType)*(*nnz));
     if (mm_is_pattern(matcode))
         *vals = NULL;
    else
        *vals = (ecType *) malloc(sizeof(ecType) * (*nnz));
    
    
    fillCSCvals(*col_ptrs, *row_inds, *vals, *N, *M, *nnz, cindx, rindx, lvals);
    free(rindx);
    free(cindx);
    free(lvals);
    return 0;
}
