#include <stdio.h>
#include <stdlib.h>

#include "utils.h"
#include "dgraph.h"
#include "option.h"



int initializeStoppingCriterion(const MLGP_option opt, const coarsen* coars)
{
    return 0;
    int nbnode;
    switch(opt.co_stop) {
    case CO_STOP_SIZE :
	nbnode = coars->graph->nVrtx;
	return nbnode;
    case CO_STOP_STEP:
	return 0;
    }
}

int stoppingCriterion(const MLGP_option opt, int* crit, coarsen* coars)
{
    /*Update the stopping criteria and return 1 if we should continue, 0 otherwise*/
    switch(opt.co_stop) {
    case CO_STOP_SIZE :
    	//printf("\ncase co stop size \n");
	*crit = coars->graph->nVrtx;
	//printf("\n\ncrit in stopping criteria = %d stop size = %d\n\n",*crit,opt.co_stop_size);

	if (*crit < opt.co_stop_size)
	    return 0;
	else
	    return 1;
    case CO_STOP_STEP:
    	//printf("\nco stop step\n");
	(*crit)++;
	if (*crit > opt.co_stop_step)
	    return 0;
	else
	    return 1;
    }
    return 0;
}


MLGP_option* copyOpt(const MLGP_option opt,int new_nbPart)
{
	/* Creates a new MLGP_option object and copies all the variables to the copy */
	MLGP_option *newOpt = (MLGP_option*)malloc(sizeof(MLGP_option));
    newOpt->nbPart=new_nbPart;
    newOpt->co_stop=opt.co_stop;
    newOpt->co_stop_size=opt.co_stop_size;
    newOpt->co_stop_step=opt.co_stop_step;
    newOpt->co_match=opt.co_match;
	newOpt->co_match_norder=opt.co_match_norder;
	newOpt->co_match_eorder=opt.co_match_eorder;
    newOpt->co_match_onedegreefirst=opt.co_match_onedegreefirst;
    newOpt->co_inipart=opt.co_inipart;
    newOpt->co_nbinipart=opt.co_nbinipart;
    newOpt->co_refinement=opt.co_refinement;
    newOpt->co_obj=opt.co_obj;
    newOpt->kernighan=opt.kernighan;
    
    newOpt->ub = (double*)malloc((newOpt->nbPart+1) * sizeof(double));
    newOpt->lb = (double*)malloc((newOpt->nbPart+1) * sizeof(double));
    /*
    newOpt->part_ub = opt.part_ub;
    newOpt->part_lb = opt.part_lb;
    */
    newOpt->ref_step = opt.ref_step;
    newOpt->debug = opt.debug;
	newOpt->print = opt.print;
	newOpt->seed = opt.seed;
	newOpt->ratio = opt.ratio;
	newOpt->runs = opt.runs;
	newOpt->step = opt.step;
	strcpy(newOpt->out_file,opt.out_file);
	strcpy(newOpt->file_name,opt.file_name);
	strcpy(newOpt->file_name_dot,opt.file_name_dot);
	return newOpt;
}

void reallocUBLB(MLGP_option* opt)
{
	free(opt->ub);
	free(opt->lb);
	opt->ub = (double*)calloc((opt->nbPart+1) , sizeof(double));
	opt->lb = (double*)calloc((opt->nbPart+1) , sizeof(double));
	return;
}

void print_opt(MLGP_option* opt)
{
	idxType i;
	printf("### opt.nbPart = %d\n", opt->nbPart);
	printf("### opt.ub = ");
	//for (i=0; i < opt->nbPart; i++)
	//	printf("%f ", opt->ub[i]);
	printf("\n");
	printf("### opt.lb = ");
	//for (i=0; i < opt->nbPart; i++)
	//	printf("%f ", opt->lb[i]);
	printf("\n");
    printf("### opt.ratio = %f\n", opt->ratio);
    return;
}

void free_opt(MLGP_option* opt)
{
  free(opt->ub);
  free(opt->lb);
}
