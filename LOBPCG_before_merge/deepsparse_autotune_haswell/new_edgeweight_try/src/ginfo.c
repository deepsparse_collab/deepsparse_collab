#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include <getopt.h>


#include "dgraph.h"
#include "bmmio.h"
#include "utils.h"
#include "debug.h"
#include "vcycle.h"
#include "partDGraphAlgos.h"
#include "matching.h"
#include "infoPart.h"

/*
char *infilename;
int K=2;
float eps = 0.03;
*/

/*********************************************************************************/
void printUsage_ginfo(char *exeName)
{
    printf("Usage: %s <input-file-name> [options...]\n", exeName);
    printf("Option:\n");


    printf("\t--tabular int \t(default = 0)\n");
    printf("\t\tTabular format outputp\n");

}


int processArgs_ginfo(int argc, char **argv, MLGP_option* opt)
{
    return 0;
}


void run_ginfo(char* file_name, MLGP_option opt)
{
}


int main(int argc, char *argv[])
{
    if (argc < 2){
	printUsage_ginfo(argv[0]);
	u_errexit("%s: There is a problem with the arguments.\n", argv[0]);
    }

    char fname[PATH_MAX];
    strncpy(fname, argv[1], PATH_MAX);

    
    int tabular = 0;

    static struct option long_options[] =
    {
	{"tabular",  required_argument, 0, 't'},
    };

    const char *delims = "t";
    char o;
    int option_index = 0;
    while ((o = getopt_long(argc, argv, delims, long_options, &option_index)) != -1) {
        switch (o) {
            case 't':
                tabular = 1;
                break;
            default:
                printUsage_ginfo(argv[0]);
		u_errexit("Option %s not recognized.", o);
                return 1;
        }
    }

    dgraph G;


    if (strstr(fname,".dot")){
        generateDGraphFromDot(&G, fname,1);
    }
    else if(strstr(fname,".mtx")){   
        generateDGraphFromMtx(&G,fname,1);
    }

    int maxindegree, minindegree, maxoutdegree, minoutdegree;
    double aveindegree, aveoutdegree;
    dgraph_info(&G, &maxindegree, &minindegree, &aveindegree, &maxoutdegree, &minoutdegree, &aveoutdegree);
    idxType* sourceOutput = (idxType*) umalloc( sizeof(idxType)*(G.nVrtx+1),"sourceOutput in ginfo.c");
    int sCount = sourcesList(&G,sourceOutput);
    int tCount = outputList(&G,sourceOutput);
    free(sourceOutput);
    if (tabular)
        printf("%s\t%d\t%d\t%d\t%d\t%d\t%d\t%.3lf\t%d\t%d\n", fname, G.nVrtx, G.nEdge, minindegree, maxindegree, minoutdegree, maxoutdegree, aveoutdegree,sCount,tCount);
    else
        printf("Nb node: %d\nNb Edges: %d\nMin in degree: %d\nMax in degree: %d\nMin out degree: %d\nMax out degree: %d\nAverage in/out degree: %f\nSource Node Count: %d\nTarget Node Count: %d\n\n", G.nVrtx, G.nEdge, minindegree, maxindegree, minoutdegree, maxoutdegree, aveoutdegree,sCount,tCount);

    return 0;
}

