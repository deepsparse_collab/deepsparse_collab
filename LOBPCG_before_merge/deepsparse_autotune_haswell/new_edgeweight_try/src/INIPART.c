#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <sys/stat.h>
#include <metis.h>

#include <getopt.h>


#include "dgraph.h"
#include "bmmio.h"
#include "utils.h"
#include "debug.h"
#include "vcycle.h"
#include "partDGraphAlgos.h"
#include "matching.h"
#include "infoPart.h"
#include "initialBissection.h"

/*
char *infilename;
int K=2;
float eps = 0.03;
*/

/*********************************************************************************/

int processArgs_INIPART(int argc, char **argv, MLGP_option* opt)
{
    int i;
    if (argc < 3){
        //printUsage_MLGP(argv[0]);
	    u_errexit("%s: There is a problem with the arguments.\n", argv[0]);
    }

    strncpy(opt->file_name, argv[1], PATH_MAX);
    strncpy(opt->out_file, opt->file_name, PATH_MAX);
    strcat(opt->out_file, "_MLGP.out");

    opt->nbPart = atoi(argv[2]);

    opt->seed = 0;
    opt->co_stop = 0;
    opt->co_stop_size = 50*opt->nbPart;
    opt->co_stop_step = 10;
    opt->co_match = 0;
    opt->co_match_norder = 1;
    opt->co_match_eorder = 1;
    opt->co_match_onedegreefirst = 1;
    opt->co_inipart = 0;
    opt->co_nbinipart = 5;
    opt->co_refinement = 1;
    opt->ub = (double*)malloc((opt->nbPart)*sizeof(double));
    opt->lb = (double*)malloc((opt->nbPart)*sizeof(double));

    for (i=0;i<opt->nbPart;i++){
        opt->ub[i] = -1;
        opt->lb[i] = -1;
    }
    opt->ref_step = 10;
    opt->co_obj = 0;
    opt->ratio = 1.03;
    opt->runs = 1;
    opt->debug = 0;
    opt->print = 0;


    static struct option long_options[] =
    {
        {"seed",  required_argument, 0, 's'},
	{"part_ub",  required_argument, 0, 'u'},
	{"part_lb",    required_argument, 0, 'l'},
	{"ratio",    required_argument, 0, 'a'},
	{"runs",    required_argument, 0, 'n'},
	{"out_file",    required_argument, 0, 'f'},
	{"debug",    required_argument, 0, 'd'},
	{"print",    required_argument, 0, 'q'},
    };

    const char *delims = "s:t:z:p:m:o:g:1:i:r:j:u:l:a:n:b:d:f:q";
    char o;
    int option_index = 0;
    while ((o = getopt_long(argc, argv, delims, long_options, &option_index)) != -1) {
        switch (o) {
            case 's':
                opt->seed = atoi(optarg);
                break;
            case 't':
                opt->co_stop = atoi(optarg);
                break;
            case 'z':
                opt->co_stop_size = atoi(optarg);
                break;
            case 'p':
                opt->co_stop_step = atoi(optarg);
                break;
            case 'm':
                opt->co_match = atoi(optarg);
                break;
            case 'o':
                opt->co_match_norder = atoi(optarg);
                break;
            case 'g':
                opt->co_match_eorder = atoi(optarg);
                break;
            case '1':
                opt->co_match_onedegreefirst = atoi(optarg);
                break;
            case 'i':
                opt->co_inipart = atoi(optarg);
                break;
            case 'b':
                opt->co_nbinipart = atoi(optarg);
                break;
            case 'r':
                opt->co_refinement = atoi(optarg);
                break;
            case 'j':
                opt->co_obj = atoi(optarg);
                break;
            case 'e':
                opt->ref_step = atoi(optarg);
                break;
            case 'u':
                opt->ub[0] = atof(optarg);
                break;
            case 'l':
                opt->lb[0] = atof(optarg);
                break;
            case 'a':
                opt->ratio = atof(optarg);
                break;
            case 'n':
                opt->runs = atof(optarg);
                break;
	    case 'd':
                opt->debug = atoi(optarg);
                break;
	    case 'q':
                opt->print = atoi(optarg);
                break;
	    case 'f':
	        strncpy(opt->out_file, optarg, PATH_MAX);
		break;
            default:
                //printUsage_MLGP(argv[0]);
		u_errexit("Option %s not recognized.", o);
                return 1;
        }
    }
    for(i=1;i<opt->nbPart;i++){
        opt->ub[i]=opt->ub[0];
        opt->lb[i]=opt->lb[0];
    }
    if ((opt->co_refinement == 2)&&(opt->nbPart < 3)){
	    opt->co_refinement = 1;
	    printf("WARNING: co_refinement has been set to 1 because nbpart <= 2\n");
    }
    
    char suf[200];
    char fname[200];
    char* split;    
    char fname_sav[200];
    sprintf(fname_sav, "%s", opt->file_name);
    if (opt->print > 0){
	split = strtok(fname_sav, "/");
	while(split != NULL){
	    sprintf(fname, "%s", split);
	    split = strtok(NULL, "/");
	}
	sprintf(opt->file_name_dot, "out/");
	mkdir(opt->file_name_dot, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IWOTH | S_IXOTH);
	strcat(opt->file_name_dot, fname);
	sprintf(suf, "__%d_%d", opt->nbPart, opt->seed);
	strcat(opt->file_name_dot, suf);
	mkdir(opt->file_name_dot, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IWOTH | S_IXOTH);
	sprintf(suf, "/graph");
	strcat(opt->file_name_dot, suf);
    }    

    return 0;
}



void run_INIPART(char* file_name, MLGP_option opt)
{
    dgraph G;

    generateDGraphFromDot(&G, file_name,1);

    int i;
    double ratio = opt.ratio;
    double eca = ((ratio-1.)*G.nVrtx)/opt.nbPart;
    for(i = 0; i<opt.nbPart; i++){
        if (opt.lb[i] < 0){
            //opt.lb[i] = G.nVrtx/(opt.nbPart*1.0) - eca;
            opt.lb[i] = 1;
        }
        if (opt.ub[i] < 0){
            opt.ub[i] = G.nVrtx/(opt.nbPart*1.0) + eca;
            //printf("new ub %lf\n", opt.ub[i]);
        }

        if ((floor(opt.lb[i]) < opt.lb[i])&&(floor(opt.lb[i]) == floor(opt.ub[i]))){
            printf("WARNING: The balance can not be matched!!!\n");
            opt.lb[i] = floor(opt.lb[i]);
            opt.ub[i] = floor(opt.ub[i])+1;
        }
    }

    printf("Nb node: %d\nNb Edges: %d\nNb part: %d\nLower Bound: %f\nUper Bound: %f\n\n", G.nVrtx, G.nEdge, opt.nbPart, opt.lb[0], opt.ub[0]);

    idxType* part = (idxType*) malloc((G.nVrtx+1) * sizeof(idxType));

    //metisAcyclicBissection(&G, part, opt);

    ecType edgecut = edgeCut(&G, part);
    ecType nbcomm = nbCommunications(&G, part);
    double latencies = computeLatency(&G, part, 1.0, 11.0);
    printf("Final run %d\t%d\t%d\t%d\t%d\t%f\n", 0, G.nVrtx, G.nEdge, edgecut, nbcomm, latencies);
    freeDGraphData(&G);
    free(part);

    freeDGraphData(&G);
    free(part);
}


int main(int argc, char *argv[])
{
    MLGP_option opt;
    processArgs_INIPART(argc, argv, &opt);

    run_INIPART(opt.file_name, opt);

    return 0;
}
