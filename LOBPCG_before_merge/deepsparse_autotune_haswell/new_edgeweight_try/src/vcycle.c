#include <stdio.h>
#include <stdlib.h>
#include <float.h>

#include "vcycle.h"
#include "option.h"
#include "utils.h"
#include "debug.h"
#include "matching.h"
#include "partDGraphAlgos.h"
#include "initialPartitioning.h"
#include "initialBissection.h"
#include "refinement.h"
#include "infoPart.h"

coarsen *initializeCoarsen(dgraph* graph)
{
    coarsen * coars = (coarsen*) umalloc(sizeof(coarsen),"coars");
    coars->graph = graph;
    coars->previous_coarsen = NULL;
    coars->next_coarsen = NULL;
    coars->leader = (idxType * ) umalloc(sizeof(idxType) * (graph->nVrtx +1),"coars->leader");
    coars->new_index = (idxType * ) umalloc(sizeof(idxType) * (graph->nVrtx +1),"coars->new_index");
    coars->part = (idxType * ) umalloc(sizeof(idxType) * (graph->nVrtx +1),"coars->part");
    coars->nbpart = 0;
    return coars;
}

/* don't call this with HEAD; or finest graph!!! */ 
void freeCoarsen(coarsen *coars)
{
    free(coars->leader);
    free(coars->new_index);
    free(coars->part);
    //printf("\tfreeDGraphData...\n");
    freeDGraphData(coars->graph);
    //printf("\t... freeDGraphData done\n");
    free(coars->graph);
    free(coars);      
  /**
    if (coars->next_coarsen != NULL){
	coars->next_coarsen->previous_coarsen = NULL;
	freeCoarsen(coars->next_coarsen);
    }
    if (coars->previous_coarsen != NULL){
	coars->previous_coarsen->next_coarsen = NULL;
	freeCoarsen(coars->previous_coarsen);
    }
    */
}

void freeCoarsenHeader(coarsen *coars)
{
    free(coars->leader);
    free(coars->new_index);
    free(coars->part);
    //freeDGraphData(coars->graph); 
    //free(coars->graph);
    free(coars);      
  /*
    if (coars->next_coarsen != NULL){
	coars->next_coarsen->previous_coarsen = NULL;
	freeCoarsen(coars->next_coarsen);
    }
    if (coars->previous_coarsen != NULL){
	coars->previous_coarsen->next_coarsen = NULL;
	freeCoarsen(coars->previous_coarsen);
    }
    */
}

ecType compute_comm_vol(MLGP_option opt, idxType nbedge, ecType* costs, vwType weight){
    ecType commvol, commvolmax, commvolsum;
    idxType i;
    switch(opt.co_obj_commvol){
        case CO_OBJ_COMMVOL_EXP :
            commvolmax = 0.0;
            commvol = 1.0;
            for (i = 0; i < nbedge; i++) {
                commvolmax = commvolmax < costs[i] ? costs[i] : commvolmax;
                commvol *= 1.0 - costs[i]/weight;
            }
            commvol = commvol / (1.0 - commvolmax / weight);
            commvol = weight - (weight - commvolmax) * commvol;
            break;

        case CO_OBJ_COMMVOL_BEST :
            commvol = 0.0;
            for (i = 0; i < nbedge; i++)
                commvol = commvol < costs[i] ? costs[i] : commvol;
            break;

        case CO_OBJ_COMMVOL_WORST :
            commvol = 0.0;
            for (i = 0; i < nbedge; i++)
                commvol += costs[i];
            commvol = commvol < weight ? commvol : weight;
            break;

        case CO_OBJ_COMMVOL_AVE :
            commvolmax = 0.0;
            commvolsum = 0.0;
            for (i = 0; i < nbedge; i++) {
                commvolsum += costs[i];
                commvolmax = commvolmax < costs[i] ? costs[i] : commvolmax;
            }
            commvol = (commvolsum + commvolmax)/2;
            break;
    }
    return commvol;
}

ecType compute_edge_cost(MLGP_option opt, idxType nbedge, ecType* costs, vwType weight){
    ecType cost;
    idxType i;
    /*printf("Compute edge cost: nbedge = %d weight = %d\n", (int) nbedge, (int) weight);
    for (i = 0; i < nbedge; i++)
        printf("%d ", (int) costs[i]);
    printf("\n");*/
    switch(opt.co_obj){
        case CO_OBJ_EC :
            cost = 0.0;
            for (i = 0; i < nbedge; i++)
                cost += costs[i];
            break;

        case CO_OBJ_CV :
            cost = compute_comm_vol(opt, nbedge, costs, weight);
            break;
    }
    //printf("cost = %d\n", (int) cost);
    return cost;
}

ecType commVol(MLGP_option opt, dgraph* G,  idxType* part){
    ecType commvol = 0.0;
    idxType node, k;
    idxType *nbedge;
    ecType **edges;
    edges = (ecType**) calloc(opt.nbPart, sizeof(ecType*));
    for (k = 0; k < opt.nbPart; k++)
        edges[k] = (ecType*) calloc(G->maxoutdegree + G->maxindegree, sizeof(ecType));

    for (node = 1; node <= G->nVrtx; node++){
        nbedge = (idxType*) calloc(opt.nbPart, sizeof(idxType));
        for (k = G->outStart[node]; k <= G->outEnd[node]; k++) {
            idxType outnode = G->out[k];
            edges[part[outnode]][nbedge[part[outnode]]++] = G->ecOut[k];
        }
        for (k = 0; k < opt.nbPart; k++)
            if (part[node] != k)
                commvol += compute_comm_vol(opt, nbedge[k], edges[k], G->vw[node]);
    }
    return commvol;
}
dgraph *initializeNextGraph(coarsen *coars, MLGP_option opt)
{
    /*Allocate and compute the next graph based on the matching in coars*/
    /*Modify coars->new_index accordingly*/
	//printf("\n\nnextgraph initialize function\n\n");
    dgraph *next_graph = NULL;
    next_graph = (dgraph*) umalloc(sizeof(dgraph),"next_graph" );
    //printf("\n nextgraph initialized\n\n");
    if(next_graph == NULL)
        u_errexit("initializeNextGraph: next_graph could not be initialized.\n");
    dgraph* graph = coars->graph;
    idxType* leader = coars->leader;
    idxType nbnewnode = 0, nbnode = graph->nVrtx;
    idxType* newnodes = coars->new_index;
    int i,j,k;
    next_graph->ecIn = next_graph->ecOut = NULL;
    next_graph->vw = NULL;
    next_graph->vertices = NULL;
    
    //printf("\n\ninitialize values for graph\n\n");

    /*Compute new_index table*/
    for (i=1; i <= nbnode; i++)	{
        if (leader[leader[i]] != leader[i])
            u_errexit("initializeNextGraph: node %d is the leader of node %d and not its own leader\n", (int) leader[i], (int) i);
        if (leader[i] == i)
            newnodes[i] = ++nbnewnode;
        //printf("\nnewnodes[%d] = %d\n",i,nbnewnode);
    }
    idxType** cluster = NULL;
    idxType* clustersize = NULL;
    cluster = (idxType**) umalloc(sizeof(idxType*)*(nbnewnode+1),"cluster" );
    clustersize = (idxType*) calloc(nbnewnode+1, sizeof(idxType));
    next_graph->vw = (vwType *) calloc(nbnewnode+1, sizeof(vwType));



    next_graph->vertices = (char**)malloc(sizeof(char*)*(nbnewnode+1));
        for(i = 0;i<nbnewnode;++i){
            next_graph->vertices[i] = (char*)malloc(100*sizeof(char));
        }
    int head = 0;
    for(i =1 ; i <=nbnode ; i++){
        
            if(leader[i] == i){
                strcpy(next_graph->vertices[head++],graph->vertices[i-1]);
            }
        
    }

    if (cluster == NULL || clustersize == NULL || next_graph->vw == NULL){
        u_errexit("initializeNextGraph: cluster, clustersize, or next_graph->vw could not be initialized.\n");
    }
    idxType maxclustersize = 0;
    for (i=1; i<= nbnode; i++){
        newnodes[i] = newnodes[leader[i]];
        clustersize[newnodes[i]]++;
        maxclustersize = maxclustersize < clustersize[newnodes[i]] ? clustersize[newnodes[i]] : maxclustersize;

    }
    for (i=1; i<= nbnewnode; i++) {
        cluster[i] = (idxType *) calloc(clustersize[i], sizeof(idxType));
        if (cluster[i] == NULL)
            u_errexit("initializeNextGraph: cluster[%d] could not be initialized.\n",i);
        
        clustersize[i] = 0;
    }

    //printf("\n\nleader clustersize done\n\n");
    for (i=1; i<= nbnode; i++){
        cluster[newnodes[i]][clustersize[newnodes[i]]++] = i;
        next_graph->vw[newnodes[i]] += graph->vw[i];
        maxclustersize = maxclustersize < clustersize[newnodes[i]] ? clustersize[newnodes[i]] : maxclustersize;
    }
    //printf("\n\ncluster nextgraph maxclustersize done\n\n");
    
    /*Compute new in edges*/
    idxType idx = 0;
    next_graph->inStart = next_graph->inEnd = next_graph->in = NULL;
    next_graph->ecIn = NULL;
    //printf(" %d %d %d %d\n", sizeof(idxType),nbnewnode+2,sizeof(idxType)*(nbnewnode+2),graph->nEdge);
//    next_graph->inStart = (idxType*) umalloc (sizeof(idxType) * (nbnewnode + 2),"next_graph->inStart" );
    next_graph->inStart = (idxType*) malloc (sizeof(idxType) * (nbnewnode + 2));
    //printf("\nallocation done\n");
    next_graph->inEnd = (idxType*) umalloc (sizeof(idxType) * (nbnewnode + 2),"next_graph->inEnd" );
    next_graph->in = (idxType*) umalloc (sizeof(idxType) * (graph->nEdge+1),"next_graph->in" );
    next_graph->ecIn = (ecType*) umalloc (sizeof(ecType) * (graph->nEdge+1),"next_graph->ecIn" );
    //printf("initializeNextGraph ds allocation done\n");
    if(next_graph->inStart == NULL || next_graph->inEnd == NULL || next_graph->in == NULL || next_graph->ecIn == NULL)
        u_errexit("vcycle.c: next_graph->inStart, next_graph->inEnd, next_graph->in, or next_graph->ecIn could not be initialized.\n");
    next_graph->maxindegree = 0;


    
    idxType *inedgeidx = NULL;
    int *marker = NULL;
    ecType* work = NULL;
    idxType atIndex;
    
    inedgeidx = (idxType*) malloc(sizeof(idxType) * (nbnewnode+1));
    work = (ecType *) malloc(sizeof(ecType) * (nbnewnode+1));
    marker = (int *) calloc(nbnewnode+1, sizeof(int));    
    
    if (inedgeidx == NULL || work == NULL || marker == NULL )
        u_errexit("initializeNextGraph: inedgeidx, work, marker could not be initialized.\n");
    
    for (i=1; i <= nbnewnode; i++) {
    //	printf("i=%d\n",i);
        next_graph->inStart[i] = idx;
        next_graph->inEnd[i-1] = idx-1;
        atIndex = 0;
        
        if (i>1){
            idxType degree = next_graph->inEnd[i-1] - next_graph->inStart[i-1] + 1;
            next_graph->maxindegree = next_graph->maxindegree < degree ? degree : next_graph->maxindegree;
        }
        
        for (j = 0; j < clustersize[i]; j++){
            idxType node = cluster[i][j];
            for (k=graph->inStart[node]; k <= graph->inEnd[node]; k++) {
                idxType pred_node = newnodes[graph->in[k]];
                if (pred_node != i) {/*this is an in-coming neighbor*/
                    if(marker[pred_node] == 0) {/*this is a new in-coming neighbor*/
                        marker[pred_node] = 1;
                        work[pred_node] = 0;
                        inedgeidx[atIndex++] = pred_node;
                    }
                    work[pred_node] += graph->ecIn[k];
                }
            }
        }
        for(k = 0; k < atIndex; k++) {
            next_graph->in[idx++] = inedgeidx [k];
            next_graph->ecIn[idx-1] =work [inedgeidx [k]];
            marker[inedgeidx [k]] = 0;
        }
    }


    int nbnewedge = idx;
    
    next_graph->inEnd[nbnewnode] = idx-1;
    int degree = next_graph->inEnd[nbnewnode] - next_graph->inStart[nbnewnode] + 1;
    next_graph->maxindegree = next_graph->maxindegree < degree ? degree : next_graph->maxindegree;
    next_graph->inStart[nbnewnode+1] = nbnewedge;
    next_graph->inEnd[nbnewnode+1] = nbnewedge-1;
    next_graph->in = (idxType*) realloc (next_graph->in, sizeof(idxType) * (nbnewedge+1));
    next_graph->ecIn = (ecType *) realloc(next_graph->ecIn, (nbnewedge+1) * sizeof(ecType));
    next_graph->ecOut = (ecType *) umalloc(sizeof(ecType) * (nbnewedge+1),"next_graph->ecOut" );
    if (next_graph->in == NULL || next_graph->ecIn == NULL || next_graph->ecOut == NULL)
        u_errexit("vcycle.c: next_graph->in, next_graph->ecIn, or next_graph->ecOut could not be initialized.\n");
    
    /*Fill next graph*/
    next_graph->frmt = graph->frmt;
    next_graph->nVrtx = nbnewnode;
    next_graph->nEdge = nbnewedge;
    next_graph->totvw = graph->totvw;
    next_graph->hollow = next_graph->outStart = next_graph->outEnd = next_graph->out = NULL;
    next_graph->hollow  = (int * ) calloc((nbnewnode + 2),sizeof(int));
    next_graph->outStart  = (idxType * ) umalloc(sizeof(idxType) * (nbnewnode + 2),"next_graph->outStart" );
    next_graph->outEnd  = (idxType * ) umalloc(sizeof(idxType) * (nbnewnode + 2),"outEnd" );
    next_graph->out = (idxType * ) umalloc(sizeof(idxType) * (nbnewedge+1),"out" );
    if (next_graph->hollow == NULL || next_graph->outStart == NULL || next_graph->outEnd == NULL || next_graph->out == NULL)
        u_errexit("vcycle.c: next_graph->hollow, next_graph->outStart, next_graph->outEnd, or next_graph->out could not be initialized.\n");
    fillOutFromIn(next_graph);

 /*   FILE *vcycle_new_graph;
    vcycle_new_graph = fopen("vcycle_new_graph.dot","w");
    fprintf(vcycle_new_graph,"digraph {\n");
    if(nbnewnode == 97){
        for (i = 1;i<=nbnewnode ; i++){
            for(j = next_graph->inStart[i] ; j <= next_graph->inEnd[i]; j++){
    //            printf("%d ---> %d\n",next_graph->in[j],i);
                fprintf(vcycle_new_graph,"%d -> %d;\n",next_graph->in[j],i);
            }
        }
    }
    fprintf(vcycle_new_graph, "}\n" );

    fclose(vcycle_new_graph);*/
    
    free(inedgeidx);
    free(work);
    free(marker);

    for (i=1; i<= nbnewnode; i++)
        free(cluster[i]);
    free(cluster);
    free(clustersize);
    
    return next_graph;
}
dgraph *initializeNextGraph_old(coarsen *coars, MLGP_option opt)
{
    /*Allocate and compute the next graph based on the matching in coars*/
    /*Modify coars->new_index accordingly*/
    dgraph *next_graph = NULL;
    next_graph = (dgraph*) umalloc(sizeof(dgraph),"next_graph" );
    if(next_graph == NULL)
        u_errexit("vcycle.c: next_graph could not be initialized.\n");
    dgraph* graph = coars->graph;
    idxType* leader = coars->leader;
    idxType nbnewnode = 0, nbnode = graph->nVrtx;
    idxType* newnodes = coars->new_index;
    int i,j,k,l;
    next_graph->vw = NULL;
    next_graph->ecIn = next_graph->ecOut = NULL;

    /*Compute new_index table*/
    for (i=1; i <= nbnode; i++)	{
        if (leader[leader[i]] != leader[i])
            u_errexit("initializeNextGraph: node %d is the leader of node %d and not its own leader\n", (int) leader[i], (int) i);
        if (leader[i] == i)
            newnodes[i] = ++nbnewnode;
    }
    idxType** cluster = NULL; 
    idxType* clustersize = NULL;
    cluster = (idxType**) umalloc(sizeof(idxType*)*(nbnewnode+1),"cluster" );
    clustersize = (idxType*) calloc(nbnewnode+1, sizeof(idxType));
    next_graph->vw = (vwType *) calloc(nbnewnode+1, sizeof(vwType));
    if (cluster == NULL || clustersize == NULL || next_graph->vw == NULL){
        u_errexit("vcycle.c: cluster, clustersize, or next_graph->vw could not be initialized.\n");
    }
    idxType maxclustersize = 0;
    for (i=1; i<= nbnode; i++){
        newnodes[i] = newnodes[leader[i]];
        clustersize[newnodes[i]]++;
        maxclustersize = maxclustersize < clustersize[newnodes[i]] ? clustersize[newnodes[i]] : maxclustersize;
    }
    for (i=1; i<= nbnewnode; i++) {
        cluster[i] = (idxType *) calloc(clustersize[i], sizeof(idxType));
        if (cluster[i] == NULL)
            u_errexit("vcycle.c: cluster[%d] could not be initialized.\n",i);

        clustersize[i] = 0;
    }
    for (i=1; i<= nbnode; i++){
      cluster[newnodes[i]][clustersize[newnodes[i]]++] = i;
      next_graph->vw[newnodes[i]] += graph->vw[i];
      maxclustersize = maxclustersize < clustersize[newnodes[i]] ? clustersize[newnodes[i]] : maxclustersize;
    }	

    /*Compute new in edges*/
    int idx = 0;
    next_graph->inStart = next_graph->inEnd = next_graph->in = NULL;
    next_graph->ecIn = NULL;
    	//printf(" %d %d %d %d\n", sizeof(idxType),nbnewnode+2,sizeof(idxType)*(nbnewnode+2),graph->nEdge);
	next_graph->inStart = (idxType*) umalloc (sizeof(idxType) * (nbnewnode + 2),"next_graph->inStart" );
    next_graph->inEnd = (idxType*) umalloc (sizeof(idxType) * (nbnewnode + 2),"next_graph->inEnd" );
    next_graph->in = (idxType*) umalloc (sizeof(idxType) * (graph->nEdge+1),"next_graph->in" );
    next_graph->ecIn = (ecType*) umalloc (sizeof(ecType) * (graph->nEdge+1),"next_graph->ecIn" );
    if(next_graph->inStart == NULL || next_graph->inEnd == NULL || next_graph->in == NULL || next_graph->ecIn == NULL)
        u_errexit("vcycle.c: next_graph->inStart, next_graph->inEnd, next_graph->in, or next_graph->ecIn could not be initialized.\n");
    next_graph->maxindegree = 0;

    idxType nbinedge = 0;
    idxType *inedgeidx = NULL, *nbinedgecost = NULL, *inedgenewidx = NULL, *inedgeinnode = NULL;
    ecType** inedgecost = NULL;

    inedgeidx = (idxType *) calloc(graph->maxindegree * maxclustersize, sizeof(idxType));
    inedgecost = (ecType **) calloc(graph->maxindegree * maxclustersize, sizeof(ecType*));
    nbinedgecost = (idxType *) calloc(graph->maxindegree * maxclustersize, sizeof(idxType));
    inedgenewidx = (idxType *) calloc(graph->maxindegree * maxclustersize, sizeof(idxType));
    inedgeinnode = (idxType *) calloc(graph->maxindegree * maxclustersize, sizeof(idxType));

    if (inedgeidx == NULL || nbinedgecost == NULL || inedgenewidx == NULL || inedgeinnode == NULL || inedgecost == NULL)
        u_errexit("vcycle.c: inedgeidx, nbinedgecost, inedgenewidx, inedgeinnode, inedgecost could not be initialized.\n");

    //printf("maxclustersize = %d\n", maxclustersize);
    //printf("graph->maxindegree %d\n",graph->maxindegree );
    //printf("maxclustersize %d\n",maxclustersize );
    for (j = 0; j < graph->maxindegree * maxclustersize; j++){
        //printf("inedgecost[%d] size: %d\n", graph->maxindegree * maxclustersize,graph->maxindegree * maxclustersize);
        inedgecost[j] = (ecType*) calloc(graph->maxindegree * maxclustersize, sizeof(ecType));
        if( inedgecost[j] == NULL )
            u_errexit("vcycle.c: inedgecost[%d] could not be initialized.\n",j);
    }

/*
    idxType* datacounted = NULL;
    if (opt.co_obj == CO_OBJ_CV){
	    datacounted = (idxType*) umalloc(sizeof(idxType)*graph->nVrtx,"datacounted" );
        if (datacounted == NULL)
            u_errexit("vcycle.c: datacounted could not be initialized.\n");
    }
  */
    for (i=1; i <= nbnewnode; i++) {
        nbinedge = 0;
        next_graph->inStart[i] = idx;
        next_graph->inEnd[i-1] = idx-1;
        if (i>1){
            int degree = next_graph->inEnd[i-1] - next_graph->inStart[i-1] + 1;
            next_graph->maxindegree = next_graph->maxindegree < degree ? degree : next_graph->maxindegree;
        }

        for (j = 0; j < clustersize[i]; j++){
            idxType node = cluster[i][j];
            for (k=graph->inStart[node]; k <= graph->inEnd[node]; k++) {
                idxType pred_node = graph->in[k];
                if (newnodes[pred_node] != i) {
                    int bool_new_pred = 1;
                    idxType old_pred_idx = -1;
                    for (l = 0; l < nbinedge; l++)
                        if (inedgeidx[l] == pred_node) {
                            bool_new_pred = 0;
                            old_pred_idx = l;
                            break;
                        }
                    if (bool_new_pred == 1) {
                        old_pred_idx = nbinedge++;
                        inedgeidx[old_pred_idx] = pred_node;
                    }
                    inedgecost[old_pred_idx][nbinedgecost[old_pred_idx]++] = graph->ecIn[k];
                    int bool_new_edge = 1;
                    idxType old_idx = -1;
                    for (l=next_graph->inStart[i]; l < idx; l++)
                        if (next_graph->in[l] == newnodes[pred_node]){
                            bool_new_edge = 0;
                            old_idx = l;
                            break;
                        }
                    if (bool_new_edge == 1) {
                      old_idx = idx++;
                    }
                    inedgenewidx[old_pred_idx] = old_idx;
                    inedgeinnode[old_pred_idx] = pred_node;
                    next_graph->in[old_idx] = newnodes[pred_node];
                }
            }
        }
        for (l = 0; l < nbinedge; l++)
            next_graph->ecIn[inedgenewidx[l]] = 0;
        for (l = 0; l < nbinedge; l++) {
            next_graph->ecIn[inedgenewidx[l]] += compute_edge_cost(opt, nbinedgecost[l], inedgecost[l], graph->vw[inedgeinnode[l]]);
            nbinedgecost[l] = 0;
        }
    }
    int nbnewedge = idx;

    next_graph->inEnd[nbnewnode] = idx-1;
    int degree = next_graph->inEnd[nbnewnode] - next_graph->inStart[nbnewnode] + 1;
    next_graph->maxindegree = next_graph->maxindegree < degree ? degree : next_graph->maxindegree;
    next_graph->ecOut = NULL;    
    next_graph->inStart[nbnewnode+1] = nbnewedge;
    next_graph->inEnd[nbnewnode+1] = nbnewedge-1;
    next_graph->in = (idxType*) realloc (next_graph->in, sizeof(idxType) * (nbnewedge+1));
    next_graph->ecIn = (ecType *) realloc(next_graph->ecIn, (nbnewedge+1) * sizeof(ecType));
    next_graph->ecOut = (ecType *) umalloc(sizeof(ecType) * (nbnewedge+1),"next_graph->ecOut" );
    if (next_graph->in == NULL || next_graph->ecIn == NULL || next_graph->ecOut == NULL)
        u_errexit("vcycle.c: next_graph->in, next_graph->ecIn, or next_graph->ecOut could not be initialized.\n");

    /*Fill next graph*/
    next_graph->frmt = graph->frmt;
    next_graph->nVrtx = nbnewnode;
    next_graph->nEdge = nbnewedge;
    next_graph->totvw = graph->totvw;
    next_graph->hollow = next_graph->outStart = next_graph->outEnd = next_graph->out = NULL;
    next_graph->hollow  = (int * ) calloc((nbnewnode + 2),sizeof(int));
    next_graph->outStart  = (idxType * ) umalloc(sizeof(idxType) * (nbnewnode + 2),"next_graph->outStart" );
    next_graph->outEnd  = (idxType * ) umalloc(sizeof(idxType) * (nbnewnode + 2),"outEnd" );
    next_graph->out = (idxType * ) umalloc(sizeof(idxType) * (nbnewedge+1),"out" );
    if (next_graph->hollow == NULL || next_graph->outStart == NULL || next_graph->outEnd == NULL || next_graph->out == NULL)
        u_errexit("vcycle.c: next_graph->hollow, next_graph->outStart, next_graph->outEnd, or next_graph->out could not be initialized.\n");
    fillOutFromIn(next_graph);

    free(inedgeidx);
    free(nbinedgecost);
    free(inedgenewidx);
    free(inedgeinnode);
    //printf("maxclustersize = %d\n", maxclustersize);
    for (j = 0; j < graph->maxindegree * maxclustersize; j++)
        free(inedgecost[j]);
    free(inedgecost);

    for (i=1; i<= nbnewnode; i++)
	free(cluster[i]);
    free(cluster);
    free(clustersize);

    if (next_graph->ecIn == NULL)
      u_errexit("!!! EcIN == NULL in initializeNextGraph!!!\n");
    return next_graph;
}

/* coarsen *coarsenStep(coarsen* coars) */
/* { */
/*     /\*Allocate, compute and return the next coarsen based on coarsen->graph and coarsen->leader*\/ */
/*     dgraph* next_graph = (dgraph*) malloc(sizeof(dgraph));  */
/*     coarsen* next_coars = (coarsen*) malloc(sizeof(dgraph));  */
/*     computeNextGraph(coars, next_graph); */
/*     initializeCoarsen(next_coars, next_graph); */
/*     coars->next_coarsen = next_coars; */
/*     next_coars->previous_coarsen = coars; */
/*     return next_coars; */
/* } */

coarsen *projectBack(coarsen* coars)
{
    /*Compute the partition for coars given the one in coars->next_coarsen*/
    coarsen* prev_coars = coars->previous_coarsen;
    idxType i;

    prev_coars->nbpart = coars->nbpart;
    for (i=1; i<=prev_coars->graph->nVrtx; i++){
	//printf("caca i=%d, coars->new_index[i] = %d, coars->part[i] = %d\n",i, coars->new_index[i], next_coars->part[coars->new_index[i]]);
	//printf("caca i=%d, coars->new_index[i] = %d\n",i, coars->new_index[i]);
	prev_coars->part[i] = coars->part[prev_coars->new_index[i]];
    }
    return prev_coars;
}


coarsen *VCycle(dgraph *igraph, MLGP_option opt, MLGP_info* info)
{
    /*Proceed a vcycle starting from graph*/
    //printf("In Vcycle deb nbpart = %d, lb0 = %f, ub0 = %f, lb1 = %f, ub1 = %f\n", opt.nbPart, opt.lb[0], opt.ub[0], opt.lb[1], opt.ub[1]);
    int s,spmm_row,spmm_col,i,j;
    char vname[100];
    /*for(s=0;s<igraph->nVrtx ; s++){
        if(igraph->vertices[s][0]=='S' && igraph->vertices[s][1]=='P' &&igraph->vertices[s][2]=='M' &&igraph->vertices[s][3]=='M' && igraph->vertices[s][4]==','){
        printf("vcycle %d = %s\n",s,igraph->vertices[s]);
        strcpy(vname,igraph->vertices[s]);
        char * token = strtok(vname,",");
        printf("%s\n", token);
        spmm_row = atoi(strtok(NULL,","));
        printf("%d\n", spmm_row);
        spmm_col = atoi(strtok(NULL,","));
        printf("%d\n", spmm_col);
    }

    }*/
    //exit(1);
    info->timing_global -= u_wseconds();
    //printf("VCycle first vertex = %s\n",igraph->vertices[87]);
    coarsen *current, *head = initializeCoarsen(igraph);

    int crit;
    opt.step = 0;
    idxType old_nbnode = igraph->nVrtx;
    idxType prev_node_count = igraph->nVrtx;

    current = head;

    crit = initializeStoppingCriterion(opt, current);
    static int ctr=0;
//    print_dgraph(*(coars->graph));
//    print_dgraph(*igraph);
    info->timing_coars -= u_wseconds();

    while (stoppingCriterion(opt, &crit, current)) {



        prev_node_count = current->graph->nVrtx;
        opt.step++;
        //printf("%d\n", opt.step);
        info->timing_coars_tab[opt.step] -= u_wseconds();
        info->timing_matching_tab[opt.step] -= u_wseconds();
        computeMatching(opt, current->graph, current->leader);
        info->timing_matching_tab[opt.step] += u_wseconds();
       // printf("\n\ncame back in vcycle after matching\n\n");
      //  print_opt(&opt);
        info->timing_buildgraph_tab[opt.step] -= u_wseconds();
        dgraph* nextgraph = initializeNextGraph(current, opt);
      //  printf("\n\nnextgraph created succssfully\n\n");
      //  printf("\nprev vertex = %d nextgraph vertex = %d\n",current->graph->nVrtx,nextgraph->nVrtx);
        info->nbnodes_coars_tab[opt.step] = nextgraph->nVrtx;
        info->nbedges_coars_tab[opt.step] = nextgraph->nEdge;
        //printf("c step = %d pointeur %p\n", opt.step, (void*) nextgraph);
        coarsen* next = initializeCoarsen(nextgraph);
        //printf("d step = %d pointeur %p\n", opt.step, (void*) next);
        info->timing_buildgraph_tab[opt.step] += u_wseconds();


        current->next_coarsen = next;
		next->previous_coarsen = current;

	    /*char fnn[40];
	    int i;
	    FILE *qq;
	    sprintf(fnn,"matching_%d.txt",ctr);
	    qq=fopen(fnn,"w");
	    for(i=1;i<=current->graph->nVrtx;++i){
	        if(i!=current->leader[i])
	            //fprintf(qq, "%d %d\n",i, current->leader[i]);
                fprintf(qq, "%s ----> %s\n", current->graph->vertices[current->leader[i]-1],current->graph->vertices[i-1]);
	    }
	    fclose(qq);*/
	    ++ctr;
        current = next;
        info->timing_coars_tab[opt.step] += u_wseconds();
        //if ((double) current->graph->nVrtx / (double) old_nbnode > 0.97)
          //  break;





        //Anik add
         if ((double) current->graph->nVrtx == (double) old_nbnode ){
    //            FILE *vcycle_new_graph;

    // //printf("Vcycle after matching nodecount = %d\n",current->graph->nVrtx);
    // vcycle_new_graph = fopen("vcycle_new_graph.dot","w");
    // fprintf(vcycle_new_graph,"digraph {\n");

    //     //printf("inside if block\n");
    //     for (i = 1;i<=current->graph->nVrtx ; i++){
    //         for(j = current->graph->inStart[i] ; j <= current->graph->inEnd[i]; j++){
    //             //printf("%d -> %d;\n",current->graph->in[j],i);
    //             fprintf(vcycle_new_graph,"%d -> %d;\n",current->graph->in[j],i);
    //         }
    //     }
    
    // fprintf(vcycle_new_graph, "}\n" );

    // fclose(vcycle_new_graph);
             break;
         }
		old_nbnode = current->graph->nVrtx;

    }
    info->coars_depth = opt.step;
//    printf("---------------matching loop done!\n");
    info->timing_coars += u_wseconds();
    //printf("ctr = %d\n",ctr);
    //exit(1);
    /*Initial partitioning*/
    info->timing_inipart -= u_wseconds();
    //printf("In Vcycle before ini part nbpart = %d, lb0 = %f, ub0 = %f, lb1 = %f, ub1 = %f\n", opt.nbPart, opt.lb[0], opt.ub[0], opt.lb[1], opt.ub[1]);
    //printf("vertex = %d \n",current->graph->nVrtx);
//    print_info_part(current->graph,current->part,opt);
    //print_opt(&opt);
    initialPartitioning(opt, current, info);
    info->timing_inipart += u_wseconds();

    if (opt.debug > 10)
		{
    		printf("\n\nCall print info part from vcycle\n\n");


			print_info_part(current->graph, current->part, opt);
		}

    if (opt.print > 10){
	    print_graph(current->graph, current->part, opt.file_name_dot, opt.step, 0);
    }

    info->timing_uncoars -= u_wseconds();
    info->timing_uncoars_tab[opt.step] -= u_wseconds();
    info->timing_refinement_tab[opt.step] -= u_wseconds();
    refinementStep(opt, current, info);
    info->timing_refinement_tab[opt.step] += u_wseconds();
    info->timing_uncoars_tab[opt.step] += u_wseconds();

    /*Uncoarsening*/
    if (opt.debug > 10)
	    printf("Step\tGraph Size\tNb Egde\tEdge cut\tNb comm\n");
    opt.step--;
    for ( ; current->previous_coarsen;  opt.step--) {
        info->timing_uncoars_tab[opt.step] -= u_wseconds();
        //printf("in loop\n");
        coarsen *prev = current;
		
		if (opt.debug > 10){
		    ecType edgecut = edgeCut(current->graph, current->part);
		    ecType nbcomm = nbCommunications(current->graph, current->part);
		    printf("%d\t%d\t%d\t%d\t%d\n", opt.step, current->graph->nVrtx, current->graph->nEdge, (int) edgecut, nbcomm);
		}

        info->timing_project_tab[opt.step] -= u_wseconds();
        current = projectBack(current);
        info->timing_project_tab[opt.step] += u_wseconds();

        if (opt.debug > 10)
		    print_info_part(current->graph, current->part, opt);

        info->timing_refinement_tab[opt.step] -= u_wseconds();
        refinementStep(opt, current, info);
        info->timing_refinement_tab[opt.step] += u_wseconds();

        //printf("refinementStep done\n");

        freeCoarsen(prev);
		prev = (coarsen*) NULL;
        info->timing_uncoars_tab[opt.step] += u_wseconds();

        /*
      printf("Part:\n");
      for (int j = 0; j < current->graph->nVrtx; j++)
          if ((current->part[j] != 0)&&(current->part[j] != 1))
          printf("%d ", current->part[j]);
      printf("\n");
      */
    }

    info->timing_uncoars += u_wseconds();
    info->timing_global += u_wseconds();


    if (opt.debug > 10){
		ecType edgecut = edgeCut(head->graph, head->part);
		ecType nbcomm = nbCommunications(head->graph, head->part);
		printf("%d\t%d\t%d\t%d\t%d\t%f\t%f\t%f\n", 0, head->graph->nVrtx, head->graph->nEdge, (int) edgecut, (int) nbcomm, info->timing_coars, info->timing_inipart, info->timing_uncoars);
    }
    if (opt.print > 0) {
        info->finalec = edgeCut(head->graph, head->part);
    }
        /*
        printf("Part 0:\n");
        for (int j = 1; j <= current->graph->nVrtx; j++)
            if (current->part[j] == 0)
            printf("%d ", j);
        printf("\nEND\n");
        printf("Part 1:\n");
        for (int j = 1; j <= current->graph->nVrtx; j++)
            if (current->part[j] == 1)
            printf("%d ", j);
        printf("\nEND\n");
        */


	return head;

} 
