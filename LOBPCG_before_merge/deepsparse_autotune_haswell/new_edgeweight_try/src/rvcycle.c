#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "rvcycle.h"
#include "vcycle.h"
#include "option.h"
#include "utils.h"
#include "debug.h"
#include "matching.h"
#include "refinement.h"
#include "infoPart.h"
#include "dgraph.h"

#include "rMLGP.h"

//#include "../../lobpcg_gen_graph_v28.h"
//#include "../../lib_dag.h"
//#include "../../lobpcg_gen_graph_v29.h"

rcoarsen *initializeRCoarsen(coarsen* icoars)
{
    rcoarsen * rcoars = (rcoarsen*) malloc(sizeof(rcoarsen));
    rcoars->coars = icoars;
    rcoars->previous_rcoarsen = NULL;
    rcoars->next_rcoarsen1 = NULL;
    rcoars->next_rcoarsen2 = NULL;
    rcoars->previous_index = (idxType * ) malloc(sizeof(idxType) * (icoars->graph->nVrtx +1));
    rcoars->next_index = (idxType * ) malloc(sizeof(idxType) * (icoars->graph->nVrtx +1));
    return rcoars;
}


void freeRCoarsen(rcoarsen *rcoars)
{
    freeCoarsen(rcoars->coars);
    free(rcoars->previous_index);
    free(rcoars->next_index);
    free(rcoars);
}

void freeRCoarsenHeader(rcoarsen *rcoars)
{
    freeCoarsenHeader(rcoars->coars);
    free(rcoars->previous_index);
    free(rcoars->next_index);
/*    free(rcoars);*/
}


void initializeNextRCoarsen(rcoarsen* rcoars, rcoarsen* rcoars1, rcoarsen* rcoars2)
{
  //printf("Begin split\n");
    dgraph* graph = rcoars->coars->graph;
    dgraph* graph1 = rcoars1->coars->graph;
    dgraph* graph2 = rcoars2->coars->graph;
    idxType i,j;
    idxType* part = rcoars->coars->part;
    idxType nVrtx = rcoars->coars->graph->nVrtx;
    idxType nEdge = rcoars->coars->graph->nEdge;
    idxType nVrtx1 = 0, nVrtx2 = 0, nEdge1 = 0, nEdge2 = 0;

    idxType* onetograph = rcoars1->previous_index;
    idxType* twotograph = rcoars2->previous_index;
    idxType* onetoedge = (idxType*) malloc((nEdge+1)*sizeof(idxType));
    idxType* twotoedge = (idxType*) malloc((nEdge+1)*sizeof(idxType));
    rcoars->next_index = (idxType*) malloc((nVrtx+1)*sizeof(idxType));
    idxType* graphto12 = rcoars->next_index;
    for (i=1; i<=nVrtx; i++){
	if (part[i] == 0){
	    onetograph[++nVrtx1] = i;
	    graphto12[i] = nVrtx1;
	}
	if (part[i] == 1){
	    twotograph[++nVrtx2] = i;
	    graphto12[i] = nVrtx2;
	} 
    }
    graph1->nVrtx = nVrtx1;
    graph2->nVrtx = nVrtx2;
    graph1->inStart = (idxType*) malloc((nVrtx1+2)*sizeof(idxType));
    graph1->inEnd = (idxType*) malloc((nVrtx1+2)*sizeof(idxType));
    graph1->outStart = (idxType*) malloc((nVrtx1+2)*sizeof(idxType));
    graph1->outEnd = (idxType*) malloc((nVrtx1+2)*sizeof(idxType));
    graph1->hollow  = (int * ) calloc(nVrtx1 + 2, sizeof(int));
    idxType* in1 = (idxType*) malloc((nEdge+1)*sizeof(idxType));
    graph2->inStart = (idxType*) malloc((nVrtx2+2)*sizeof(idxType));
    graph2->inEnd = (idxType*) malloc((nVrtx2+2)*sizeof(idxType));
    graph2->outStart = (idxType*) malloc((nVrtx2+2)*sizeof(idxType));
    graph2->outEnd = (idxType*) malloc((nVrtx2+2)*sizeof(idxType));
    graph2->hollow  = (int * ) calloc(nVrtx2 + 2, sizeof(int));
    idxType* in2 = (idxType*) malloc((nEdge+1)*sizeof(idxType));
    for (i=1; i<=nVrtx; i++){
	if (part[i] == 0){
	    graph1->inStart[graphto12[i]] = nEdge1;
	    if (i > 0)
		graph1->inEnd[graphto12[i]-1] = nEdge1-1;
	}
	if (part[i] == 1){
	    graph2->inStart[graphto12[i]] = nEdge2;
	    if (i > 0)
		graph2->inEnd[graphto12[i]-1] = nEdge2-1;
	}
	for (j=graph->inStart[i]; j<=graph->inEnd[i]; j++){
	    idxType innode = graph->in[j];
	    if ((part[i] == 0)&&(part[innode] == 0)){
		in1[++nEdge1] = graphto12[innode];
		onetoedge[nEdge1] = j;
	    }
	    if ((part[i] == 1)&&(part[innode] == 1)){
		in2[++nEdge2] = graphto12[innode];
		twotoedge[nEdge2] = j;
	    }
	}
    }
    graph1->nEdge = nEdge1;
    graph2->nEdge = nEdge2;
    graph1->inEnd[nVrtx1] = nEdge1-1;
    graph2->inEnd[nVrtx2] = nEdge2-1;
    graph1->inStart[nVrtx1+1] = nEdge1;
    graph1->inEnd[nVrtx1+1] = nEdge1-1;
    graph2->inStart[nVrtx2+1] = nEdge2;
    graph2->inEnd[nVrtx2+1] = nEdge2-1;
    graph1->in = (idxType*) malloc((nEdge1+1)*sizeof(idxType));
    graph2->in = (idxType*) malloc((nEdge2+1)*sizeof(idxType));
    graph1->out = (idxType*) malloc((nEdge1+1)*sizeof(idxType));
    graph2->out = (idxType*) malloc((nEdge2+1)*sizeof(idxType));
    for (i=1; i<=nEdge1; i++)
	graph1->in[i] = in1[i];
    for (i=1; i<=nEdge2; i++)
	graph2->in[i] = in2[i];
    graph1->vw = (idxType*) malloc((nVrtx1+1)*sizeof(idxType));
    graph2->vw = (idxType*) malloc((nVrtx2+1)*sizeof(idxType));
    graph1->totvw = 0;
    for (i=1; i<=nVrtx1; i++){
	graph1->vw[i] = graph->vw[onetograph[i]];
	graph1->totvw += graph1->vw[i];
    } 
    graph2->totvw = 0;
    for (i=1; i<=nVrtx2; i++){
	graph2->vw[i] = graph->vw[twotograph[i]];
	graph2->totvw += graph2->vw[i];
    }
    graph1->totec = 0;
    graph1->ecIn = (ecType*) malloc((nEdge1+1)*sizeof(ecType));
    graph1->ecOut = (ecType*) malloc((nEdge1+1)*sizeof(ecType));
    for (i=1; i<=nEdge1; i++){
	graph1->ecIn[i] = graph->ecIn[onetoedge[i]];
	graph1->totec += graph1->ecIn[i];
    }
    graph2->totec = 0;
    graph2->ecIn = (ecType*) malloc((nEdge2+1)*sizeof(ecType));
    graph2->ecOut = (ecType*) malloc((nEdge2+1)*sizeof(ecType));
    for (i=1; i<=nEdge2; i++){
	graph2->ecIn[i] = graph->ecIn[twotoedge[i]];
	graph1->totec += graph2->ecIn[i];
    }
    graph1->frmt = graph->frmt;
    graph2->frmt = graph->frmt;
    free(in1);
    free(in2);
    free(onetoedge);
    free(twotoedge);
    fillOutFromIn(graph1);
    fillOutFromIn(graph2);
    //printf("End split\n");
}

void extractPartition(rcoarsen *rcoars, idxType* part)
{
    int i, icurrent, p;
    rcoarsen *current;
    for (i=1; i<= rcoars->coars->graph->nVrtx; i++){
	p = 0;
	icurrent = i;
	while (current){
	    p *= 2;
	    p += rcoars->coars->part[icurrent];
	    icurrent = rcoars->next_index[icurrent];
	    if (rcoars->coars->part[icurrent] == 0)
		current = rcoars->next_rcoarsen1;
	    if (rcoars->coars->part[icurrent] == 1)
		current = rcoars->next_rcoarsen2;
	}
	part[i] = p;
    }
}

void splitCoarsenLive(rcoarsen* rcoars, dgraph* G1, dgraph* G2)
{
    //printf("splitCoarsen\n");
    /*Build G1 and G2 and fill rcoars->next_index based on rcoars->coars->part*/
    coarsen* C = rcoars->coars;
    idxType* part = C->part;
    idxType nVrtx = C->graph->nVrtx;
    idxType* inStart = C->graph->inStart;
    idxType* inEnd = C->graph->inEnd;
    idxType* outStart = C->graph->outStart;
    idxType* outEnd = C->graph->outEnd;
    idxType* next_index = rcoars->next_index;
    dgraph* graph = C->graph;
    idxType *weightmarker1,*weightmarker2;
    weightmarker1 = (idxType*) calloc ((nVrtx + 2),sizeof(idxType));
    weightmarker2 = (idxType*) calloc ((nVrtx + 2),sizeof(idxType));
    int* both = (int*) calloc ((nVrtx + 2),sizeof(int));
    idxType nVrtx1 = 1, nVrtx2 = 1;
    idxType nEdge1 = 0, nEdge2 = 0;
    idxType i,j;
    int degree;
    //idxType hin1=0,hin2=0;
    //idxType hout1=0,hout2=0;
    /*Fill rcoars->next_index and compute nVrtx1, nVrtx2, nEdge1 and nEdge2*/
    for (i=1; i <= nVrtx; i++){
        if( part[i] == 0 ){
            for (j=inStart[i]; j<=inEnd[i]; j++){
                if(part[graph->in[j]]==1){
                    both[i] = 1;
                    break;
                }
            }
            for (j=outStart[i]; j<=outEnd[i]; j++){
                if(part[graph->out[j]]==1){
                    both[i] = 1;
                    break;
               }
            }
        }
        else {
            for (j=inStart[i]; j<=inEnd[i]; j++){
                if(part[graph->in[j]]==0){
                    both[i]=1;
                    break;
                }
            }

            for (j=outStart[i]; j<=outEnd[i]; j++){
                if(part[graph->out[j]]==0){
                    both[i]=1;
                    break;
                }
            }
        }
    }
    for (i=1; i <= nVrtx; i++){
        if(both[i]==1){
            if(part[i]==0)
                next_index[i]=nVrtx1;
            else
                next_index[i]=nVrtx2;

            weightmarker1[i]=nVrtx1;
            weightmarker2[i]=nVrtx2;
            nVrtx1++;
            nVrtx2++;
        }
        else{
            if(part[i]==0){
                next_index[i]=nVrtx1;
                weightmarker1[i]=nVrtx1;
                nVrtx1++;
            }
            else{ //part[i]==1
                next_index[i]=nVrtx2;
                weightmarker2[i]=nVrtx2;
                nVrtx2++;
            }
        }
    }
    for (i=1; i <= nVrtx; i++){
        for (j=inStart[i]; j<=inEnd[i]; j++){
    
            if (weightmarker1[i]>0 && weightmarker1[graph->in[j]]>0)
                if (part[i]!=1 || part[graph->in[j]]!=1)
                    nEdge1++;
            if (weightmarker2[i]>0 && weightmarker2[graph->in[j]]>0)
                if (part[i]!=0 || part[graph->in[j]]!=0)
                    nEdge2++;

    	}
    }
    nVrtx1--;
    nVrtx2--;
    //printf("nVrtx: %d n: %d %d e: %d %d\n",nVrtx, nVrtx1,nVrtx2,nEdge1,nEdge2 );
    //printf("hollow nodes in out1:: in out2:: %d %d %d %d\n",hin1,hout1,hin2,hout2);
    //printf("G1 G2 sizes: %d %d\n",nVrtx1,nVrtx2);
    //printf("Let's allocate everything, nVrtx1 = %d, nEdge1 = %d, nVrtx2 = %d, nEdge2 = %d\n", nVrtx1, nEdge1, nVrtx2, nEdge2);
    /*Allocate everything*/
    G1->inStart = (idxType*) malloc (sizeof(idxType) * (nVrtx1 + 2));
    G1->inEnd = (idxType*) malloc (sizeof(idxType) * (nVrtx1 + 2));
    G1->in = (idxType*) malloc (sizeof(idxType) * (nEdge1 + 2));
    G1->outStart = (idxType*) malloc (sizeof(idxType) * (nVrtx1 + 2));
    G1->hollow = (int*) calloc ((nVrtx1 + 2),sizeof(int));
    G1->outEnd = (idxType*) malloc (sizeof(idxType) * (nVrtx1 + 2));
    G1->out = (idxType*) malloc (sizeof(idxType) * (nEdge1 + 2));
    G1->vw = (idxType*) calloc ((nVrtx1 + 2),sizeof(idxType));
    G1->ecIn = (ecType*) malloc (sizeof(ecType) * (nEdge1 + 2));
    G1->ecOut = (ecType*) malloc (sizeof(ecType) * (nEdge1 + 2));
    G2->inStart = (idxType*) malloc (sizeof(idxType) * (nVrtx2 + 2));
    G2->inEnd = (idxType*) malloc (sizeof(idxType) * (nVrtx2 + 2));
    G2->in = (idxType*) malloc (sizeof(idxType) * (nEdge2 + 2));
    G2->outStart = (idxType*) malloc (sizeof(idxType) * (nVrtx2 + 2));
    G2->hollow = (int*) calloc ((nVrtx2 + 2), sizeof(int));
    G2->outEnd = (idxType*) malloc (sizeof(idxType) * (nVrtx2 + 2));
    G2->out = (idxType*) malloc (sizeof(idxType) * (nEdge2 + 2));
    G2->vw = (idxType*) calloc ((nVrtx2 + 2),sizeof(idxType));
    G2->ecIn = (ecType*) malloc (sizeof(ecType) * (nEdge2 + 2));
    G2->ecOut = (ecType*) malloc (sizeof(ecType) * (nEdge2 + 2));

    /*Fill inStart1, inStart2, inEnd1, inEnd2, in1 and in2*/
    
    idxType idx1 = 0, idx2 = 0;

    for (i=1; i <= nVrtx; i++){
        if(both[i]){
            if(part[i]==0){
                G2->hollow[weightmarker2[i]]=1;
            }
            else{
                G1->hollow[weightmarker1[i]]=1;
            }
        }
    }
    for (i=1; i <= nVrtx; i++){
        if(weightmarker1[i]>0){ //part 0 or borh
            G1->inEnd[weightmarker1[i]-1]=idx1-1;
            G1->inStart[weightmarker1[i]]=idx1;
            for(j=inStart[i];j<=inEnd[i];++j){
                if(weightmarker1[graph->in[j]]>0){
                    if (part[i] != 1 || part[graph->in[j]] != 1){
                        G1->in[idx1] = weightmarker1[graph->in[j]];
                        if(part[i]==0 && part[graph->in[j]]==0)
                            G1->ecIn[idx1]=graph->ecIn[j];
                        else
                            G1->ecIn[idx1]=0;
                        ++idx1;
                    }
                }
            }
        }
    }
    for (i=1; i <= nVrtx; i++){
        if(weightmarker2[i]>0){ //part 1 or borh
            G2->inEnd[weightmarker2[i]-1]=idx2-1;
            G2->inStart[weightmarker2[i]]=idx2;
            for(j=inStart[i];j<=inEnd[i];++j){
                if(weightmarker2[graph->in[j]]>0){
                    if (part[i]!=0 || part[graph->in[j]]!=0){
                        G2->in[idx2]=weightmarker2[graph->in[j]];
                        if(part[i]==1 && part[graph->in[j]]==1)
                            G2->ecIn[idx2]=graph->ecIn[j];
                        else
                            G2->ecIn[idx2]=0;
                        ++idx2;
                    }
                }
            }
        }
    }
    //printf("after loop %d %d %d %d\n",idx1,nEdge1,idx2,nEdge2);
    G1->inEnd[nVrtx1] = nEdge1-1;
    G1->inStart[nVrtx1+1] = nEdge1;
    G1->inEnd[nVrtx1+1] = nEdge1-1;
    G2->inEnd[nVrtx2] = nEdge2-1;
    G2->inStart[nVrtx2+1] = nEdge2;
    G2->inEnd[nVrtx2+1] = nEdge2-1;

    G1->maxindegree = 0;
    G2->maxindegree = 0;
    for (i=1; i <= nVrtx; i++) {
        if ((part[i] == 0)||(both[i] == 1)) {
            degree = G1->inEnd[weightmarker1[i]] - G1->inStart[weightmarker1[i]] + 1;
            G1->maxindegree = G1->maxindegree < degree ? degree : G1->maxindegree;
        }
        if ((part[i] == 1)||(both[i] == 1)) {
            degree = G2->inEnd[weightmarker2[i]] - G2->inStart[weightmarker2[i]] + 1;
            G2->maxindegree = G2->maxindegree < degree ? degree : G2->maxindegree;
        }
    }

    vwType G1weight = 0, G2weight = 0;
    /*Fill vw1 and vw2*/
    for (i=1; i<=graph->nVrtx; i++){
        //if (weightmarker1[i] ==0 && weightmarker2[i]==0)
            //printf("big problem\n");
    	if (part[i] == 0){
            //printf("node %d is mapped to p0 node %d  -- p1 node %d\n",i,weightmarker1[i],weightmarker2[i] );
    	    G1->vw[weightmarker1[i]] = C->graph->vw[i];
            G1weight += G1->vw[weightmarker1[i]];
        }
    	else if (part[i] == 1){
            //printf("node %d is mapped to p0 node %d -- p1 node %d\n",i,weightmarker1[i],weightmarker2[i] );
    	    G2->vw[weightmarker2[i]] = C->graph->vw[i];
            G2weight += G2->vw[weightmarker2[i]];
        }
        else{
            //printf("wms: %d %d \n", weightmarker1[i],weightmarker2[i]);
            //printf("weight in G1 G2: %d %d\n",G1->vw[weightmarker1[i]],G2->vw[weightmarker2[i]]);
        }
    }
    //don't assign weights to hollow nodes, calloc makes them zero
    /*
    printf("FIRST IN\n");
    for(i=0;i<nEdge1;++i)
        printf("%d ", G1->in[i]);
    printf("\nFIRST\n");
    for(i=1;i<=nVrtx;++i)
        if (part[i]==0 || both[i])
            printf("i=%d,wm1[i]=%d, (%d)-(%d)\n",i,weightmarker1[i], G1->inStart[weightmarker1[i]],G1->inEnd[weightmarker1[i]]);
    printf("\nSECOND IN\n");
    for(i=0;i<nEdge2;++i)
        printf("%d ", G2->in[i]);
    printf("\nSECOND\n");
    for(i=1;i<=nVrtx;++i)
        if (part[i]==1 || both[i])
            printf("i=%d,wm2[i]=%d, (%d)-(%d)\n",i,weightmarker2[i], G2->inStart[weightmarker2[i]],G2->inEnd[weightmarker2[i]]);
    printf("\n");
    */
    /*Fill G1 and G2*/
    G1->frmt = graph->frmt;
    G1->nVrtx = nVrtx1;
    G1->nEdge = nEdge1;
    G1->totvw = G1weight;
    G2->frmt = graph->frmt;
    G2->nVrtx = nVrtx2;
    G2->nEdge = nEdge2;
    G2->totvw = G2weight;
    fillOutFromIn(G1);
    //printf("first done\n");
    fillOutFromIn(G2);
    //printf("second done\n");
    free(weightmarker2);
    free(weightmarker1);
    free(both);
}

void check(coarsen* C)
{
  if (C->graph->ecIn == NULL)
    printf("!!!ECIN IS NULL!!!\n");
}

void splitCoarsen(rcoarsen* rcoars, dgraph* G1, dgraph* G2, int* part1, int* part2)
{
    /*Build G1 and G2 and fill rcoars->next_index based on rcoars->coars->part*/
    coarsen* C = rcoars->coars;
    idxType* part = C->part;
    idxType nVrtx = C->graph->nVrtx;
    idxType* inStart = C->graph->inStart;
    idxType* inEnd = C->graph->inEnd;
    idxType* in = C->graph->in;
    idxType* next_index = rcoars->next_index;
    dgraph* graph = C->graph;

    idxType nVrtx1 = 1, nVrtx2 = 1;
    idxType nEdge1 = 0, nEdge2 = 0;
    idxType vertexName1=0,vertexName2=0;
    idxType i,j;
    int degree;
    int partin = -1, partout = -1;

    for (i=1; i <= nVrtx; i++){
        for (j=inStart[i]; j<=inEnd[i]; j++) {
            if (part[i] != part[C->graph->in[j]]){
                partin = part[C->graph->in[j]];
                partout = part[i];
                break;
            }
        }
        if (partin != -1)
            break;
    }
    if ((partin == -1)||(partout == -1)){
      partin = 0;
      partout = 1;
    }
    //printf("\npartin = %d partout = %d\n",partin,partout);
    *part1 = partin;
    *part2 = partout;

    /*Fill rcoars->next_index and compute nVrtx1, nVrtx2, nEdge1 and nEdge2*/
    for (i=1; i <= nVrtx; i++){
	    if (part[i] == partin){
	        next_index[i] = nVrtx1;
	   //     printf("\nchanged index of %d = %d\n",i,nVrtx1);
	        nVrtx1++;
	    }
	    if (part[i] == partout){
	        next_index[i] = nVrtx2;
	   //     printf("\nchanged index of %d = %d\n",i,nVrtx2);
	        nVrtx2++;
	    }
	    for (j=inStart[i]; j<=inEnd[i]; j++){
	        if ((part[i] == partin)&&(part[in[j]] == partin)){
		        nEdge1++;
		  //      printf("\nwe are adding %d and %d in part 1\n ",i,in[j]);
	        }
	        if ((part[i] == partout)&&(part[in[j]] == partout)){
		        nEdge2++;
		  //      printf("\nwe are adding %d and %d in part 2\n ",i,in[j]);
	        }
	    }
    }
    nVrtx1--;
    nVrtx2--;
    
    /*Fill G1 and G2*/
    G1->frmt = graph->frmt;
    G1->nVrtx = nVrtx1;
    G1->nEdge = nEdge1;
    G2->frmt = graph->frmt;
    G2->nVrtx = nVrtx2;
    G2->nEdge = nEdge2;

    // vertex name allocation
    G1->vertices = (char**)malloc(sizeof(char*)*(nVrtx1+1));
    for(i = 0;i<nVrtx1;++i){
        G1->vertices[i] = (char*)malloc(100*sizeof(char));
    }

    G2->vertices = (char**)malloc(sizeof(char*)*(nVrtx2+1));
    for(i = 0;i<nVrtx2;++i){
        G2->vertices[i] = (char*)malloc(100*sizeof(char));
    }

    for(i=1;i<=nVrtx;i++){
       // printf("next_index[%d] = %d\n",i,next_index[i]);
    }

    for (i=1; i <= nVrtx; i++){
        if (part[i] == partin){
            strcpy(G1->vertices[vertexName1],C->graph->vertices[i-1]);
        //    printf("\nG1 %d = %s\n",vertexName1,G1->vertices[vertexName1]);
            vertexName1++;
        }
        if (part[i] == partout){
            strcpy(G2->vertices[vertexName2],C->graph->vertices[i-1]);
        //    printf("\nG2 %d = %s\n",vertexName2,G2->vertices[vertexName2]);
            vertexName2++;
        }
        
    }

    //printf("Let's allocate everything, nVrtx1 = %d, nEdge1 = %d, nVrtx2 = %d, nEdge2 = %d, partin = %d, partout = %d\n", nVrtx1, nEdge1, nVrtx2, nEdge2, partin, partout);
    /*Allocate everything*/
    G1->inStart = (idxType*) malloc (sizeof(idxType) * (nVrtx1 + 2));
    G1->inEnd = (idxType*) malloc (sizeof(idxType) * (nVrtx1 + 2));
    G1->in = (idxType*) malloc (sizeof(idxType) * (nEdge1 + 2));
    G1->outStart = (idxType*) malloc (sizeof(idxType) * (nVrtx1 + 2));
    G1->outEnd = (idxType*) malloc (sizeof(idxType) * (nVrtx1 + 2));
    G1->out = (idxType*) malloc (sizeof(idxType) * (nEdge1 + 2));
    G1->vw = (idxType*) malloc (sizeof(idxType) * (nVrtx1 + 1));
    G1->ecIn = (ecType*) malloc (sizeof(ecType) * (nEdge1 + 2));
    G1->ecOut = (ecType*) malloc (sizeof(ecType) * (nEdge1 + 2));
    G1->hollow  = (int * ) calloc(nVrtx1 + 2, sizeof(int));
    G1->vWeight = (ecType*) malloc (sizeof(ecType) * (nVrtx1 + 1));
//    G1->incoming_edge_weight = (ecType*)calloc((nVrtx1+1),sizeof(ecType));

    G2->inStart = (idxType*) malloc (sizeof(idxType) * (nVrtx2 + 2));
    G2->inEnd = (idxType*) malloc (sizeof(idxType) * (nVrtx2 + 2));
    G2->in = (idxType*) malloc (sizeof(idxType) * (nEdge2 + 2));
    G2->outStart = (idxType*) malloc (sizeof(idxType) * (nVrtx2 + 2));
    G2->outEnd = (idxType*) malloc (sizeof(idxType) * (nVrtx2 + 2));
    G2->out = (idxType*) malloc (sizeof(idxType) * (nEdge2 + 2));
    G2->vw = (idxType*) malloc (sizeof(idxType) * (nVrtx2 + 1));
    G2->ecIn = (ecType*) malloc (sizeof(ecType) * (nEdge2 + 2));
    G2->ecOut = (ecType*) malloc (sizeof(ecType) * (nEdge2 + 2));
    G2->hollow  = (int * ) calloc(nVrtx2 + 2, sizeof(int));
    G2->vWeight = (ecType*) malloc (sizeof(ecType) * (nVrtx2 + 1));
//    G2->incoming_edge_weight = (ecType*)calloc((nVrtx2+1),sizeof(ecType));



    /*Fill inStart1, inStart2, inEnd1, inEnd2, in1 and in2*/
    idxType idx1 = 0, idx2 = 0, outnode = 0;
    ecType weight;
    for (i=1; i <= nVrtx; i++){
	    //printf("Loop %d / %d \n", i, nVrtx);
	    if (part[i] == partin){
	        G1->inStart[next_index[i]] = idx1;
	    //    printf("graph 1 instart[%d] = %d\n",next_index[i],idx1);
	        if (next_index[i] > 0)
		        G1->inEnd[next_index[i]-1] = idx1-1;
        }
	    if (part[i] == partout){
	        G2->inStart[next_index[i]] = idx2;
	    //    printf("graph 2 instart[%d] = %d\n",next_index[i],idx2);
	        if (next_index[i] > 0)
		        G2->inEnd[next_index[i]-1] = idx2-1;
	    }
        G1->totec = 0.0;
        G2->totec = 0.0;
        for (j=inStart[i]; j<=inEnd[i]; j++){

	        outnode = C->graph->in[j];
      //      printf("outnode = %d, idx1 = %d, idx2 = %d\n", outnode, idx1, idx2);
	        weight = C->graph->ecIn[j];
	  //      printf("ecIn[%d] = %lf\n",j,C->graph->ecIn[j]);
	        if ((part[i] == partin)&&(part[outnode] == partin)){
                G1->in[idx1++] = next_index[outnode];
	//	        G1->ecIn[idx1-1] = weight;
//		        printf("G1 ecIn[%d] = %lf\n",idx1-1,G1->ecIn[idx1-1]);
//		        G1->ecOut[idx1-1] = weight;
                G1->totec += G1->ecIn[idx1-1];
	        }
	        if ((part[i] == partout)&&(part[outnode] == partout)){
		        G2->in[idx2++] = next_index[outnode];
	//	        G2->ecIn[idx2-1] = weight;
//		        printf("G2 ecIn[%d] = %lf\n",idx2-1,G2->ecIn[idx2-1]);
//		        G2->ecOut[idx2-1] = weight;
                G2->totec += G2->ecIn[idx2-1];
            }
	    }

    }

    for(i=0;i<nEdge1;++i){
 //   	printf("G1->ecin[%d] = %lf\n",i,G1->ecIn[i]);
    }
    for(i=0;i<nEdge2;++i){
//    	printf("G2->ecin[%d] = %lf\n",i,G2->ecIn[i]);
    }



    G1->inEnd[nVrtx1] = nEdge1-1;
    G1->inStart[nVrtx1+1] = nEdge1;
    G1->inEnd[nVrtx1+1] = nEdge1-1;
    G2->inEnd[nVrtx2] = nEdge2-1;
    G2->inStart[nVrtx2+1] = nEdge2;
    G2->inEnd[nVrtx2+1] = nEdge2-1;




/*    ///////////print instart inend in of G1 and G2////////
    for(i=0;i<nVrtx1+2;++i){
    	printf("G1->inStart[%d] = %d\n",i,G1->inStart[i]);
    }
    for(i=0;i<nVrtx1+2;++i){
    	printf("G1->inEnd[%d] = %d\n",i,G1->inEnd[i]);
    }
    for(i=0;i<nEdge1;++i){
    	printf("G1->in[%d] = %d\n",i,G1->in[i]);
    }*/




    G1->maxindegree = 0;
    G2->maxindegree = 0;
    for (i=1; i <= nVrtx; i++) {
        if (part[i] == partin) {
            degree = G1->inEnd[next_index[i]] - G1->inStart[next_index[i]] + 1;
            G1->maxindegree = G1->maxindegree < degree ? degree : G1->maxindegree;
        }
        if (part[i] == partout) {
            degree = G2->inEnd[next_index[i]] - G2->inStart[next_index[i]] + 1;
            G2->maxindegree = G2->maxindegree < degree ? degree : G2->maxindegree;
        }
    }

    /*Fill vw1 and vw2*/
    G1->totvw = 0.0;
    G2->totvw = 0.0;
    for (i=1; i<=graph->nVrtx; i++){
	    if (part[i] == partin) {
            G1->vw[next_index[i]] = C->graph->vw[i];
            G1->vWeight[next_index[i]] = C->graph->vWeight[i];
            G1->totvw += G1->vw[next_index[i]];
        }
        if (part[i] == partout) {
            G2->vw[next_index[i]] = C->graph->vw[i];
            G2->vWeight[next_index[i]] = C->graph->vWeight[i];
            G2->totvw += G2->vw[next_index[i]];
        }
    }    



//    printf("graph 1 instart[1] =%d\n",G1->inStart[1]);
//    printf("graph 2 instart[1] =%d\n",G2->inStart[1]);
//    for (i = 0; i <= G2->nVrtx+1; i++)
//        printf("G2 prev instart[%d] = %d\n",i,G2->inStart[i]);

//    for (i = 0; i <= G2->nVrtx+1; i++)
//        printf("G2 instart[%d] = %d\n",i,G2->inStart[i]);
//    printf("graph 2 instart[1] =%d\n",G2->inStart[1]);

    for(i=0;i<G1->nEdge;++i){
    //	printf("G1->ecin[%d] = %lf\n",i,G1->ecIn[i]);
    }
    for(i=0;i<G2->nEdge;++i){
    //	printf("G2->ecin[%d] = %lf\n",i,G2->ecIn[i]);
    }
    idx1 = 0 ;
    idx2 = 0 ;
    outnode = 0;

    double part_to_part = 0;

    for (i=1; i <= nVrtx; i++){
        //printf("Loop %d / %d \n", i, nVrtx);
 //     printf("\n\nweight loop\n\n");

        G1->totec = 0.0;
        G2->totec = 0.0;

        if ((part[i] == partin)){
  //              G1->incoming_edge_weight[next_index[i]] = graph->incoming_edge_weight[i];
                //printf("G1 %s added %lf\n",G1->vertices[next_index[i]-1], graph->incoming_edge_weight[i]);
            }
        if ((part[i] == partout)){
   //             G2->incoming_edge_weight[next_index[i]] = graph->incoming_edge_weight[i];
                //printf("G2 %s added %lf\n",G2->vertices[next_index[i]-1], graph->incoming_edge_weight[i]);
            }
        for (j=inStart[i]; j<=inEnd[i]; j++){

            outnode = C->graph->in[j];
      //      printf("outnode = %d, idx1 = %d, idx2 = %d\n", outnode, idx1, idx2);
            weight = C->graph->ecIn[j];
      //      printf("ecIn[%d] = %lf\n",j,C->graph->ecIn[j]);
            if ((part[i] == partin)&&(part[outnode] == partin)){
          //      G1->in[idx1++] = next_index[outnode];
                G1->ecIn[idx1++] = weight;
    //          printf("G1 ecIn[%d] = %lf\n",idx1-1,G1->ecIn[idx1-1]);
//              G1->ecOut[idx1-1] = weight;
                G1->totec += G1->ecIn[idx1-1];
            }


            if ((part[i] == partout)&&(part[outnode] == partout)){
        //        G2->in[idx2++] = next_index[outnode];
                G2->ecIn[idx2++] = weight;
//              printf("G2 ecIn[%d] = %lf\n",idx2-1,G2->ecIn[idx2-1]);
//              G2->ecOut[idx2-1] = weight;
                G2->totec += G2->ecIn[idx2-1];
            }
            if ((part[i] == partout)&&(part[outnode] == partin)){
     //           G2->incoming_edge_weight[next_index[i]] +=  weight;
                part_to_part += weight;
                //printf("adding %s-->%s (%lf)\n", graph->vertices[outnode-1],G2->vertices[next_index[i]-1],weight);
            }
        }


    }

    printf("\n\n");
    double inc_G1=0, inc_G2=0;

  //  for(i=1; i <= G1->nVrtx ; i++)
  //      inc_G1 += G1->incoming_edge_weight[i];

 //   for(i=1; i <= G2->nVrtx ; i++)
   //     inc_G2 += G2->incoming_edge_weight[i];

    //printf("G1 vertex %d incomin edge weight %lf\n",G1->nVrtx,inc_G1);
    //printf("G2 vertex %d incomin edge weight %lf\n",G2->nVrtx,inc_G2);

    //printf("\n\npart to part added = %lf\n\n", part_to_part);


    fillOutFromIn(G2);
    fillOutFromIn(G1);




    for(i=0;i<nEdge1;++i){
    	//printf("G1->ecin[%d] = %lf\n",i,G1->ecIn[i]);
    }
    for(i=0;i<nEdge2;++i){
    	//printf("G2->ecin[%d] = %lf\n",i,G2->ecIn[i]);
    }
}



rcoarsen* rVCycle(dgraph *igraph, MLGP_option opt, rMLGP_info* info)
{


    info->timing_global -= u_wseconds();

    double my_weight = 0 ;

    double incoming_edge_weight_in_part_time;

    double spmm_memory,xy_memory,xty_memory,setzero_memory;



    info->depth++;
  /*part is allocated and this function fills it*/
    /*Warning: Here opt.ub is the targeted value for each part*/

    printf("\n\ndepth = %d\n\n",info->depth);
    int nbpart = opt.nbPart;
    ecType ratio = opt.ratio;
    ecType* targeted_weight = (ecType*)malloc((opt.nbPart)*sizeof(ecType));
    ecType* lb_save = (ecType*)malloc((opt.nbPart)*sizeof(ecType));
    int nVrtx = igraph->nVrtx, partin = -1, partout = -1;
    idxType i,j;

    ecType incoming_edge_weight_in_part = 0;
    ecType task_memory = 0;
    ecType total_memory = 0;

    for (i=0; i < opt.nbPart; i++){
        targeted_weight[i] = opt.ub[i];
        lb_save[i] = opt.lb[i];
    }

    if (opt.debug > 0){
      //for (i=0; i<info->depth; i++)
	       //printf("@");
        printf("In rVcycle nVrtx = %d, nbpart = %d, lb0 = %f, ub0 = %f, lb1 = %f, ub1 = %f\n", nVrtx, opt.nbPart, opt.lb[0], opt.ub[0], opt.lb[1], opt.ub[1]);
    }
    

    printf("vweight of this part = %lf vertex count = %d\n",my_weight,igraph->nVrtx);

    idxType* my_toporder = (idxType*)malloc((igraph->nVrtx+1)*sizeof(idxType));
    DFSsort(igraph,my_toporder);

    idxType* my_bfs_traversal = (idxType*)malloc((igraph->nVrtx+2)*sizeof(idxType));
    my_BFS(igraph,my_bfs_traversal);


    idxType* my_dfs_traversal = (idxType*)malloc((igraph->nVrtx+1)*sizeof(idxType));
    my_DFS(igraph,my_dfs_traversal);


     incoming_edge_weight_in_part_time = u_wseconds();
    for(i=1;i<=nVrtx;++i){
        if(igraph->vertices[my_dfs_traversal[i]-1][0] != '_') {

        char *task ;
        task = strtok(igraph->vertices[my_dfs_traversal[i]-1],",");
        printf("%s\n",task);
    }
    	for(j=igraph->inStart[my_dfs_traversal[i]];j<=igraph->inEnd[my_dfs_traversal[i]];++j){
    //		printf("vertex = %d j = %d incoming edge weight = %lf\n",i,j,igraph->ecIn[j]);
    		incoming_edge_weight_in_part += igraph->ecIn[j];

    	}
    }
    incoming_edge_weight_in_part_time = u_wseconds() - incoming_edge_weight_in_part_time;
   // printf("incoming_edge_weight_in_part_time = %lf\n",incoming_edge_weight_in_part_time);

    /*for(i=1;i<=nVrtx;++i){
    	for(j=igraph->outStart[i];j<=igraph->outEnd[i];++j){
    //		printf("vertex = %d j = %d outgoing edge weight = %lf\n",i,j,igraph->ecOut[j]);
    	//	incoming_edge_weight_in_part += igraph->ecIn[j];

    	}
    }*/

    printf("incoming edge weight in this part = %lf\n",incoming_edge_weight_in_part);



   /* for(i = 1 ; i <= igraph->nVrtx ; ++i){
    	my_weight += igraph->vw[my_dfs_traversal[i]];
    //	printf("vw[%d] = %lf\n",i,igraph->vw[i]);
    }*/

//dfs traversal done, now we have the traversal list
//now we will parse the node names and calculate memories

    printf("my dfs for traversal = %d\n",igraph->nVrtx);
    char* task = (char*)malloc(50*sizeof(char));
    for(i = 1 ; i <= igraph->nVrtx ; ++i){

        //if(igraph->nVrtx >= 274600 && igraph->nVrtx < 300000)
    	//   printf("%d = %s\n",my_dfs_traversal[i],igraph->vertices[my_dfs_traversal[i]-1]);
        //printf("trav[%d] = %s\n",my_dfs_traversal[i]-1,igraph->vertices[my_dfs_traversal[i]-1]);
        



       /* char* underscore = strchr(igraph->vertices[my_dfs_traversal[i]-1],'_');
        int under_index = (int)(underscore-igraph->vertices[my_dfs_traversal[i]-1]);
        char* par = strchr(igraph->vertices[my_dfs_traversal[i]-1],'(');
        int par_index = (int)(par-igraph->vertices[my_dfs_traversal[i]-1]);

    //    printf("under_index = %d par_index = %d\n",under_index,par_index);
        if(under_index <= 0)continue;

        int copy_index = 0;
        under_index++;
        while(igraph->vertices[my_dfs_traversal[i]-1][under_index] != '('){
            task[copy_index++] = igraph->vertices[my_dfs_traversal[i]-1][under_index];
            under_index++;

        }

        task[copy_index] = '\0';
    //    printf("%s\n",task );

        //internal memory values... now dummy
        //has to be replaced by something meaningful

        if(!strcmp(task,"XTY")){
            task_memory += 10;

        }
        else if(!strcmp(task,"XY")){
            task_memory += 10;

        }
        else if(!strcmp(task,"REDUCTION")){

            task_memory += 10;
        }
        else if(!strcmp(task,"DLACPY")){
            task_memory += 10;
        }
        else if(!strcmp(task,"ADD")){
            task_memory += 10;
        }
        else if(!strcmp(task,"INV")){
            task_memory += 10;
        }
        else if(!strcmp(task,"GET")){
            task_memory += 10;
        }
        else if(!strcmp(task,"SPMM")){
            task_memory += 10;
        }
        else if(!strcmp(task,"SETZERO")){
            task_memory += 10;
        }
        else if(!strcmp(task,"UPDATE")){
            task_memory += 10;
        }
        else if(!strcmp(task,"CHOL")){
            task_memory += 10;
        }
        else if(!strcmp(task,"COLLAPSE")){
            task_memory += 10;
        }
        else if(!strcmp(task,"SUM")){
            task_memory += 10;
        }
        else if(!strcmp(task,"SQRT")){
            task_memory += 10;
        }
        else if(!strcmp(task,"SUB")){
            task_memory += 10;
        }
        else if(!strcmp(task,"MULT")){
            task_memory += 10;
        }
        //more to follow

*/
        

        // free allocated memory
       

    }
    free(task);
    free(my_toporder);
    free(my_bfs_traversal);
    free(my_dfs_traversal);

    //printf("\n");

    printf("task_memory = %lf\n",task_memory);

    total_memory = incoming_edge_weight_in_part+task_memory;

    printf("rVCycle first vertex = %s\n",igraph->vertices[0]);

    printf("\n\n\n\ntotal memory needed in this partition = %lf\n\n\n",total_memory);







    coarsen* C;
 //   if(my_weight<600)
 //   if(my_weight<300)
 //   if(my_weight>4 && my_weight < 6)
 //   if(my_weight>300 && my_weight < 590)
 //   if(incoming_edge_weight_in_part > 100 && incoming_edge_weight_in_part < 300)
 //   if(total_memory < 15000000000)
     if(total_memory < 41943040) //40 MB

 //  if(total_memory < 536870912) //512 MB
 //   if(total_memory < 1073741824) // 1GB

    {


    	C = initializeCoarsen(igraph);
    	printf("coarse vertex count = %d\n",C->graph->nVrtx);
    	for(i = 1 ; i<= C->graph->nVrtx ; ++i){
    		C->new_index[i] = i;
    		C->part[i] = 0;
    		C->leader[i] = 0 ;
    	}
        C->nbpart = 1;
        info->depth--;

    	rcoarsen* rcoarse = initializeRCoarsen(C);
    	return rcoarse;
    }

    if ((nbpart == 1)||(nbpart == 2)){
      //dgraph *G = copyDGraph(igraph);
        printf("First opt\n");//anik comment out
        print_opt(&opt);//anik comment out
        for (i=0; i<opt.nbPart; i++) {
            opt.ub[i] = targeted_weight[i] * opt.ratio;
            opt.lb[i] = ((2.0 - opt.ratio)/opt.ratio) * opt.ub[i];
        }
        printf("Second opt\n");//anik comment out
        print_opt(&opt);//anik comment out

        //divide in the smallest two parts
        C = VCycle(igraph, opt, &info->info);

        for (i=0; i<opt.nbPart; i++) {
            opt.ub[i] = targeted_weight[i] * opt.ratio;
            opt.lb[i] = lb_save[i];
        }
        if (nbpart == 2) {
            for (i = 1; i <= nVrtx; i++) {
                for (j = igraph->inStart[i]; j <= igraph->inEnd[i]; j++) {
               // 	printf("\nC->part[%d] = %d C->part[igraph->in[%d] = %d\n",i,C->part[i],j,C->part[igraph->in[j]]);
                    if (C->part[i] != C->part[igraph->in[j]]) {
                        partin = C->part[igraph->in[j]];
                        partout = C->part[i];

                        break;
                    }
                }
                if (partin != -1)
                    break;
            }
	    printf("partin %d partout %d\n", partin, partout);
	    if (partin != -1)
	      for (i = 1; i <= nVrtx; i++) {
                if (C->part[i] == partin)
                    C->part[i] = 0;
                else if (C->part[i] == partout)
                    C->part[i] = 1;
	      }
        }
	free(targeted_weight);
	free(lb_save);
	info->depth--;

        info->timing_global += u_wseconds();
        info->timing_coars = info->info.timing_coars;
        info->timing_inipart = info->info.timing_inipart;
        info->timing_uncoars = info->info.timing_uncoars;
        return initializeRCoarsen(C);
    }

    //First we compute the bissection

    double Gweight_ub1 = 0;  //anik
    double Gweight_ub2 = 0;  //anik

    for (i=0; i<opt.nbPart/2; i++) {
        Gweight_ub1 += opt.ub[i];
        printf("bissection opt.ub[%d] = %lf Gweight ub1 = %lf\n",i,opt.ub[i],Gweight_ub1);
    }
    for (i=opt.nbPart/2; i<opt.nbPart; i++) {
        Gweight_ub2 += opt.ub[i];
        printf("bissection opt.ub[%d] = %lf Gweight ub2 = %lf\n",i,opt.ub[i],Gweight_ub2);
    }
    opt.nbPart = 2;
    opt.ratio = 0.7*(ratio-1) / (log2(nbpart)) + 1;

    opt.ub[0] = Gweight_ub1*opt.ratio;
    opt.lb[0] = ((2.0 - opt.ratio)/opt.ratio) * opt.ub[0];
    opt.ub[1] = Gweight_ub2*opt.ratio;
    opt.lb[1] = ((2.0 - opt.ratio)/opt.ratio) * opt.ub[1];
//    opt.lb[0] = 1;
//    opt.ub[0] = igraph->totvw;

    if (opt.debug > 0){
      //for (i=0; i<info->depth; i++)
	//printf("@");
      printf("In recursive nVrtx = %d, nbpart = %d, lb0 = %f, ub0 = %f, lb1 = %f, ub1 = %f\n", nVrtx, opt.nbPart, opt.lb[0], opt.ub[0], opt.lb[1], opt.ub[1]);
    }
    
    //printf("Start VCycle\n");
    C = VCycle(igraph, opt, &info->info);
    check(C);
    //printf("End VCycle\n");


    printf("\nAfter coarsen graph nodes = %d\n",C->graph->nVrtx);
    for (i=1; i<=C->graph->nVrtx; i++){
//	fprintf(file, "%s = part %d\n",G.vertices[i], (int) rcoars->coars->part[i]);
//	printf("%d = part %d\n",i, (int) C->part[i]);
    }

    int part1, part2;
    rcoarsen *rcoars = initializeRCoarsen(C);
    
    //We build the two subgraphes based on the bissection

    dgraph *G1 = (dgraph*) malloc(sizeof(dgraph));
    dgraph *G2 = (dgraph*) malloc(sizeof(dgraph));
    splitCoarsen(rcoars, G1, G2, &part1, &part2);

    //We compute recursive bissection for G1 and G2

    vwType G1weight = 0;
    for (i=1; i<=G1->nVrtx; i++)
	    G1weight += G1->vw[i];
    vwType G2weight = 0;
    for (i=1; i<=G2->nVrtx; i++)
	    G2weight += G2->vw[i];
    opt.nbPart = nbpart/2;
    //double max_weight = opt.ratio*Gweight/nbPart;
    for (i=0; i<nbpart/2; i++) {
        opt.ub[i] = targeted_weight[i];
    }

    opt.ratio = ratio;
    //opt.ratio = (Gweight_ub1 * ratio) / G1weight;
    //double eca_ub1 = ((ratio-1) * Gweight_ub1)/2;
    //double upratio1 = ((Gweight_ub1/2.+eca_ub1)/G1weight) * opt.nbPart;
    //opt.ratio = upratio1;
//    if(G1weight < 300) return rcoars;
    printf("NOW REC 1 !!!\n");
    printf("G1weight = %f, G2weight = %f, ratio = %f\n", (double) G1weight, (double) G2weight, opt.ratio);
    print_opt(&opt);

    info->rec1 = (rMLGP_info*)malloc (sizeof (struct rMLGP_info));
    initRInfoPart(info->rec1);


    printf("before G1 rvcycle call\n");
    for(i=0;i<=G1->nEdge;i++){
    //	printf("G1 ecIn[%d] = %lf\n",i,G1->ecIn[i]);
    }

    rcoarsen *rcoars1 = rVCycle(G1, opt, info->rec1); ///anik recursive call for G1
    printf("END REC 1 !!!\n");
    print_info_part(rcoars1->coars->graph, rcoars1->coars->part, opt);

    printf("\nAfter coarsen graph 1 nodes = %d\n",rcoars1->coars->graph->nVrtx);
    for (i=1; i<=rcoars1->coars->graph->nVrtx; i++){
//	fprintf(file, "%s = part %d\n",G.vertices[i], (int) rcoars->coars->part[i]);
//	printf("%d = part %d\n",i, (int) rcoars1->coars->part[i]);
    }

    for (i=0; i<nbpart/2; i++) {
        opt.ub[i] = targeted_weight[i+nbpart/2];
    }
    opt.ratio = ratio;
    //opt.ratio = (Gweight_ub2 * ratio) / G2weight;
    //double eca_ub2 = ((ratio-1) * Gweight_ub2)/2;
    //double upratio2 = (Gweight_ub2/2.+eca_ub2/G2weight) * opt.nbPart;
    //opt.ratio = upratio2;

    printf("NOW REC 2 !!!\n");
    printf("G1weight = %f, G2weight = %f, ratio = %f\n", (double) G1weight, (double) G2weight, opt.ratio);
    print_opt(&opt);
    info->rec2 = (rMLGP_info*)malloc (sizeof (struct rMLGP_info));
    initRInfoPart(info->rec2);
    rcoarsen *rcoars2 = rVCycle(G2, opt, info->rec2);   /// anik recursive call for G2
    printf("END REC 2 !!!\n");
    print_info_part(rcoars2->coars->graph, rcoars2->coars->part, opt);

    printf("\nAfter coarsen graph 2 nodes = %d\n",rcoars2->coars->graph->nVrtx);
    for (i=1; i<=rcoars2->coars->graph->nVrtx; i++){
//	fprintf(file, "%s = part %d\n",G.vertices[i], (int) rcoars->coars->part[i]);
//	printf("%d = part %d\n",i, (int) rcoars2->coars->part[i]);
    }
    //We rebuild and return final solution

    rcoars->next_rcoarsen1 = rcoars1;
    rcoars->next_rcoarsen2 = rcoars2;
    rcoars1->previous_rcoarsen = rcoars;
    rcoars2->previous_rcoarsen = rcoars;

    for (i=1; i<=nVrtx; i++){
  //    printf("i  = %d nvrtx = %d ", i, nVrtx);
 //     printf("part %d next_index %d\n", C->part[i], rcoars->next_index[i]);
      if (C->part[i] == part1)
	        rcoars1->previous_index[rcoars->next_index[i]] = i;
	    if (C->part[i] == part2)
	        rcoars2->previous_index[rcoars->next_index[i]] = i;
    }

    for (i=1; i<=nVrtx; i++){
      //printf("%d / %d \t", i, nVrtx);
	    if (C->part[i] == part1) {
	      if (rcoars->next_index[i] > rcoars1->coars->graph->nVrtx){
		//for (i=0; i<info->depth; i++)
		  //printf("@");
		printf("GROS CACA next_index %d > %d nVrtx1\n", rcoars->next_index[i], rcoars1->coars->graph->nVrtx);
	      }
	      //printf("%d %d\t", C->part[i], (int) i);
	      //printf("%d %d\n", rcoars->next_index[i], rcoars1->coars->graph->nVrtx);
            C->part[i] = rcoars1->coars->part[rcoars->next_index[i]];
         //   printf("C->part[%d] = \t%d(rcoars1)\n", i,C->part[i], rcoars1->coars->part[rcoars->next_index[i]]);
        }
	    else {
	      if (rcoars->next_index[i] > rcoars2->coars->graph->nVrtx){
		for (i=0; i<info->depth; i++)
		  printf("@");
	      printf("GROS CACA next_index %d > %d nVrtx2\n", rcoars->next_index[i], rcoars2->coars->graph->nVrtx);
	      }
	      //printf("%d %d\t", C->part[i], (int) i);
	      //printf("%d %d\n", rcoars->next_index[i], rcoars2->coars->graph->nVrtx);
            C->part[i] = (nbpart / 2) + rcoars2->coars->part[rcoars->next_index[i]];
       //     printf("C->part[%d] = \t%d(rcoarse2)\n", i,C->part[i], rcoars2->coars->part[rcoars->next_index[i]]);
            //printf("%d %d\t%d\n", C->part[i], (int) i, rcoars2->coars->part[rcoars->next_index[i]]);
        }
    }

    opt.nbPart = nbpart;
    for (i=0; i<nbpart; i++) {
        opt.ub[i] = targeted_weight[i];
    }
    opt.ratio = ratio;

    //printf("Now free\n");
    free(targeted_weight);
    free(lb_save);
    freeRCoarsen(rcoars1);
    rcoars1 = (rcoarsen*) NULL;
    freeRCoarsen(rcoars2);
    rcoars2 = (rcoarsen*) NULL;
    //printf("Done free\n");
    info->depth--;
    info->timing_global += u_wseconds();
    info->timing_coars = info->rec1->timing_coars + info->rec2->timing_coars;
    info->timing_inipart = info->rec1->timing_inipart + info->rec2->timing_inipart;
    info->timing_uncoars = info->rec1->timing_uncoars + info->rec2->timing_uncoars;

/*    for (i=1; i<=rcoars->coars->graph->nVrtx; i++){
//	fprintf(file, "%s = part %d\n",G.vertices[i], (int) rcoars->coars->part[i]);
	printf("%d = part %d\n",i, (int) rcoars->coars->part[i]);
    }*/

    return rcoars;
}




rcoarsen* rVCycleLive(dgraph *igraph, MLGP_option opt, MLGP_info* info, int *resultingNbPart)
{
  info->depth++;
    //printf("in rVCycle first line\n");
    double ratio = opt.ratio;
    int nVrtx = igraph->nVrtx;
    coarsen* C;
    idxType i;
    if (opt.debug > 0){
      for (i=0; i<info->depth; i++)
	printf("@");
      printf("In rVcycle nVrtx = %d first line\n", nVrtx);
    }
    
    int ls = calcLiveSizeMixMulti(igraph, 10);

    if (opt.debug > 0){
      for (i=0; i<info->depth; i++)
	printf("@");
        printf("Begin rVCycleLive liveSize: %d\n", ls);
    }
	
    if (opt.debug > 10)
        if(ls <= opt.maxliveset){
            printf("ls=%d maxliveset=%d actual size: %d\n",ls,opt.maxliveset,igraph->nVrtx);
        }

    vwType Gweight = 0;
    for (i=1; i<=igraph->nVrtx; i++)
        Gweight += igraph->vw[i];

    if((ls <= opt.maxliveset) || (Gweight <= opt.maxliveset)){
        C = initializeCoarsen(igraph);
        for (i=1; i<=igraph->nVrtx; i++)
            C->part[i] = 0;
        C->nbpart = 1;
        *resultingNbPart = 1;
	info->depth--;
        return initializeRCoarsen(C);
    }

    opt.nbPart = 2;
    opt.ratio = 1.5;
    opt.ub[0] = (Gweight/2.) * opt.ratio;
    opt.ub[1] = (Gweight/2.) * opt.ratio;
    opt.lb[0] = ((2.0 - opt.ratio)/opt.ratio) * opt.ub[0];
    opt.lb[1] = ((2.0 - opt.ratio)/opt.ratio) * opt.ub[1];

    if (opt.debug > 0){
      for (i=0; i<info->depth; i++)
	printf("@");
	   printf("In recursive totvw = %d,nVrtx = %d, nbpart = %d, lb = %f, ub = %f\n",Gweight, nVrtx, opt.nbPart, opt.lb[0], opt.ub[0]);
    }
    
    C = VCycle(igraph, opt, info);

    rcoarsen *rcoars = initializeRCoarsen(C);

    //We build the two subgraphes based on the bissection

    dgraph *G1 = (dgraph*) malloc(sizeof(dgraph));
    dgraph *G2 = (dgraph*) malloc(sizeof(dgraph));
    splitCoarsenLive(rcoars, G1, G2);

    int liveset1 = calcLiveSizeMixMulti(G1,10);
    int liveset2 = calcLiveSizeMixMulti(G2,10);
    int nbrecut = 0;

    //printf("G1 live set %d\n", calcLiveSizeMix(G1));
    //printf("G2 live set %d\n", calcLiveSizeMix(G2));

    switch (opt.live_cut) {
        case LIVE_CUT_RECUT:
	  while ((((liveset1 < opt.maxliveset / 2)&&(liveset2 > opt.maxliveset))||((liveset2 < opt.maxliveset / 2)&&(liveset1 > opt.maxliveset)))&&(nbrecut < 1)){
                if (liveset1 < opt.maxliveset / 2){
                    opt.nbPart = 2;
                    opt.ub[0] = (2*Gweight/3.) * opt.ratio < 0.9 * Gweight ? (2*Gweight/3.) * opt.ratio : 0.9 * Gweight;
                    opt.ub[1] = (Gweight/3.) * opt.ratio;
                    opt.lb[0] = ((2.0 - opt.ratio)/opt.ratio) * opt.ub[0];
		    opt.lb[1] = ((2.0 - opt.ratio)/opt.ratio) * opt.ub[1];

                    C = VCycle(igraph, opt, info);
                    rcoars = initializeRCoarsen(C);
                    splitCoarsenLive(rcoars, G1, G2);
                    liveset1 = calcLiveSizeMixMulti(G1,10);
                    liveset2 = calcLiveSizeMixMulti(G2,10);
                }
                if (liveset2 < opt.maxliveset / 2){
                    opt.nbPart = 2;
                    opt.ub[0] = (Gweight/3.) * opt.ratio;
                    opt.ub[1] = (2*Gweight/3.) * opt.ratio < 0.9 * Gweight ? (2*Gweight/3.) * opt.ratio : 0.9 * Gweight;
                    opt.lb[0] = ((2.0 - opt.ratio)/opt.ratio) * opt.ub[0];
                    opt.lb[1] = ((2.0 - opt.ratio)/opt.ratio) * opt.ub[1];

                    C = VCycle(igraph, opt, info);
                    rcoars = initializeRCoarsen(C);
                    splitCoarsenLive(rcoars, G1, G2);
                    liveset1 = calcLiveSizeMixMulti(G1,10);
                    liveset2 = calcLiveSizeMixMulti(G2,10);
                }
		nbrecut++;
            }
            break;
    }

    vwType G1weight = 0;
    for (i=1; i<=G1->nVrtx; i++)
        G1weight += G1->vw[i];
    vwType G2weight = 0;
    for (i=1; i<=G2->nVrtx; i++)
        G2weight += G2->vw[i];

    if (opt.debug){
      for (i=0; i<info->depth; i++)
	printf("@");
        printf("In recursive G1 size = %d, G1 weight=%d, G2 size = %d, G2 weight = %d\n", G1->nVrtx,G1weight,G2->nVrtx,G2weight);
    }
    
    int dumpnbpart1=-1,dumpnbpart2=-1;
    rcoarsen *rcoars1 = rVCycleLive(G1, opt, info, &dumpnbpart1);

    //printf("first subpart done! size:%d weight: %d\n", G1->nVrtx,G1->totvw);
    //printf("dump part 1: %d\n",dumpnbpart1);

    rcoarsen *rcoars2 = rVCycleLive(G2, opt, info, &dumpnbpart2);

    rcoars->next_rcoarsen1 = rcoars1;
    rcoars->next_rcoarsen2 = rcoars2;
    rcoars1->previous_rcoarsen = rcoars;
    rcoars2->previous_rcoarsen = rcoars;

    for (i=1; i<=nVrtx; i++){
        if (C->part[i] == 0)
            rcoars1->previous_index[rcoars->next_index[i]] = i;
        if (C->part[i] == 1)
            rcoars2->previous_index[rcoars->next_index[i]] = i;
    }

    for (i=1; i<=nVrtx; i++){
        if (C->part[i] == 0)
            C->part[i] = rcoars1->coars->part[rcoars->next_index[i]];
        else
            C->part[i] = dumpnbpart1 + rcoars2->coars->part[rcoars->next_index[i]];
    }

    int nbmerge, livesetpart;
    nbmerge = 0;
    if (opt.live_cut_merge) {
        nbmerge = 0;
        livesetpart = calcLiveSizePartMixMulti(igraph, C->part, dumpnbpart1-1, 10);
        idxType* part_save = (idxType*) malloc(sizeof(idxType) * (igraph->nVrtx+1));
        //printf("liveset before merge %d\n", livesetpart);
        while ((livesetpart < opt.maxliveset)&&(nbmerge < dumpnbpart2)){
            for (i=1; i<=nVrtx; i++){
                part_save[i] = C->part[i];
                if (C->part[i] >= dumpnbpart1)
                    C->part[i] -= 1;
            }
            nbmerge++;
            livesetpart = calcLiveSizePartMixMulti(igraph, C->part, dumpnbpart1-1, 10);
            //printf("liveset after merge %d\n", livesetpart);
        }
        if (nbmerge > 0) {
            nbmerge--;
            for (i = 1; i <= nVrtx; i++)
                C->part[i] = part_save[i];
        }
        if (opt.debug > 1){
	  for (i=0; i<info->depth; i++)
	    printf("@");
            printf("We did %d merge\n", nbmerge);
	}
    }

    //printf("second subpart done! size:%d\n", G2->nVrtx);
    if (opt.debug > 1){
      for (i=0; i<info->depth; i++)
	printf("@");
        printf("dumps: %d %d\n",dumpnbpart1,dumpnbpart2);
    }
    //We rebuild and return final solution
    
    //----------------
    *resultingNbPart = dumpnbpart2 + dumpnbpart1 - nbmerge;
    //----------------

    opt.ratio = ratio;
    info->depth--;

    return rcoars;
}




/////////my_rvCycle//////////////////
rcoarsen* my_rVCycle(dgraph *igraph, MLGP_option opt, rMLGP_info* info,int *resultingNbPart)
{
    //printf("rVCycle first vertex = %s\n",igraph->vertices[0]);

    info->timing_global -= u_wseconds();

    double my_weight = 0 ;

    double incoming_edge_weight_in_part_time;

    double spmm_memory,xy_memory,xty_memory,setzero_memory;



    info->depth++;
  /*part is allocated and this function fills it*/
    /*Warning: Here opt.ub is the targeted value for each part*/


    //printf("main graph vertex = %d\n\n",main_graph.nVrtx);

    //printf("\n\ndepth = %d\n\n",info->depth);
    int nbpart = opt.nbPart;
    ecType ratio = opt.ratio;
    ecType* targeted_weight = (ecType*)malloc((opt.nbPart)*sizeof(ecType));
    ecType* lb_save = (ecType*)malloc((opt.nbPart)*sizeof(ecType));
    int nVrtx = igraph->nVrtx, partin = -1, partout = -1;
    idxType i,j,m;

    ecType incoming_edge_weight_in_part = 0;
    ecType task_memory = 0;
    ecType total_memory = 0;

    for (i=0; i < opt.nbPart; i++){
        targeted_weight[i] = opt.ub[i];
        lb_save[i] = opt.lb[i];
    }

    if (opt.debug > 10){
      //for (i=0; i<info->depth; i++)
           //printf("@");
        printf("In rVcycle nVrtx = %d, nbpart = %d, lb0 = %f, ub0 = %f, lb1 = %f, ub1 = %f\n", nVrtx, opt.nbPart, opt.lb[0], opt.ub[0], opt.lb[1], opt.ub[1]);
    }
    

    //printf("vweight of this part = %lf vertex count = %d\n",my_weight,igraph->nVrtx);



//    idxType* my_toporder = (idxType*)malloc((igraph->nVrtx+1)*sizeof(idxType));
//    DFSsort(igraph,my_toporder);

//    idxType* my_bfs_traversal = (idxType*)malloc((igraph->nVrtx+2)*sizeof(idxType));
//    my_BFS(igraph,my_bfs_traversal);


    idxType* my_dfs_traversal = (idxType*)malloc((igraph->nVrtx+1)*sizeof(idxType));
    my_DFS(igraph,my_dfs_traversal);

    char* input1 = (char*)malloc(200*sizeof(char));
    char* input2 = (char*)malloc(200*sizeof(char));
    char* output = (char*)malloc(200*sizeof(char));

    clear_InOutVariable();



    incoming_edge_weight_in_part_time = u_wseconds();
    double diff_part_weight = 0;

    int map_size = 0;
    char mem_name[100];
    double mem_value = 0;
    for(i=1;i<=nVrtx;++i){
//        get_input_output(igraph->vertices[my_dfs_traversal[i]-1],input1,input2,output);

//        printf("\n  %s(%lf)  %s  %s  %s \n",igraph->vertices[my_dfs_traversal[i]-1],igraph->incoming_edge_weight[my_dfs_traversal[i]], input1,input2,output);
        //map_size = get_internal_map_size(igraph->vertices[my_dfs_traversal[i]-1]);


        map_size = get_inp_mem_size(igraph->vertices[my_dfs_traversal[i]-1]);

        printf("internal map size of %s = %d\n",igraph->vertices[my_dfs_traversal[i]-1],map_size);
        double already_counted = 0;

//        if(strcmp(input1,"null") && search_in_InOutVariable(input1))printf("%s is in the hashtable\n", input1);
//        if(strcmp(input2,"null") && search_in_InOutVariable(input2))printf("%s is in the hashtable\n", input2);
//        if(strcmp(output,"null") && search_in_InOutVariable(output))printf("%s is in the hashtable\n", output);

        char **input_values ;

        input_values = (char**)malloc(sizeof(char*)*(map_size+1));
        for(m = 0;m<=map_size;++m){
            input_values[m] = (char*)malloc(200*sizeof(char));

        }

if(map_size > 0){
            get_all_the_keys_input(igraph->vertices[my_dfs_traversal[i]-1],input_values);
            for(m = 0;m<map_size;++m){
                //printf("keys %s\n",input_values[m]);
                get_incoming_memory_name(igraph->vertices[my_dfs_traversal[i]-1],input_values[m],mem_name);
                mem_value = get_incoming_memory_value(igraph->vertices[my_dfs_traversal[i]-1],input_values[m]);
                if(strcmp(mem_name,"null") && search_in_InOutVariable(mem_name)){
	           // if(igraph->vertices[my_dfs_traversal[i]-1][0] == 'X' && igraph->vertices[my_dfs_traversal[i]-1][1] == 'T' && igraph->vertices[my_dfs_traversal[i]-1][2] == 'Y')
			 printf("%s(%s) ---> %s(%lf) is already in the map\n", mem_name,input_values[m],igraph->vertices[my_dfs_traversal[i]-1],mem_value);
                    already_counted += mem_value;
                }
                else {
	           // if(igraph->vertices[my_dfs_traversal[i]-1][0] == 'X' && igraph->vertices[my_dfs_traversal[i]-1][1] == 'T' && igraph->vertices[my_dfs_traversal[i]-1][2] == 'Y')
                    printf("%s(%s) ---> %s(%lf) adding in the map\n", mem_name,input_values[m], igraph->vertices[my_dfs_traversal[i]-1],mem_value);
                    incoming_edge_weight_in_part += mem_value;
                }
            }
        }

        map_size = get_out_mem_size(igraph->vertices[my_dfs_traversal[i]-1]);

        printf("output map size of %s = %d\n",igraph->vertices[my_dfs_traversal[i]-1],map_size);
        char **output_values ;

        output_values = (char**)malloc(sizeof(char*)*(map_size+1));
        for(m = 0;m<=map_size;++m){
            output_values[m] = (char*)malloc(200*sizeof(char));

        }

        if(map_size > 0){
            get_all_the_keys_output(igraph->vertices[my_dfs_traversal[i]-1],output_values);
            for(m = 0;m<map_size;++m){
                //printf("keys %s\n",input_values[m]);
                get_outgoing_memory_name(igraph->vertices[my_dfs_traversal[i]-1],output_values[m],mem_name);
                mem_value = get_outgoing_memory_value(igraph->vertices[my_dfs_traversal[i]-1],output_values[m]);
                if(strcmp(mem_name,"null") && search_in_InOutVariable(mem_name)){
                    //printf("%s is in the map\n", mem_name);
	           // if(igraph->vertices[my_dfs_traversal[i]-1][0] == 'X' && igraph->vertices[my_dfs_traversal[i]-1][1] == 'T' && igraph->vertices[my_dfs_traversal[i]-1][2] == 'Y')
                    printf("%s(%s) <--- %s(%lf) is already in the map\n", mem_name,output_values[m],igraph->vertices[my_dfs_traversal[i]-1],mem_value);
                    already_counted += mem_value;
                }
                else {
                    //printf("adding %s in the map\n", mem_name);
	            //if(igraph->vertices[my_dfs_traversal[i]-1][0] == 'X' && igraph->vertices[my_dfs_traversal[i]-1][1] == 'T' && igraph->vertices[my_dfs_traversal[i]-1][2] == 'Y')
                    printf("%s(%s) <--- %s(%lf) adding in the map\n", mem_name, output_values[m],igraph->vertices[my_dfs_traversal[i]-1],mem_value);
                    incoming_edge_weight_in_part += mem_value;
                }
            }
        }

        diff_part_weight +=  igraph->incoming_edge_weight[i];

        //get_output_of_a_task(igraph->vertices[my_dfs_traversal[i]-1],output);
        //if(strcmp(output,"null") && search_in_InOutVariable(output))printf("%s is in the hashtable\n", output);


        if(igraph->vertices[my_dfs_traversal[i]-1][0] != '_') {

        char *task ;
    //    task = strtok(igraph->vertices[my_dfs_traversal[i]-1],",");
        //printf("node = %d weight %lf\n",my_dfs_traversal[i],igraph->vWeight[my_dfs_traversal[i]]);
        //task_memory += igraph->vWeight[my_dfs_traversal[i]];
        //printf("%s\n",task);
    }
 /*       for(j=igraph->inStart[my_dfs_traversal[i]];j<=igraph->inEnd[my_dfs_traversal[i]];++j){
//            printf(" %s --> %s %lf\n",igraph->vertices[igraph->in[j]-1],igraph->vertices[my_dfs_traversal[i]-1],igraph->ecIn[j]);
//          get_input_output(igraph->vertices[igraph->in[j]-1],input1,input2,output);
            get_output_of_a_task(igraph->vertices[igraph->in[j]-1],output);
          //printf("%s(%lf) %s %s %s\n",igraph->vertices[igraph->in[j]-1],igraph->ecIn[j], input1,input2,output);

//          printf("\n  \t%s(%lf)  %s  %s  %s \n",igraph->vertices[igraph->in[j]-1],igraph->ecIn[j], input1,input2,output);

            if(strcmp(output,"null") && search_in_InOutVariable(output)){
                //printf("%s is in the hashtable\n", output);
            }

            else{ 
                incoming_edge_weight_in_part += igraph->ecIn[j];
            }

    
        }*/
    }
    incoming_edge_weight_in_part_time = u_wseconds() - incoming_edge_weight_in_part_time;
//    printf("edge weight from diff part = %lf vertex count = %d\n ", diff_part_weight,igraph->nVrtx);
//    printf("incoming_edge_weight_in_part_time = %lf\n",incoming_edge_weight_in_part_time);



    /*for(i=1;i<=nVrtx;++i){
        for(j=igraph->outStart[i];j<=igraph->outEnd[i];++j){
    //      printf("vertex = %d j = %d outgoing edge weight = %lf\n",i,j,igraph->ecOut[j]);
        //  incoming_edge_weight_in_part += igraph->ecIn[j];

        }
    }*/

    //printf("incoming edge weight in this part = %lf\n",incoming_edge_weight_in_part);





    //printf("\n");

    //printf("task_memory = %lf\n",task_memory);

    total_memory = incoming_edge_weight_in_part+task_memory;
	
    printf("\n\nincoming weight = %lf task weight = %lf total memory  = %lf\n\n",incoming_edge_weight_in_part,task_memory,total_memory);




    //printf("rVCycle first vertex = %s\n",igraph->vertices[87]);

//    FILE* fp = fopen("part_terminal.txt","a");
    coarsen* C;
 //   if(my_weight<600)
 //   if(my_weight<300)
 //   if(my_weight>4 && my_weight < 6)
 //   if(my_weight>300 && my_weight < 590)
 //   if(incoming_edge_weight_in_part > 100 && incoming_edge_weight_in_part < 300)
 //   if(total_memory < 15000000000)
 //    if(total_memory <= 44040192) //42 MB for skl 32 rhs

//     if(total_memory <= 36700160) //35 MB for haswell 16/8 rhs
 //    if(total_memory < 26214400) //25 MB for haswell L3 data cache
      if(total_memory < 52428800)  //50MB try with loose bound
//  if(total_memory < 536870912) //512 MB
 //   if(total_memory < 1073741824) // 1GB
//if(total_memory < 3221225472) // 3GB ->3221225472 || MatA100 -> 5000

    {


  //      fprintf(fp, "%lf %lf\n", incoming_edge_weight_in_part,task_memory);
        C = initializeCoarsen(igraph);
        //printf("coarse vertex count = %d\n",C->graph->nVrtx);
        for(i = 1 ; i<= C->graph->nVrtx ; ++i){
            //C->new_index[i] = i;
            C->part[i] = 0;
            //C->leader[i] = 0 ;
        }
        C->nbpart = 1;
        info->depth--;
        *resultingNbPart = 1;

        rcoarsen* rcoarse = initializeRCoarsen(C);
    //    fclose(fp);
        return rcoarse;
}

    if ((nbpart == 1)||(nbpart == 2)){
      //dgraph *G = copyDGraph(igraph);
        //printf("First opt\n");//anik comment out
        //print_opt(&opt);//anik comment out
        for (i=0; i<opt.nbPart; i++) {
            opt.ub[i] = targeted_weight[i] * opt.ratio;
            opt.lb[i] = ((2.0 - opt.ratio)/opt.ratio) * opt.ub[i];
        }
        //printf("Second opt\n");//anik comment out
        //print_opt(&opt);//anik comment out

        //divide in the smallest two parts
        C = VCycle(igraph, opt, &info->info);

        for (i=0; i<opt.nbPart; i++) {
            opt.ub[i] = targeted_weight[i] * opt.ratio;
            opt.lb[i] = lb_save[i];
        }
        if (nbpart == 2) {
            for (i = 1; i <= nVrtx; i++) {
                for (j = igraph->inStart[i]; j <= igraph->inEnd[i]; j++) {
               //   printf("\nC->part[%d] = %d C->part[igraph->in[%d] = %d\n",i,C->part[i],j,C->part[igraph->in[j]]);
                    if (C->part[i] != C->part[igraph->in[j]]) {
                        partin = C->part[igraph->in[j]];
                        partout = C->part[i];

                        break;
                    }
                }
                if (partin != -1)
                    break;
            }
        //printf("partin %d partout %d\n", partin, partout);
        if (partin != -1)
          for (i = 1; i <= nVrtx; i++) {
                if (C->part[i] == partin)
                    C->part[i] = 0;
                else if (C->part[i] == partout)
                    C->part[i] = 1;
          }
        }
    free(targeted_weight);
    free(lb_save);
    info->depth--;

    *resultingNbPart = 2;
        info->timing_global += u_wseconds();
        info->timing_coars = info->info.timing_coars;
        info->timing_inipart = info->info.timing_inipart;
        info->timing_uncoars = info->info.timing_uncoars;
        return initializeRCoarsen(C);
    }

    //First we compute the bissection

    double Gweight_ub1 = 0;  //anik
    double Gweight_ub2 = 0;  //anik

    for (i=0; i<opt.nbPart/2; i++) {
        Gweight_ub1 += opt.ub[i];
        //printf("bissection opt.ub[%d] = %lf Gweight ub1 = %lf\n",i,opt.ub[i],Gweight_ub1);
    }
    for (i=opt.nbPart/2; i<opt.nbPart; i++) {
        Gweight_ub2 += opt.ub[i];
        //printf("bissection opt.ub[%d] = %lf Gweight ub2 = %lf\n",i,opt.ub[i],Gweight_ub2);
    }
    opt.nbPart = 2;
    opt.ratio = 0.7*(ratio-1) / (log2(nbpart)) + 1;

    opt.ub[0] = Gweight_ub1*opt.ratio;
    opt.lb[0] = ((2.0 - opt.ratio)/opt.ratio) * opt.ub[0];
    opt.ub[1] = Gweight_ub2*opt.ratio;
    opt.lb[1] = ((2.0 - opt.ratio)/opt.ratio) * opt.ub[1];
//    opt.lb[0] = 1;
//    opt.ub[0] = igraph->totvw;

    if (opt.debug > 10){
      //for (i=0; i<info->depth; i++)
    //printf("@");
      printf("In recursive nVrtx = %d, nbpart = %d, lb0 = %f, ub0 = %f, lb1 = %f, ub1 = %f\n", nVrtx, opt.nbPart, opt.lb[0], opt.ub[0], opt.lb[1], opt.ub[1]);
    }
    
    //printf("Start VCycle\n");
    C = VCycle(igraph, opt, &info->info);
    check(C);
    //printf("End VCycle\n");


    //printf("\nAfter coarsen graph nodes = %d\n",C->graph->nVrtx);
    for (i=1; i<=C->graph->nVrtx; i++){
//  fprintf(file, "%s = part %d\n",G.vertices[i], (int) rcoars->coars->part[i]);
//  printf("%d = part %d\n",i, (int) C->part[i]);
    }

    int part1, part2;
    rcoarsen *rcoars = initializeRCoarsen(C);
    
    //We build the two subgraphes based on the bissection

    dgraph *G1 = (dgraph*) malloc(sizeof(dgraph));
    dgraph *G2 = (dgraph*) malloc(sizeof(dgraph));
    splitCoarsen(rcoars, G1, G2, &part1, &part2);

    //We compute recursive bissection for G1 and G2

    vwType G1weight = 0;
    for (i=1; i<=G1->nVrtx; i++)
        G1weight += G1->vw[i];
    vwType G2weight = 0;
    for (i=1; i<=G2->nVrtx; i++)
        G2weight += G2->vw[i];
    opt.nbPart = nbpart/2;
    //double max_weight = opt.ratio*Gweight/nbPart;
    for (i=0; i<nbpart/2; i++) {
        opt.ub[i] = targeted_weight[i];
    }

    opt.ratio = ratio;

    int partcount1 = -1,partcount2 = - 1;
    //opt.ratio = (Gweight_ub1 * ratio) / G1weight;
    //double eca_ub1 = ((ratio-1) * Gweight_ub1)/2;
    //double upratio1 = ((Gweight_ub1/2.+eca_ub1)/G1weight) * opt.nbPart;
    //opt.ratio = upratio1;
//    if(G1weight < 300) return rcoars;
//    printf("NOW REC 1 !!!\n");
    //printf("G1weight = %f, G2weight = %f, ratio = %f\n", (double) G1weight, (double) G2weight, opt.ratio);
    //print_opt(&opt);

    info->rec1 = (rMLGP_info*)malloc (sizeof (struct rMLGP_info));
    initRInfoPart(info->rec1);


//    printf("before G1 rvcycle call\n");
    for(i=0;i<=G1->nEdge;i++){
    //  printf("G1 ecIn[%d] = %lf\n",i,G1->ecIn[i]);
    }

    rcoarsen *rcoars1 = my_rVCycle(G1, opt, info->rec1,&partcount1); ///anik recursive call for G1
//    printf("END REC 1 !!!\n");
    print_info_part(rcoars1->coars->graph, rcoars1->coars->part, opt);

//    printf("\nAfter coarsen graph 1 nodes = %d\n",rcoars1->coars->graph->nVrtx);
    for (i=1; i<=rcoars1->coars->graph->nVrtx; i++){
//  fprintf(file, "%s = part %d\n",G.vertices[i], (int) rcoars->coars->part[i]);
//  printf("%d = part %d\n",i, (int) rcoars1->coars->part[i]);
    }

    for (i=0; i<nbpart/2; i++) {
        opt.ub[i] = targeted_weight[i+nbpart/2];
    }
    opt.ratio = ratio;
    //opt.ratio = (Gweight_ub2 * ratio) / G2weight;
    //double eca_ub2 = ((ratio-1) * Gweight_ub2)/2;
    //double upratio2 = (Gweight_ub2/2.+eca_ub2/G2weight) * opt.nbPart;
    //opt.ratio = upratio2;

//    printf("NOW REC 2 !!!\n");
    //printf("G1weight = %f, G2weight = %f, ratio = %f\n", (double) G1weight, (double) G2weight, opt.ratio);
    //print_opt(&opt);
    info->rec2 = (rMLGP_info*)malloc (sizeof (struct rMLGP_info));
    initRInfoPart(info->rec2);
    rcoarsen *rcoars2 = my_rVCycle(G2, opt, info->rec2,&partcount2);   /// anik recursive call for G2
//    printf("END REC 2 !!!\n");
    print_info_part(rcoars2->coars->graph, rcoars2->coars->part, opt);

 //   printf("\nAfter coarsen graph 2 nodes = %d\n",rcoars2->coars->graph->nVrtx);
    for (i=1; i<=rcoars2->coars->graph->nVrtx; i++){
//  fprintf(file, "%s = part %d\n",G.vertices[i], (int) rcoars->coars->part[i]);
//  printf("%d = part %d\n",i, (int) rcoars2->coars->part[i]);
    }
    //We rebuild and return final solution

    rcoars->next_rcoarsen1 = rcoars1;
    rcoars->next_rcoarsen2 = rcoars2;
    rcoars1->previous_rcoarsen = rcoars;
    rcoars2->previous_rcoarsen = rcoars;

    for (i=1; i<=nVrtx; i++){
  //    printf("i  = %d nvrtx = %d ", i, nVrtx);
 //     printf("part %d next_index %d\n", C->part[i], rcoars->next_index[i]);
      if (C->part[i] == part1)
            rcoars1->previous_index[rcoars->next_index[i]] = i;
        if (C->part[i] == part2)
            rcoars2->previous_index[rcoars->next_index[i]] = i;
    }

    for (i=1; i<=nVrtx; i++){
      //printf("%d / %d \t", i, nVrtx);
        if (C->part[i] == part1) {
          if (rcoars->next_index[i] > rcoars1->coars->graph->nVrtx){
        //for (i=0; i<info->depth; i++)
          //printf("@");
        //printf("GROS CACA next_index %d > %d nVrtx1\n", rcoars->next_index[i], rcoars1->coars->graph->nVrtx);
          }
          //printf("%d %d\t", C->part[i], (int) i);
          //printf("%d %d\n", rcoars->next_index[i], rcoars1->coars->graph->nVrtx);
            C->part[i] = rcoars1->coars->part[rcoars->next_index[i]];
         //   printf("C->part[%d] = \t%d(rcoars1)\n", i,C->part[i], rcoars1->coars->part[rcoars->next_index[i]]);
        }
        else {
          if (rcoars->next_index[i] > rcoars2->coars->graph->nVrtx){
        for (i=0; i<info->depth; i++)
          printf("@");
          //printf("GROS CACA next_index %d > %d nVrtx2\n", rcoars->next_index[i], rcoars2->coars->graph->nVrtx);
          }
          //printf("%d %d\t", C->part[i], (int) i);
          //printf("%d %d\n", rcoars->next_index[i], rcoars2->coars->graph->nVrtx);
            C->part[i] = partcount1+ rcoars2->coars->part[rcoars->next_index[i]];
       //     printf("C->part[%d] = \t%d(rcoarse2)\n", i,C->part[i], rcoars2->coars->part[rcoars->next_index[i]]);
            //printf("%d %d\t%d\n", C->part[i], (int) i, rcoars2->coars->part[rcoars->next_index[i]]);
        }
    }

    opt.nbPart = nbpart;
    for (i=0; i<nbpart; i++) {
        opt.ub[i] = targeted_weight[i];
    }
    opt.ratio = ratio;

    *resultingNbPart = partcount1+partcount2;

    //printf("Now free\n");
    free(targeted_weight);
    free(lb_save);
    freeRCoarsen(rcoars1);
    rcoars1 = (rcoarsen*) NULL;
    freeRCoarsen(rcoars2);
    rcoars2 = (rcoarsen*) NULL;
    //printf("Done free\n");
    info->depth--;
    info->timing_global += u_wseconds();
    info->timing_coars = info->rec1->timing_coars + info->rec2->timing_coars;
    info->timing_inipart = info->rec1->timing_inipart + info->rec2->timing_inipart;
    info->timing_uncoars = info->rec1->timing_uncoars + info->rec2->timing_uncoars;

/*    for (i=1; i<=rcoars->coars->graph->nVrtx; i++){
//  fprintf(file, "%s = part %d\n",G.vertices[i], (int) rcoars->coars->part[i]);
    printf("%d = part %d\n",i, (int) rcoars->coars->part[i]);
    }*/

    return rcoars;
}
    
