#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <assert.h>


#include "metis.h"
#include "debug.h"
#include "dgraph.h"
#include "utils.h"
#include "initialBissection.h" 
#include "refinement.h"


void dgraph_to_metis(dgraph* G, idx_t* in, idx_t* adj, idx_t* vw, idx_t* adjw)
{
    idxType i,j,k=0;

    for (i=1; i<=G->nVrtx; i++) {
        in[i-1] = k;
        vw[i-1] = G->vw[i];
        for (j = G->inStart[i]; j <= G->inEnd[i]; j++) {
            adj[k] = G->in[j]-1;
            adjw[k++] = G->ecIn[j];
        }
        for (j = G->outStart[i]; j <= G->outEnd[i]; j++) {
            adj[k] = G->out[j]-1;
            adjw[k++] = G->ecOut[j];
        }
    }
    in[G->nVrtx] = k;    
}


void fixAcyclicityBottom(dgraph* G, idxType *part, idxType inpartidx, idxType outpartidx)
{
    /*Fix acyclicity of a bissection where inpartidx -> outpartidx by puting every child of outpartidx to outpartidx*/
    idxType to = 1;
    idxType* ready = (idxType*) calloc(G->nVrtx, sizeof(idxType));
    idxType i,j;
    idxType nbready = sourcesList(G, ready);
    printf("fixAcyclicityBottom nbready = %d\n");
    idxType* done = (idxType *) calloc(G->nVrtx+1, sizeof(idxType));
    while (nbready > 0)
    {
        //if (to > G->nVrtx)
            //printf("Proof to = %d\n", (int) to);
        idxType node = ready[nbready-1];
        //printf("node = %d\n",node);
        nbready--;
        to = to + 1;
        done[node] = 1;
        if (part[node] == outpartidx)
            for (i = G->outStart[node]; i <= G->outEnd[node]; i++)
                part[G->out[i]] = outpartidx;
        for (i = G->outStart[node]; i <= G->outEnd[node]; i++)
        {
            idxType succ = G->out[i];
            int is_ready = 1;
            for (j = G->inStart[succ]; j <= G->inEnd[succ]; j++)
                {
                    //printf("%s\n", );
                    if (done[G->in[j]] == 0)
                        is_ready = 0;
                }
            if (is_ready == 1)
                ready[nbready++] = succ;
        }
    }
    printf("fixAcyclicityBottom to value = %d vertex = %d\n",to,G->nVrtx);
    if (to != G->nVrtx+1)
        u_errexit("fixAcyclicityBottom Problem, not every node concerned!\n");
    free(done);
    free(ready);
}

void fixAcyclicityUp(dgraph* G, idxType *part, idxType inpartidx, idxType outpartidx)
{
    /*Fix acyclicity of a bissection where inpartidx -> outpartidx by puting every child of outpartidx to outpartidx*/
    idxType* ready;
    idxType to = 1;
    ready = (idxType*) malloc(sizeof(idxType) * G->nVrtx);
    idxType i,j;
    idxType nbready = outputList(G, ready);
    idxType* done = (idxType *) calloc(G->nVrtx+1, sizeof(idxType));
    while (nbready > 0)
    {
        if (to > G->nVrtx)
            printf("Proof to = %d\n", to);
        idxType node = ready[nbready-1];
        nbready--;
        to++;
        done[node] = 1;
        if (part[node] == inpartidx)
            for (i = G->inStart[node]; i <= G->inEnd[node]; i++)
                part[G->in[i]] = inpartidx;
        for (i = G->inStart[node]; i <= G->inEnd[node]; i++)
        {
            idxType pred = G->in[i];
            int is_ready = 1;
            for (j = G->outStart[pred]; j <= G->outEnd[pred]; j++)
                if (done[G->out[j]] == 0) {
                    is_ready = 0;
                }
            if (is_ready == 1)
                ready[nbready++] = pred;
        }
    }
    if (to != G->nVrtx+1)
        u_errexit("fixAcyclicityUp: Problem, not every node concerned! to = %d != %d = nVrtx\n", (int) to, (int) G->nVrtx);
    free(done);
    free(ready);
}


void metisAcyclicBissection(ecType* edgecutmin, MLGP_info* info, coarsen* coars, idxType* part, MLGP_option opt)
{
    dgraph* G = coars->graph;
//    idx_t nVertices = (idx_t) G->nVrtx;
//    idx_t nEdges = (idx_t) G->nEdge;
//    idx_t nWeights = (idx_t) 1;
//    idx_t nParts = (idx_t) opt.nbPart;
      idxType nVertices = G->nVrtx;
      idxType nEdges = G->nEdge;
      idxType nWeights = 1;
      idxType nParts = opt.nbPart;
    assert(nParts == 2);
    ecType locedgecut = ecType_MAX;
    idxType* list = (idxType*) malloc((G->nVrtx+1)*sizeof(idxType));

    idx_t objval;
    idx_t* xadj = (idx_t*) calloc(nVertices+1, sizeof(idx_t));
    idx_t* vw = (idx_t*) calloc(nVertices+1, sizeof(idx_t));
    idx_t* adjncy = (idx_t*) calloc(2*nEdges, sizeof(idx_t));
    idx_t* adjw = (idx_t*) calloc(2*nEdges, sizeof(idx_t));
 //   idx_t* part_metis = (idx_t*) calloc(nVertices+1, sizeof(idx_t)) ;
 //   idx_t* part_up = (idx_t*) calloc(nVertices+1, sizeof(idx_t));
 //   idx_t* part_down = (idx_t*) calloc(nVertices+1, sizeof(idx_t));


 //   idxType objval;
 //   idxType* xadj = (idxType*) calloc(nVertices+1, sizeof(idxType));
 //   idxType* vw = (idxType*) calloc(nVertices+1, sizeof(idxType));
 //   idxType* adjncy = (idxType*) calloc(2*nEdges, sizeof(idxType));
 //   idxType* adjw = (idxType*) calloc(2*nEdges, sizeof(idxType));
    idxType* part_metis = (idxType*) calloc(nVertices+1, sizeof(idxType)) ;
    idxType* part_up = (idxType*) calloc(nVertices+1, sizeof(idxType));
    idxType* part_down = (idxType*) calloc(nVertices+1, sizeof(idxType));


    idxType node,i;
    idxType* partsize;
    int ctr = 0;
    int inOut = 0,inOutLimit = 2;
    dgraph_to_metis(G, xadj, adjncy, vw, adjw);
    idx_t options[METIS_NOPTIONS];
    METIS_SetDefaultOptions(options);
    options[METIS_OPTION_SEED] = opt.seed;
    options[METIS_OPTION_NCUTS] = 1;

    //metis variable defined in int64 by anik
    idx_t metis_nVertices = (idx_t)nVertices;
    idx_t metis_nWeights = (idx_t)nWeights;
    idx_t metis_nParts = (idx_t)nParts;
    idx_t* metis_part_metis = (idx_t*)part_metis;


    for(ctr=1;ctr<=opt.co_nbinipart;++ctr){


        int ret = METIS_PartGraphKway(&metis_nVertices, &metis_nWeights, xadj, adjncy,
                                      vw, NULL, adjw, &metis_nParts, NULL,
                                      NULL, options, &objval, metis_part_metis);
        if (ret!=METIS_OK) {
            fprintf(stderr, "Metis failed with error code:%d\n", ret);
        }
        if(opt.debug > 0)
            printf("MetisCut: %d\n", objval);

        /* check if the part has any source nodes */
        int qq = sourcesList(G,list);
        printf("sourcesList output = %d\n",qq);
        int fir = 0,sec=0;
        for (i=0;i<qq;++i)
            if(part_metis[list[i]-1]==0)
                fir+=1;
            else
                sec+=1;
        // printf("%d %d\n", fir,sec);
        if(fir ==0)
            inOut = 1;
        else if(sec == 0)
            inOutLimit = 1;
        
        /* check if the part has any target (output) nodes */
        qq = outputList(G,list);
        printf("outputlist output =%d\n",qq);
        fir = 0;sec=0;
        for (i=0;i<qq;++i)
            if(part_metis[list[i]-1]==0)
                fir+=1;
            else
                sec+=1;
        if(fir == 0)
            inOutLimit = 1;
        if(sec == 0)
            inOut = 1;

        for(;inOut<inOutLimit;++inOut){
            for (i=1; i<=G->nVrtx; i++) {
                part_up[i] = part_metis[i - 1];
                part_down[i] = part_metis[i - 1];
            }

            idxType inpartidx = inOut;
            idxType outpartidx = 1-inOut;
            idxType* toporderpart = (idxType*) malloc (2 * sizeof(idxType));
            toporderpart[0] = inpartidx;
            toporderpart[1] = outpartidx;

            /*
            idxType nb0 = 0, nb1 = 0;
            for (node = 1; node <= nVertices; node++) {
                if (part_down[node] == 0)
                    nb0++;
                else
                    nb1++;
            }
            printf("Obj_up nb0 = %d\tnb1 = %d\n", nb0, nb1);
            */



            //fixAcyclicityBottom(G, part_down, inpartidx, outpartidx);
            printf("fixAcyclicityBottom function executed\n");

            /*
            nb0 = 0; nb1 = 0;
            for (node = 1; node <= nVertices; node++) {
                if (part_down[node] == 0)
                    nb0++;
                else
                    nb1++;
            }
            printf("Obj_down nb0 = %d\tnb1 = %d\n", nb0, nb1);
             */

            partsize = forcedBalance(G, toporderpart, opt.ub, part_down, nParts, opt, info);
            free(partsize);
            
            // char name_bottom[20] = "acycl_bottom";
            // dgraph_to_dot(G, part_down, name_bottom);

            //fixAcyclicityUp(G, part_up, inpartidx, outpartidx);

            partsize = forcedBalance(G, toporderpart, opt.ub, part_up, nParts, opt, info);
            free(partsize);
            
            // char name_up[20] = "acycl_up";
            // dgraph_to_dot(G, part_up, name_up);

            ecType obj_up, obj_down;

            switch(opt.co_obj) {
                case CO_OBJ_EC:
                    obj_up = edgeCut(G, part_up);
                    obj_down = edgeCut(G, part_down);
                    break;

                case CO_OBJ_CV:
                    obj_up = nbCommunications(G, part_up);
                    obj_down = nbCommunications(G, part_down);
                    break;

            }
            //printf("obj_up = %d obj_down = %d\n", obj_up, obj_down);
            if (obj_up > obj_down)
                for (i=1; i<=G->nVrtx; i++)
                    part[i] = part_down[i];
            else
                for (i=1; i<=G->nVrtx; i++)
                    part[i] = part_up[i];

            ecType edgecut = obj_up < obj_down ? obj_up : obj_down;
            if (edgecut < locedgecut)
                locedgecut = edgecut;
            info->ec_inipart_tab[CO_INIPART_METIS] = locedgecut;
            if (edgecut < *edgecutmin) {
                *edgecutmin = edgecut;
                info->best_inipart = CO_INIPART_METIS;
                info->iniec = edgecut;
                for (i = 0; i <= coars->graph->nVrtx; i++)
                    coars->part[i] = part[i];
            }
            free(toporderpart);
        }
    }
    
    /*free(xadj);
    free(adjncy);
    free(vw);
    free(adjw);
    free(part_metis);
    free(part_up);
    free(part_down);*/
    printf("metisAcyclicBissection function end\n\n");

}
