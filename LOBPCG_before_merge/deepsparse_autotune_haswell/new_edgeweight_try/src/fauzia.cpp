#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include <getopt.h>

#include "dgraph.h"
#include "bmmio.h"
#include "utils.h"
#include "debug.h"
#include "vcycle.h"
#include "partDGraphAlgos.h"
#include "matching.h"
#include "infoPart.h"
#include "option.h"
#include "fauzia.hpp"

#include <list>
#include <vector>
#include <iostream>

/*
char *infilename;
int K=2;
float eps = 0.03;
*/

/*********************************************************************************/
void printUsage_fauzia(char *exeName)
{
    printf("Usage: %s <input-file-name> <#parts> [options...]\n", exeName);
    printf("Option:\n");

    //seed
    printf("\t-seed int \t(default = 0)\n");
    printf("\t\tSeed for the random generator\n");

    //runs
    printf("\t-runs int \t(default = 1)\n");
    printf("\t\tNumber of runs\n");

    //part_ub
    printf("\t-part_ub double \t(default = (tot_weight/nbpart) * 1.03)\n");
    printf("\t\tUpper bound for the partition\n");

    //part_lb
    printf("\t-part_lb double \t(default = (tot_weight/nbpart) / 1.03)\n");
    printf("\t\tLower bound for the partition\n");

    //out_file
    printf("\t--out_file filename \t(default = graph_algo.out)\n");
    printf("\t\tOutput filename\n");
}

int processArgs_fauzia(int argc, char **argv, MLGP_option* opt)
{
    int i;
    if (argc < 3){
	    printUsage_fauzia(argv[0]);
	    u_errexit((char *)"%s: There is a problem with the arguments.\n", argv[0]);
    }
	
    strncpy(opt->file_name, argv[1], PATH_MAX);
    strncpy(opt->out_file, opt->file_name, PATH_MAX);
    strcat(opt->out_file, "_fauzia.out");

    opt->nbPart = atoi(argv[2]);
    opt->seed = 0;
    opt->ub = (double*)malloc((opt->nbPart)*sizeof(double));
    opt->lb = (double*)malloc((opt->nbPart)*sizeof(double));
    for (i=0;i<opt->nbPart;i++){
        opt->ub[i] = -1;
        opt->lb[i] = -1;
    }
    opt->runs = 1;

    static struct option long_options[] =
    {
        {"seed",  required_argument, 0, 's'},
	    {"part_ub",  required_argument, 0, 'u'},
	    {"part_lb",    required_argument, 0, 'l'},
	    {"runs",    required_argument, 0, 'n'},
	    {"out_file",    required_argument, 0, 'o'}
    };

    const char *delims = "s:u:l:n:";
    char o;
    int option_index = 0;
    while ((o = getopt_long(argc, argv, delims, long_options, &option_index)) != -1) {
        switch (o) {
            case 's':
                opt->seed = atoi(optarg);
                break;
            case 'u':
                opt->ub[0] = atof(optarg);
                break;
            case 'l':
                opt->lb[0] = atof(optarg);
                break;
            case 'n':
                opt->runs = atoi(optarg);
                break;
	    case 'o':
	        strncpy(opt->out_file, optarg, PATH_MAX);
		break;
            default:
                printUsage_fauzia(argv[0]);
		u_errexit((char *)"Option %s not recognized.", o);
                return 1;
        }
    }
    for(i=1;i<opt->nbPart;i++){
        opt->ub[i]=opt->ub[0];
        opt->lb[i]=opt->lb[0];
    }
    return 0;
}

std::vector<idxType> fauziaNeighbors(const dgraph& g, idxType n, std::vector<int>& neighborsMask){
    std::vector<idxType> result;
    idxType i,j;

    for (i = g.outStart[n]; i <= g.outEnd[n]; ++i){ // Loop over n's out-neighbors i
        for (j = g.inStart[g.out[i]]; j <= g.inEnd[g.out[i]]; ++j){ // Loop over i's in-neighbors j
            if (neighborsMask[g.in[j]] != n && (g.in[j] != n)){ // Check if this node has already been marked as a neighbor
	            neighborsMask[g.in[j]] = n;
	            result.push_back(g.in[j]);
            }
        }
    }
    return result;
}


idxType selectBestNode(const dgraph& g, idxType n, cc& partition, const std::list<idxType> readyVec, std::vector<int>& readyMask, std::vector<int>& neighborsMask, std::vector<int>& readyNeighborsMask, std::vector<int>& readySuccessorsMask, int partCount, double priority){
    std::vector<idxType> neighborsList = fauziaNeighbors(g, n, neighborsMask); // Get list of nodes that share a successor with n
    for (auto it=neighborsList.begin(); it!=neighborsList.end(); ++it){
        if (readyMask[*it] == 1 && (readyNeighborsMask[*it] != partCount+1)){
            partition.readyNeighbors.push_back(*it); // If the neighbor is ready and not already in the readyNeighbor queue, queue it
            readyNeighborsMask[*it] = partCount+1;
        }
    }
    for (idxType i=g.outStart[n]; i<=g.outEnd[n]; ++i){ // If the successor is ready and not already in the readySucessor queue, queue it
        idxType node = g.out[i];
        if (readyMask[node] == 1 && (readySuccessorsMask[node] != partCount+1)){
            partition.readySuccessors.push_back(node);
            readySuccessorsMask[node] = partCount+1;
        }
    }
    idxType returnNode = -1;
    partition.readyNeighbors.remove(n);
    partition.readySuccessors.remove(n);
    if ((partition.takenNeighbors < partition.takenSuccessors*priority) // Take either a neighbor or successor based on priority
      && !partition.readyNeighbors.empty()){
        partition.takenNeighbors += 1.0;
        returnNode = partition.readyNeighbors.front();
        partition.readyNeighbors.pop_front();
        return returnNode;
    }
    else if (!partition.readySuccessors.empty()){
        partition.takenSuccessors += 1.0;
        returnNode = partition.readySuccessors.front();
        partition.readySuccessors.pop_front();
        return returnNode;
    }
    else {
        if (!readyVec.empty()){
            idxType rNodeIdx = rand() % readyVec.size();
            auto it = std::next(readyVec.begin(), rNodeIdx);
            return *it;
        }
    }
    return returnNode;
}


int fauziaPartition(const dgraph& g, double uB, double priority, idxType* partVec){
    std::list<idxType> ready;
    int n = g.nVrtx;
    std::vector<int> readyMask (n+1), neighborsMask (n+1), readyNeighborsMask (n+1), readySuccessorsMask (n+1);
    std::vector<int> predecessorCount (n+1);
    for (idxType i=1; i <= n; ++i){
        predecessorCount[i] = g.inEnd[i]-g.inStart[i]+1; // Count in-neighbors of each node
        if (predecessorCount[i] == 0){ // Add source nodes to ready list
            ready.push_back(i);
            readyMask[i] = 1;
        }
    }
    int partitionCount = 0;
    while (!ready.empty()){
        idxType rNodeIdx = rand() % ready.size(); // Randomly select a ready node
        auto it = std::next(ready.begin(), rNodeIdx); // Could be improved, need a data structure that can index and remove
        idxType node = *it;
        cc partition = {};
        while (!ready.empty() && (partition.weight < uB)){
            partition.weight += 1.0;
            partVec[node] = partitionCount; // Add node to the current partition
            ready.remove(node);
            readyMask[node] = -1;
            for (idxType i=g.outStart[node]; i<=g.outEnd[node]; ++i){ // Update the ready list
	            idxType outNode = g.out[i];
	            predecessorCount[outNode]--;
	            if (predecessorCount[outNode] == 0 && (readyMask[outNode] == 0)) {
	                ready.push_back(outNode);
	                readyMask[outNode] = 1;
	            }
            }
            node = selectBestNode(g, node, partition, ready, readyMask, neighborsMask, readyNeighborsMask, readySuccessorsMask, partitionCount, priority); // Select the next node to try to add to the partition
        }
        ++partitionCount;
    }
    return 0;
}


void runFauzia(char* file_name, MLGP_option opt)
{
    dgraph G;
    //idxType i;
    int r,i;
    ecType* edgecut = (ecType*) malloc(sizeof(ecType) * opt.runs);
    ecType* nbcomm = (ecType*) malloc(sizeof(ecType) * opt.runs);
    double* latencies = (double*) malloc(sizeof(double) * opt.runs);

    generateDGraphFromDot(&G, file_name, 1);

    FILE * pFile = fopen(opt.out_file, "w");
    char *out_filep = opt.file_name;
    out_filep[strlen(out_filep)-4]='\0';
    for (r=0; r<opt.runs; r++){
	printf("########################## RUN %d ########################\n", r);
	srand((unsigned) opt.seed + r);
	idxType *partvec;
	partvec = (idxType*) malloc(sizeof(idxType)*(G.nVrtx+1));

	//double ratio = 1.03;
	//double eca = ((ratio-1.)*G.nVrtx)/opt.nbPart;

    for (i=0; i<opt.nbPart; i++){
        if (opt.lb[i] < 0)
            opt.lb[i] = 1;
        //Warning: Here the upper bound is the targeted value for rvcycle
        if (opt.ub[i] < 0)
            opt.ub[i] = G.nVrtx/(opt.nbPart*1.0);

        if ((floor(opt.lb[i]) < opt.lb[i])&&(floor(opt.lb[i]) == floor(opt.ub[i]))){
            printf("WARNING: The balance can not be matched!!!\n");
            opt.lb[i] = floor(opt.lb[i]);
            opt.ub[i] = floor(opt.ub[i])+1;
        }
    }
	//opt.part_ub = G.nVrtx/(opt.nbPart*1.0) + eca;

	double t = -u_wseconds();
	fauziaPartition(G, opt.ub[0], 0.0, partvec);
	//kernighanSequentialPart(&G, toporder, lb_pw, ub_pw, partvec, &edgecut[r]);
	t += u_wseconds();
	
	edgecut[r] = edgeCut(&G, partvec);
	nbcomm[r] = nbCommunications(&G, partvec);
	latencies[r] = computeLatency(&G, partvec, 1.0, 11.0);
	printf("Final run %d\t%d\t%d\t%d\t%d\t%f\n", r, G.nVrtx, G.nEdge, (int) edgecut[r], nbcomm[r], latencies[r]);
	fprintf(pFile, "fauzia\t%s\t%d\t%d\t%d\t%d\t%.3lf\t%.3lf\n", opt.file_name, opt.nbPart, opt.seed + r, (int) edgecut[r], nbcomm[r], latencies[r], t);

	/*
      FILE *file;
      char name_tmp[200];
      sprintf(name_tmp,".kernighan.part.%d.seed.%d", opt.nbPart, opt.seed);
      char res_name[200];
      strcpy(res_name, file_name);
      strcat(res_name, name_tmp);
      file = fopen(res_name, "w");
      for (i=1; i<=G.nVrtx; i++)
      fprintf(file, "%d\n", (int) partvec[i]);
      fclose(file);
	*/
	free(partvec);
	printf("######################## END RUN %d ########################\n\n", r);
    }
    fclose (pFile);
    free(edgecut);
    free(nbcomm);
    free(latencies);
    freeDGraphData(&G); 
}


int main(int argc, char *argv[])
{
    MLGP_option opt;
    processArgs_fauzia(argc, argv, &opt);

    runFauzia(opt.file_name, opt);

    return 0;
}

