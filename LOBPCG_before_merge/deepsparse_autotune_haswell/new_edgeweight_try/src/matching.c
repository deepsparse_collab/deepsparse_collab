 #include <stdio.h>
#include <stdlib.h>

#include "matching.h"
#include "utils.h"
#include "dgraph.h"



void matchingToplevels(dgraph* G, idxType* leader, const MLGP_option opt)
{
  //printf("Begining matching Toplevels\n");
    int* matched = (int*) calloc(G->nVrtx+1, sizeof(int));
    idxType i, n, j,k;
    int* forbidenv = (int*) calloc(G->nVrtx+1, sizeof(int));
    idxType* toplevels = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 1));
    //printf("Now Toplevels\n");
    computeToplevels(G, toplevels);
    //printf("Done computetoplevel in matching\n");
    idxType* toporder = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 1));
    //printf("Matching with order = %d\n", opt.co_match_norder);//anik comment out
    if (opt.co_match_norder == CO_MATCH_NORD_RAND)
	    shuffleTab(1, G->nVrtx, toporder+1);
    if (opt.co_match_norder == CO_MATCH_NORD_DFSTOP)
	    DFStopsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_RDFSTOP)
	    randDFStopsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_BFSTOP)
	    BFStopsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_RBFSTOP)
	    randBFSsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_DFS)
	    DFSsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_RDFS)
	    randDFSsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_BFS)
	    BFSsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_RBFS)
	    randBFSsort(G, toporder);
    if (opt.co_match_onedegreefirst == 1)
	    oneDegreeFirst(G, toporder);
    int* outnodes = (idxType*) malloc(sizeof(idxType)*G->nVrtx);
    double weight_lim = 0.1 * G->totvw / opt.nbPart;
    //double weight_lim = 2;
    //printf("done order comput in matching\n");
    for (i=1; i<=G->nVrtx; i++) {
        leader[i] = i;
    }
    int ctr=0;
    for (n=1; n<=G->nVrtx; n++) {
		i = toporder[n];
		if (matched[i] == 1)
	    	continue;
		int nboutnodes = G->outEnd[i] - G->outStart[i] + 1;
		int only_output = (nboutnodes == 1);
        for (j=0; j<nboutnodes; j++)
            outnodes[j] = G->out[G->outStart[i]+j]; /*BU2JH: evitons cette copie?*/
		if (opt.co_match_eorder == CO_MATCH_EORD_DFSTOP)
			sortOutNeighboursWeight(G, i, outnodes); /*BU2JH: a regarder*/
		if (opt.co_match_eorder == CO_MATCH_EORD_RDFSTOP)
	    	sortOutNeighboursRatio(G, i, outnodes); /*BU2JH: a regarder*/
        for (j=0; j< nboutnodes; j++) {
	    	idxType node = outnodes[j];
            if (node > G->nVrtx)
                u_errexit("In matching node = %d for i = %d, j=%d\n", (int) node, i, j);
			if (matched[node] == 1)
				continue;
			if (forbidenv[node] == 1)
				continue;
	    	int only_input = (G->inEnd[node] - G->inStart[node] + 1 == 1);
	    	if ((toplevels[i] != toplevels[node] -1)&&(!only_input)&&(!only_output))
				continue;
            if (G->vw[i] + G->vw[node] > weight_lim)
				continue;
	    	leader[node] = i;
			matched[i] = 1;
	    	matched[node] = 1;
        //if(!strcmp(G->vertices[i-1],"DLACPY,1,1"))
		//  printf("We matched %s ----> %s\n", G->vertices[i-1],G->vertices[node-1]);//anik comment out
        //    printf(" matching %s\n",G->vertices[0]);
	    	++ctr;
            for (k=G->outStart[i]; k<= G->outEnd[i]; k++){
				if (toplevels[i] != toplevels[G->out[k]] -1)
		    		continue;
				forbidenv[G->out[k]] = 1;
	    	}
	    	break;
		}
    }

    //printf("matching done. %d matched!\n",ctr);
    free(matched);
    free(toplevels);
    free(toporder);
    free(forbidenv);
    free(outnodes);
    //printf("\n inside matchingToplevels everything cleared\n");
}

// void my_matchingToplevels(dgraph* G, idxType* leader, const MLGP_option opt)
// {
//   //printf("Begining matching Toplevels\n");

//     idxType i, n, j,k;
//     int* matched = (int*) calloc(G->nVrtx+1, sizeof(int));
//     int ok_to_match = 0;

//     idxType* toplevels = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 1));
//     printf("Now Toplevels\n");
//     computeToplevels(G, toplevels);
//     printf("Done computetoplevel in matching\n");
//     idxType* toporder = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 1));
//     printf("Matching with order = %d\n", opt.co_match_norder);//anik comment out


//     DFStopsort(G,toporder);
//     //BFStopsort(G,toporder);
//     //randTopsort(G, toporder);
//     for(i=1;i<=G->nVrtx ; i++){
//                 //printf("node %d =  toplevel %d \n",i,toplevels[i]);

//             }
//     for(i=1;i<=G->nVrtx ; i++){
//                 printf("%d ",toporder[i]);

//             }
//     printf("\n");

//     int* sorted_outnodes = (idxType*) malloc(sizeof(idxType)*G->nVrtx);
//     double weight_lim = 0.1 * G->totvw / opt.nbPart;
//     //double weight_lim = 2;
//     //printf("done order comput in matching\n");
//     for (i=1; i<=G->nVrtx; i++) {
//         leader[i] = i;
//        // printf("toplevel[%d] = %d\n",i,toplevels[i]);
//        // printf("toporder[%d] = %d\n",i,toporder[i]);
//     }
//     int ctr=0;
//     int m;
//     for (m=1; m<=G->nVrtx; m++) {
//         i = toporder[m];
//         if(matched[i]==1)continue;
//         matched[i] = 1;
//         //printf("matching top level %d = %d\n",m,toporder[m]);

//         int nboutnodes = G->outEnd[i] - G->outStart[i] + 1;
//         int only_output = (nboutnodes == 1);
//         for (j=0; j<nboutnodes; j++)
//             sorted_outnodes[j] = G->out[G->outStart[i]+j]; /*BU2JH: evitons cette copie?*/
//         if (opt.co_match_eorder == CO_MATCH_EORD_DFSTOP)
//             my_sortNeighbourWithWeight(G, i, sorted_outnodes); /*BU2JH: a regarder*/
// //      if (opt.co_match_eorder == CO_MATCH_EORD_RDFSTOP)
// //          sortOutNeighboursRatio(G, i, outnodes); /*BU2JH: a regarder*/
//         for (j=0; j< nboutnodes; j++) {
//             idxType node = sorted_outnodes[j];
//             //printf("\t\tnode = %d\n",node);
//             for(k=G->inStart[node] ; k <= G->inEnd[node] ; k++){
//                 if(matched[G->in[k]] == 0){
//                     ok_to_match = 1;
//                     break;
//                 }
//             }
//             if(!ok_to_match){
//                 leader[node] = leader[i];
//                 matched[node] = 1;

//             printf("We matched %d and %d\n", node, i);//anik comment out
//             ++ctr;
//             }



//         }
//         if(
//                 ctr >= G->nVrtx/2)break;
//     }
//     for(i = 1 ; i <= G->nVrtx ; i++){
//         //printf("leader[%d] = %d\n",i,leader[i]);
//     }

//     printf("matching done. %d matched!\n",ctr);

//     free(toplevels);
//     free(toporder);

//     free(sorted_outnodes);
//     printf("\n inside matchingToplevels everything cleared\n");
// }


void shuffleArray(idxType* array, idxType n){
    
    if (n > 1) 
    {
        size_t i;
        for (i = 1; i < n ; i++) 
        {
          size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
          int t = array[j];
          array[j] = array[i];
          array[i] = t;
        }
    }
}

void my_matchingToplevels(dgraph* G, idxType* leader, const MLGP_option opt)
{
  //printf("Begining matching Toplevels\n");

    idxType i, n, j,k;
    int* matched = (int*) calloc(G->nVrtx+1, sizeof(int));
    int ok_to_match = 0;

    idxType* toplevels = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 1));
    printf("Now Toplevels\n");
    computeToplevels(G, toplevels);
    printf("Done computetoplevel in matching\n");
    idxType* toporder = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 1));
    printf("Matching with order = %d\n", opt.co_match_norder);//anik comment out
    //int node1 = 63;
    //int node2 = 3;
    //int common_ancestor = my_LCA(G,node1,node2);
    //printf("common ancestor of node %d and node %d is %d\n",node1,node2,common_ancestor);

    DFStopsort(G,toporder);
    //BFStopsort(G,toporder);
    //randTopsort(G, toporder);
    shuffleArray(toporder,G->nVrtx);
    for(i=1;i<=G->nVrtx ; i++){
                //printf("node %d =  toplevel %d \n",i,toplevels[i]);

            }
    for(i=1;i<=G->nVrtx ; i++){
                //printf("%d ",toporder[i]);

            }
    //printf("\n");

    int* sorted_outnodes = (idxType*) malloc(sizeof(idxType)*G->nVrtx);
    //printf("sortedoutnodes allocated\n");
    double weight_lim = 0.1 * G->totvw / opt.nbPart;
    //double weight_lim = 2;
    //printf("done order comput in matching\n");
    for (i=1; i<=G->nVrtx; i++) {
        leader[i] = i;
       // printf("toplevel[%d] = %d\n",i,toplevels[i]);
       // printf("toporder[%d] = %d\n",i,toporder[i]);
    }
    int ctr=0;
    int m;
    for (m=1; m<=G->nVrtx; m++) {
        i = toporder[m];
        //if(matched[i]==1)continue;
        //matched[i] = 1;
        printf("matching top level %d = %d\n",m,toporder[m]);

        if(i==115) printf("node %s outnodes = %d\n",G->vertices[114],G->outEnd[i] - G->outStart[i] + 1);

        int nboutnodes = G->outEnd[i] - G->outStart[i] + 1;
        int only_output = (nboutnodes == 1);
        for (j=0; j<nboutnodes; j++)
            sorted_outnodes[j] = G->out[G->outStart[i]+j]; /*BU2JH: evitons cette copie?*/
        if (opt.co_match_eorder == CO_MATCH_EORD_DFSTOP)
            my_sortNeighbourWithWeight(G, i, sorted_outnodes); /*BU2JH: a regarder*/
//      if (opt.co_match_eorder == CO_MATCH_EORD_RDFSTOP)
//          sortOutNeighboursRatio(G, i, outnodes); /*BU2JH: a regarder*/
        for (j=0; j< nboutnodes; j++) {
            idxType node = sorted_outnodes[j];
            if(i==115)
            printf("\t\tnode = %d\n",node);
/*          for(k=G->inStart[node] ; k <= G->inEnd[node] ; k++){
                if(matched[G->in[k]] == 0){
                    ok_to_match = 1;
                    matched[i] = 1;
                    break;
                }
            }*/

            for(k=G->inStart[node] ; k <= G->inEnd[node] ; k++){
                //if(node==263) printf("%s\n", );
                //if(G->in[k] == i && (G->inEnd[node] - G->inStart[node]+1) == 1 ) continue;
                int parent = my_LCA(G,G->in[k],i);
                //if(parent > 0 and (toplevels[parent] - toplevels[i]) > 2)
                if(parent)
                {
                    ok_to_match = 1;
                    break;
                }
            }

            if((G->inEnd[node] - G->inStart[node]+1) == 1 ){
                //printf("indegree of node %d = %d and parent is %d\n",node,G->inEnd[node] - G->inStart[node],G->in[G->inStart[node]]);
                ok_to_match = 0;
            }
            if(!ok_to_match){
                leader[node] = leader[i];
                //matched[node] = 1;

            printf("We matched %d and %d\n", node, i);//anik comment out
            ++ctr;
            }



        }
        if(ctr >= G->nVrtx/2)break;
    }
    int lead_count = 0;
    for(i = 1 ; i <= G->nVrtx ; i++){
        //printf("leader[%d] = %d\n",i,leader[i]);
        int temp_n;
        temp_n = i;
        while(leader[temp_n]!= temp_n){
            temp_n = leader[temp_n];
        }
        leader[i] = temp_n;

    }

    printf("matching done. %d matched! and leader count %d\n",ctr,lead_count);

    free(toplevels);
    free(toporder);

    free(sorted_outnodes);
    printf("\n inside matchingToplevels everything cleared\n");
}

void aggregativeToplevels(dgraph* G, idxType* leader, const MLGP_option opt)
{
    //printf("Begin matching TopLevels\n");
    idxType i, n, j,k;
    int* matched = (int*) calloc(G->nVrtx+1, sizeof(int));
    int* forbiden = (int*) calloc(G->nVrtx+1, sizeof(int));
    idxType* toporder = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 1));
    idxType* toplevels = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 1));
    computeToplevels(G, toplevels);
    //printf("Matching with order = %d\n", opt.co_match_order);
    if (opt.co_match_norder == CO_MATCH_NORD_RAND)
        shuffleTab(1, G->nVrtx, toporder+1);
    if (opt.co_match_norder == CO_MATCH_NORD_DFSTOP)
        DFStopsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_RDFSTOP)
        randDFStopsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_BFSTOP)
        BFStopsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_RBFSTOP)
        randBFSsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_DFS)
        DFSsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_RDFS)
        randDFSsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_BFS)
        BFSsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_RBFS)
        randBFSsort(G, toporder);
    if (opt.co_match_onedegreefirst == 1)
        oneDegreeFirst(G, toporder);
    idxType* outnodes = (idxType*) malloc(sizeof(idxType)*G->nVrtx);
    double weight_lim = 0.1 * G->totvw / opt.nbPart;
    vwType* weight = (vwType*) calloc(G->nVrtx+1, sizeof(vwType));
    idxType* minouttlnode=NULL, *maxtlcluster=NULL, *minouttlcluster=NULL;
    if (opt.co_match == CO_MATCH_AGGREGATIVEMAX) {
        minouttlnode = (idxType *) calloc(G->nVrtx + 1, sizeof(idxType));
        maxtlcluster = (idxType *) calloc(G->nVrtx + 1, sizeof(idxType));
        minouttlcluster = (idxType *) calloc(G->nVrtx + 1, sizeof(idxType));
    }

    for (i=1; i<=G->nVrtx; i++) {
        leader[i] = i;
        weight[i] = G->vw[i];
        if (opt.co_match == CO_MATCH_AGGREGATIVEMAX) {
            for (j = G->outStart[i]; j <= G->outEnd[i]; j++)
                minouttlnode[i] = minouttlnode[i] < toplevels[G->out[j]] ? minouttlnode[i] : toplevels[G->out[j]];
            minouttlcluster[i] = minouttlnode[i];
            maxtlcluster[i] = toplevels[i];
        }
    }
    for (n=1; n<=G->nVrtx; n++) {
	    i = toporder[n];
	    if ((leader[i] != i)||(matched[i] == 1)||(forbiden[i] == 1))
	        continue;
	    int nboutnodes = G->outEnd[i] - G->outStart[i] + 1;
	    int only_output = (nboutnodes == 1);
        for (j=0; j<nboutnodes; j++)
            outnodes[j] = G->out[G->outStart[i]+j];
	    if (opt.co_match_eorder == CO_MATCH_EORD_DFSTOP)
	        sortOutNeighboursWeight(G, i, outnodes);
	    if (opt.co_match_eorder == CO_MATCH_EORD_RDFSTOP)
	        sortOutNeighboursRatio(G, i, outnodes);
        for (j=0; j< nboutnodes; j++) {
            idxType node = outnodes[j];
            if (node > G->nVrtx)
                u_errexit("In aggregativeTopLevel node = %d pour i = %d, j=%d\n", (int) node, i, j);
            if (matched[node] == 1)
                continue;
            //printf("We concider nodes %d and %d\n", node, i);
            //printf("Not matched\n");
            if (forbiden[node] == 1)
                continue;
            //printf("Not forbiden toplevels[%d] = %d toplevels[%d] = %d\n", i, (int) toplevels[i], node, (int) toplevels[node]);
            if (opt.co_match == CO_MATCH_AGGREGATIVETP)
                if (toplevels[i] != toplevels[node] -1)
                    continue;
            if (opt.co_match == CO_MATCH_AGGREGATIVEMAX)
                if (minouttlnode[node] > maxtlcluster[leader[i]])
                    continue;
            //printf("Good toplevels\n");
            if (weight[leader[i]] + G->vw[node] > weight_lim)
                    continue;
            leader[node] = leader[i];
            weight[leader[i]] += G->vw[node];
            matched[i] = 1;
            matched[node] = 1;
            //printf("!!! We match nodes %d and %d (leader %d) !!!\n", i, node, leader[i]);
            if (opt.co_match == CO_MATCH_AGGREGATIVEMAX) {
                minouttlcluster[leader[i]] = minouttlcluster[leader[i]] < minouttlnode[node] ? minouttlcluster[leader[i]] : minouttlnode[node];
                maxtlcluster[leader[i]] = maxtlcluster[leader[i]] < toplevels[node] ? toplevels[node] : maxtlcluster[leader[i]];
            }
        }
        if (opt.co_match == CO_MATCH_AGGREGATIVETP)
            if (matched[i] == 1)
                for (k=G->outStart[i]; k<= G->outEnd[i]; k++){
                    if (toplevels[i] != toplevels[G->out[k]] - 1)
                        continue;
                    forbiden[G->out[k]] = 1;
                }
    }
    free(matched);
    free(toplevels);
    free(toporder);
    free(forbiden);
    free(outnodes);
    if (minouttlnode) free(minouttlnode);
    if (maxtlcluster) free(maxtlcluster);
    if (minouttlcluster) free(minouttlcluster);
}

void aggregativeToplevelsTP(dgraph* G, idxType* leader, const MLGP_option opt)
{
    //printf("Begin matching TopLevels\n");
    idxType i, n, j,k;
    int* matched = (int*) calloc(G->nVrtx+1, sizeof(int));
    int* forbiden = (int*) calloc(G->nVrtx+1, sizeof(int));
    idxType* toporder = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 1));
    idxType* toplevels = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 1));
    idxType* toplevelscluster = (idxType*) malloc(sizeof(idxType)*(G->nVrtx + 1));
    computeToplevels(G, toplevels);
    //printf("Matching with order = %d\n", opt.co_match_order);
    if (opt.co_match_norder == CO_MATCH_NORD_RAND)
        shuffleTab(1, G->nVrtx, toporder+1);
    if (opt.co_match_norder == CO_MATCH_NORD_DFSTOP)
        DFStopsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_RDFSTOP)
        randDFStopsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_BFSTOP)
        BFStopsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_RBFSTOP)
        randBFSsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_DFS)
        DFSsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_RDFS)
        randDFSsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_BFS)
        BFSsort(G, toporder);
    if (opt.co_match_norder == CO_MATCH_NORD_RBFS)
        randBFSsort(G, toporder);
    if (opt.co_match_onedegreefirst == 1)
        oneDegreeFirst(G, toporder);
    idxType* outnodes = (idxType*) malloc(sizeof(idxType)*G->nVrtx);
    double weight_lim = 0.1 * G->totvw / opt.nbPart;
    vwType* weight = (vwType*) calloc(G->nVrtx+1, sizeof(vwType));
    int* matchedu = (int*) calloc(G->nVrtx+1, sizeof(int));
    int* matchedv = (int*) calloc(G->nVrtx+1, sizeof(int));
    idxType* minouttlnode, *maxtlcluster, *minouttlcluster;
    for (i=1; i<=G->nVrtx; i++) {
        leader[i] = i;
        weight[i] = G->vw[i];
    }
    for (n=1; n<=G->nVrtx; n++) {
        i = toporder[n];
        if ((matchedu[i] == 1)||(matchedv[i] == 1))
            continue;
        int nbbadoutput = 0, badoutputindex = -1;
        for (j = G->outStart[i]; j <= G->outEnd[i]; j++){
            idxType node = G->out[j];
            if ((matchedv[node] == 1)&&(toplevels[node] == toplevels[i] + 1)){
                nbbadoutput++;
                badoutputindex = node;
            }
        }
        if (nbbadoutput > 1)
            continue;
        if (nbbadoutput == 1){
            idxType node = badoutputindex;
            if (weight[leader[node]] + G->vw[node] > weight_lim)
                continue;
            leader[i] = leader[node];
            weight[leader[i]] += G->vw[i];
            matchedu[i] = 1;
        }
        if (nbbadoutput == 0) {
            int nboutnodes = G->outEnd[i] - G->outStart[i] + 1;
            for (j = 0; j < nboutnodes; j++)
                outnodes[j] = G->out[G->outStart[i] + j];
            if (opt.co_match_eorder == CO_MATCH_EORD_DFSTOP)
                sortOutNeighboursWeight(G, i, outnodes);
            if (opt.co_match_eorder == CO_MATCH_EORD_RDFSTOP)
                sortOutNeighboursRatio(G, i, outnodes);
            for (j = 0; j < nboutnodes; j++) {
                idxType node = outnodes[j];
                if (node > G->nVrtx)
                    u_errexit("In aggregativeTopLevel node = %d pour i = %d, j=%d\n", (int) node, i, j);
                if (toplevels[node] != toplevels[i] + 1)
                    continue;
                if (matchedu[node] == 1)
                    continue;
                //Here both i and node are unmatched
                if (weight[i] + G->vw[node] > weight_lim)
                    continue;
                leader[node] = i;
                weight[leader[i]] += G->vw[node];
                matchedu[i] = 1;
                matchedv[node] = 1;
                //printf("!!! We match nodes %d (tl %d) and %d (tl %d) with leader %d weigth %d !!!\n", (int) i, (int) toplevels[i], (int) node, (int) toplevels[node], (int) leader[i], (int) weight[leader[i]]);
            }
        }
    }
    free(matched);
    free(toplevels);
    free(toporder);
    free(forbiden);
    free(outnodes);
}


void computeMatching(const MLGP_option opt, dgraph *G, idxType* matching){
  //printf("In matching %d\n", opt.co_match);
  switch(opt.co_match){
        case CO_MATCH_DIFF :
            matchingToplevels(G, matching, opt);
            //my_matchingToplevels(G, matching, opt);
	    break;

        case CO_MATCH_AGGREGATIVETP :
            aggregativeToplevelsTP(G, matching, opt);
            break;

        case CO_MATCH_AGGREGATIVEMAX :
            aggregativeToplevels(G, matching, opt);
            break;

    }
}
