#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <sys/stat.h>

#include <getopt.h>


#include "dgraph.h"
#include "bmmio.h"
#include "utils.h"
#include "debug.h"
#include "vcycle.h"
#include "partDGraphAlgos.h"
#include "matching.h"
#include "infoPart.h"

/*
char *infilename;
int K=2;
float eps = 0.03;
*/

/*********************************************************************************/
void printUsage_MLGP(char *exeName)
{
    printf("Usage: %s <input-file-name> <#parts> [options...]\n", exeName);
    printf("Option:\n");

    //seed
    printf("\t--seed int \t(default = 0)\n");
    printf("\t\tSeed for the random generator\n");

    //runs
    printf("\t--runs int \t(default = 1)\n");
    printf("\t\tNumber of runs for the graph\n");

    //ratio
    printf("\t--ratio float \t(default = 1.03)\n");
    printf("\t\tImbalance ration between parts\n");

    //debug
    printf("\t--debug int \t(default = 0)\n");
    printf("\t\tDebug value\n");

    //print
    printf("\t--print int \t(default = 0)\n");
    printf("\t\tValue to output graphs\n");

    //co_stop
    printf("\t--co_stop int \t(default = 0)\n");
    printf("\t\tSet the stoping criteria pour the coarsening step\n");
    printf("\t\t= 0 : Stop when the size of graph is < co_stop_size\n");
    printf("\t\t= 1 : Stop after co_stop_step steps\n");

    //co_stop_size
    printf("\t--co_stop_size int \t(default = 50 * #parts)\n");
    printf("\t\tSet co_stop_size when co_stop = 0\n");
    
    //co_stop_step
    printf("\t--co_stop_step int \t(default = 10)\n");
    printf("\t\tSet co_stop_step when co_stop = 1\n");

    //co_match
    printf("\t--co_match int \t(default = 0)\n");
    printf("\t\tSet the matching technic for the coarsening step\n");
    printf("\t\t= 0 : Pairwise matching with top level difference 1\n");
    printf("\t\t= 1 : Aggregative matching with top level difference 1\n");
    printf("\t\t= 2 : Aggregative matching with out top level constraint\n");

    //co_match_norder
    printf("\t--co_match_norder int \t(default = 1)\n");
    printf("\t\tSet the node traversal order for the matching phase\n");
    printf("\t\t= 0 : Random traversal\n");
    printf("\t\t= 1 : Depth First Topological order\n");
    printf("\t\t= 2 : Random Depth First Topological order\n");
    printf("\t\t= 3 : Breadth First Topological order\n");
    printf("\t\t= 4 : Random Breadth First Topological order\n");
    printf("\t\t= 5 : Depth First order\n");
    printf("\t\t= 6 : Random Depth First order\n");
    printf("\t\t= 7 : Breadth First order\n");
    printf("\t\t= 8 : Random Breadth First order\n");

    //co_match_eorder
    printf("\t--co_match_eorder int \t(default = 1)\n");
    printf("\t\tSet the edge traversal order for the matching phase\n");
    printf("\t\t= 0 : Natural construction order\n");
    printf("\t\t= 1 : Edge weight priority\n");
    printf("\t\t= 2 : Edge weight / node weight priority\n");

    //co_match_onedegreefirst
    printf("\t--co_match_onedegreefirst int \t(default = 1)\n");
    printf("\t\tDo we traverse the nodes with degree 1 first in the matching?\n");
    printf("\t\t= 0 : No\n");
    printf("\t\t= 1 : Yes\n");

    //co_init
    printf("\t--co_inipart int \t(default = 0)\n");
    printf("\t\tDetermine the initial partitioning\n");
    printf("\t\t= 0 : try all partitioning and pick the best one\n");
    printf("\t\t= 1 : Kernighan with random DFS\n");
    printf("\t\t= 2 : Kernighan with random BFS\n");
    printf("\t\t= 3 : Kernighan with random mix BFS-DFS\n");
    printf("\t\t= 4 : Greedy graph growing approach\n");
    printf("\t\t= 5 : Forced balance initial partitioning\n");
    printf("\t\t= 6 : Metis with fixing acyclicity and forced balance\n");

    //co_nbinit
    printf("\t--co_nbinipart int \t(default = 5)\n");
    printf("\t\tNumber of runs for the initial partitioning\n");

    //co_refinement
    printf("\t--co_refinement int \t(default = 1)\n");
    printf("\t\tDetermine the refinement in uncoarsening step\n");
    printf("\t\t= 0 : no refinement\n");
    printf("\t\t= 1 : toward the largest partition in topological order\n");
    printf("\t\t= 2 : toward the best partition if it creates no cycle\n");
    printf("\t\t= 3 : refinement using method 1 and 2 consecutively\n");
    printf("\t\t= 4 : refinement by swaping nodes (for bisection only)\n");
    printf("\t\t= 5 : refinement from largest part to smallest (for bisection only)\n");
    printf("\t\t= 6 : refinement using method 5 and 4 consecutively (for bisection only)\n");
    printf("\t\t= 7 : refinement randomized selection of boundary vertices to move\n");

    //co_obj
    printf("\t--co_obj int \t(default = 0)\n");
    printf("\t\tDetermine the objective we are trying to minimize\n");
    printf("\t\t= 0 : minimize edge-cut\n");
    printf("\t\t= 1 : minimize communication volume\n");

    //co_obj_commvol
    printf("\t--co_obj_commvol int \t(default = 0)\n");
    printf("\t\tDetermine how we approximate the communication volume when --co_obj=1 for nodes with multiple children\n");
    printf("\t\t= 0 : expected value if data are equally distributed \n");
    printf("\t\t= 1 : best case scenario\n");
    printf("\t\t= 2 : worst case scenario\n");
    printf("\t\t= 3 : average between best case scenario and worst case\n");

    //ref_step
    printf("\t--ref_step int \t(default = 4)\n");
    printf("\t\tNumber of steps in the refinement\n");

    //part_ub
    printf("\t--part_ub double \t(default = (tot_weight/nbpart) * 1.03)\n");
    printf("\t\tUpper bound for the partition\n");

    //part_lb
    printf("\t--part_lb double \t(default = (tot_weight/nbpart) / 1.03)\n");
    printf("\t\tLower bound for the partition\n");

    //out_file
    printf("\t--out_file filename \t(default = graph_algo.out)\n");
    printf("\t\tOutput filename\n");

    //use_binary_input
    printf("\t--use_binary_input (-x) int \t(default = 1)\n");
    printf("\t\tCreate/Read binary version of the input graph.\n");
}


int processArgs_MLGP(int argc, char **argv, MLGP_option* opt)
{
    if (argc < 3){
	printUsage_MLGP(argv[0]);
	u_errexit("%s: There is a problem with the arguments.\n", argv[0]);
    }

    strncpy(opt->file_name, argv[1], PATH_MAX);
    strncpy(opt->out_file, opt->file_name, PATH_MAX);
    strcat(opt->out_file, "_MLGP.out");

    opt->nbPart = atoi(argv[2]);
    opt->seed = 0;
    opt->co_stop = 0;
    opt->co_stop_size = 50*opt->nbPart;
    opt->co_stop_step = 10;
    opt->co_match = 0;
    opt->co_match_norder = 1;
    opt->co_match_eorder = 1;
    opt->co_match_onedegreefirst = 1;
    opt->co_inipart = 0;
    opt->co_nbinipart = 5;
    opt->co_refinement = 1;
    opt->co_obj = 0;
    opt->co_obj_commvol = 0;
    
    opt->ub = (double*)malloc((opt->nbPart+1)*sizeof(double));
    opt->lb = (double*)malloc((opt->nbPart+1)*sizeof(double)); 
    int i;
    for(i=0; i< opt->nbPart; i++){
        opt->ub[i]=-1;
        opt->lb[i]=-1;
    }
    opt->ref_step = 10;
    opt->ratio = 1.03;
    opt->runs = 1;
    opt->debug = 0;
    opt->print = 0;
    opt->use_binary_input = 1;

    static struct option long_options[] =
    {
        {"seed",  required_argument, 0, 's'},
	{"co_stop",  required_argument, 0, 't'},
	{"co_stop_size",  required_argument, 0, 'z'},
	{"co_stop_step",  required_argument, 0, 'p'},
	{"co_match",  required_argument, 0, 'm'},
        {"co_match_norder",  required_argument, 0, 'o'},
        {"co_match_eorder",  required_argument, 0, 'g'},
	{"co_match_onedegreefirst",  required_argument, 0, '1'},
	{"co_inipart",  required_argument, 0, 'i'},
	{"co_nbinipart",  required_argument, 0, 'b'},
	{"co_refinement",  required_argument, 0, 'r'},
	{"co_obj",  required_argument, 0, 'j'},
    	{"co_obj_commvol",  required_argument, 0, 'v'},
    	{"ref_step",  required_argument, 0, 'e'},
	{"part_ub",  required_argument, 0, 'u'},
	{"part_lb",    required_argument, 0, 'l'},
	{"ratio",    required_argument, 0, 'a'},
	{"runs",    required_argument, 0, 'n'},
	{"out_file",    required_argument, 0, 'f'},
	{"debug",    required_argument, 0, 'd'},
	{"print",    required_argument, 0, 'q'},
    {"use_binary_input", required_argument, 0, 'x'},
    };

    const char *delims = "s:t:z:p:m:o:g:v:1:i:b:r:j:u:l:a:n:b:d:f:q:x:";
    char o;
    int option_index = 0;
    while ((o = getopt_long(argc, argv, delims, long_options, &option_index)) != -1) {
        switch (o) {
            case 's':
                opt->seed = atoi(optarg);
                break;
            case 't':
                opt->co_stop = atoi(optarg);
                break;
            case 'z':
                opt->co_stop_size = atoi(optarg);
                break;
            case 'p':
                opt->co_stop_step = atoi(optarg);
                break;
            case 'm':
                opt->co_match = atoi(optarg);
                break;
            case 'o':
                opt->co_match_norder = atoi(optarg);
                break;
            case 'g':
                opt->co_match_eorder = atoi(optarg);
                break;
            case 'v':
                opt->co_obj_commvol= atoi(optarg);
                break;
            case '1':
                opt->co_match_onedegreefirst = atoi(optarg);
                break;
            case 'i':
                opt->co_inipart = atoi(optarg);
                break;
            case 'b':
                opt->co_nbinipart = atoi(optarg);
                break;
            case 'r':
                opt->co_refinement = atoi(optarg);
                break;
            case 'j':
                opt->co_obj = atoi(optarg);
                break;
            case 'e':
                opt->ref_step = atoi(optarg);
                break;
            case 'u':
                opt->ub[0] = atof(optarg);
                break;
            case 'l':
                opt->lb[0] = atof(optarg);
                break;
            case 'a':
                opt->ratio = atof(optarg);
                break;
            case 'n':
                opt->runs = atof(optarg);
                break;
	    case 'd':
                opt->debug = atoi(optarg);
                break;
	    case 'q':
                opt->print = atoi(optarg);
                break;
	    case 'f':
	        strncpy(opt->out_file, optarg, PATH_MAX);
		break;
            case 'x':
                opt->use_binary_input = atoi(optarg);
                break;
            default:
                printUsage_MLGP(argv[0]);
		u_errexit("Option %s not recognized.", o);
                return 1;
        }
    }
    for(i=1; i<opt->nbPart; i++){
        opt->ub[i]=opt->ub[0];
        opt->lb[i]=opt->lb[0];

    }
    if ((opt->co_refinement == 2)&&(opt->nbPart < 3)){
	opt->co_refinement = 1;
	printf("\t\tWARNING: co_refinement has been set to 1 because nbpart <= 2\n");
    }
    
    char suf[200];
    char fname[200];
    char* split;    
    char fname_sav[200];
    sprintf(fname_sav, opt->file_name);
    if (opt->print > 0){
	split = strtok(fname_sav, "/");
	while(split != NULL){
	    sprintf(fname, split);
	    split = strtok(NULL, "/");
	}
	sprintf(opt->file_name_dot, "out/");
	mkdir(opt->file_name_dot, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IWOTH | S_IXOTH);
	strcat(opt->file_name_dot, fname);
	sprintf(suf, "__%d_%d", opt->nbPart, opt->seed);
	strcat(opt->file_name_dot, suf);
	mkdir(opt->file_name_dot, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IWOTH | S_IXOTH);
	sprintf(suf, "/graph");
	strcat(opt->file_name_dot, suf);
    }    

    return 0;
}


void run_MLGP(char* file_name, MLGP_option opt)
{
    MLGP_info info;
    dgraph G;
    int i;
    int maxindegree, minindegree, maxoutdegree, minoutdegree;
    double aveindegree, aveoutdegree;
    
    if (strstr(file_name,".dot")){
        generateDGraphFromDot(&G, file_name,opt.use_binary_input);
    }
    else if(strstr(file_name,".mtx")){   
        generateDGraphFromMtx(&G,file_name,opt.use_binary_input);
    }
    // generateDGraphFromDot(&G, file_name,opt.use_binary_input);

    
    set_dgraph_info(&G);
    dgraph_info(&G, &maxindegree, &minindegree, &aveindegree, &maxoutdegree, &minoutdegree, &aveoutdegree);
    
    double ratio = opt.ratio;
    double eca = ((ratio-1.)*G.nVrtx)/opt.nbPart;

    for(i = 0; i<opt.nbPart; i++){
        if (opt.lb[i] < 0){
            //opt.lb[i] = G.nVrtx/(opt.nbPart*1.0) - eca;
            opt.lb[i] = 1;
        }
        if (opt.ub[i] < 0){
           opt.ub[i] = G.nVrtx/(opt.nbPart*1.0) + eca;
           //printf("new ub %lf\n", opt.ub[i]);
        }
        
        if ((floor(opt.lb[i]) < opt.lb[i])&&(floor(opt.lb[i]) == floor(opt.ub[i]))){
            printf("WARNING: The balance can not be matched!!!\n");
            opt.lb[i] = floor(opt.lb[i]);
            opt.ub[i] = floor(opt.ub[i])+1; 
        }    
    }
    //opt.ub[0] = 1.5*opt.ub[0];

    if (opt.debug)
        printf("Nb node: %d\nNb Edges: %d\nNb part: %d\nLower Bound: %f\nUper Bound: %f\n\n", G.nVrtx, G.nEdge, opt.nbPart, opt.lb[0], opt.ub[0]);

    ecType* edgecut = (ecType*) malloc(sizeof(ecType) * opt.runs);
    ecType* nbcomm = (ecType*) malloc(sizeof(ecType) * opt.runs);
    double* latencies = (double*) malloc(sizeof(double) * opt.runs);
    int r;
    coarsen* C;
    FILE * pFile = fopen(opt.out_file, "w");
    char *out_filep = opt.file_name;
    out_filep[strlen(out_filep)-4]='\0';
    for (r = 0; r<opt.runs; r++){
        printf("########################## RUN %d ########################\n", r);
        srand((unsigned) opt.seed + r);
        initInfoPart(&info);
        
	    C = VCycle(&G, opt, &info);

	    edgecut[r] = edgeCut(&G, C->part);
	    nbcomm[r] = nbCommunications(&G, C->part);
	    latencies[r] = computeLatency(&G, C->part, 1.0, 11.0);
	    printf("Final run %d\t%d\t%d\t%d\t%d\t%f\n", r, G.nVrtx, G.nEdge, (int) edgecut[r], (int) nbcomm[r], latencies[r]);
	    printf("Times\t%.3lf\t%.3lf\t%.3lf\t%.3lf\n", info.timing_coars, info.timing_inipart, info.timing_uncoars,info.timing_global);
	    //print_info_part(&G, C->part, opt);
        //checkAcyclicity(&G, C->part, opt.nbPart);
	    //fprintf(pFile, "mlgp\t%s\t%d\t%d\t%d\t%d\t%.3lf\t%.3lf\t%.3lf\t%.3lf\t%.3lf\n", opt.file_name, opt.nbPart, opt.seed + r, edgecut[r], nbcomm[r], latencies[r], info.timing_global, info.timing_coars, info.timing_inipart, info.timing_uncoars);
	/*
	int* partsize = (int*) malloc(sizeof(int) * opt.nbPart);
	int nn = nbPart(&G, C->part, partsize);
	printf("Nbpart = %d\nPartsize: ", nn);
	int k;
	for (k = 0; k<nn; k++)
	    printf("%d ", partsize[k]);
        	free(partsize);
	*/
	    printf("######################## END RUN %d ########################\n\n", r);
        if (opt.print > 0) {
            printf("######################## OUTPUT %d ########################\n\n", r);
            printInfoPart(&info, C, opt);
            printf("\n###########################################################\n\n");
        }
        freeInfoPart(&info);
    }
    fclose (pFile);

    double edgecutave = 0.0, nbcommave = 0.0, edgecutsd = 0.0, nbcommsd = 0.0, latencyave = 0.0;
    for (r = 0; r<opt.runs; r++){
	edgecutave += edgecut[r];
	nbcommave += nbcomm[r];
	latencyave += latencies[r];
    }
    edgecutave = edgecutave / opt.runs;
    nbcommave = nbcommave / opt.runs;
    latencyave = latencyave / opt.runs;
    for (r = 0; r<opt.runs; r++){
	edgecutsd = edgecut[r] - edgecutave < 0? edgecutave - edgecut[r] : edgecut[r] - edgecutave;
	nbcommsd = nbcomm[r] - nbcommave < 0? nbcommave - nbcomm[r] : nbcomm[r] - nbcommave;
    }
    edgecutsd = edgecutsd / opt.runs;
    nbcommsd = nbcommsd / opt.runs;
    printf("Final runs\t%d\t%d\t%f\t%f\t%f\t%f\t%f\n", G.nVrtx, G.nEdge, edgecutave, edgecutsd, nbcommave, nbcommsd, latencyave);

    /*
    FILE *file;
    char name_tmp[200];
    sprintf(name_tmp,".MLGP.part.%d.seed.%d", nbpart, opt.seed);
    char res_name[200];
    strcpy(res_name, file_name);
    strcat(res_name, name_tmp);
    file = fopen(res_name, "w");
    idxType i;

    //printf("Final part\n");
    for (i=1; i<=G.nVrtx; i++){
	//printf("Node %d -> %d\n", i, C->part[i]); 
	fprintf(file, "%d\n", (int) C->part[i]);
    }
    fclose(file);
    */

    free(edgecut);
    free(nbcomm);
    free(latencies);
    freeCoarsenHeader(C);
    C = (coarsen*) NULL;
}


int main(int argc, char *argv[])
{
    MLGP_option opt;
    processArgs_MLGP(argc, argv, &opt);

    run_MLGP(opt.file_name, opt);

    return 0;
}
