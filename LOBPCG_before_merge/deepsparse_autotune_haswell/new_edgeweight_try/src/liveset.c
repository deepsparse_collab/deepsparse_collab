#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <sys/stat.h>

#include <getopt.h>


#include "dgraph.h"
#include "bmmio.h"
#include "utils.h"
#include "debug.h"
#include "vcycle.h"
#include "partDGraphAlgos.h"
#include "matching.h"
#include "infoPart.h"


int calcLiveSize(dgraph *g,idxType* toporder){
	idxType* incoming = (idxType*) calloc(sizeof(idxType),(g->nVrtx + 2));
    idxType* outgoing = (idxType*) calloc(sizeof(idxType),(g->nVrtx + 2));
    int i=0,j=0;
    int r=0,rend=0;
    int looper=0,lend=0;
    int maxLiveSize=0;
    int liveSize=0;
    /*
    printf("inEdges\n");
    for(i=0;i<g->nEdge;++i)
        printf("%d ",g->in[i]);
    printf("\n");
    printf("outEdges\n");
    for(i=0;i<g->nEdge;++i)
        printf("%d ",g->out[i]);
    
    printf("\ninEdgeRanges\n");
    for(i=1;i<=g->nVrtx;++i)
        printf("%d:: %d to %d\n",i,g->inStart[i],g->inEnd[i]);
    printf("\noutEdgeRanges\n");
    for(i=1;i<=g->nVrtx;++i)
        printf("%d:: %d to %d\n",i,g->outStart[i],g->outEnd[i]);
    printf("toporder\n");
    for(i=1;i<=g->nVrtx;++i){
        printf("%d, ",toporder[i] );
    }
    printf("\n");
    */
    j=1;
    while(j<=g->nVrtx){

    	i = toporder[j];
    	++liveSize;
        //printf("%d is alive now\n", i);
    	if(liveSize>maxLiveSize){
    		maxLiveSize=liveSize;
            //printf("maxLiveSize updated to %d\n", maxLiveSize);
        }
    	r = g->inStart[i];
    	rend = g->inEnd[i];
    	while(r <= rend){
    		++outgoing[g->in[r]];
            //printf("increasing out of %d %d\n",g->in[r], outgoing[g->in[r]]);
    		if(outgoing[g->in[r]]>=g->outEnd[g->in[r]]-g->outStart[g->in[r]]+1){
                //printf("%d dies\n", g->in[r] );
    			--liveSize;
    		}
            else{
                //printf(" %d still alive %d --  %d\n", g->in[r],outgoing[g->in[r]],g->outEnd[g->in[r]]-g->outStart[g->in[r]]+1);
            }
    		++r;
    	}
		looper = g->outStart[i];
        lend = g->outEnd[i];
        if (lend<looper){
            //printf("no child, %d dies, too\n",i);
            --liveSize;
        }
    	while(looper<=lend){
    		++incoming[g->out[looper]];
    		++looper;
    	}
    	++j;
    }


    free(incoming);
    free(outgoing);
    return maxLiveSize;
}

int main(){
    // init input
    char *filename = (char*)malloc(sizeof(char)*15);
    sprintf(filename,"test.dgraph");
    
    // create variables for the function
    dgraph G;
    
    generateDGraphFromFile(&G,filename);
    
    // USAGE
    idxType* toporder = (idxType*) malloc(sizeof(idxType)*(G.nVrtx + 2));
    randBFStopsort(&G, toporder);
    int ls = calcLiveSize(&G,toporder);
    printf("randBFStopsort liveSize: %d\n", ls);

    randDFStopsort(&G, toporder);
    ls = calcLiveSize(&G,toporder);
    printf("randDFStopsort liveSize: %d\n", ls);
    randTopsort(&G, toporder);
    ls = calcLiveSize(&G,toporder);
    printf("randTopsort liveSize: %d\n", ls);
    BFStopsort(&G, toporder);
    ls = calcLiveSize(&G,toporder);
    printf("BFStopsort liveSize: %d\n", ls);
    //-------------
    free(toporder);
    return 0;
}