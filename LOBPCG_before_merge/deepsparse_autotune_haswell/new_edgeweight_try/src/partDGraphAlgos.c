#include <stdio.h>
#include <stdlib.h>
#include <float.h>

#include "debug.h"
#include "dgraph.h"
#include "utils.h"
#include "partDGraphAlgos.h"
#include "refinement.h"

#define VERTEXWEIGHTS(ind) ((G->frmt == 1 || G->frmt == 3) ? G->vw[toporder[ind]] : 1)

/*******************************************************************************/

void print_1d(idxType *a,idxType size){
	int i;

}

int kernighanSequentialPart(dgraph *G, idxType *toporder,
                             double lb_pw, double ub_pw,
			    idxType *partvec, ecType* obj_value, MLGP_option opt)
{
    idxType i, j, nVrtx = G->nVrtx, *outStart= G->outStart, *outEnd = G->outEnd,
    *inStart= G->inStart, *inEnd = G->inEnd, *in = G->in;

    printf("kernighan vrtx = %d\n",nVrtx);

    printf("\nlb_pw = %lf ub_pw = %lf\n",lb_pw,ub_pw);

//
//    for (i = 0 ; i < G->nEdge ; ++i){
//    	printf("in[%d] = %d\n",i,in[i]);
//    }
//    for (i = 0 ; i <= G->nEdge ; ++i){
//    	printf("out[%d] = %d\n",i,G->out[i]);
//    }
//    for (i = 1 ; i <= nVrtx ; ++i){
//    	printf("instart[%d] = %d\n",i,inStart[i]);
//    }
//    for (i = 1 ; i <= nVrtx ; ++i){
//    	printf("inend[%d] = %d\n",i,inEnd[i]);
//    }
//    for (i = 1 ; i <= nVrtx ; ++i){
//    	printf("outStart[%d] = %d\n",i,outStart[i]);
//    }
//    for (i = 1 ; i <= nVrtx ; ++i){
//    	printf("outend[%d] = %d\n",i,outEnd[i]);
//
//    }


//   print_dgraph(*G);
    idxType *seps, x, y, ymin, ymax, minat;
    ecType *outcost = G->ecOut, *incost = G->ecIn;
    double *prefixSum, *T, *cxy, *cx_1y, sumx_1n, sumyx_1,tymin;/*double because addign them up could overflow if int*/
    idxType *topindex, pno;
    topindex = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    double t_start,t_end;
    
    
    /*allocate, prefix sum*/
    prefixSum = (double *) malloc(sizeof(double) * (nVrtx + 2));
    T =         (double *) malloc(sizeof(double) * (nVrtx + 2));
    cxy =       (double *) malloc(sizeof(double) * (nVrtx + 2));
    /* we will compute for x, x+1, x+2; we will have cxy comoputed for x, then update it fot using with j x+1*/
    cx_1y =     (double *) malloc(sizeof(double) * (nVrtx + 2));
    
    seps = (idxType*) malloc(sizeof(idxType) * (nVrtx + 2));

    printf("\n\ninside kernighan sequential part , all allocation done\n\n");
    
    t_start = u_wseconds();
    for (i = 0; i<=nVrtx+1; i++)
   {
        seps[i] = 0;
        T[i] = DBL_MAX;
        cxy[i] = cx_1y[i]= 0.0;
    }
    t_end = u_wseconds();
    printf("kernighanSequentialPart initialize values time = %lf\n",t_end-t_start);
    
    prefixSum[0] = 0.0;
    if(G->frmt ==1 || G->frmt == 3)
        for (i=1; i<=nVrtx; i++){
            prefixSum[i] = prefixSum[i-1] + G->vw[toporder[i]];
//            printf("\ntoporder[%d] = %d weight = %d prefixsum[%d] = %.0lf\n",i,toporder[i],G->vw[toporder[i]],i,prefixSum[i]);
//            printf("\nprefixSum[%d] = %d\t vw[%d] = %d\n",i,prefixSum[i],toporder[i],G->vw[toporder[i]]);
	   }


    else
        for (i=1; i<=nVrtx; i++)
            prefixSum[i] = i;
    
       // for(i = 1 ; i <= nVrtx ; ++ i ){
      // 	printf("\nprefixSum[%d] = %d\n",i,prefixSum[i]);
      //  }

    for (i = 1; i <= nVrtx; i++)
        topindex[toporder[i]] = i;


//    for(i = 1 ; i <= nVrtx ; ++ i ){
//    	printf("\ntoporder[%d] = %d\n",i,toporder[i]);
//    }
//    for(i = 1 ; i <= nVrtx ; ++ i ){
//        	printf("\ntopindex[%d] = %d\n",i,topindex[i]);
//        }
    
    /*initialize dyn prog*/
    
    /*we can start with x=1 as the first vertex*/
    seps[1] = 0;
    T[0] = T[1] = 0.0;
    
    t_start = u_wseconds();
    x=2;
    for(; x <=nVrtx+1; x++){ 
        double loop_start = 0;
        double loop_end = 0;
        //printf("inside x loop x = %d\n",x );       
        tymin = DBL_MAX;
        minat = -1;
        //for(ymin = x-1; ymin>=1 &&  prefixSum[x-1]-prefixSum[ymin]+VERTEXWEIGHTS(ymin)<=ub_pw; ymin--)
        loop_start = u_wseconds();
	for(ymin = x-1; ymin>=1 &&  prefixSum[x-1]-prefixSum[ymin-1]<=ub_pw; ymin--)
            ;
        ymin++;
        
        for(ymax = ymin;  ymax <x && prefixSum[x-1] - prefixSum[ymax-1]>=lb_pw; ymax++)
            ;
        ymax--;
        loop_end = u_wseconds();
     //   printf("x = %d y mix and min time = %lf\n",x,loop_end-loop_start);
 //       printf("For x = %d, ymin = %d and ymax = %d\noutStart[toporder[x-1]] = outStart[%d] = %d\noutEnd[toporder[x-1]] = %d\n", (int) x, (int) ymin, (int) ymax, (int) toporder[x-1], (int) outStart[toporder[x-1]], (int) outEnd[toporder[x-1]]);
        
        sumx_1n = 0.0;

        loop_start = u_wseconds();
        for(j = outStart[toporder[x-1]]; j<=outEnd[toporder[x-1]]; j++)
            sumx_1n += (G->frmt == 2 || G->frmt == 3) ? outcost[j] : 1;

        loop_end = u_wseconds();
     //   printf("x = %d sumx_1n time = %lf\n",x,loop_end-loop_start);
        sumyx_1 = 0.0;
        loop_start = u_wseconds();
        for(j=inStart[toporder[x-1]]; j<=inEnd[toporder[x-1]]; j++)
        {
            cx_1y[topindex[in[j]]] = (G->frmt == 2 || G->frmt == 3) ? incost[j] : 1;
            
            if(topindex[in[j]] >= ymin )
                sumyx_1  +=  (G->frmt == 2 || G->frmt == 3) ? incost[j] : 1;
        }
        loop_end = u_wseconds();
      //  printf("x = %d sumyx_1 time = %lf\n",x,loop_end-loop_start);



        for(y = ymin; y <= ymax ; y++)/*we only check for the feasible ones*/
        {
            cxy[y] = cxy[y] + sumx_1n - sumyx_1;
            sumyx_1 -= cx_1y[y];
            if(T[y] != DBL_MAX)
            {
                minat = tymin <= T[y] + cxy[y] ? minat : y  ;
                tymin = tymin <= T[y] + cxy[y] ? tymin : T[y] + cxy[y] ;
            }
        }
        
        loop_start = u_wseconds();
        if(minat != -1)
        {
            T[x] = tymin;
            seps[x] = minat;
//	    printf("Set T[%d] = %f\tseps[%d] = %d\n\twith T[%d] + cxy[%d] = %f + %f\n",x,tymin,x,minat,minat,minat,T[minat],cxy[minat]);
            for(y = ymax+1; y < x ; y++)/*till x so that cxy's are up to date when we need them*/
            {
                cxy[y] = cxy[y] + sumx_1n - sumyx_1;
                sumyx_1 -= cx_1y[y];                
            }
        }
        else
        {
            if(ymin>=ymax)
                for(y = ymax+1; y < x ; y++)/*till x so that cxy's are up to date when we need them*/
                {
                    cxy[y] = cxy[y] + sumx_1n - sumyx_1;
                    sumyx_1 -= cx_1y[y];
                    
                }
			else
			for(y = ymax+1; y < x ; y++)/*till x so that cxy's are up to date when we need them*/
				{
				cxy[y] = cxy[y] + sumx_1n - sumyx_1;
				sumyx_1 -= cx_1y[y];
				}
        }
        loop_end = u_wseconds();
      //  printf("x = %d set t and sep time = %lf\n",x,loop_end-loop_start);
        for(j=inStart[toporder[x-1]]; j<=inEnd[toporder[x-1]]; j++)
            cx_1y[topindex[in[j]]] = 0.0;
    }
    t_end = u_wseconds();

    printf("big for loop time = %lf vertex = %d\n",t_end-t_start,nVrtx);
    
    /*dynp prog done. now recover the solution.*/
//    if (opt.debug > 0)
//	   printf("\n\n\nKernighan Initial partition cost %.0f\n\n", T[nVrtx+1]);
    *obj_value = (ecType) T[nVrtx+1];
    pno = 0;
    x = nVrtx+1;
    while( x>=1 ) {
        for(y = seps[x]; y < x; y++)
            if (y > 0)
                partvec[toporder[y]] = pno;
	//printf("x = %d\tpno = %d\tseps[x] = %d cxy = %d\n", (int) x, (int) pno, (int) seps[x], cxy[seps[x]]);
        x = seps[x];
        pno ++;
    }
    pno = pno-1;/*we renumber everything so that the pnos are in topological order*/
    if (opt.debug > 0)
        printf(" (nbpart = %d)\n", pno);
    for(x = 1; x<=nVrtx; x++) {
        partvec[x] = pno-partvec[x]-1;
 //       printf("Partvec[%d] = %d\n", x, partvec[x]);
    }
    free(topindex);
    free(seps);
    free(cxy);
    free(T);
    free(cx_1y);
    free(prefixSum);
    printf("kernighanSequentialPart part no = %d\n",pno);
    return pno; /* return #parts found */
}


int kernighanSequentialExactPart(dgraph *G, idxType *toporder,
				 int nbpart, double* ub_pw,
			    idxType *partvec, ecType* obj_value, MLGP_option opt)
//Kernighan algorithm with exact number of partition components
{
    //double eca = ub_pw - G->nVrtx/(nbpart*1.0);
    //double lb_pw = G->nVrtx/(nbpart*1.0) - (nbpart - 1)*eca;
    //lb_pw = lb_pw < 0.0 ? 0.0 : lb_pw;
    double lb_pw = 1.0;
    //double lb_pw = 0.0;

    idxType i, j, k, nVrtx = G->nVrtx, *outStart= G->outStart, *outEnd = G->outEnd,
    *inStart= G->inStart, *inEnd = G->inEnd, *in = G->in;
    
    idxType **seps, x, y, ymin, ymax, minat;
    ecType *outcost = G->ecOut, *incost = G->ecIn;
    double *prefixSum, **T, *cxy, *cxy_save, *cx_1y, sumx_1n, sumyx_1,tymin;/*double because addign them up could overflow if int*/
    idxType *topindex, pno;
    topindex = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    
    
    /*allocate, prefix sum*/
    prefixSum = (double *) malloc(sizeof(double) * (nVrtx + 2));
    T =         (double **) malloc(sizeof(double*) * (nVrtx + 2));
    cxy =       (double *) calloc((nVrtx + 2), sizeof(double));
    cxy_save =       (double *) calloc((nVrtx + 2), sizeof(double));
    /* we will compute for x, x+1, x+2; we will have cxy comoputed for x, then update it fot using withj x+1*/
    cx_1y =     (double *) calloc((nVrtx + 2), sizeof(double));
    
    seps = (idxType**) malloc(sizeof(idxType*) * (nVrtx + 2));

    for (i=0; i<=nVrtx+1; i++){
        seps[i] = (idxType *) calloc(nbpart, sizeof(idxType));
        T[i] = (double *) malloc(nbpart * sizeof(double));
        for (k=0; k<nbpart; k++)
            T[i][k] = DBL_MAX;
    }    

    prefixSum[0] = 0.0;
    if(G->frmt ==1 || G->frmt == 3)
        for (i=1; i<=nVrtx; i++){
            prefixSum[i] = prefixSum[i-1] + G->vw[toporder[i]];
            topindex[toporder[i]] = i;  
        }
    else
        for (i=1; i<=nVrtx; i++){
            prefixSum[i] = i;
            topindex[toporder[i]] = i;
        }
    for(x=2; x <=nVrtx+1; x++){
        tymin = DBL_MAX;
        minat = -1;
        
        
 //       printf("For k = %d x = %d, ymin = %d ymax = %d\n",k,x,ymin,ymax);
        double sumyx_1_save = sumyx_1;
        for(y = 0; y < x ; y++)
            cxy_save[y] = cxy[y];

        for (k=0; k<nbpart; k++){

            for(ymin = x-1; ymin>=1 &&  prefixSum[x-1]-prefixSum[ymin-1]<=ub_pw[k]; ymin--)
                ;
            ymin++;
            
            for(ymax = ymin; ymax <x && prefixSum[x-1] - prefixSum[ymax-1]>=lb_pw; ymax++)
                ;
            ymax--;

            if (k>0){
                sumyx_1 = sumyx_1_save;
                for(y = ymin; y < x ; y++)
                    cxy[y] = cxy_save[y];
            }


            sumyx_1 = 0.0;
            for(j=inStart[toporder[x-1]]; j<=inEnd[toporder[x-1]]; j++)
            {
                cx_1y[topindex[in[j]]] = (G->frmt == 2 || G->frmt == 3) ? incost[j] : 1;
                    
                if(topindex[in[j]] >= ymin )
                    sumyx_1  +=  (G->frmt == 2 || G->frmt == 3) ? incost[j] : 1;
            }
               
            sumx_1n = 0.0;
            for(j = outStart[toporder[x-1]]; j<=outEnd[toporder[x-1]]; j++)
                sumx_1n += (G->frmt == 2 || G->frmt == 3) ? outcost[j] : 1;
                

            for(y = ymin; y <= ymax; y++)/*we only check for the feasible ones*/
            {
                cxy[y] = cxy[y] + sumx_1n - sumyx_1;
                sumyx_1 -= cx_1y[y];
                
                if (k>0)
                    if(T[y][k-1] != DBL_MAX){
                        minat = tymin <= T[y][k-1] + cxy[y] ? minat : y  ;
                        tymin = tymin <= T[y][k-1] + cxy[y] ? tymin : T[y][k-1] + cxy[y] ;      
                    }
            }
            for(y = ymax+1; y < x ; y++)/*till x so that cxy's are up to date when we need them*/
            {
                cxy[y] = cxy[y] + sumx_1n - sumyx_1;
                sumyx_1 -= cx_1y[y];                
            }
            if (k==0){
                if (prefixSum[x-1] <= ub_pw[k]){
                    minat = 1;
                    tymin = cxy[1];
                }
                else{
                    minat = -1;
                    tymin = DBL_MAX;
                }
            }       
            if(minat != -1)
            {
                T[x][k] = tymin;
                seps[x][k] = minat;
                //printf("Set T[%d][%d] = %f\tseps[%d][%d] = %d\nymin = %d ymax = %d\n\twith T[%d][%d] + cxy[%d] = %f + %f\n",x,k,tymin,x,k,minat,ymin,ymax,minat,k-1,minat,T[minat][k-1],cxy[minat]);
            }
        }
        for(j=inStart[toporder[x-1]]; j<=inEnd[toporder[x-1]]; j++)
            cx_1y[topindex[in[j]]] = 0.0;
    }
    /*dynp prog done. now recover the solution.*/
    if (opt.debug > 0)
       printf("\n\nExact Kernighan Initial partition cost %.0f\n\n", T[nVrtx+1][nbpart-1]);
    *obj_value = (ecType) T[nVrtx+1][nbpart-1];
    pno = 0;
    x = nVrtx+1;
    while( x>1 ) {
        for(y = seps[x][nbpart-1-pno]; y < x; y++)
            if (y > 0)
                partvec[toporder[y]] = pno;
    //printf("x = %d\tpno = %d\tseps[x][nbpart-1-pno] = %d cxy = %d\n", (int) x, (int) pno, (int) seps[x][nbpart-1-pno], cxy[seps[x][nbpart-1-pno]]);
        x = seps[x][nbpart-1-pno];
        pno ++;
    }
    if (opt.debug > 0)
        printf(" (nbpart = %d)\n", pno);
    //pno = pno-1;/*we renumber everything so that the pnos are in topological order*/
    for(x = 1; x<=nVrtx; x++) {
        partvec[x] = pno-partvec[x]-1;
    }
    free(topindex);
    free(cxy);
    free(cxy_save);
    for (i=0; i<=nVrtx+1; i++){
        free(T[i]);
        free(seps[i]);
    }
    free(seps);
    free(T);
    free(cx_1y);
    free(prefixSum);
    return pno; /* return #parts found */
}


void computeMovabletoNodeIni(dgraph* G, idxType* part, int nbpart, idxType* topsortpart, idxType* toporderpart, idxType* partsize, idxType* movableto, idxType i)
{
    idxType moveup = -1, j;
    for (j=G->inStart[i]; j<=G->inEnd[i]; j++){
    idxType innode = G->in[j];
    idxType parti = part[innode];
    if (moveup == -1)
        moveup = parti;
    if ((topsortpart[parti] > topsortpart[moveup])&&(topsortpart[parti] <= topsortpart[part[i]]))
        moveup = parti;
    }
    if (moveup == -1)
    moveup = topsortpart[part[i]] == 0 ? part[i] : toporderpart[topsortpart[part[i]]-1];
    idxType movedown = -1;
    for (j=G->outStart[i]; j<=G->outEnd[i]; j++){
	idxType outnode = G->out[j];
	idxType parti = part[outnode];
	if (movedown == -1)
	    movedown = parti;
	if ((topsortpart[parti] < topsortpart[movedown])&&(topsortpart[parti] >= topsortpart[part[i]]))
	    movedown = parti;
    }
    if (movedown == -1)
	movedown = topsortpart[part[i]] == nbpart-1 ? part[i] : toporderpart[topsortpart[part[i]]+1];
    if ((movedown == part[i])&&(moveup == part[i]))
	movableto[i] = part[i];
    else if ((movedown != part[i])&&(moveup != part[i]))
	if (partsize[movedown] < partsize[moveup])
	    movableto[i] = movedown;
	else
	    movableto[i] = moveup;
    else if (moveup != part[i])
	movableto[i] = moveup;
    else
	movableto[i] = movedown;
}


void initialPartitioningForcedBalance(dgraph *G, double* ub_pw, idxType *part, idxType nbpart, MLGP_option opt)
{
    int frmt = G->frmt;
    idxType i,j,k;
    idxType* movableto = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    idxType* partsize = (idxType*) calloc(nbpart+1, sizeof(idxType));
    ecType* gain = (ecType*) malloc(sizeof(ecType)*(G->nVrtx+1));
    int* inheap = (int*) malloc(sizeof(int)*(G->nVrtx+1));
    idxType* heap = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    idxType hsize = 0;
    int cur_filling_part = nbpart-1;

    for (i=1; i<=G->nVrtx; i++){
    part[i] = 0;
    if (frmt & DG_FRMT_VW) /* if it has vertex weights */
        partsize[part[i]] += G->vw[i];  
    else
        partsize[part[i]] += 1;
    }
    
    while (cur_filling_part > 0){
        hsize = 0;
        for (i=1; i<=G->nVrtx; i++)
            if (part[i] == 0)
            movableto[i] = cur_filling_part;
        for (i=1; i<=G->nVrtx; i++){
            inheap[i] = 0;
            gain[i] = computeGainNode(G, part, movableto[i], i, opt);
            int putinheap = part[i] == 0 ? 1 : 0;
            for (j = G->outStart[i]; j <= G->outEnd[i]; j++)
            if (part[G->out[j]] == part[i]){
                putinheap = 0;
                break;
            }
            if (putinheap){
                heap[++hsize] = i;
                inheap[i] = 1;
            }
            if (frmt & DG_FRMT_VW) /* if it has vertex weights */
               partsize[part[i]] += G->vw[i];   
            else
               partsize[part[i]] += 1;
        }

        randHeapBuild(G, heap, gain, hsize);

        idxType node = 1;

        while ((hsize > 0) &&(partsize[cur_filling_part] < 0.9*ub_pw[cur_filling_part])){
        //while ((hsize > 0) &&(partsize[cur_filling_part] < 0.9*ub_pw)){
            node = heapExtractMax(G, heap, gain, &hsize);
            inheap[node] = 0;
            
            partsize[part[node]] = partsize[part[node]] - G->vw[node];
            partsize[movableto[node]] = partsize[movableto[node]] + G->vw[node];

            part[node] = movableto[node];

            for (j=G->inStart[node]; j<=G->inEnd[node]; j++){
            idxType father = G->in[j];
            gain[father] = computeGainNode(G, part, movableto[father], father, opt);
            int putinheap = part[father] == 0 ? 1 : 0;
            for (k = G->outStart[father]; k <= G->outEnd[father]; k++)
                if (part[G->out[k]] == part[father]){
                putinheap = 0;
                break;
                }
            if (putinheap){
                randHeapInsert(G, heap, gain, &hsize, father);
                inheap[father] = 1;
            }   
            }
        }
        cur_filling_part--;
    }   
    free(heap);
    free(movableto);
    free(partsize);        
    free(gain);
    free(inheap);
    /*
    for(i=1;i<=G->nVrtx;++i)
        printf("i=%d part[i]=%d\n",i,part[i]);
    */
    if(opt.debug > 3)
        printf("initialPartitioningForcedBalance end\n");
}

void greedyGraphGrowing(dgraph *G, double* ub_pw, idxType *part, idxType nbpart, MLGP_option opt)
{
    // ONLY FOR 2 PARTITIONS
    int frmt = G->frmt; 
    idxType i,j,k;
    idxType* movableto = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    idxType* partsize = (idxType*) calloc(nbpart+1, sizeof(idxType));
    ecType* gain = (ecType*) malloc(sizeof(ecType)*(G->nVrtx+1));
    int* inheap = (int*) malloc(sizeof(int)*(G->nVrtx+1));
    idxType* heap = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    idxType* outputNodes = (idxType*) calloc((G->nVrtx+1),sizeof(idxType));
    idxType outputCount = 0;
    idxType hsize = 0;
    int cur_filling_part = nbpart-1;

    for (i=1; i<=G->nVrtx; i++){
        part[i] = 0;
        if (frmt & DG_FRMT_VW) /* if it has vertex weights */
            partsize[part[i]] += G->vw[i];  
        else
            partsize[part[i]] += 1;
    }

    // ONLY FOR 2 PARTS
    // Find random initial point
    outputCount = outputList(G,outputNodes);
    /*
    printf("outputCount %d\n",outputCount );
    for(i=0;i<outputCount;++i)
        printf("%d ",outputNodes[i]);
    printf("outputnodes end\n");
    */
    int coreIdx;
    int starterFlag = 1;
    coreIdx = rand() % outputCount; //some random node in the graph
    //printf("coreIdx=%d node = %d\n",coreIdx,outputNodes[coreIdx] );
    hsize = 0;
    for (i=1; i<=G->nVrtx; i++)
        if (part[i] == 0)
            movableto[i] = cur_filling_part;
    for (i=1; i<=G->nVrtx; i++) {
        inheap[i] = 0;
        gain[i] = computeGainNode(G, part, movableto[i], i, opt);
        int putinheap = part[i] == 0 ? 1 : 0;
        for (j = G->outStart[i]; j <= G->outEnd[i]; j++)
            if (part[G->out[j]] == part[i]){
                putinheap = 0;
                break;
            }
        // don't put the core node in the heap. We start with that node
        if (putinheap && outputNodes[coreIdx]!=i){
            heap[++hsize] = i;
            inheap[i] = 1;
        }
        if (frmt & DG_FRMT_VW) /* if it has vertex weights */
            partsize[part[i]] += G->vw[i];
        else
            partsize[part[i]] += 1;
    }

    randHeapBuild(G, heap, gain, hsize);

    idxType node = 1;

    while ((starterFlag) || ((hsize > 0) &&(partsize[cur_filling_part] < 0.9*ub_pw[cur_filling_part]))){
        if (starterFlag) {
            node = outputNodes[coreIdx];
            //printf(" initial node[%d] = %d from STARTERS\n",coreIdx,node );
            /*
            while(outputNodes[coreIdx]==node){
               coreIdx = rand()%outputCount; //some random node in the graph
                // This is in case a node does not provide at least one growth
                //printf("starter loop %d\n",coreIdx);
            }
             node = outputNodes[coreIdx];
            */
            starterFlag=0;
        }
        else {
            node = heapExtractMax(G, heap, gain, &hsize);
            //printf("node getting from heap %d\n", node);
        }
        inheap[node] = 0;
        //printf("node %d\n",node );
        partsize[part[node]] = partsize[part[node]] - G->vw[node];
        partsize[movableto[node]] = partsize[movableto[node]] + G->vw[node];

        part[node] = movableto[node];

        for (j=G->inStart[node]; j<=G->inEnd[node]; j++) {
            idxType father = G->in[j];
            gain[father] = computeGainNode(G, part, movableto[father], father, opt);
            int putinheap = part[father] == 0 ? 1 : 0;
            for (k = G->outStart[father]; k <= G->outEnd[father]; k++)
                if (part[G->out[k]] == part[father]){
                    putinheap = 0;
                    break;
                }
            if (putinheap) {
                randHeapInsert(G, heap, gain, &hsize, father);
                inheap[father] = 1;
                starterFlag=0;
            }
        }
    }
    
    free(heap);
    free(movableto);
    free(partsize);        
    free(gain);
    free(inheap);
    free(outputNodes);
    /*
    for(i=1;i<=G->nVrtx;++i)
        printf("i=%d part[i]=%d\n",i,part[i]);
    */
    if(opt.debug > 3)
        printf("greedyGraphGrowing end\n");
}

void BFSGrowing(dgraph *G, double* ub_pw, idxType *part, idxType nbpart, MLGP_option opt, MLGP_info* info)
{
    //Actually, this is DFSGrowing.

    if(opt.debug > 10)
        printf("BFSGrowing start\n");
    int frmt = G->frmt;
    idxType i;
    idxType* partsize = (idxType*) calloc(nbpart+1, sizeof(idxType));
    int* queue = (int*) malloc((G->nVrtx+1)*sizeof(int));
    int* mark = (int*) calloc(G->nVrtx+1,sizeof(int)); 
    idxType node = 0,qend=0,qstart = 0;
    int coreIdx;
    int cur_filling_part = nbpart-1; // 2 - 1 = 1
    for (i=1; i<=G->nVrtx; i++){
        part[i] = 0;
        if (frmt & DG_FRMT_VW) /* if it has vertex weights */
            partsize[part[i]] += G->vw[i];  
        else
            partsize[part[i]] += 1;
    }
    
    while (cur_filling_part > 0){
        
        while (partsize[cur_filling_part] < 0.85*ub_pw[cur_filling_part]){
            //printf("partsize[%d] = %d / %d\n", cur_filling_part, (int) partsize[cur_filling_part], (int) ub_pw[cur_filling_part]);
            coreIdx = rand()%G->nVrtx + 1; //some random node in the graph 
            if(opt.debug > 10)
                printf("coreIdx=%d\n",coreIdx );
            partsize[part[coreIdx]] -= G->vw[coreIdx];
            part[coreIdx] = cur_filling_part;
            partsize[part[coreIdx]] += G->vw[coreIdx];
            queue[0]=coreIdx;
            qstart = 0;
            qend=1;
            while(qstart<qend){
          //  while(qend){
                //--qend; //last location in queue
                //node=queue[qend];
                node = queue[qstart];
                //printf("traversed node = %d\n",node);
                qstart++;
                //printf("BFSgrowing travers node = %d\n",node);
                partsize[part[node]] -= G->vw[node];
                part[node] = cur_filling_part;
                partsize[part[node]] += G->vw[node];
                for(i=G->outStart[node];i<=G->outEnd[node];++i){
                    if(mark[G->out[i]]==0){
                        mark[G->out[i]]=1;
                        queue[qend]=G->out[i];
                        ++qend;
                    }

                }
            }
        }
        cur_filling_part--;
    }
    idxType* toporder = (idxType*) malloc(sizeof(idxType)*nbpart);
    randTopSortOnParts(G, part, toporder, nbpart);
    free(partsize);
    partsize = forcedBalance(G, toporder, opt.ub, part, nbpart, opt, info);
    free(toporder);
    //free(fBpartsize);
    if(opt.debug > 10){
        printf("\n");
        int c1=0,c2=0;
        for(i=1;i<=G->nVrtx;++i){
            printf("%d ",part[i] );
            if(part[i])
                c2++;
            else
                c1++;
        }
        printf("\n0-->%d        1-->%d\n",c1,c2);
        printf("\npartsizes\n0-->%d        1-->%d\n",partsize[0],partsize[1]);
    }
    free(mark);
    free(queue);
    free(partsize);
    if(opt.debug > 10)
        printf("BFSGrowing end\n");
}

void CocktailShake(dgraph *G, double* ub_pw, idxType *part, idxType nbpart, MLGP_option opt)
{
    if(opt.debug > 3)
        printf("CocktailShake start\n");
    
    int mainCounter = 5;
    int frmt = G->frmt;
    idxType i;
    idxType* partsize = (idxType*) calloc(nbpart+1, sizeof(idxType));
    int* queue = (int*) malloc((G->nVrtx+1)*sizeof(int));
    int* mark = (int*) calloc(G->nVrtx+1,sizeof(int)); 
    idxType node = 0,qend=0;
    int coreIdx;
    int cur_filling_part = nbpart-1; // 2 - 1 = 1
    for (i=1; i<=G->nVrtx; i++){
        part[i] = 0;
        if (frmt & DG_FRMT_VW) /* if it has vertex weights */
            partsize[part[i]] += G->vw[i];  
        else
            partsize[part[i]] += 1;
    }
    do{
        while (partsize[cur_filling_part] < 0.85*ub_pw[cur_filling_part]){
            coreIdx = rand()%G->nVrtx + 1; //some random node in the graph 
            if(opt.debug > 10)
                printf("coreIdx=%d\n",coreIdx );
            mark[coreIdx]=1;
            partsize[part[coreIdx]] -= G->vw[coreIdx];
            part[coreIdx] = cur_filling_part;
            partsize[part[coreIdx]] += G->vw[coreIdx];
            queue[0]=coreIdx;
            qend=1;
            while(qend){
                --qend; //last location in queue
                node=queue[qend];
                partsize[part[node]] -= G->vw[node];
                part[node] = cur_filling_part;
                partsize[part[node]] += G->vw[node];
                for(i=G->outStart[node];i<=G->outEnd[node];++i){
                    if(mark[G->out[i]]==0){
                        mark[G->out[i]]=1;
                        queue[qend]=G->out[i];
                        ++qend;
                    }

                }
            }
        }
        if(partsize[cur_filling_part]>ub_pw[cur_filling_part] && mainCounter>0){
            memset(mark, 0, sizeof(int) * (G->nVrtx+1));
            while (partsize[cur_filling_part] > ub_pw[cur_filling_part]){
            
                coreIdx = rand()%G->nVrtx + 1; //some random node in the graph 
                if(opt.debug > 10)
                    printf("coreIdx=%d\n",coreIdx );
                mark[coreIdx]=1;
                partsize[part[coreIdx]] -= G->vw[coreIdx];
                part[coreIdx] = 0;
                partsize[part[coreIdx]] += G->vw[coreIdx];
                queue[0]=coreIdx;
                qend=1;
                while(qend){
                    --qend; //last location in queue
                    node=queue[qend];
                    partsize[part[node]] -= G->vw[node];
                    part[node] = 0;
                    partsize[part[node]] += G->vw[node];
                    for(i=G->inStart[node];i<=G->inEnd[node];++i){
                        if(mark[G->in[i]]==0){
                            mark[G->in[i]]=1;
                            queue[qend]=G->in[i];
                            ++qend;
                        }

                    }
                }
            }
        
        }
        else{
            break;
        }
        --mainCounter;
        memset(mark, 0, sizeof(int) * (G->nVrtx+1));
    }while(1);
        cur_filling_part--; 
    if(opt.debug > 10){
        printf("\n");
        int c1=0,c2=0;
        for(i=1;i<=G->nVrtx;++i){
            printf("%d ",part[i] );
            if(part[i])
                c2++;
            else
                c1++;
        }
        printf("\n0-->%d        1-->%d\n",c1,c2);
        printf("\npartsizes\n0-->%d        1-->%d\n",partsize[0],partsize[1]);
    }
    free(mark);
    free(queue);
    free(partsize);
    
    if(opt.debug > 3)
        printf("CocktailShake end\n");

}

void peripheralRootGreedyGraphGrowing(dgraph *G, double* ub_pw, idxType *part, idxType nbpart, MLGP_option opt)
{

    //NOTE: DO NOT USE FOR TESTING


    // ONLY FOR 2 PARTITIONS
    int frmt = G->frmt; 
    idxType i,j,k;
    idxType* movableto = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    idxType* partsize = (idxType*) calloc(nbpart+1, sizeof(idxType));
    ecType* gain = (ecType*) malloc(sizeof(ecType)*(G->nVrtx+1));
    int* inheap = (int*) malloc(sizeof(int)*(G->nVrtx+1));
    idxType* heap = (idxType*) malloc(sizeof(idxType)*(G->nVrtx+1));
    idxType* inputNodes = (idxType*) calloc((G->nVrtx+1),sizeof(idxType));
    idxType hsize = 0;
    int* queue = (int*) malloc((G->nVrtx+1)*sizeof(int));
    int* queueDep = (int*) malloc((G->nVrtx+1)*sizeof(int));
    int* mark = (int*) calloc((G->nVrtx+1),sizeof(int));
    int inputCount;

    int cur_filling_part = nbpart-1;

    for (i=1; i<=G->nVrtx; i++){
        part[i] = 0;
        if (frmt & DG_FRMT_VW) /* if it has vertex weights */
            partsize[part[i]] += G->vw[i];  
        else
            partsize[part[i]] += 1;
    }

    // ONLY FOR 2 PARTS
    // Find random initial point
    inputCount = sourcesList(G,inputNodes);

    int coreIdx,coreDep;
    int starterFlag = 1;
    // variables below for diameter
    int maxdist = -1, maxdistnode;
    int qend,qstart;
    int forward = 1;
    int lastNode=inputNodes[0];
    int curdist = 0,curdistnode=lastNode;
    int finisherNode = -1;
    // variables end

    //find diameter
    while(curdist > maxdist){
        maxdist= curdist;
        maxdistnode = curdistnode;
        qend=1,qstart=0;
        memset(queueDep, 0, sizeof(int) * (G->nVrtx+1));
        memset(queue, 0, sizeof(int) * (G->nVrtx+1));
        memset(mark, 0, sizeof(int) * (G->nVrtx+1));
        curdist=0;
        queue[0]=lastNode;
        if(!forward)
            finisherNode = lastNode;
        queueDep[0]=0;
        while(qend!=qstart){
            coreIdx =queue[qstart];
            coreDep = queueDep[qstart];
            if(coreDep > curdist){
                curdist=coreDep;
                curdistnode=coreIdx;
            }
            qstart++;
            qstart=qstart%(G->nVrtx+1);
            mark[coreIdx]=1;
            if (forward)
                for(i=G->outStart[coreIdx];i<=G->outEnd[coreIdx];++i){
                    if(mark[G->out[i]]==0){
                        mark[G->out[i]]=1;
                        queue[qend]=G->out[i];
                        queueDep[qend]=coreDep+1;
                        ++qend;
                        qend=qend%(G->nVrtx+1);
                    }
                }
            else
                for(i=G->inStart[coreIdx];i<=G->inEnd[coreIdx];++i){
                    if(mark[G->in[i]]==0){
                        mark[G->in[i]]=1;
                        queue[qend]=G->in[i];
                        queueDep[qend]=coreDep+1;
                        ++qend;
                        qend=qend%(G->nVrtx+1);
                    }
                }
        }
        lastNode = curdistnode;
        forward = 1-forward;
    }

    free(queueDep);        
    free(queue);        
    free(mark);
    //printf("num node %d\n",G->nVrtx );
    //printf("curdist %d curdistnode %d\n",maxdist,maxdistnode );
    //printf("finisherNode %d in\n",finisherNode );

    hsize = 0;
    for (i=1; i<=G->nVrtx; i++)
        if (part[i] == 0)
            movableto[i] = cur_filling_part;
    for (i=1; i<=G->nVrtx; i++){
        inheap[i] = 0;
        gain[i] = computeGainNode(G, part, movableto[i], i, opt);
        int putinheap = part[i] == 0 ? 1 : 0;
        for (j = G->outStart[i]; j <= G->outEnd[i]; j++)
            if (part[G->out[j]] == part[i]){
                putinheap = 0;
                break;
            }
        // don't put the core node in the heap. We start with that node
        if (putinheap && finisherNode!=i){
            heap[++hsize] = i;
            inheap[i] = 1;
        }
        if (frmt & DG_FRMT_VW) /* if it has vertex weights */
            partsize[part[i]] += G->vw[i];  
        else
            partsize[part[i]] += 1;
    }

        randHeapBuild(G, heap, gain, hsize);

    idxType node = 1;
    
        while ((starterFlag)||((hsize > 0) &&(partsize[cur_filling_part] < 0.9*ub_pw[cur_filling_part]))){
            if(starterFlag){
                node = finisherNode;
                //printf(" initial node[%d] = %d from STARTERS\n",coreIdx,node );
                /*
                while(inputNodes[coreIdx]==node){
                    coreIdx = rand()%inputCount; //some random node in the graph
                    // This is in case a node does not provide at least one growth
                    //printf("starter loop %d\n",coreIdx);
                }
                node = inputNodes[coreIdx];
                */
                starterFlag=0;
            }
            else{
                node = heapExtractMax(G, heap, gain, &hsize);
                //printf("node getting from heap %d\n", node);
            }
            inheap[node] = 0;
            //printf("node %d\n",node );
            partsize[part[node]] = partsize[part[node]] - G->vw[node];
            partsize[movableto[node]] = partsize[movableto[node]] + G->vw[node];

            part[node] = movableto[node];

            for (j=G->inStart[node]; j<=G->inEnd[node]; j++){
            idxType father = G->in[j];
            gain[father] = computeGainNode(G, part, movableto[father], father, opt);
            int putinheap = part[father] == 0 ? 1 : 0;
            for (k = G->outStart[father]; k <= G->outEnd[father]; k++)
                if (part[G->out[k]] == part[father]){
                putinheap = 0;
                break;
                }
            if (putinheap){
                randHeapInsert(G, heap, gain, &hsize, father);
                inheap[father] = 1;
                starterFlag=0;
            }   
            }
        }
    
    free(heap);
    free(movableto);
    free(partsize);           
    free(gain);
    free(inheap);
    free(inputNodes);
    /*
    printf("parts\n");
    for(i=1;i<=G->nVrtx;++i)
        printf("i=%d part[i]=%d\n",i,part[i]);
    */
    if(opt.debug > 10)
        printf("per end\n");

}
