#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include "dgraph.h"
#include "bmmio.h"
#include "utils.h"
#include "debug.h"
#include "vcycle.h"
#include "partDGraphAlgos.h"
#include "matching.h"
#include "infoPart.h"

char *infilename;
int K=2;
float eps = 0.03;


/*********************************************************************************/
void printUsage(char *exeName)
{
    printf("Usage:  %s -m matrixMarketFile [-k numParts] [-e epsBalance]\n", exeName);
    
    printf("\tdefault for k=2, epsBalance=0.03\n");
}

int processArgs(int argc, char **argv)
{
    const char *delims = "h:m:k:e:p:";
    int opt;
    while ((opt = getopt(argc, argv, delims)) != -1) {
        switch (opt) {
            case 'm':
                infilename = optarg;
                break;
            case 'k':
                K = atoi(optarg);
                break;
            case 'e':
                eps = atof(optarg);
                break;
            default:
                printUsage(argv[0]);
                return 1;
        }
    }
    if (infilename == NULL || K<= 1 ) {
        printf("%s: There is a problem with the arguments.\n", argv[0]);
        return 1;
    }
    return 0;
}

void readPartFromFile(char* file_name, idxType* part, idxType nbnode)
{
    /*Assume that part is already allocated of size nbnode
      Fill part with part[i] partition set number of node i*/
    FILE* file;
    file = fopen(file_name, "r");
    idxType i;
    int tmp;
    for (i=0; i< nbnode; i++){
	fscanf(file, "%d", &tmp);
	part[i] = (idxType) tmp;
    }
}

void renameTMP(char* graph_name)
{
    dgraph G;
    char file_name[200];
    char tmp[200];    
    char full_name[200];
    strcpy(full_name, "/home.local/jherrman/Documents/parti-dir/mtx/");
    strcat(full_name, graph_name);
    strcat(full_name, ".dgraph");

    generateDGraphFromFile(&G, full_name);

    idxType* part;
    part = (idxType*) malloc(sizeof(idxType)*(G.nVrtx));
       
    int seed, i;
    strcpy(full_name, "/home.local/jherrman/Documents/parti-dir/res/");
    strcat(full_name, graph_name);

    for (i=1; i<=5; i++){
	for (seed=1; seed<=100; seed++){
	    printf("i=%d seed=%d nbnode=%d\n", i, seed, (int) G.nVrtx);
	    sprintf(tmp, ".kernighan.part.%d.seed.%d", (int) pow(2,i), seed);
	    strcpy(file_name, full_name);
	    strcat(file_name, tmp);
	       
	    readPartFromFile(file_name, part, G.nVrtx);

	    int bb = 1, j;
	    for (j=0; j<G.nVrtx; j++)
		if (part[j] == 0)
		    bb = 0;
	    if (bb==0)
		break;
	    
	    FILE* file;
	    file = fopen(file_name, "w");
	    int tmp;
	    for (j=0; j<G.nVrtx; j++){
		tmp = (int) part[j]-1;
		fprintf(file, "%d\n", tmp);
	    }
	    fclose(file);
	}
    }
    free(part);
    freeDGraphData(&G); 
}

void analyse(char* graph_name)
{
    printf("Analyse %s\n", graph_name);
    dgraph G;
    char full_name[200];
    char file_name[200];
    char tmp[200];
    strcpy(full_name, "/home.local/jherrman/Documents/parti-dir/mtx/");
    strcat(full_name, graph_name);
    strcat(full_name, ".dgraph");

    generateDGraphFromFile(&G, full_name);

    idxType* part;
    part = (idxType*) malloc(sizeof(idxType)*(G.nVrtx));
    int* partsize;
    partsize = (int*) malloc(sizeof(int)*(G.nVrtx));
       
    int seed, i;
    strcpy(full_name, "/home.local/jherrman/Documents/parti-dir/res/");
    strcat(full_name, graph_name);

    //Kernighan
    for (i=1; i<=5; i++){
	break;
	int edgecut = 0, comm = 0;
	for (seed=1; seed<=100; seed++){
	    sprintf(tmp, ".kernighan.part.%d.seed.%d", (int) pow(2,i), seed);
	    strcpy(file_name, full_name);
	    strcat(file_name, tmp);
	       
	    readPartFromFile(file_name, part, G.nVrtx);
	    
	    int nbpart = nbPart(&G, part, partsize);
	    if (nbpart > (int) pow(2,i))
		printf("Warning!!! Kernighan did %d partitions instead of %d in seed = %d\n", nbpart, (int) pow(2,i), seed);
	    //double imba = partImbalance(partsize, nbpart);
	    edgecut += edgeCut(&G, part);
	    comm += nbCommunications(&G, part);
	}
	double edgecutmoy = edgecut/100.;
	double commmoy = comm/100.;
	printf("Kernighan part %d\tedgecut = %.2f\tnbcomm = %.2f\n", (int) pow(2,i), edgecutmoy, commmoy);
    }

    //Metis
    for (i=1; i<=5; i++){
	break;
	int edgecut = 0, comm = 0;
	for (seed=1; seed<=100; seed++){
	    sprintf(tmp, ".graph.part.%d.seed.%d", (int) pow(2,i), seed);
	    strcpy(file_name, full_name);
	    strcat(file_name, tmp);
	       
	    readPartFromFile(file_name, part, G.nVrtx);
	    
	    int nbpart = nbPart(&G, part, partsize);
	    if (nbpart != (int) pow(2,i))
		printf("Warning!!! Metis did %d partitions instead of %d in seed = %d\n", nbpart, (int) pow(2,i), seed);
	    //double imba = partImbalance(partsize, nbpart);
	    edgecut += edgeCut(&G, part);
	    comm += nbCommunications(&G, part);
	}
	double edgecutmoy = edgecut/100.;
	double commmoy = comm/100.;
	printf("Metis part %d\tedgecut = %.2f\tnbcomm = %.2f\n", (int) pow(2,i), edgecutmoy, commmoy);
    }

    //Patoh
    for (i=1; i<=5; i++){
	ecType edgecut = 0, comm = 0;
	for (seed=1; seed<=100; seed++){
	    sprintf(tmp, ".patoh.part.%d.seed.%d", (int) pow(2,i), seed);
	    strcpy(file_name, full_name);
	    strcat(file_name, tmp);
	       
	    readPartFromFile(file_name, part, G.nVrtx);
	    
	    int nbpart = nbPart(&G, part, partsize);
	    if (nbpart != (int) pow(2,i))
		printf("Warning!!! Patoh did %d partitions instead of %d in seed = %d\n", nbpart, (int) pow(2,i), seed);
	    //double imba = partImbalance(partsize, nbpart);
	    edgecut += edgeCut(&G, part);
	    comm += nbCommunications(&G, part);
	}
	double edgecutmoy = edgecut/100.;
	double commmoy = comm/100.;
	printf("Patoh part %d\tedgecut = %.2f\tnbcomm = %.2f\n", (int) pow(2,i), edgecutmoy, commmoy);
    }

    free(part);
    free(partsize);
    freeDGraphData(&G); 
}
