#ifndef OPTION_H_
#define OPTION_H_

#include <stdio.h>
#include <memory.h>
#include <string.h>
#include <limits.h>

#include "dgraph.h"
#ifndef PATH_MAX
#define PATH_MAX 300 
#endif

#define CO_STOP_SIZE 0  /*the stoping criteria is when the size of the graph < co_stop_size*/
#define CO_STOP_STEP 1  /*the stoping criteria is the number of coarsening steps = co_stop_step*/

#define CO_MATCH_DIFF 0  /*the matching is the one described in Coarsening 2*/
#define CO_MATCH_AGGREGATIVETP 1  /*the matching is the aggregative with diff of TL 1*/
#define CO_MATCH_AGGREGATIVEMAX 2  /*the matching is the aggregative with max out TL bigger than max TL*/

#define CO_MATCH_NORD_RAND 0 /*Random traversal for the matching*/
#define CO_MATCH_NORD_DFSTOP 1 /*DFS top order traversal for the matching*/
#define CO_MATCH_NORD_RDFSTOP 2 /*Random DFS top order for the matching*/
#define CO_MATCH_NORD_BFSTOP 3 /*BFS traversal top order for the matching*/
#define CO_MATCH_NORD_RBFSTOP 4 /*Random BFS top order for the matching*/
#define CO_MATCH_NORD_DFS 5 /*DFS traversal for the matching*/
#define CO_MATCH_NORD_RDFS 6 /*Random DFS for the matching*/
#define CO_MATCH_NORD_BFS 7 /*BFS traversal for the matching*/
#define CO_MATCH_NORD_RBFS 8 /*Random BFS for the matching*/

#define CO_MATCH_EORD_RAND 0 /*Traverse the edges in the construction order*/
#define CO_MATCH_EORD_DFSTOP 1 /*Traverse the edges in the non-increasing order of their weight*/
#define CO_MATCH_EORD_RDFSTOP 2 /*Traverse the edges in the non-increasing order of the ratio weight / node weight*/

#define CO_INIPART_ALL 0 /*try all initial partitioning and pick the best one*/
#define CO_INIPART_KERDFS 1 /*the initial partitioning is done with Kernighan-Lin algo with DFS traversal*/
#define CO_INIPART_KERBFS 2 /*the initial partitioning is done with Kernighan-Lin algo with BFS traversal*/
#define CO_INIPART_KERDFSBFS 3 /*the initial partitioning is done with Kernighan-Lin algo with mixed BFS-DFS traversal*/
#define CO_INIPART_GREEDY 4 /*the initial partitioning is done with greedy graph growing approach*/
#define CO_INIPART_FORCED 5 /*the initial partitioning is done with the Forced Balance algo*/
#define CO_INIPART_METIS 6 /*the initial partitioning is done with Metis, then fixing acyclicity and forced balancing*/
#define CO_INIPART_BFS 7 /*the initial partitioning is done with BFS Growing*/
#define CO_INIPART_CS 8 /*the initial partitioning is done with CocktailShake*/
#define CO_INIPART_DIA_GREEDY 9 /*the initial partitioning is done with CO_INIPART_GREEDY but starting with a node on the diameter of the graph*/

#define CO_KERNIGHAN_DEFAULT 0 /*We use the regular Kernighan-Lin algo with increasing bound until we have the correct number of partition*/
#define CO_KERNIGHAN_EXACT 1 /*We use the variant of Kernighan-Lin algo where exact number of component is ensured*/

#define CO_REFINEMENT_NOTHING 0 /*No refinement*/
#define CO_REFINEMENT_PO 1 /*the refinement toward the greatest partition in post order*/
#define CO_REFINEMENT_TB 2 /*the refinement toward the best partition if it creats no cycle*/
#define CO_REFINEMENT_POTB 3 /*performes combinaison of refinement_po and then refinement_tb*/
#define CO_REFINEMENT_SWAP 4 /*the refinement toward the greatest partition in post order*/
#define CO_REFINEMENT_MAX 5 /*the refinement toward the greatest partition in post order*/
#define CO_REFINEMENT_COMBINED 6 /*the refinement combination of CO_REFINEMENT_MAX and CO_REFINEMENT_SWAP*/
#define CO_REFINEMENT_RANDOMIZED 7 /*the refinement where the node to be moved to another partition is selected randomly among boundary vertices.*/

#define CO_OBJ_EC 0 /*edge-cut is the objective to minimize*/
#define CO_OBJ_CV 1 /*communication volume is the objective to minimize*/

#define CO_OBJ_COMMVOL_EXP 0 /*communication volume is computed using expected value*/
#define CO_OBJ_COMMVOL_BEST 1 /*communication volume is computed using best case scenario*/
#define CO_OBJ_COMMVOL_WORST 2 /*communication volume is computed using worst case scenario*/
#define CO_OBJ_COMMVOL_AVE 3 /*communication volume is computed using average case scenario*/

#define LIVE_CUT_MAX 0 /*Graph is cut is recursively cut in half until the live set value is smaller than opt.maxliveset*/
#define LIVE_CUT_RECUT 1 /*We cut in half and recut if 2/3 1/3 if one part as a liveset smaller than opt.maxliveset / 2*/


typedef struct{
    char file_name[PATH_MAX];
    char file_name_dot[PATH_MAX];
    int nbPart;

    int co_stop; /*describe the stopping criteria for the coarsening*/
    int co_stop_size; /*size of graph for stoping criteria*/
    int co_stop_step; /*number of step for stoping criteria*/
    
    int co_match; /*describe the matching we use for the coarsening*/
    int co_match_norder; /*describe the node traversal order for the matching*/
    int co_match_eorder; /*describe the edge traversal order for the matching*/
    int co_match_onedegreefirst; /*do we traverse one degree nodes first*/
    int co_inipart; /*describe the initial partitioning*/
    int co_nbinipart; /*number of initial partitioning*/
    int co_refinement; /*describe the refinement*/
    int co_obj; /*describe the objective to minimize*/
    int co_obj_commvol; /*describe how to compute communication volume*/
    int kernighan; /*describe the variant of the Kernighan algorithm*/
    int live_cut; /*describe the cuting strategy for rMLGPlive*/
    int live_cut_merge; /*describe if we merge parts after recursive cut*/
    int maxliveset; 
    int use_binary_input; /* if not present, generate a binary file using graph data.
                        otherwise, use binary version of the input graph to read*/
    //double part_ub; /*upper bound for the partitions*/
    //double part_lb; /*lower bound for the partitions*/

    double* ub; /*upper bound for the partitions*/
    double* lb; /*lower bound for the partitions*/

    int ref_step; /*Number of reffinement steps*/ 

    int debug; /*Debug value*/ 
    int print; /*Print value*/ 
    int seed; /*Random seed*/
    double ratio; /*Imbalance ratio*/
    int runs; /*Number of runs*/
    char out_file[PATH_MAX];
    int step; /*Temp current step*/
} MLGP_option;

#include "vcycle.h"

int initializeStoppingCriterion(const MLGP_option opt, const coarsen* coars);
int stoppingCriterion(const MLGP_option opt, int* crit, coarsen* coars);
MLGP_option* copyOpt(const MLGP_option opt,int new_nbPart);
void reallocUBLB(MLGP_option* opt);
void print_opt(MLGP_option* opt);
void free_opt(MLGP_option* opt);

#endif
