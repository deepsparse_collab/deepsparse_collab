#ifndef _FAUZIA_H_
#define _FAUZIA_H_

#include <list>
#include "dgraph.h"
#include <vector>

struct cc{
  double weight = 0.0, takenNeighbors = 0.0, takenSuccessors = 0.0;
  std::list<idxType> readyNeighbors, readySuccessors;
};

std::vector<idxType> fauziaNeighbors(const dgraph& g, idxType n, std::vector<int>& neighborsMask);
idxType selectBestNode(const dgraph& g, idxType n, cc& partition, const std::list<idxType> readyVec, std::vector<int>& readyMask, std::vector<int>& neighborsMask, std::vector<int>& readyNeighborsMask, std::vector<int>& readySuccessorsMask, int partCount, double priority);
int fauziaPartition(const dgraph& g, double uB, double priority, idxType* partVec);

#endif
