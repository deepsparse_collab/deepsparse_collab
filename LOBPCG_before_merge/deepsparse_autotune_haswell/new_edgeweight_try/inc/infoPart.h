#ifndef INFOPART_H_
#define INFOPART_H_

#include <stdio.h>
#include <memory.h>
#include <string.h>

#include "dgraph.h"

#define IMBAL_IS_MAXIMBAL
#define INFO_LENGTH 3000


typedef struct{
    double timing_global;
    double timing_coars;
    double timing_inipart;
    double timing_uncoars;

    double* timing_coars_tab;
    double* timing_matching_tab;
    double* timing_buildgraph_tab;

    double* timing_inipart_tab;
    ecType* ec_inipart_tab;

    double* timing_uncoars_tab;
    double* timing_refinement_tab;
    double* timing_project_tab;
    double* timing_forced_tab;

    ecType* ec_afterforced_tab;
    int* nbref_tab;
    ecType** ec_afterref_tab;
    double** timing_refinement_tab_tab;

    idxType* nbnodes_coars_tab;
    idxType* nbedges_coars_tab;

    int coars_depth;
    int best_inipart;
    ecType iniec;
    ecType finalec;

    //some info used in the recursive bissection for debugging
    int depth;
    ecType current_edgecut;
} MLGP_info;

typedef struct rMLGP_info{
    double timing_global;
    double timing_coars;
    double timing_inipart;
    double timing_uncoars;

    MLGP_info info;
    struct rMLGP_info* rec1;
    struct rMLGP_info* rec2;

    int depth;
} rMLGP_info;

#include "option.h"
#include "rvcycle.h"


void initInfoPart(MLGP_info* info);
void freeInfoPart(MLGP_info* info);
void printInfoPart(MLGP_info* info, coarsen* C, MLGP_option opt);

void initRInfoPart(rMLGP_info* info);
void freeRInfoPart(rMLGP_info* info);



int nbPart(dgraph* G, idxType* part, vwType* partsize);
double partImbalance(vwType* partsize, int nbpart);
ecType edgeCut(dgraph* G, idxType* part);
ecType nbCommunications(dgraph* G, idxType* part);
int print_info_part(dgraph* G, idxType* part, MLGP_option opt);
int print_info_part_file(dgraph* G, idxType* part, MLGP_option opt);


#endif
