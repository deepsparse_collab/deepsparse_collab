#ifndef RMLGP_H_
#define RMLGP_H_


#include "option.h"
#include "dgraph.h"
#include "bmmio.h"
#include "utils.h"
#include "debug.h"
#include "vcycle.h"
#include "rvcycle.h"
#include "partDGraphAlgos.h"
#include "matching.h"
#include "infoPart.h"

//#include "../../lobpcg_gen_graph_v28.h"

//#include "../../lobpcg_gen_graph_v29.h"
//#include "../../lobpcg_gen_graph_v29_global.h"

#include "../../lobpcg_gen_graph_v30.h"
//#include "../../lobpcg_gen_graph_v30_global.h"

//#include "../../lib_dag.h"


extern dgraph main_graph;
void printUsage_rMLGP(char *exeName);
int processArgs_rMLGP(int argc, char **argv, MLGP_option* opt);
int my_processArgs_rMLGP(int partCount, MLGP_option* opt);
//void run_rMLGP(char* file_name, MLGP_option opt, int *edge_u, int *edge_v, double *edge_weight, int edgeCount, int vertexCount);
void run_rMLGP(char* file_name, MLGP_option opt, int *edge_u, int *edge_v, double *edge_weight, int edgeCount, int vertexCount,const char** vertexName,double* vertexWeight, int loopType, char* matrix_name);
//void run_rMLGP(char* file_name, MLGP_option opt);
void get_task_name(const char* node_name, char* task);
void make_new_table_file(rcoarsen* rcoarse, int *my_partition_topsort, char* loopType, char* matrix_name, int large_blk, int small_blk, int partCount);
//void make_new_table_file_global(const char** vertexName, char* loopname, char* matrix_name, int large_blk, int small_blk, int nvrtx, int partCount);


#endif
