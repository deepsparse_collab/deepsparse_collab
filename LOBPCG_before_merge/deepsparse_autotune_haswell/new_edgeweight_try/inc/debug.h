#ifndef DEBUG_H_
#define DEBUG_H_

#include <stdio.h>
#include <memory.h>
#include <string.h>

#include "dgraph.h"

void createDGraph_example(dgraph* G);
void print_dgraph(dgraph G);
void dgraph_to_dot(dgraph* G, idxType* part, char* file_name);
void print_graph(dgraph* G, idxType* part, char* path, int arg1, int arg2);

#endif
