#ifndef VCYCLE_H_
#define VCYCLE_H_

#include <stdio.h>
#include <memory.h>
#include <string.h>

#include "dgraph.h"

typedef struct coarsen {
    /*Coarsening process*/
    dgraph *graph; /*Current graph*/
    struct coarsen *previous_coarsen; /*Previous graph in the coarsening*/
    struct coarsen *next_coarsen; /*Graph obtained by applying the matching below*/

    /*Matching description*/
    idxType* leader; /*If leader[i] == leader[j], nodes i and j are in the same node in next_graph (next coarsen)*/
    idxType* new_index; /*new_index[i] is the new index of node i in next_coarsen*/

    /*Partition description*/
    idxType* part; /*part[i] is the index of the partition set node i belongs to (starting from 0)*/
    dgraph *quotient; /*quotient graph for the given partition*/
    int      nbpart;
} coarsen;

#include "infoPart.h"


coarsen* initializeCoarsen(dgraph *graph);
void freeCoarsen(coarsen *coars);
void freeCoarsenHeader(coarsen *coars);

coarsen *VCycle(dgraph* graph, MLGP_option opt, MLGP_info* info);

/*

dgraph* initializeNextGraph(coarsen *coars);


coarsen *coarsenStep(coarsen* coars);
void initialPartitioning(MLGP_option opt, coarsen* coars);
void refinementStep(MLGP_option opt, coarsen* coars);
coarsen *projectBack(coarsen* coars);

*/

#endif
