#ifndef DGRAPH_H_
#define DGRAPH_H_

/*vertex ids start from 1*/

#include <stdio.h>
#include <memory.h>
#include <string.h>
#include <limits.h>

typedef int idxType; /*indices*/
typedef double ecType; /*edge costs*/
typedef int vwType; /*vertex weights*/

#define ecType_MAX ULLONG_MAX

#define VWORKMULT 2 /*vertex work array's size*/


#define DG_FRMT_UN 0  /*no weight ; no cost*/
#define DG_FRMT_VW 1  /*vertex weight*/
#define DG_FRMT_EC 2  /*edge cost*/
#define DG_FRMT_VWEC 3 /*vertex weight edge cost*/





typedef  struct{
    idxType nVrtx; /*num vertices*/
    idxType nEdge; /*num edges*/
    
    idxType *inStart;
    idxType *inEnd;
    idxType *in; /*for each vertex v, we will sore the edges
                  u->v in in[inStart[v]...inEnd[v]]*/
    
    idxType *outStart;
    idxType *outEnd;
    idxType *out;/*for each vertex v, we will sore the edges
                  v-> in out[outStart[v]...outEnd[v]]*/
    
    idxType *vw;
    ecType *ecIn; /*we store edge weights twice; this is at j for the edge (i->j) */
    ecType *ecOut;/*we store edge weights twice; this is at i for the edge (i->j) */
    int *hollow;
    /*information fields*/
    int frmt;
    double totvw; ///anik assuming totvw = total vertex weight
    double totec;
    idxType maxindegree;
    idxType maxoutdegree;
    vwType maxVW;
    ecType maxEC;

    double* incoming_edge_weight; 
    
    /*extra storage*/
    
    /* These were not maintained/espected. I am removong them (21 Sep 2017) --.BU.
     
     idxType *vwork;
     idxType *ework;
     
     */
    char** vertices;
    ecType* vWeight;
    



} dgraph;

//typedef enum { false, true } bool; // or #define bool int

int calcLiveSizeMix(dgraph *G);
int calcLiveSizeMixMulti(dgraph *G, int rep);
int calcLiveSize(dgraph *g,idxType* toporder);
int calcLiveSizeMix_with_part(dgraph *G, idxType* part, int nbpart, idxType* toporder);
int calcLiveSizeMix_with_part_Multi(dgraph *G, idxType* part, int nbpart, idxType* toporder, int rep);
int calcLiveSizePart(dgraph *g, idxType* toporder, idxType* part, idxType part_idx);
int calcLiveSizePartMix(dgraph *G, idxType* part, idxType part_idx);
int calcLiveSizePartMixMulti(dgraph *G, idxType* part, idxType part_idx, int rep);
void loadFromCSC(dgraph *G, idxType nVrtx, idxType nEdge, int frmt, idxType *col_ptrs, idxType *row_inds, ecType *vals);

void allocateDGraphData(dgraph *G, idxType nVrtx, idxType nEdge, int frmt);
void fillOutFromIn(dgraph *G);
void sortNeighborLists(dgraph *G);
void setVertexWeights(dgraph *G, vwType *vw);
void transitivelyReduce(dgraph *G);
void freeDGraphData(dgraph *G);
void dgraph_info(dgraph* G, int* maxindegree, int* minindegree, double* aveindegree, int* maxoutdegree, int* minoutdegree, double* aveoutdegree);
void set_dgraph_info(dgraph* G);


void checkTopsort(dgraph *G, idxType *toporder);
void topSortOnParts(dgraph *G, idxType *part, idxType *toporder, idxType nbpart);
void randTopSortOnParts(dgraph *G, idxType *part, idxType *toporder, idxType nbpart);
void topsortwithTies(dgraph *G, idxType *toporder);
void bfsTopSortOnParts(dgraph *G, idxType *part, idxType *toporder, idxType nbpart);

idxType sourcesList(dgraph *G, idxType* sources);
idxType sourcesListPart(dgraph* G, idxType* sources, idxType *part, int part_idx);
idxType outputList(dgraph *G, idxType* outputs);
void DFStopsort_with_part(dgraph* G, idxType *part, int nbpart, idxType *toporder);
void randDFStopsort_with_part(dgraph* G, idxType *part, int nbpart, idxType *toporder);
void BFStopsort_with_part(dgraph* G, idxType *part, int nbpart, idxType *toporder);
void randBFStopsort_with_part(dgraph* G, idxType *part, int nbpart, idxType *toporder);
int checkAcyclicity(dgraph *G, idxType *part, idxType nbpart);
void shuffleTab(idxType deb, idxType end, idxType* shuffle);
void DFStopsort(dgraph *G, idxType *toporder);
void randDFStopsort(dgraph *G, idxType *toporder);
void BFStopsort(dgraph *G, idxType *toporder);
void mixtopsort(dgraph *G, idxType *toporder,int priority,int first);
void randBFStopsort(dgraph *G, idxType *toporder);
void DFSsort(dgraph *G, idxType *toporder);
void randDFSsort(dgraph *G, idxType *toporder);
void BFSsort(dgraph *G, idxType *toporder);
void randBFSsort(dgraph *G, idxType *toporder);
void randTopsort(dgraph *G, idxType *toporder);
void oneDegreeFirst(dgraph* G, idxType* order);
void computeToplevels(dgraph *G, int* toplevels);
void computeToplevelsWithTopOrder(dgraph* G, int* toplevels, idxType* toporder);

void generateDGraphFromFile(dgraph *G, char* file_name);
void generateDGraphFromMtx(dgraph *G,char* file_name,int use_binary_input);
void generateDGraphFromBinaryDot(dgraph *G, char* file_name);
void generateDGraphFromDot(dgraph *G, char* file_name, int use_binary_input);
void my_generate_graph(dgraph *G, char* file_name,int use_binary_input);
//fazlay change
//void my_generate_graph_fazlay(dgraph *G, char* file_name, int use_binary_input, int *edge_u, int *edge_v, double *edge_weight, int edgeCount, int vertexCount);
void my_generate_graph_fazlay(dgraph *G, char* file_name, int use_binary_input, int *edge_u, int *edge_v, double *edge_weight, int edgeCount, int vertexCount, const char** vertexName,double* vertexWeight);
double computeLatency(dgraph* G, idxType* part, double l1, double l2);
void sortOutNeighboursWeight(dgraph* G, idxType node, idxType* outnodes);
void sortOutNeighboursRatio(dgraph* G, idxType node, idxType* outnodes);
void my_DFS(dgraph* G, idxType* visited);
void my_BFS(dgraph* G, idxType* visited);
void my_sortNeighbourWithWeight(dgraph* G, idxType node, idxType* sorted_outnodes);
idxType my_LCA(dgraph* G, idxType node1, idxType node2);
void my_generate_smallgraph(dgraph *smallG, char* file_name, int use_binary_input, int *edge_u, int *edge_v, double *edge_weight, int edgeCount, int vertexCount, const char** vertexName,double* vertexWeight, int block_divisor);
void create_smallgraph_datastructure_sparse(int *edge_u, int *edge_v, double *edge_weight, int edgeCount, int vertexCount, const char** vertexName,double* vertexWeight, int block_divisor, char*** newVertexName, int new_vertexcount,
                                        int new_EdgeCount,int **newEdge_u, int **newEdge_v, double** newEdge_weight, int** prev_vertex, double** newVertex_weight, int** nnzblock_matrix, int newRblock, int newCblock, 
                                            int* updatedVertexCount,int *updatedEdgeCount, int*** vmap);

//void create_smallgraph_datastructure(int *edge_u, int *edge_v, double *edge_weight, int edgeCount, int vertexCount, const char** vertexName,double* vertexWeight, int block_divisor, char*** newVertexName, int new_vertexcount,
//                                            int new_edgeCount, int **newEdge_u, int **newEdge_v, double** newEdge_weight, int** prev_vertex , double** newVertex_weight,int** nnzblock_matrix, int newRblock, int newCblock){

int node_type(const char* node);
void task_name(const char *node_name, char** task);


#endif

