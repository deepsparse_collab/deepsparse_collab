#ifndef UTILS_H_
#define UTILS_H_

#include <stdio.h>
#include <stdlib.h>

#include <ctype.h>
#include <stdarg.h>
#include <fcntl.h>

#include "dgraph.h"

#define mymax(a, b)	(((a) >= (b)) ? (a) : (b))

void heapRemove(dgraph *G, idxType *heapIds, ecType* key, idxType *sz, idxType i);
void randHeapRemove(dgraph *G, idxType *heapIds, ecType* key, idxType *sz, idxType i);
void heapInsert(dgraph *G, idxType *heapIds, ecType* key, idxType *sz, idxType i);
void randHeapInsert(dgraph *G, idxType *heapIds, ecType* key, idxType *sz, idxType i);
void heapSort(dgraph *G, idxType *heapIds, ecType* key, idxType sz, idxType i);
void randHeapSort(dgraph *G, idxType *heapIds, ecType* key, idxType sz, idxType i);
idxType heapExtractMax(dgraph *G, idxType *heapIds, ecType* key, idxType *sz);
idxType randHeapExtractMax(dgraph *G, idxType *heapIds, ecType* key, idxType *sz);
void heapBuild(dgraph *G, idxType *heapIds, ecType* key, idxType sz);
void randHeapBuild(dgraph *G, idxType *heapIds, ecType* key, idxType sz);
void* umalloc(long size, char* msg);
void printNodeInfo(dgraph *G,idxType* part,idxType node);
void u_errexit(char * f_str,...);
double u_wseconds(void);
void shuffleArray(idxType size, idxType* toBeShuffled, idxType cnt);

#endif
