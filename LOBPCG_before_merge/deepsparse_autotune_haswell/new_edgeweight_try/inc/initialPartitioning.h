#ifndef INITIALBISSECTION_H_
#define INITIALBISSECTION_H_

#include "option.h"

/* returns #parts found */
void initialPartitioning(MLGP_option opt, coarsen*  coars, MLGP_info* info);

#endif
