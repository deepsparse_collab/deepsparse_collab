#ifndef PARTDGRAPHALGOS_H_
#define PARTDGRAPHALGOS_H_

#include "option.h"

/* returns #parts found */
int kernighanSequentialPart(dgraph *G, idxType *toporder,
                            double lb_pw, double ub_pw,
                            idxType *partvec, ecType* obj_value, MLGP_option opt);
int kernighanSequentialExactPart(dgraph *G, idxType *toporder,
                            int k, double* ub_pw,
                            idxType *partvec, ecType* obj_value, MLGP_option opt);
void initialPartitioningForcedBalance(dgraph *G, double* ub_pw, idxType *part, idxType nbpart, MLGP_option opt);
void greedyGraphGrowing(dgraph *G, double* ub_pw, idxType *part, idxType nbpart, MLGP_option opt);
void reverseGreedyGraphGrowing(dgraph *G, double* ub_pw, idxType *part, idxType nbpart, MLGP_option opt);

void BFSGrowing(dgraph *G, double* ub_pw, idxType *part, idxType nbpart, MLGP_option opt, MLGP_info* info);
void peripheralRootGreedyGraphGrowing(dgraph *G, double* ub_pw, idxType *part, idxType nbpart, MLGP_option opt);
void CocktailShake(dgraph *G, double* ub_pw, idxType *part, idxType nbpart, MLGP_option opt);

#endif
