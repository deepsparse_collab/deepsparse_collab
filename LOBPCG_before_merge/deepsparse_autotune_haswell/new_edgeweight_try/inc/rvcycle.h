#ifndef RVCYCLE_H_
#define RVCYCLE_H_

#include <stdio.h>
#include <memory.h>
#include <string.h>

#include "vcycle.h"


typedef struct rcoarsen {
    /*Coarsening process*/
    coarsen  *coars; /*Current coarsen*/
    struct rcoarsen *previous_rcoarsen; /*Previous rcoarsen in the recursion*/
    struct rcoarsen *next_rcoarsen1; /*One of the next rcoarsen in the recursion*/
    struct rcoarsen *next_rcoarsen2; /*One of the next rcoarsen in the recursion*/

    /*Link between the recursion*/
    idxType* previous_index; /*previous_index[i] is the index of i in previous_rcoarsen*/
    idxType* next_index; /*next_index[i] is the new index of node i in next_rcoarsen1 or next_rcoarsen2*/

} rcoarsen;

#include "infoPart.h"

rcoarsen* initializeRCoarsen(coarsen* icoars);
void freeRCoarsen(rcoarsen *rcoars);
void freeRCoarsenHeader(rcoarsen *rcoars);
void initializeNextRCoarsen(rcoarsen* rcoars, rcoarsen* rcoars1, rcoarsen* rcoars2);
void extractPartition(rcoarsen *rcoars, idxType* part);
void splitCoarsen(rcoarsen* coars, dgraph* G1, dgraph* G2, int* part1, int* part2);
rcoarsen* rVCycle(dgraph *igraph, MLGP_option opt, rMLGP_info* info);
rcoarsen* rVCycleLive(dgraph *igraph, MLGP_option opt, MLGP_info* info,int *resultingNbPart);
rcoarsen* my_rVCycle(dgraph *igraph, MLGP_option opt, rMLGP_info* info,int *resultingNbPart);
void printRInfoPart(rMLGP_info* info, rcoarsen* C, MLGP_option opt);


#endif
