#ifndef MATCHING_H_
#define MATCHING_H_

#include <stdio.h>
#include <memory.h>
#include <string.h>

#include "dgraph.h"
#include "option.h"


#define CO_MATCH_DIFF 0  /*the matching is the one described in Coarsening 2*/


void matchingToplevels(dgraph* G, idxType* matching, const MLGP_option opt);
void computeMatching(const MLGP_option opt, dgraph *G, idxType* part);
void my_matchingToplevels(dgraph* G,idxType* matching, const MLGP_option opt);

#endif
