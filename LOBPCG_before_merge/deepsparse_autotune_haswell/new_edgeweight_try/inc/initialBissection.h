#ifndef INITIALBISSECTION_H_
#define INITIALBISSECTION_H_

#include "option.h"

/* returns #parts found */
void metisAcyclicBissection(ecType* edgecutmin, MLGP_info* info, coarsen* coars, idxType* part, MLGP_option opt);
void dgraph_to_metis(dgraph* G, idx_t* in, idx_t* adj, idx_t* vw, idx_t* adjw);


#endif
