#ifndef REFINEMENT_H_
#define REFINEMENT_H_

#include <stdio.h>
#include <memory.h>
#include <string.h>

#include "option.h"
#include "vcycle.h"

void refinementStep(MLGP_option opt, coarsen* coars, MLGP_info* info);
ecType computeGainNode(dgraph *G, idxType *part, idxType movableto, idxType node, MLGP_option opt);
idxType* forcedBalance(dgraph *G, idxType *toporderpart,
		   			double* ub_pw,
		      		idxType *part, idxType nbpart, MLGP_option opt, MLGP_info* info);
#endif
