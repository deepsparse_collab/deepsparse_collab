CC=icpc

CFLAGS= -std=c++11 -Wall -Wextra -Wconversion -O3

LIBS=-ldgraph -lm
LIBSA=-lmetis $(LIBS)
AR=ar rv
RANLIB=ranlib


METISLIBDIR=/usr/local/Cellar/metis/5.1.0/lib
#/Users/ubora/science/metis-5.1.0/metis-5.1.0/build/Darwin-x86_64/libmetis

METISHDIR=/usr/local/Cellar/metis/5.1.0/include
#/Users/ubora/science/metis-5.1.0/metis-5.1.0/include
