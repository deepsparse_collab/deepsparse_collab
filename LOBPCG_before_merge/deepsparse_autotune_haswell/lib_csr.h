#ifndef LIB_CSR_H
#define LIB_CSR_H
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <iomanip>
#include <cmath>
#include <algorithm>
#include <sstream>
#include <cstring>
#include <fstream>
using namespace std;

#include <mkl.h>
#include <omp.h>

extern long position;
extern int *colptrs, *irem;
extern int  *ia , *ja;

extern int numcols, numrows, nnonzero, nthrds;
extern int nrows, ncols, nnz;
extern int wblk, nrowblks, ncolblks;

//CSB structure
template<typename T>
struct block
{
    int nnz;
    int roffset, coffset;
    unsigned short int *rloc, *cloc;
    T *val;
};

void _XTY(double *X, double *Y, double *result ,int M, int N, int P, int blocksize);
void transpose(double *src, double *dst, const int N, const int M);
void inverse(double *arr, int m, int n);
void print_mat(double *arr, const int row, const int col);
void make_identity_mat(double *arr, const int row, const int col);
void diag(double *src, double *dst, const int size);
void mat_sub(double *src1, double *src2, double *dst, const int row, const int col);
void mat_addition(double *src1, double *src2, double *dst, const int row, const int col);
void mat_mult(double *src1, double *src2, double *dst, const int row, const int col);
void sum_sqrt(double *src, double *dst, const int row, const int col);
void update_activeMask(int *activeMask, double *residualNorms, double residualTolerance, int blocksize);
void getActiveBlockVector(double *activeBlockVectorR, int *activeMask, double *blockVectorR, int M, int blocksize, int currentBlockSize);
void updateBlockVector(double *activeBlockVectorR, int *activeMask, double *blockVectorR, int M, int blocksize, int currentBlockSize);
void mat_copy(double *src,  int row, int col, double *dst, int start_row, int start_col, int ld_dst);
void print_eigenvalues( MKL_INT n, double* wr, double* wi );

template<typename T>
void read_custom(char* filename, T *&xrem);

// template<typename T>
// void read_custom_csr(char* filename, T *&xrem);

template<typename T>
void csc2blkcoord(block<T> *&matrixBlock, T *xrem);

// template<typename T>
// void spmm_blkcoord(int R, int C, int M, int nthrds, T *X,  T *Y, block<T> *H);

// template<typename T>
// void read_ascii(char* filename, T *&xrem);

void custom_dlacpy(double *src, double *dst, int m, int n);

template<typename T>
bool checkEquals( T* a, T* b, size_t outterSize, size_t innerSize);
#endif