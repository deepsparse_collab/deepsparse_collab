#ifndef LIB_HEADER_H
#define LIB_HEADER_H

#include <map>
#include <vector>
#include <iterator>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std;

#include "omp.h"

/* CSB block structure */
template<typename T>
struct block
{
  int nnz;
  int roffset, coffset;
  unsigned short int *rloc, *cloc;
  T *val;
};

struct TaskInfo
{
  int opCode;
  int numParamsCount;
  int *numParamsList;
  int strParamsCount;
  char **strParamsList;
  int taskID;
  int partitionNo;
  int priority;
};

extern struct TaskInfo *taskInfo_nonLoop, *taskInfo_firstLoop, *taskInfo_secondLoop;
/* declaration of global variables */

extern int nrows, ncols, nnz;
extern int wblk, nrowblks, ncolblks;
extern int numcols, numrows, nthrds;
extern int nnonzero;
extern long position;
extern int *colptrs, *irem;
extern double **taskTiming;
extern int numOperation;

/* definition of templated functions goes here */

template<typename T>
extern void read_custom(char* filename, T *&xrem);

template<typename T>
extern void csc2blkcoord(block<T> *&matrixBlock, T *xrem);

template<typename T>
extern void spmm_blkcoord(int R, int C, int M, int nthrds, T *X,  T *Y, block<T> *H);

template<typename T>
extern void spmm_blkcoord_finegrained_exe_fixed_buf(int R, int C, int M, int nbuf, T *X,  T *Y, block<T> *H, int row_id, int col_id, int buf_id, int block_width);

template<typename T>
extern void spmm_blkcoord_finegrained_SPMMRED_fixed_buf(int R, int M, int nbuf, T *Y, T *spmmBUF, int block_id, int block_width);

template<typename T>
extern void spmm_blkcoord_finegrained_exe_fixed_buf_unrolled32(int R, int C, int M, int nbuf, T *X,  T *Y, block<T> *H, int row_id, int col_id, int buf_id, int block_width);

#endif