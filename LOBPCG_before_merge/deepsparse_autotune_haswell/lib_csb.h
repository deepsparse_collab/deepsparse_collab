#ifndef LIB_CSB_H
#define LIB_CSB_H

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <iomanip>
#include <cmath>
#include <algorithm>
#include <sstream>
#include <cstring>
#include <fstream>
using namespace std;

#include <mkl.h>
#include <omp.h>

/****************************************************
*   lib_csb: based on lib_v2 (tested on Nm7-original.cus, working)
*   trying to convert to csr and trying to use dcsrmm
*****************************************************/

extern long position;
extern int *colptrs, *irem;
extern int  *ia , *ja;

extern int numcols, numrows, nnonzero, nthrds;
extern int nrows, ncols, nnz;
extern int wblk, nrowblks, ncolblks;

//CSB structure
template<typename T>
struct block
{
    int nnz;
    int roffset, coffset;
    unsigned short int *rloc, *cloc;
    T *val;
};

extern void _XTY(double *X, double *Y, double *result ,int M, int N, int P, int blocksize);
extern void transpose(double *src, double *dst, const int N, const int M);
extern void inverse(double *arr, int m, int n);
extern void print_mat(double *arr, const int row, const int col);
extern void make_identity_mat(double *arr, const int row, const int col);
extern void diag(double *src, double *dst, const int size);
extern void mat_sub(double *src1, double *src2, double *dst, const int row, const int col);
extern void mat_addition(double *src1, double *src2, double *dst, const int row, const int col);
extern void mat_mult(double *src1, double *src2, double *dst, const int row, const int col);
extern void sum_sqrt(double *src, double *dst, const int row, const int col);
extern void update_activeMask(int *activeMask, double *residualNorms, double residualTolerance, int blocksize);
extern void getActiveBlockVector(double *activeBlockVectorR, int *activeMask, double *blockVectorR, int M, int blocksize, int currentBlockSize);
extern void updateBlockVector(double *activeBlockVectorR, int *activeMask, double *blockVectorR, int M, int blocksize, int currentBlockSize);
extern void mat_copy(double *src,  int row, int col, double *dst, int start_row, int start_col, int ld_dst);
extern void print_eigenvalues( MKL_INT n, double* wr, double* wi );
extern void custom_dlacpy(double *src, double *dst, int m, int n);
extern bool checkEquals( double* a, double* b, size_t outterSize, size_t innerSize);

template<typename T>
extern void read_custom(char* filename, T *&xrem);

template<typename T>
extern void csc2blkcoord(block<T> *&matrixBlock, T *xrem);

template<typename T>
extern void spmm_blkcoord_loop(int R, int C, int blocksize, int nthrds, double *X,  double *Y, block<T> *H);

// template<typename T>
// extern void spmm_blkcoord(int R, int C, int M, int nthrds, T *X,  T *Y, block<T> *H);

// template<typename T>
// extern void read_ascii(char* filename, T *&xrem);


#endif