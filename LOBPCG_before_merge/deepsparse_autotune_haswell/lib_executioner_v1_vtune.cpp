#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
using namespace std;

#include <omp.h>
#include <mkl.h>
#include "lib_variable_v1.h"
#include "lib_executioner_v1.h"


void _XTY_v1_exe(double *X, double *Y, double *buf ,int M, int N, int P, int block_width, int block_id, int buf_id)
{
    //code: 10
    /*
    _XTY_v1: adding partial sums block by block, not row by row
    Input: X[M*N], Y[M*P]
    Output: result[N*P]
    nthrds : global variable, total # of threads
    buf : how to free/deallocate corresponding memory location
    block_width: each chunk
    */

    int i, j, k, l, blksz, tid, nthreads, length;
    double sum, tstart, tend;
    
    k = block_id * block_width; //start row # of block
    blksz = block_width;
    if(k + blksz > M)
        blksz = M - k;
    length = blksz * N ;
    
    i = buf_id;
    
    #pragma omp task private(tid, tstart, tend)\
    firstprivate(k, M, N, P, blksz, length, X, Y, nthrds, buf, block_id, block_width, buf_id, taskTiming)\
    depend(in: X[k * N : blksz * N], M, N, P)\
    depend(in: Y[k * P : blksz * P])\
    depend(out: buf[buf_id * N * P : N * P])
    {   
        //printf("block_id: %d XTY in: %p[%d : %d]     %p[%d : %d]\n", block_id, X, k * N , blksz * N, Y, k * N , blksz * N);
        tid = omp_get_thread_num();
        //tstart = omp_get_wtime();

        cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, N, P, blksz, 1.0, X+(k*N), N, Y+(k*P), P, 1.0, buf+(tid * N * P), P);
        
        //tend = omp_get_wtime();
        //taskTiming[10][tid] += (tend - tstart);
        
    } //task end
}

void _XTY_v1_RED(double *buf, double *result, int N, int P, int block_width)
{
    //code: 11
    /*
    _XTY_v1_RED: adding partial sums block by block, not row by row
    Input: buf: nthds * [N * P]
    Output: result[N * P]
    nthrds : global variable, total # of threads
    buf : how to free/deallocate corresponding memory location
    */
    
    int i, j, k, l, blksz, tid, nthreads, length;
    double sum, tstart, tend;
    
    int nbuf = nthrds;

    for(i = 0 ; i < N ; i = i + block_width)
    {
        //reduce

        blksz = block_width;
        if(i + blksz > N)
            blksz = N - i;

        #pragma omp task private(sum, k, l, tid, tstart, tend)\
        firstprivate(i, nthrds, blksz, buf, N, P, result, block_width, taskTiming)\
        depend(in: N, P) depend(out: result[i * P : blksz * P])\
        depend(in: buf[0*N*P:N*P], buf[1*N*P:N*P], buf[2*N*P:N*P], buf[3*N*P:nthrds*N*P], buf[4*N*P:N*P], buf[5*N*P:N*P], buf[6*N*P:N*P],\
        buf[7*N*P:N*P], buf[8*N*P:N*P], buf[9*N*P:N*P], buf[10*N*P:nthrds*N*P], buf[11*N*P:N*P], buf[12*N*P:N*P], buf[13*N*P:N*P],\
        buf[14*N*P:N*P], buf[15*N*P:N*P])
        {
            //tid = omp_get_thread_num();
            //tstart = omp_get_wtime();
            
            for( l = i ; l < (i + blksz) ; l++) //for each row in the block
            {
                for(k  = 0 ; k < P ; k++) //each col
                {
                    sum = 0.0;
                    for(j = 0 ; j < nthrds ; j++) //for each thread access corresponding N*N matrix
                    {
                        sum += buf[j * N * P + l * P + k];
                    }
                    result[l * P + k] = sum;
                }
                
            } //end inner for 1

            //tend = omp_get_wtime();
            //taskTiming[11][tid] += (tend - tstart);
        }// end of task 
    }//end outer for
}

void _XY_exe(double *X, double *Y, double *result ,int M, int N, int P, int block_width, int block_id)
{
    //code: 1
    /*
    Input: X[M * N], Y[N * P] (RHS matrix)
    Output: result[M * P] (result = X * Y)
    nthrds : global variable, total # of threads
    */

    int i, j, k, blksz, tid;
    double tstart;
    
    k = block_id * block_width; //starting row # of the current block

    blksz = block_width;
        
    if(k + blksz > M)
        blksz = M - k;

    #pragma omp task private(tid, tstart) firstprivate(k, blksz, X, Y, result, M, N, P, taskTiming)\
    depend(in: X[k * N : blksz * N], Y[0 : N * P], M, N, P)\
    depend(out: result[k * P : blksz * P]) //should be P here, check other version if those have N here. 
    {
        //tid = omp_get_thread_num();
        //tstart = omp_get_wtime();
        
        cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, blksz, P, N, 1.0, X + (k * N), N, Y, P, 0.0, result + (k * P), P);
        
        //taskTiming[1][tid] += (omp_get_wtime() - tstart);
    }//end task
}

void mat_addition_task_exe(double *src1, double *src2, double *dst, const int row,
                       const int col, int block_width, int block_id)
{
    //code: 21
    int i, j, k, tid;
    int blksz;
    double tstart, tend;
    
    i = block_id * block_width; //starting point of the block
    blksz = block_width;
    if(i + block_width > row)
        blksz = row - i;
    #pragma omp task private(j, k, tid, tstart, tend)\
    firstprivate(i, blksz, src1, src2, row, col, dst, block_width, taskTiming)\
    depend(in: src1[i * col : blksz * col])\
    depend(in: src2[i * col : blksz * col])\
    depend(out: dst[i * col : blksz * col])
    {
        //tid = omp_get_thread_num();
        //tstart = omp_get_wtime();
        
        for(k = i ; k < i + blksz ; k++)
        {
            for(j = 0 ; j < col ; j++)
            {
                dst[k * col + j] = src1[k * col + j] + src2[k * col + j];
            }
        }

        //tend = omp_get_wtime();
        //taskTiming[21][tid] += (tend - tstart);
    } //task end
}

void mat_sub_task_exe(double *src1, double *src2, double *dst, const int row,
                       const int col, int block_width, int block_id)
{
    //code: 2
    int i, j, k, tid;
    int blksz;
    double tstart;
    
    i = block_id * block_width; //starting point of the block
    blksz = block_width;
    if(i + block_width > row)
        blksz = row - i;
    #pragma omp task firstprivate(i, blksz, src1, src2, row, col, dst, taskTiming)\
    private(j, k, tid, tstart)\
    depend(in: src1[i * col : blksz * col], row, col)\
    depend(in: src2[i * col : blksz * col])\
    depend(out: dst[i * col : blksz * col])
    {
        //tid = omp_get_thread_num();
        //tstart = omp_get_wtime();
        
        for(k = i ; k < i + blksz ; k++)
        {
            for(j = 0 ; j < col ; j++)
            {
                dst[k * col + j] = src1[k * col + j] - src2[k * col + j];
            }
        }
        //taskTiming[2][tid] += (omp_get_wtime() - tstart);
    }//task end
}

void mat_mult_task_exe(double *src1, double *src2, double *dst, const int row,
                       const int col, int block_width, int block_id)
{
    //code: 3
    int i, j, k, tid;
    int blksz;
    double tstart;
    i = block_id * block_width; //starting point of the block
    blksz = block_width;
    if(i + block_width > row)
        blksz = row - i;
    #pragma omp task firstprivate(i, blksz, src1, src2, row, col, dst, taskTiming)\
    private(j, k, tstart, tid)\
    depend(in: src1[i * col : blksz * col])\
    depend(in: src2[i * col : blksz * col])\
    depend(out: dst[i * col : blksz * col])
    {
        //tid = omp_get_thread_num();
        //tstart = omp_get_wtime();
        
        for(k = i ; k < i + blksz ; k++)
        {
            for(j = 0 ; j < col ; j++)
            {
                dst[k * col + j] = src1[k * col + j] * src2[k * col + j];
            }
        }
        //taskTiming[3][tid] += (omp_get_wtime() - tstart);
    }//end task
}

void dot_mm_exe(double *src1, double *src2, double *buf, const int row,
                       const int col, int block_width, int block_id, int buf_id)
{
    //code: 3
    int i, j, k, tid;
    int buf_base;
    int blksz;
    double tstart;
    i = block_id * block_width; //starting point of the block
    blksz = block_width;
    if(i + block_width > row)
        blksz = row - i;
        
    #pragma omp task firstprivate(i, blksz, src1, src2, buf, row, col, taskTiming, block_id, buf_id)\
    private(j, k, tstart, tid, buf_base)\
    depend(in: src1[i * col : blksz * col])\
    depend(in: src2[i * col : blksz * col])\
    depend(out: buf[buf_id * col : col])
    {
        tid = omp_get_thread_num();
        //tstart = omp_get_wtime();
        buf_base = tid * col;
        
        //printf("tid: %d col: %d block_id: %d buf_id: %d\n", tid, col, block_id, buf_id);

        for(k = i ; k < i + blksz ; k++) //row
        {
            for(j = 0 ; j < col ; j++) //col
            {
                buf[buf_base + j] += src1[k * col + j] * src2[k * col + j];
                //printf("%lf\n", buf[buf_base + j]);
            }
        }
        //taskTiming[3][tid] += (omp_get_wtime() - tstart);
    }//end task
}

void sum_sqrt_task_COL(double *src, double *dst, const int row, const int col, int block_width, int block_id, int buf_id, double *buf)
{
    //code: 4
    int i, j, k, l, length, blksz, tid;
    int nbuf = nthrds;
    double tstart, tend;
    
    k = block_id * block_width; //starting point of the block
    blksz = block_width;
    i = buf_id; //pseudo_tid;

    if(k + blksz > row)
        blksz = row - k;
    
    #pragma omp task private(tid, j, l, tstart, tend)\
    firstprivate(k, blksz, src, nthrds, buf, block_width, buf_id, col, taskTiming)\
    depend(in: src[k * col : blksz * col])\
    depend(out: buf[buf_id * col : col])
    {
        tid = omp_get_thread_num();
        //tstart = omp_get_wtime();
        //printf("k: %d blksz: %d pseudo_tid: %d tid: %d\n", k, blksz, pseudo_tid, tid);

        for(j = k ; j < (k + blksz) ; j++) //row
        {
            for(l = 0 ; l < col ; l++) //col
            {
                buf[tid * col + l] += src[j * col + l];
                //printf("tid: %d col: %d j: %d l: %d\n", tid, col, j, l);
            }
        }

        //tend = omp_get_wtime();
        //taskTiming[4][tid] += (tend - tstart); 
    } //end task
}

void sum_sqrt_task_RNRED(double *buf, double *dst, const int col)
{
    //code: 6
    int i, j, k, l, length, blksz, tid;
    int nbuf = nthrds;
    double tstart, tend;
    //printf("nthrds: %d \n", nthrds);
    //adding partial sums
    #pragma omp task private(i, j, tid, tstart, tend)\
    firstprivate(nthrds, buf, col, dst, taskTiming)\
    depend(in: buf[0 * col : col], buf[1 * col : col], buf[2 * col : col],\
    buf[3 * col : col], buf[4 * col : col], buf[5 * col : col], buf[6 * col : col],\
    buf[7 * col : col], buf[8 * col : col], buf[9 * col : col], buf[10 * col : col],\
    buf[11 * col : col], buf[12 * col : col], buf[13 * col : col], buf[14 * col : col], buf[15 * col : col])\
    depend(inout: dst[0 : col])
    {
        //tid = omp_get_thread_num();
        //tstart = omp_get_wtime();

        for(i = 0 ; i < nthrds ; i++) //threads
        {
            for(j = 0 ; j < col ; j++) //each col
            {
                dst[j] += buf[i * col + j]; 
                
            }
        } //end for

        // for(i = 0 ; i < col ; i++)
        //     printf("%lf ", dst[i]);
        // printf("\n\n");

        //tend = omp_get_wtime();
        //taskTiming[6][tid] += (tend - tstart); 
    } //end task
}

void sum_sqrt_task_SQRT(double *dst, const int col)
{
    //code: 7
    int i, j, k, l, length, blksz, tid;
    int nbuf = nthrds;
    double tstart, tend;

    #pragma omp task private(i, tid, tstart, tend)\
    firstprivate(dst, col, taskTiming)\
    depend(inout: dst[0 : col])
    {
        //tid = omp_get_thread_num();
        //tstart = omp_get_wtime();

        for(i = 0; i < col; i++) //i->col
        {
            dst[i] = sqrt(dst[i]);
            //printf("%lf ");
        }
        //printf("\n\n");

        //tend = omp_get_wtime();
        //taskTiming[7][tid] += (tend - tstart);         
    }
}

void update_activeMask_task_exe(int *activeMask, double *residualNorms, double residualTolerance, int blocksize)
{
    //code: 13
    int i, tid;
    double tstart, tend;

    #pragma omp task private(i, tid, tstart, tend)\
    firstprivate(activeMask, residualNorms, blocksize, residualTolerance, taskTiming)\
    depend(in: residualNorms[0 : blocksize], activeMask[0 : blocksize])\
    depend(out: activeMask[0 : blocksize]) 
    {
        //tid = omp_get_thread_num();
        //tstart = omp_get_wtime();

        for(i = 0 ; i < blocksize ; i++)
        {
            if((residualNorms[i] > residualTolerance) && activeMask[i] == 1)
                activeMask[i] = 1;
            else
                activeMask[i] = 0;
            //printf("residualTolerance: %lf residualNorms[%d]: %lf activeMask[%d] : %d\n", residualTolerance, i, residualNorms[i], i, activeMask[i]);
        }   
        //tend = omp_get_wtime();
        //taskTiming[13][tid] += (tend - tstart); 
    }//end task 

}

void getActiveBlockVector_task_exe(double *activeBlockVectorR, int *activeMask, double *blockVectorR, 
                               int M, int blocksize, int currentBlockSize, int block_width, int block_id)
{
    //code: 9
    /*  
        activeBlockVectorR dimension: M * currentBlockSize 
        blockVectorR dimension: M * blocksize 
        activeMask tells which columns are active now 
    */

    int i, j, k, l, blksz, tid;
    double tstart, tend;

    i = block_id * block_width; //starting point of the block
    blksz = block_width;
    if(i + blksz > M)
        blksz = M - i;

    #pragma omp task private(j, k, l, tstart, tend, tid)\
    firstprivate(activeBlockVectorR, blockVectorR, activeMask, blksz, block_width, currentBlockSize, blocksize, i, taskTiming)\
    depend(in : activeMask[0 : blocksize], activeBlockVectorR[i * currentBlockSize : blksz * currentBlockSize], currentBlockSize)\
    depend(in : blockVectorR[i * blocksize : blksz * blocksize], M, blocksize)\
    depend(out : activeBlockVectorR[i * currentBlockSize : blksz * currentBlockSize])
    {
        //tid = omp_get_thread_num();
        //tstart = omp_get_wtime();

        for(j = i ; j < i + blksz ; j++) //rows
        {
            l = 0;
            for(k = 0 ; k < blocksize ; k++) //cols
            {
                if(activeMask[k] == 1)
                {
                   activeBlockVectorR[j * currentBlockSize + l] = blockVectorR[j * blocksize + k];
                   l++;
                }   
            }
            
        }
        //tend = omp_get_wtime();
        //taskTiming[9][tid] += (tend - tstart);
    } //end task
}

void updateBlockVector_task_exe(double *activeBlockVectorR, int *activeMask, double *blockVectorR, 
                             int M, int blocksize, int currentBlockSize, int block_width, int block_id)
{
    //code: 13
    /*  
        activeBlockVectorR dimension: M * currentBlockSize 
        blockVectorR dimension: M * blocksize 
        activeMask tells which columns are active now 
    */
    
    int i, j, k, l, blksz, tid;
    double tstart, tend;
    i = block_id * block_width; //starting point of the block
    blksz = block_width;
    
    if(i + blksz > M)
        blksz = M - i;

    #pragma omp task private(j, k, tstart, tend)\
    firstprivate(activeBlockVectorR, blockVectorR, activeMask, blksz, block_width, currentBlockSize, blocksize, i, taskTiming)\
    depend(in : activeMask[0 : blocksize],  currentBlockSize, M, blocksize)\
    depend(in : activeBlockVectorR[i * currentBlockSize : blksz * currentBlockSize])\
    depend(inout : blockVectorR[i * blocksize : blksz * blocksize])
    {
        //tid = omp_get_thread_num();
        //tstart = omp_get_wtime();

        for(j = i ; j < i + blksz ; j++) //rows
        {
            l = 0;
            for(k = 0 ; k < blocksize ; k++) //cols
            {
                if(activeMask[k] == 1)
                {
                    blockVectorR[j * blocksize + k] = activeBlockVectorR[j * currentBlockSize + l];
                    l++;
                } 
            } 
        }

        //tend = omp_get_wtime();
        //taskTiming[13][tid] += (tend - tstart);
    } //end task
}


void custom_dlacpy_task_exe(double *src, double *dst, int row, int col, int block_width, int block_id)
{
    //code: 14
    /* src and dst dimension: row * col */

    int i, j, k, blksz, tid;
    double tstart, tend;
    
    k = block_id * block_width; //starting point of the block 
    blksz = block_width;
        
    if(k + blksz > row)
        blksz = row - k;

    #pragma omp task private(i, j, tid, tstart, tend)\
    firstprivate(blksz, row, col, src, dst, block_width, k, taskTiming)\
    depend(in: src[k * col : blksz * col], row, col)\
    depend(out: dst[k * col : blksz * col])
    {
        //printf("DLACPY dst[%d * %d : %d * %d]\n", k, col, blksz, col);
        //printf("dst k: %d blksz: %d\n", k, blksz);
        
        //tid = omp_get_thread_num();
        //tstart = omp_get_wtime();

        for(i = k; i < k + blksz ; i++) //each row
        {
            for(j = 0 ; j < col ; j++) //each column
            {
                dst[i * col + j] = src[i * col + j];
            }
        }

        //tend = omp_get_wtime();
        //taskTiming[14][tid] += (tend - tstart);
    }  //end task
}



int split (const char *str, char c, char ***arr)
{
    int count = 1;
    int token_len = 1;
    int i = 0;
    const char *p;
    char *t;

    p = str;
    while (*p != '\0')
    {
        if (*p == c)
            count++;
        p++;
    }

    *arr = (char**) malloc(sizeof(char*) * count);
    if (*arr == NULL)
        return -1;

    /*p = str;
    while (*p != '\0')
    {
        if (*p == c)
        {
            (*arr)[i] = (char*) malloc( sizeof(char) * token_len );
            if ((*arr)[i] == NULL)
                return -1;

            token_len = 0;
            i++;
        }
        p++;
        token_len++;
    }
    (*arr)[i] = (char*) malloc( sizeof(char) * token_len );
    if ((*arr)[i] == NULL)
        return -1;

    i = 0;
    p = str;
    t = ((*arr)[i]);
    while (*p != '\0')
    {
        if (*p != c && *p != '\0')
        {
            *t = *p;
            t++;
        }
        else
        {
            *t = '\0';
            i++;
            t = ((*arr)[i]);
        }
        p++;
    }*/

    //how about using strtok
    i = 0;
    char *tempStr = (char *) malloc ((strlen(str)+1) * sizeof(char));
    strcpy(tempStr, str);

    char *token = strtok(tempStr, ",");
 
    // Keep printing tokens while one of the
    // delimiters present in str[].
    while (token != NULL)
    {
        (*arr)[i] = (char*) malloc( sizeof(char) * (strlen(token)+1) );
        strcpy((*arr)[i],token);
        token = strtok(NULL, ",");
        i++;
    }
 

    return count;
}



int split_v1(const char *line, char c, char ***tokens) 
{
    static const char *delimiter = ",";
    
    int count = 1;
    int token_len = 1;
    int i = 0;
    char *p;
    strcpy(p, line);
    char *t;
    strcpy(t, line);
    
    while (*p != '\0')
    {
        if (*p == ',')
            count++;
        p++;
    }

    *tokens = (char **) malloc(count * sizeof(char**));

    for (size_t i = 0; i < count; ++i) 
    {
        (*tokens)[i] = strtok(t, delimiter);
        line = NULL;
    }
    return count;
}