#ifndef LIB_MTXOP_H
#define LIB_MTXOP_H


extern void _XTY(double *X, double *Y, double *result ,int M, int N, int P, int blocksize);
extern void transpose(double *src, double *dst, const int N, const int M);
extern void inverse(double *arr, int m, int n);
extern void print_mat(double *arr, const int row, const int col);
extern void make_identity_mat(double *arr, const int row, const int col);
extern void diag(double *src, double *dst, const int size);
extern void mat_sub(double *src1, double *src2, double *dst, const int row, const int col);
extern void mat_addition(double *src1, double *src2, double *dst, const int row, const int col);
extern void mat_mult(double *src1, double *src2, double *dst, const int row, const int col);
extern void sum_sqrt(double *src, double *dst, const int row, const int col);
extern void update_activeMask(int *activeMask, double *residualNorms, double residualTolerance, int blocksize);
extern void getActiveBlockVector(double *activeBlockVectorR, int *activeMask, double *blockVectorR, int M, int blocksize, int currentBlockSize);
extern void updateBlockVector(double *activeBlockVectorR, int *activeMask, double *blockVectorR, int M, int blocksize, int currentBlockSize);
extern void mat_copy(double *src,  int row, int col, double *dst, int start_row, int start_col, int ld_dst);
extern void print_eigenvalues( MKL_INT n, double* wr, double* wi );
extern void custom_dlacpy(double *src, double *dst, int m, int n);

#endif