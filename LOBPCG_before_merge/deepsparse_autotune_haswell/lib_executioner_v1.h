#ifndef LIB_EXECUTIONER_H
#define LIB_EXECUTIONER_H

extern void _XY_exe(double *X, double *Y, double *result ,int M, int N, int P, int block_width, int block_id);

extern void _XTY_v1_exe(double *X, double *Y, double *buf ,int M, int N, int P, int block_width, int block_id, int buf_id);

extern void _XTY_v1_RED(double *buf, double *result, int N, int P, int block_width);

extern void mat_addition_task_exe(double *src1, double *src2, double *dst, const int row,
                       const int col, int block_width, int block_id);

extern void mat_sub_task_exe(double *src1, double *src2, double *dst, const int row,
                       const int col, int block_width, int block_id);

extern void mat_mult_task_exe(double *src1, double *src2, double *dst, const int row,
                       const int col, int block_width, int block_id);

extern void sum_sqrt_task_COL(double *src, double *dst, const int row, const int col, int block_width, int block_id, int buf_id, double *buf);

extern void sum_sqrt_task_RNRED(double *buf, double *dst, const int col);

extern void sum_sqrt_task_SQRT(double *dst, const int col);

extern void update_activeMask_task_exe(int *activeMask, double *residualNorms, double residualTolerance, int blocksize);

extern void getActiveBlockVector_task_exe(double *activeBlockVectorR, int *activeMask, double *blockVectorR, 
                               int M, int blocksize, int currentBlockSize, int block_width, int block_id);

extern void updateBlockVector_task_exe(double *activeBlockVectorR, int *activeMask, double *blockVectorR, 
                             int M, int blocksize, int currentBlockSize, int block_width, int block_id);

extern void custom_dlacpy_task_exe(double *src, double *dst, int row, int col, int block_width, int block_id);

extern int split (const char *str, char c, char ***arr);

void dot_mm_exe(double *src1, double *src2, double *result, const int row, const int col, int block_width, int block_id, int buf_id);

#endif