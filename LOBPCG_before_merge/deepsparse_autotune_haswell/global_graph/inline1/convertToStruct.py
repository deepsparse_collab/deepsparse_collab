
import os
import platform
import sys
import glob


def ConvertToStruct(filename):
	index = filename.find('.')
	newName = filename[:index]; 
	newName += "_taskinfo" + filename[index:];
	#print(newName);

	runBuildStructCommand = "./buildTaskInfoStruct.x " + filename;
	#print(runBuildStructCommand);
	os.system(runBuildStructCommand)
		
	renameCommand = "mv taskinfo.txt " + newName;
	#print(renameCommand);  
	os.system(renameCommand)


def main():
	fileList = glob.glob("inline1*_globalgraph.txt");
	for i in range(len(fileList)):
		print(str( fileList[i]));
		ConvertToStruct(fileList[i]);
	

if __name__ == '__main__':
	main()

