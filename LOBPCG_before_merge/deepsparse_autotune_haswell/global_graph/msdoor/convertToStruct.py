
import os
import platform
import sys
import glob

########### inline globalgraph ###########
inline = ["inline_1k_nonloop_globalgraph.txt", "inline_2k_nonloop_globalgraph.txt", "inline_4k_nonloop_globalgraph.txt", "inline_8k_nonloop_globalgraph.txt", "inline_1k_firstloop_globalgraph.txt", "inline_2k_firstloop_globalgraph.txt", "inline_4k_firstloop_globalgraph.txt", "inline_8k_firstloop_globalgraph.txt", "inline_1k_secondloop_globalgraph.txt", "inline_2k_secondloop_globalgraph.txt", "inline_4k_secondloop_globalgraph.txt", "inline_8k_secondloop_globalgraph.txt"]

########### msdoor global ###########
msdoor = ["msdoor_1k_nonloop_globalgraph.txt", "msdoor_2k_nonloop_globalgraph.txt", "msdoor_4k_nonloop_globalgraph.txt", "msdoor_8k_nonloop_globalgraph.txt", "msdoor_1k_firstloop_globalgraph.txt", "msdoor_2k_firstloop_globalgraph.txt", "msdoor_4k_firstloop_globalgraph.txt", "msdoor_8k_firstloop_globalgraph.txt", "msdoor_1k_secondloop_globalgraph.txt", "msdoor_2k_secondloop_globalgraph.txt", "msdoor_4k_secondloop_globalgraph.txt", "msdoor_8k_secondloop_globalgraph.txt"]


def ConvertToStruct(filename):
	index = filename.find('.')
	newName = filename[:index]; 
	newName += "_taskinfo" + filename[index:];
	#print(newName);

	runBuildStructCommand = "./buildTaskInfoStruct.x " + filename;
	#print(runBuildStructCommand);
	os.system(runBuildStructCommand)
		
	renameCommand = "mv taskinfo.txt " + newName;
	#print(renameCommand);  
	os.system(renameCommand)


def main():
	fileList = glob.glob("msdoor*_globalgraph.txt");
	for i in range(len(fileList)):
		print(str( fileList[i]));
		ConvertToStruct(fileList[i]);
	

if __name__ == '__main__':
	main()



'''
matpath = "/global/cscratch1/sd/rabbimd/LOBPCG/partition"

nonLoopFile = [ "Nm7_1k_nonloop_globalgraph_taskinfo.txt",
				"Nm7_2k_nonloop_globalgraph_taskinfo.txt",
				"Nm7_4k_nonloop_globalgraph_taskinfo.txt",
				"Nm7_8k_nonloop_globalgraph_taskinfo.txt" ]
			
firstLoopFile = [ "Nm7_1k_firstloop_globalgraph_taskinfo.txt",
			"Nm7_2k_firstloop_globalgraph_taskinfo.txt",
			"Nm7_4k_firstloop_globalgraph_taskinfo.txt",
			"Nm7_8k_firstloop_globalgraph_taskinfo.txt" ]

secondLoopFile = [	"Nm7_1k_secondloop_globalgraph_taskinfo.txt", 
				"Nm7_2k_secondloop_globalgraph_taskinfo.txt",
				"Nm7_4k_secondloop_globalgraph_taskinfo.txt",
				"Nm7_8k_secondloop_globalgraph_taskinfo.txt" ]

'''