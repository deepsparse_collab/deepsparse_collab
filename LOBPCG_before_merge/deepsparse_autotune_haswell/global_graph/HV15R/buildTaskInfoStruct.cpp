#include <iostream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include <algorithm>
#include <sstream>
#include <cstring>
#include <cstdio>
#include <string>

using namespace std;

#include <stdlib.h>

struct TaskInfo
{
  int opCode;
  int numParamsCount;
  int *numParamsList;
  int strParamsCount;
  char **strParamsList;
  int taskID;
  int partitionNo;
  int priority;
} *taskInfo;

void str_rev(char *str);
void myitoa(int x, char* dest);
void buildTaskInfoStruct(struct TaskInfo *taskInfo, char *partFile);

int split(const char *str, char c, char ***arr);
void structToString(struct TaskInfo taskInfo, char *structToStr);
void reverse(char str[], int length);

int main(int argc, char *argv[])
{
	char taskName[20000], ary[200];
	char buffer [33];
    char **splitParams;
    int partNo, tokenCount, priority;
    int vertexCount = 0, taskCounter = 0;

    string numStr;

	//opening partition file
    ifstream nonLoopFile(argv[1]);
    ofstream taskInfoFile("taskinfo.txt");
    
    if(nonLoopFile.fail())
    {
        printf("Non-loop File doesn't exist\n");
        exit(1);
    }

    while(nonLoopFile >> taskName >> partNo)
    {
    	vertexCount++;
    }
    nonLoopFile.close();

    taskInfoFile << vertexCount << endl;

    //printf("vertexCount: %d\n", vertexCount);

    nonLoopFile.open((argv[1]));

    while(nonLoopFile >> taskName >> partNo)
    {
    	tokenCount = split(taskName, ',', &splitParams);

    	if(taskName[0] == '_') 
        {
        	strcpy(ary, "22,0,1,");
        	strcat(ary, splitParams[0]);
        	strcat(ary, ",-1,");
        	myitoa (partNo, buffer);
        	strcat(ary, buffer);
        	strcat(ary, ",0");
        	taskInfoFile << ary << endl;

            //printf("taskName: %s --> %s\n", taskName, ary);
            continue;
        }
        
        
        if(tokenCount == 1)
        {
        	strcpy(ary, "21,0,1,");
        	strcat(ary, splitParams[0]);
        	strcat(ary, ",-1,");
        	myitoa (partNo, buffer);
        	strcat(ary, buffer);
        	strcat(ary, ",0");
        	taskInfoFile << ary << endl;
        	
        	//printf("taskName: %s --> %s\n", taskName, ary);
        }
        else
        {
        	if(!strcmp(splitParams[0], "RESET")) /* taskName starts RESET */
            {
            	strcpy(ary, "1,0,1,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",-1,");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;

	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
            else if(!strcmp(splitParams[0], "SPMM"))
            {
            	strcpy(ary, "2,3,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",");
	        	strcat(ary, splitParams[2]);
	        	strcat(ary, ",");
	        	strcat(ary, splitParams[3]);
	        	strcat(ary, ",0");
	        	strcat(ary, ",-1,");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;

	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
            else if(!strcmp(splitParams[0], "XTY")) /* taskName starts XTY */
            {
            	strcpy(ary, "3,2,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",");
	        	strcat(ary, splitParams[2]);
	        	strcat(ary, ",0,");
	        	strcat(ary, splitParams[3]);
	        	strcat(ary, ",");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;

	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
            else if(!strcmp(splitParams[0], "RED")) /* XTY partial sum reduction */
            {
            	strcpy(ary, "4,1,");
	        	strcat(ary, splitParams[2]);
	        	strcat(ary, ",1,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",-1,");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;

	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
            else if(!strcmp(splitParams[0], "XY")) /* taskName starts XY */
            {
            	strcpy(ary, "5,1,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",0,");
	        	strcat(ary, splitParams[2]);
	        	strcat(ary, ",");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;

	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
            else if(!strcmp(splitParams[0], "ADD")) /* taskName starts ADD */
            {
            	strcpy(ary, "6,1,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",0,");
	        	strcat(ary, splitParams[2]);
	        	strcat(ary, ",");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;

	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
            else if(!strcmp(splitParams[0], "DLACPY")) /* taskName starts SUB */
            {
            	strcpy(ary, "7,1,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",0,");
	        	strcat(ary, splitParams[2]);
	        	strcat(ary, ",");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;

	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
            else if(!strcmp(splitParams[0], "UPDATE")) /* taskName starts SUB */
            {
            	strcpy(ary, "8,1,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",0,");
	        	strcat(ary, splitParams[2]);
	        	strcat(ary, ",");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;

	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
            else if(!strcmp(splitParams[0], "SUB")) /* taskName starts SUB */
            {
            	strcpy(ary, "9,1,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",0,");
	        	strcat(ary, splitParams[2]);
	        	strcat(ary, ",");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;

	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
            else if(!strcmp(splitParams[0], "MULT")) /* taskName starts MULT */
            {
            	strcpy(ary, "10,1,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",0,-1,");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;

	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
            else if(!strcmp(splitParams[0], "COL")) /* taskName starts COL */
            {
            	strcpy(ary, "11,2,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",");
	        	strcat(ary, splitParams[2]);
	        	strcat(ary, ",0,-1,");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;

	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
            else if(!strcmp(splitParams[0], "RNRED")) /* taskName starts RNRED */
            {
            	strcpy(ary, "12,0,1,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",-1,");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;

	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
            else if(!strcmp(splitParams[0], "SQRT")) /* taskName starts SQRT */
            {
            	strcpy(ary, "13,0,1,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",-1,");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;

	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
            else if(!strcmp(splitParams[0], "GET")) /* taskName starts SUB */
            {
            	strcpy(ary, "14,1,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",0,");
	        	strcat(ary, splitParams[2]);
	        	strcat(ary, ",");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;

	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
            else if(!strcmp(splitParams[0], "TRANS")) /* taskName starts RNRED */
            {
            	strcpy(ary, "15,0,1,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",-1,");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;

	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
            else if(!strcmp(splitParams[0], "SPEUPDATE")) /* taskName starts RNRED */
            {
            	strcpy(ary, "16,0,1,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",-1,");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;

	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
            else if(!strcmp(splitParams[0], "CHOL")) /* taskName starts RNRED */
            {
            	strcpy(ary, "17,0,1,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",-1,");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;
	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
            else if(!strcmp(splitParams[0], "INV")) /* taskName starts RNRED */
            {
            	strcpy(ary, "18,0,1,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",-1,");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;
	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
            else if(!strcmp(splitParams[0], "SETZERO")) /* taskName starts SUB */
            {
            	strcpy(ary, "19,1,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",0,");
	        	strcat(ary, splitParams[2]);
	        	strcat(ary, ",");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;
	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
            else if(!strcmp(splitParams[0], "CONV")) /* taskName starts RNRED */
            {
            	strcpy(ary, "20,0,1,");
	        	strcat(ary, splitParams[1]);
	        	strcat(ary, ",-1,");
	        	myitoa (partNo, buffer);
	        	strcat(ary, buffer);
	        	strcat(ary, ",0");
	        	taskInfoFile << ary << endl;

	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
			else if(!strcmp(splitParams[0], "DOT")) /* taskName starts DOT */
            {
            	strcpy(ary, "26,2,");
	        	strcat(ary, splitParams[1]); //row id
	        	strcat(ary, ",");
	        	strcat(ary, splitParams[2]); //buf_id
	        	strcat(ary, ",0,");
	        	strcat(ary, "-1,"); //task id
	        	//strcat(ary, ",");
	        	myitoa (partNo, buffer); //part id
	        	strcat(ary, buffer);
	        	strcat(ary, ",0"); //priority
	        	taskInfoFile << ary << endl;

	        	//printf("taskName: %s --> %s\n", taskName, ary);
            }
			
        }// end if(tokenCount == 1)
    } //end while (file read)

    taskInfoFile.close();

    buildTaskInfoStruct(taskInfo, "taskinfo.txt");

	return 0;
}


void buildTaskInfoStruct(struct TaskInfo *taskInfo, char *partFile)
{
	char taskName[20000], ary[200], structToStr[20000];
	char buffer [33];
    char **splitParams;
    int partNo, tokenCount, priority, opCode, numParamsCount, strParamsCount;
    int vertexCount = 0, taskCounter = 0, index;
    string numStr;
    int i, j;

	//opening partition file
    ifstream partitionFile(partFile);

    partitionFile >> vertexCount;

    printf("vertexCount: %d\n", vertexCount);

    taskInfo = (struct TaskInfo *) malloc(vertexCount * sizeof(struct TaskInfo));

    while(partitionFile >> taskName)
    {
    	tokenCount = split(taskName, ',', &splitParams);
    	
    	taskInfo[taskCounter].opCode = atoi(splitParams[0]);  
    	taskInfo[taskCounter].numParamsCount = atoi(splitParams[1]);  //1
    		
    	index = 1; //numParamsCount
    		
    	if(taskInfo[taskCounter].numParamsCount > 0)
    	{
    		taskInfo[taskCounter].numParamsList = (int *) malloc(taskInfo[taskCounter].numParamsCount * sizeof(int));
    		for(i = 0 ; i < taskInfo[taskCounter].numParamsCount ; i++)
    		{
    			taskInfo[taskCounter].numParamsList[i] = atoi(splitParams[index + 1 + i]);
    		}	
    	}
    		
    	taskInfo[taskCounter].strParamsCount = atoi(splitParams[index + taskInfo[taskCounter].numParamsCount + 1]); //2
    		
    	if(taskInfo[taskCounter].strParamsCount > 0)
    	{
    		taskInfo[taskCounter].strParamsList = (char **) malloc(taskInfo[taskCounter].strParamsCount * sizeof(char *)); //3
	    	for(i = 0 ; i < taskInfo[taskCounter].strParamsCount ; i++)
	    	{
	    		taskInfo[taskCounter].strParamsList[i] = (char *) malloc(strlen(splitParams[index + taskInfo[taskCounter].numParamsCount + 2 + i]) * sizeof(char));
	    		strcpy(taskInfo[taskCounter].strParamsList[i], splitParams[index + taskInfo[taskCounter].numParamsCount + 2 + i]);
	    	}	
    	}
    		
    	taskInfo[taskCounter].taskID = atoi(splitParams[tokenCount - 3]);
    	taskInfo[taskCounter].partitionNo = atoi(splitParams[tokenCount - 2]);
    	taskInfo[taskCounter].priority = atoi(splitParams[tokenCount - 1]);
    	
    	// if(opCode == 15 || opCode == 16 || opCode == 17 || opCode == 18 || opCode == 20)
    	// {
    	// 	if(taskInfo[taskCounter].numParamsCount == 0 && taskInfo[taskCounter].strParamsCount == 1)
    	// 		printf("%s ---> %d,%d,%d,%s,%d,%d,%d\n", taskName, taskInfo[taskCounter].opCode, taskInfo[taskCounter].numParamsCount, taskInfo[taskCounter].strParamsCount, taskInfo[taskCounter].strParamsList[0], taskInfo[taskCounter].taskID, taskInfo[taskCounter].partitionNo, taskInfo[taskCounter].priority);
    		
    		
    	// }
    	structToString(taskInfo[taskCounter], structToStr);

    	if(strcmp(taskName, structToStr) != 0)
    		printf("%s --> %s\n", taskName, structToStr);

    	taskCounter++;

    }//end while

    printf("Finish allocating taskInfo\n");

}

void structToString(struct TaskInfo taskInfo, char *structToStr)
{
	int i;
	char buffer[50];
	
	myitoa(taskInfo.opCode, buffer);
	strcpy(structToStr, buffer);
	
	strcat(structToStr, ",");
	
	myitoa(taskInfo.numParamsCount, buffer);
	strcat(structToStr, buffer);
	strcat(structToStr, ",");

	if(taskInfo.numParamsCount > 0)
	{
		for(i = 0 ; i < taskInfo.numParamsCount ; i++)
		{
			myitoa(taskInfo.numParamsList[i], buffer);
			strcat(structToStr, buffer);
			strcat(structToStr, ",");
		}
	}

	myitoa(taskInfo.strParamsCount, buffer);
	strcat(structToStr, buffer);
	strcat(structToStr, ",");

	if(taskInfo.strParamsCount > 0)
	{
		for(i = 0 ; i < taskInfo.strParamsCount ; i++)
		{
			strcat(structToStr, taskInfo.strParamsList[i]);
			strcat(structToStr, ",");
		}
	}
	
	
	myitoa(taskInfo.taskID, buffer);
	strcat(structToStr, buffer);
	
	strcat(structToStr, ",");
	
	myitoa(taskInfo.partitionNo, buffer);
	strcat(structToStr, buffer);

	strcat(structToStr, ",");

	myitoa(taskInfo.priority, buffer);
	strcat(structToStr, buffer);
}

int split(const char *str, char c, char ***arr)
{
    int count = 1;
    int token_len = 1;
    int i = 0;
    const char *p;
    char *t;

    p = str;
    while (*p != '\0')
    {
        if (*p == c)
            count++;
        p++;
    }

    *arr = (char**) malloc(sizeof(char*) * count);
    if (*arr == NULL)
        return -1;

    //how about using strtok
    i = 0;
    char *tempStr = (char *) malloc ((strlen(str)+1) * sizeof(char));
    strcpy(tempStr, str);

    char *token = strtok(tempStr, ",");
 
    // Keep printing tokens while one of the
    // delimiters present in str[].
    while (token != NULL)
    {
        (*arr)[i] = (char*) malloc( sizeof(char) * (strlen(token)+1) );
        strcpy((*arr)[i],token);
        token = strtok(NULL, ",");
        i++;
    }
 

    return count;
}

void str_rev(char *str)
{
    char *p1, *p2;
    if (! str || ! *str)
        return;
    int len = strlen(str);
    for (p1 = str, p2 = str + len - 1; p2 > p1; ++p1, --p2)
    {
        *p1 ^= *p2;
        *p2 ^= *p1;
        *p1 ^= *p2;
    }
}

// Implementation of itoa() 
void myitoa(int num, char* str) 
{ 
	int i = 0, base = 10; 
	bool isNegative = false; 

	/* Handle 0 explicitely, otherwise empty string is printed for 0 */
	if (num == 0) 
	{ 
		str[i++] = '0'; 
		str[i] = '\0'; 
	} 
	else
	{

		// In standard itoa(), negative numbers are handled only with 
		// base 10. Otherwise numbers are considered unsigned. 
		if (num < 0) 
		{ 
			isNegative = true; 
			num = -num; 
		} 

		// Process individual digits 
		while (num != 0) 
		{ 
			int rem = num % base; 
			str[i++] = (rem > 9)? (rem-10) + 'a' : rem + '0'; 
			num = num/base; 
		} 

		// If number is negative, append '-' 
		if (isNegative) 
			str[i++] = '-'; 

		str[i] = '\0'; // Append string terminator 
		// Reverse the string 
		str_rev(str); 
	}

	

	//return str; 
} 


