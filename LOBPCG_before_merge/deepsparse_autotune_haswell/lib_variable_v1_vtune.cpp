#ifndef LIB_VARIABLE_H
#define LIB_VARIABLE_H
#include "lib_variable_v1.h"

/* see this post: https://bytefreaks.net/programming-2/c/c-undefined-reference-to-templated-class-function 
				  https://isocpp.org/wiki/faq/templates#separate-template-fn-defn-from-decl
*/

/* definition of global variables */
int nrows = 0, ncols = 0, nnz = 0;
int wblk = 0, nrowblks = 0, ncolblks = 0;
int numcols = 0, numrows = 0, nthrds = 1;
int nnonzero = 0;
long position = 0;
int *colptrs = NULL, *irem = NULL;
double **taskTiming = NULL;
int numOperation;
struct TaskInfo *taskInfo_nonLoop, *taskInfo_firstLoop, *taskInfo_secondLoop;


template<typename T>
void read_custom(char* filename, T *&xrem)
{
	int i, j;
  	ifstream file (filename, ios::in|ios::binary);
  	if (file.is_open())
  	{
      	int a = 0, c = 0;
      	long int b = 0;
      	
      	float d = 0;
      	file.read ((char*)&numrows,sizeof(numrows));
      	cout<<"row: "<<numrows<<endl;
      	file.read(reinterpret_cast<char*>(&numcols), sizeof(numcols));
      	cout<<"colum: "<<numcols<<endl;

      	file.read(reinterpret_cast<char*>(&nnonzero), sizeof(nnonzero));
      	cout<<"non zero: "<<nnonzero<<endl;

      	colptrs = new int[numcols + 1];
      	irem = new int[nnonzero];
      	xrem = new T[nnonzero];

      	//cout << "Memory allocaiton finished" << endl;
      	
      	position = 0;
      	while(!file.eof() && position <= numcols)
      	{
        	file.read(reinterpret_cast<char*>(&a), sizeof(a)); //irem(j)
        	colptrs[position++] = a;
    	}
    	//cout << "finished reading colptrs" << endl;
    	position = 0;
    	while(!file.eof() && position < nnonzero)
    	{
        	file.read(reinterpret_cast<char*>(&a), sizeof(a));
        	irem[position++] = a;
    	}

    	position = 0;
    	while(!file.eof() && position < nnonzero)
    	{
        	file.read(reinterpret_cast<char*>(&d), sizeof(d)); //irem(j)
        	xrem[position++] = d;
        }
	}
}

template struct block<double>;
template struct block<float>;
template void read_custom<double>(char* filename, double *&xrem);


template<typename T>
void csc2blkcoord(block<T> *&matrixBlock, T *xrem)
{
    int i, j, r, c, k, k1, k2, blkr, blkc, tmp;
    int **top;
    nrowblks = ceil(numrows/(float)(wblk));
    ncolblks = ceil(numcols/(float)(wblk));
    cout<<" nrowblks = "<<nrowblks<<endl;
    cout<<" ncolblks = "<<ncolblks<<endl;

    matrixBlock = new block<T>[nrowblks * ncolblks];
    top = new int*[nrowblks];

    for(i = 0 ; i < nrowblks ; i++)
    {
        top[i] = new int[ncolblks];
    }

    for(blkr = 0 ; blkr < nrowblks ; blkr++)
    {
        for(blkc = 0 ; blkc < ncolblks ; blkc++)
        {
            top[blkr][blkc] = 0;
            matrixBlock[blkr * ncolblks + blkc].nnz = 0;
        }
    }
    //cout << "Finish memory allocation for block.." << endl;


    //calculatig nnz per block
    for(c = 0 ; c < numcols ; c++)
    {
        k1 = colptrs[c];
        k2 = colptrs[c + 1] - 1;
        blkc = ceil((c + 1)/(float)wblk);
        //cout<<"K1: "<<k1<<" K2: "<<k2<<" blkc: "<<blkc<<endl;

        for(k = k1 - 1 ; k < k2 ; k++)
        {
            r = irem[k];
            blkr = ceil(r/(float)wblk);
            matrixBlock[(blkr - 1) * ncolblks + (blkc - 1)].nnz++;
        }
    }

    for(blkc = 0 ; blkc < ncolblks ; blkc++)
    {
        for(blkr = 0 ; blkr < nrowblks ; blkr++)
        {
            matrixBlock[blkr * ncolblks + blkc].roffset = blkr * wblk;
            matrixBlock[blkr * ncolblks + blkc].coffset = blkc * wblk;
            
            if(matrixBlock[blkr * ncolblks + blkc].nnz>0)
            {
                matrixBlock[blkr * ncolblks + blkc].rloc = new unsigned short int[matrixBlock[blkr * ncolblks + blkc].nnz];
                matrixBlock[blkr * ncolblks + blkc].cloc = new unsigned short int[matrixBlock[blkr * ncolblks + blkc].nnz];
                matrixBlock[blkr * ncolblks + blkc].val = new T[matrixBlock[blkr * ncolblks + blkc].nnz];
            }
            else
            {
                matrixBlock[blkr * ncolblks + blkc].rloc = NULL;
                matrixBlock[blkr * ncolblks + blkc].cloc = NULL;
            }
        }
    }

    for(c = 0 ; c < numcols ; c++)
    {
        k1 = colptrs[c];
        k2 = colptrs[c + 1] - 1;
        blkc = ceil((c + 1)/(float)wblk);

        for(k = k1 - 1 ; k < k2 ; k++)
        {
            r = irem[k];
            blkr = ceil(r/(float)wblk);

            matrixBlock[(blkr - 1) * ncolblks + blkc - 1].rloc[top[blkr - 1][blkc - 1]] = r - matrixBlock[(blkr - 1) * ncolblks + blkc - 1].roffset;
            matrixBlock[(blkr - 1) * ncolblks + blkc - 1].cloc[top[blkr - 1][blkc - 1]] = (c + 1) -  matrixBlock[(blkr - 1) * ncolblks + blkc - 1].coffset;
            matrixBlock[(blkr - 1) * ncolblks + blkc - 1].val[top[blkr - 1][blkc - 1]] = xrem[k];

            top[blkr - 1][blkc - 1] = top[blkr - 1][blkc - 1] + 1;
        }
    }

    //printf("Total nonzero blocks: %d\n", spmmTaskCounter);

    for(i = 0 ; i < nrowblks ; i++)
    {
        delete [] top[i];//=new int[ncolblks];
    }
    delete [] top;
} //end csc2blkcoord

template void csc2blkcoord<double>(block<double> *&matrixBlock, double *xrem); 

template<typename T>
void spmm_blkcoord_finegrained_exe_fixed_buf(int R, int C, int M, int nbuf, T *X,  T *Y, block<T> *H, int row_id, int col_id, int buf_id, int block_width)
{
    //code: 15

    int k, k1, k2, tid, blksz, offset;
    int spmm_offset, spmm_blksz;
    int i, j, l, rbase, cbase, r, c, nthreads;
    double tstart, tend;

    i = row_id;
    j = col_id;

    blksz = block_width;
    offset = j * block_width; //starting row of RHS matrix
    
    if(offset + block_width > R)
        blksz = R - offset;
    
    spmm_offset = i * block_width;
    spmm_blksz = block_width;
    
    if(spmm_offset + block_width > R)
        spmm_blksz = R - spmm_offset;
    
    T xcoef, sum;

    //if(H[i * ncolblks + j].nnz > 0)
    //{
        #pragma omp task firstprivate(i, j, buf_id, X, Y, H, R, C, M, nbuf, block_width, offset, blksz, spmm_blksz, spmm_offset, taskTiming)\
        private(k, l, r, c, xcoef, rbase, cbase, tid, tstart, tend)\
        depend(inout: Y[spmm_offset * M : spmm_blksz * M])\
        depend(in: X[offset * M : blksz * M])
        {
            //printf("i: %d j: %d SPMM inout: %p[%d : %d]\nSPMM in: %p[%d : %d]\n", i, j, Y, spmm_offset * M, spmm_blksz * M, X, offset * M, blksz * M);
            //tid = omp_get_thread_num();
            //tstart = omp_get_wtime();

            rbase = H[i * ncolblks + 0].roffset;
            cbase = H[i * ncolblks + j].coffset;
            
            for(k = 0 ; k < H[i * ncolblks + j].nnz ; k++)
            {
                r = rbase + H[i * ncolblks + j].rloc[k] - 1;
                c = cbase + H[i * ncolblks + j].cloc[k] - 1;
                xcoef = H[i * ncolblks + j].val[k];
                
                #pragma omp simd    
                for(l = 0 ; l < M ; l++)
                {
                    Y[r * M + l] = Y[r * M + l] + xcoef * X[c * M + l];
                }
            }
            //tend = omp_get_wtime();
            //taskTiming[15][tid] += (tend - tstart);
        }// end task
    //} 
}

template void spmm_blkcoord_finegrained_exe_fixed_buf<double>(int R, int C, int M, int nbuf, double *X,  double *Y, block<double> *H, int row_id, int col_id, int buf_id, int block_width);

template<typename T>
void spmm_blkcoord_finegrained_SPMMRED_fixed_buf(int R, int M, int nbuf, T *Y, T *spmmBUF, int block_id, int block_width)
{
    int k, k1, k2, tid, blksz, offset;
    int i, j, l, rbase, cbase, r, c, nthreads;
    double tstart, tend;

    T sum;
    blksz = block_width;
    offset = block_id * block_width;
    
    if(offset + blksz > R)
        blksz = R - offset;
    
    

    #pragma omp task firstprivate(offset, blksz, R, M, nbuf, spmmBUF, Y, block_width, block_id, taskTiming)\
    private(i, j, k, sum, tid, tstart, tend)\
    depend(inout: Y[offset * M : blksz * M])\
    depend(in: spmmBUF[0 * R * M + (block_id * block_width * M) : blksz * M]) //for one buffer only
    //depend(in: spmmBUF[0 * R * M + (block_id * block_width * M) : blksz * M], spmmBUF[1 * R * M + (block_id * block_width * M) : blksz * M],\
    spmmBUF[2 * R * M + (block_id * block_width * M) : blksz * M], spmmBUF[3 * R * M + (block_id * block_width * M) : blksz * M],\
    spmmBUF[4 * R * M + (block_id * block_width * M) : blksz * M], spmmBUF[5 * R * M + (block_id * block_width * M) : blksz * M],\
    spmmBUF[6 * R * M + (block_id * block_width * M) : blksz * M])
    {
        tid = omp_get_thread_num();
        tstart = omp_get_wtime();

        for(i = offset ; i < offset + blksz ; i++)
        {
                
            for(k = 0 ; k < M ; k++)
            {
                sum = 0.0;
                for(j = 0 ; j < nbuf; j++) //for each thread access corresponding N * N matrix
                {
                    sum += spmmBUF[j * R * M + i * M + k];
                }
                Y[i * M + k] = sum;
            }
        }

        tend = omp_get_wtime();
        taskTiming[16][tid] += (tend - tstart);
    } //end task
}

template void spmm_blkcoord_finegrained_SPMMRED_fixed_buf<double>(int R, int M, int nbuf, double *Y, double *spmmBUF, int block_id, int block_width);


template<typename T>
void spmm_blkcoord(int R, int C, int M, int nthrds, T *X,  T *Y, block<T> *H)
{
    int k, k1, k2;
    int i, j, l, rbase, cbase, r, c;
    double xcoef;

    #pragma omp parallel num_threads(nthrds)
    {
        #pragma omp single
        {
            for(i = 0 ; i < nrowblks ; i++)
            {
                #pragma omp task default(shared) firstprivate(i) private(j, k, l, rbase, cbase, r, c, xcoef)
                {
                    rbase = H[i * ncolblks + 0].roffset;
                    for(j = 0 ; j < ncolblks ; j++)
                    {
                        cbase = H[i * ncolblks + j].coffset;
                        if(H[i * ncolblks + j].nnz > 0)
                        {
                            for(k = 0 ; k < H[i * ncolblks + j].nnz ; k++)
                            {
                                r = rbase + H[i * ncolblks + j].rloc[k] - 1;
                                c = cbase + H[i * ncolblks + j].cloc[k] - 1;
                                xcoef = H[i * ncolblks + j].val[k];
                                #pragma omp simd 
                                for(l = 0 ; l < M ; l++)
                                {
                                    Y[r * M + l] = Y[r * M + l] + xcoef * X[c * M +l];
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    #pragma omp taskwait
}

template void spmm_blkcoord<double>(int R, int C, int M, int nthrds, double *X,  double *Y, block<double> *H);


template<typename T>
void spmm_blkcoord_finegrained_exe_fixed_buf_unrolled32(int R, int C, int M, int nbuf, T *X,  T *Y, block<T> *H, int row_id, int col_id, int buf_id, int block_width)
{
    //code: 15
    int k, k1, k2, tid, blksz, offset;
    int spmm_offset, spmm_blksz;
    int offset1, offset2, offset3;
    int i, j, l, rbase, cbase, r, c, nthreads;
    double tstart, tend;

    i = row_id;
    j = col_id;

    blksz = block_width;
    offset = j * block_width; //starting row of RHS matrix
    
    if(offset + block_width > R)
        blksz = R - offset;
    
    spmm_offset = i * block_width;
    spmm_blksz = block_width;
    
    if(spmm_offset + block_width > R)
        spmm_blksz = R - spmm_offset;
    
    T xcoef, sum;

    if(H[i * ncolblks + j].nnz > 0)
    {
        #pragma omp task firstprivate(i, j, buf_id, X, Y, H, R, C, M, nbuf, block_width, offset, blksz, spmm_blksz, spmm_offset, taskTiming)\
        private(k, l, r, c, xcoef, rbase, cbase, tid, tstart, tend, offset1, offset2, offset3)\
        depend(in: Y[i * block_width * nbuf * M : spmm_blksz * nbuf * M])\
        depend(in: X[offset * M : blksz * M])\
        depend(out: Y[(buf_id * R * M) + (i * block_width * M) : spmm_blksz * M])
        {
            tid = omp_get_thread_num();
            tstart = omp_get_wtime();

            rbase = H[i * ncolblks + 0].roffset;
            cbase = H[i * ncolblks + j].coffset;

            for(k = 0 ; k < H[i * ncolblks + j].nnz ; k++)
            {
                    r = rbase + H[i * ncolblks + j].rloc[k] - 1;
                    c = cbase + H[i * ncolblks + j].cloc[k] - 1;
                    xcoef = H[i * ncolblks + j].val[k];

                    offset1 = buf_id * R * 32 + r * 32;
                    offset2 = c * 32;

                    #pragma omp simd
                    for(l = 0 ; l < 32 ; l++)
                    {
                        Y[offset1 + l] = Y[offset1 + l] + xcoef * X[offset2 + l];
                    }
            }
            tend = omp_get_wtime();
            taskTiming[15][tid] += (tend - tstart);
        }// end task
    } 
}

template void spmm_blkcoord_finegrained_exe_fixed_buf_unrolled32<double>(int R, int C, int M, int nbuf, double *X,  double *Y, block<double> *H, int row_id, int col_id, int buf_id, int block_width);

#endif