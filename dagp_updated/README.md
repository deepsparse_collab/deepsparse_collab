## LICENSE

See LICENSE file

## CITATION AND CONTACT

For updates, latest version, please check: [http://tda.gatech.edu](http://tda.gatech.edu)


### Contact
    `tdalab@cc.gatech.edu`


Citation for the partitioner (BibTeX):

```
    @techreport { Herrmann18-HAL,
        keyword = {directed graph ; acyclic partitioning ; multilevel partitioning ; partitionnement acyclic ; partitionnement multiniveau ; graphes orient{\'e}s},
        author = {Julien Herrmann and M. Yusuf {\"O}zkaya and Bora U{\c c}ar and Kamer Kaya and {\"{U}}mit V. {\c{C}}ataly{\"{u}}rek},
        title = {Acyclic partitioning of large directed acyclic graphs},
        number = {RR-9163},
        month = {Mar},
        link = {https://hal.inria.fr/hal-01744603},
        year = {2018},
        pdf = {https://hal.inria.fr/hal-01744603/file/RR-9163.pdf},
        type = {Research Report},
        institution = {Inria - Research Centre Grenoble -- Rh{\^o}ne-Alpes},
    }
```

Citation for the Scheduler (BibTeX):

```
    @inproceedings { Ozkaya19-IPDPS,
        title = {A scalable clustering-based task scheduler for homogeneous processors using DAG partitioning},
        booktitle = {33rd IEEE International Parallel and Distributed Processing Symposium},
        author = {M. Yusuf \"Ozkaya and Anne Benoit and Bora U{\c{c}}ar and Julien Herrmann and {\"{U}}mit V. {\c{C}}ataly{\"{u}}rek},
        month = {May},
        year = {2019},
    }
```

## HOW TO BUILD

1.  install `scons`

2.  Prepare `config.py` file.

    if undirected initial partitioning will be used:

    - set `metis` and/or `scotch` to 1.

    - set up the paths for `metis`/`scotch` external libraries

    set up the compilers of choice

3.  Run `scons` to build

## HOW TO EXECUTE

1.  Build the project

2.  For the partitioner:
        run `./exe/rMLGP`

    if trying to run scheduler:
        run `./exe/rMLGPruntime`

    the applications will show the command line parameter usage

## GRAPH FORMAT

The application accepts a number of different formats:

  - `.dgraph` (`.dg`),
  - `.dot` (graphviz dot language), and
  - `.mtx` (Matrix Market File Format)

You can check out sample graphs or the websites for the respective graph formats.

The application has an option to generate binary versions of the input files.
This is, by default, enabled. If there is a binary version of the input,
the application prefers reading it instead of actual file.
Thus, if there is a change in the input file, where there is also a binary
version of a file with the same name, it will not be visible to the program.
The user should remove any `.bin` files for the inputs in this case.