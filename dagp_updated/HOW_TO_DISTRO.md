# To generate a distribution (package, shareable code)

## BUT FIRST, SOME PRELIMINARY INFO.

1. DIFFERENT RELEASES

    Currently, we have (keywords):
        - `partition`
        - `schedule`

2. FILES

    - `config.py.template` :
        Not a big change on this one. Just update thisif there is
        a major change in configurations (config.py)

    - `SConstruct.template`  :   
        This file is not a ready-to-use SConstruct file,
        but contains some tags in `<!keyword>` format.
        These are used ONLY on the lines where there is a
        difference between distros (their SConstruct files).
        So, if there is a change in SConstruct file (like adding
        a new source file, header, library, another release, etc.),
        this file also **needs to be updated**.

    - `contributors.txt` :
        Add if there are any other contributors.
        Otherwise, this will be added as is, to the package.

    - `LICENSE` :
        **LGPL v3.0** file from GNU's website.

    - `README` :
        Contains links to the publications, contact info,
        how to build and how to execute info. Update as necessary.

## COMING BACK TO HOW TO GENERATE RELEASE

1. Prepare/Update `SConstruct.template` and `config.py.template`

2. Run these codes:

    `scons -c`

    `python genSConsAndDistro.py`

    - 'genSConsAndDistro.py' takes a parameter:  `schedule` or `partition`.

        Default is `partition`.

3. Double check the generated package (`dagP.tar.gz` or `dagPScheduler.tar.gz`)
        before releasing.
